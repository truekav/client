import { Howl } from 'howler'
import { formula } from '../../src/classes/Formula'
import { Monster } from '../../src/classes/Monster'
import { monsterAudio } from '../../src/data/audio'
import { getMonster, TMonsterName } from '../../src/data/monster/allMonsters'
import { IAttack } from '../../src/models/functions'
import { getUniqueID } from '../../src/util/helper'

const mon = (name: TMonsterName, level: number) =>
	new Monster({
		...getMonster(name),
		level,
		uid: getUniqueID(),
		audio: monsterAudio[name]
	})

export const testDamage = (attacker: TMonsterName, attack: IAttack, recipient: TMonsterName) => {
	// const y = mon('Draggle', 10)
	for (let i = 1; i <= 100; i++) {
		const x = mon(attacker, i)
		const y = mon(recipient, i)
		console.log(`lvl${i} - ${attack.name} : `, formula.calcDamage(x, attack, y))
	}
}

export const testMaxHP = (name: TMonsterName) => {
	for (let i = 1; i <= 100; i++) {
		const x = mon(name, i)
		console.log(`${name} lvl${i} maxHP: `, x.maxHP)
	}
}

export const testXPGain = (winner: TMonsterName, looser: TMonsterName) => {
	let totalXPScaled = 0
	let totalXPFlat = 0
	const x = mon(winner, 10)
	const y = mon(looser, 15)
	for (let i = 1; i <= 100; i++) {
		const xp1 = formula.calcScaledXPGain(x, y)
		// const xp2 = formula.calcFlatXPGain(y)
		totalXPScaled += xp1
		// totalXPFlat += xp2
		console.log(`lvl${i} ${x.name} scaled: ${xp1}XP vs ${y.name}`)
		// console.log(`lvl${i} ${x.name} flat: ${xp2}XP vs ${y.name}`)
	}
	console.log('in Total scaled: ', totalXPScaled)
	console.log('in Total Flat: ', totalXPFlat)
}

export const testMonStats = (name: TMonsterName) => {
	for (let i = 1; i <= 100; i++) {
		const x = mon(name, i)
		const stats = formula.calcStatsIS(x.stats, i)
		console.log(`lvl${i} ${name}: `, stats)
	}
}

export const testIsCatched = (ballName: string, monsterName: TMonsterName) => {

	const x = mon(monsterName, 50)
	// x.status.paralized = true
	let catched = 0

	for (let i = 1; i <= 100; i++) {

		// x.stats.IS.hp -= 1
		// console.log('HP: ', x.stats.IS.hp)
		const isCatch = formula.isCatched(ballName, x)
		if (!isCatch.brokeFree) {
			catched += 1
		}
		console.log(`Ball-${i}: `, isCatch)
	}
	console.log('catched times: ', catched)
}
