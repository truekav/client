import { getAllMonsters } from '../src/data/monster/allMonsters'
import { getMonsterTypeNameById, monsterTypes } from '../src/data/monster/types'

// This function helps creating a game balance between monster types
export const countMonsterTypes = () => {
	const monsters = getAllMonsters()
	const types: { [key in monsterTypes]: number } = {
		normal: 0, fire: 0, water: 0, electric: 0, grass: 0, ice: 0, fighting: 0, poison: 0, ground: 0,
		flying: 0, psychic: 0, bug: 0, rock: 0, ghost: 0, dragon: 0, dark: 0, steel: 0, fairy: 0
	}
	for (let i = 0; i < Object.entries(monsters).length; i++) {
		const monster = Object.entries(monsters)[i][1]
		for (let j = 0; j < monster.type.length; j++) {
			const type = getMonsterTypeNameById(monster.type[j])
			types[type] += 1
		}
	}
	console.log(types)
}
// countMonsterTypes()