import { config } from '../../config'
import { MonsterNames } from './allMonsters'

type TMonsterLocation = {
	// map id
	[key: string]: {
		// battlezone id
		[key: string]: MonsterNames[]
	}
}

/**
 * This variable stores the monster that are possible to spawn based on player map & battlezone location 
 */
export const monsterLocation: TMonsterLocation = {
	// snab-town
	20: {
		255: config.wildMonster.length ? config.wildMonster : [
			MonsterNames.Mamus,
			MonsterNames.Fletchling,
			MonsterNames.Catus,
		],
		254: [
			MonsterNames.Bunnelby,
			MonsterNames.Rookidee,
			MonsterNames.Mamus,
			MonsterNames.Fletchling,
		],
		253: [
			MonsterNames.Yungoos,
			MonsterNames.Fletchling,
			MonsterNames.Draggle
		],
	},
	// snab-forest
	22: {
		255: config.wildMonster.length ? config.wildMonster : [
			MonsterNames.Mamus,
			MonsterNames.Blipbug,
			MonsterNames.Scatterbug,
			MonsterNames.Dottler,
			MonsterNames.Spewpa,
			MonsterNames.Bunnelby,
			MonsterNames.Sheely,
			MonsterNames.Fletchling,
			MonsterNames.Rookidee
		],
	},
	// canis-town
	23: {
		255: config.wildMonster.length ? config.wildMonster : [
			MonsterNames.Mamus,
			MonsterNames.Bunnelby,
			MonsterNames.Sheely,
			MonsterNames.Fletchling,
			MonsterNames.Catus,
		],
		254: [
			MonsterNames.Blipbug,
			MonsterNames.Scatterbug,
			MonsterNames.Dottler,
			MonsterNames.Spewpa,
			MonsterNames.Bunnelby,
			MonsterNames.Rookidee
		],
	},
	// new map
	24: {},
	// new map
	25: {},
	// new map
	26: {},
	// new map
	27: {},
	// new map
	28: {},
}
