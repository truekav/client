import { allAttacks } from '../../classes/Attack'
import { MonsterType } from './types'

// This file contains the relevant info about a monster for the Monster class initialization

export type TMonsterName = keyof typeof monsters
export const getMonster = (name: keyof typeof monsters) => structuredClone(monsters[name])
export const getMonsterByIdx = (idx: number) => structuredClone(Object.values(monsters).find(mon => mon.idx === idx))
export const getMonsterNameByIdx = (idx: number) => monNames[idx]
export const getAllMonsters = () => structuredClone(monsters)

// initial monster status and stats objects
export const initialStatus = {
	paralyzed: 0,
	poisoned: 0,
	confused: 0,
	sleeping: 0,
	burning: 0,
	frozen: 0,
	bleeding: 0,
	inLove: 0,
	hasEgg: 0,
	XPPower: 1,
	affection: 1,
}

export const initialStats = {
	hp: 0,
	att: 0,
	def: 0,
	ini: 0,
	spAtt: 0,
	spDef: 0
}

export enum MonsterNames {
	Draggle,
	Dragolino,
	Dragolon,
	Emby,
	Flemby,
	Flembolon,
	Sheely,
	Sheezor,
	Sheerazor,
	Rookidee,
	Corvisquire,
	Corviknight,
	Fletchling,
	Fletchinder,
	Talonflame,
	Yungoos,
	Gumshoos,
	Scatterbug,
	Spewpa,
	Vivillon,
	Blipbug,
	Dottler,
	Orbeetle,
	Bunnelby,
	Diggersby,
	Catus,
	Cacatus,
	Mamus,
	Marlagus,
	Etha,
	Etherea,
	Ethereus,
	Ripta,
	Tyrantrum,
	Shabbly,
	Sharkle,
	Shreedar,
	Finagron,
	Carkol,
	Coalossal,
	Bergmite,
	Avalugg
}

const monNames = [
	'Draggle',
	'Dragolino',
	'Dragolon',
	'Emby',
	'Flemby',
	'Flembolon',
	'Sheely',
	'Sheezor',
	'Sheerazor',
	'Rookidee',
	'Corvisquire',
	'Corviknight',
	'Fletchling',
	'Fletchinder',
	'Talonflame',
	'Yungoos',
	'Gumshoos',
	'Scatterbug',
	'Spewpa',
	'Vivillon',
	'Blipbug',
	'Dottler',
	'Orbeetle',
	'Bunnelby',
	'Diggersby',
	'Catus',
	'Cacatus',
	'Mamus',
	'Marlagus',
	'Etha',
	'Etherea',
	'Ethereus',
	'Ripta',
	'Tyrantrum',
	'Shabbly',
	'Sharkle',
	'Shreedar',
	'Finagron',
	'Carkol',
	'Coalossal',
	'Bergmite',
	'Avalugg'
] as const

// @param cat - a number (1 | 2 | 3 | 4) which indicates the XP-growth-rate of a monster
// - 1 = slow: 1,250,000 XP needed for level 100
// - 2 = medium-slow: 1,059,860 XP needed for level 100
// - 3 = medium-fast: 1,000,000 XP needed for level 100
// - 4 = fast: 800,000 XP needed for level 100

/* all monsters */
const monsters = {
	// 1
	Draggle: {
		idx: 0,
		name: 'Draggle',
		image: { src: './imgs/monsters/draggle/draggle0.png' },
		imageFaceUp: { src: './imgs/monsters/draggle/draggle.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'It looks like a retarded bird, but appearances are deceptive. This creature has a lot of potential!',
		weight: 11, // kg
		height: 59, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.water],
		stats: {
			base: {
				hp: 50,
				att: 35,
				def: 40,
				ini: 40,
				spAtt: 39,
				spDef: 45
			},
			IV: {
				hp: 4,
				att: 3,
				def: 4,
				ini: 5,
				spAtt: 4,
				spDef: 6
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 1,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 255 each stat and max 510 in total
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 42,
		cat: 3,
		status: initialStatus,
		catchRate: 100,
		spawnChance: .01,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Waterball,
			3: allAttacks.Tackle,
			7: allAttacks.Waterball,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 16,
		transTo: 1
	},
	Dragolino: {
		idx: 1,
		name: 'Dragolino',
		image: { src: './imgs/monsters/dragolino/dragolino0.png' },
		imageFaceUp: { src: './imgs/monsters/dragolino/dragolino.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'A flying water bomb that can mercilessly drown its opponents.',
		weight: 28, // kg
		height: 81, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.water, MonsterType.flying],
		stats: {
			base: {
				hp: 59,
				att: 45,
				def: 51,
				ini: 50,
				spAtt: 49,
				spDef: 55
			},
			IV: {
				hp: 5,
				att: 4,
				def: 6,
				ini: 6,
				spAtt: 5,
				spDef: 7
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 1,
				ini: 0,
				spAtt: 0,
				spDef: 1
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 64,
		cat: 3,
		status: initialStatus,
		catchRate: 80,
		spawnChance: .1,
		lvlAttacks: {
			16: allAttacks.Cut,
		} as const,
		transformLvl: 36,
		transTo: 2
	},
	Dragolon: {
		idx: 2,
		name: 'Dragolon',
		image: { src: './imgs/monsters/dragolon/dragolon0.png' },
		imageFaceUp: { src: './imgs/monsters/dragolon/dragolon.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Dragolon can create storms with its strong wings and he is afraid of nothing.',
		weight: 140, // kg
		height: 260, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.water, MonsterType.flying],
		stats: {
			base: {
				hp: 75,
				att: 70,
				def: 80,
				ini: 70,
				spAtt: 80,
				spDef: 90
			},
			IV: {
				hp: 7,
				att: 6,
				def: 7,
				ini: 7,
				spAtt: 6,
				spDef: 8
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 0,
				spDef: 3
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 110,
		cat: 3,
		status: initialStatus,
		catchRate: 50,
		spawnChance: .1,
		lvlAttacks: {
			// 2: allAttacks.Growl,
			// 37: allAttacks.WingAttack
		} as const,
	},
	// 2
	Emby: {
		idx: 3,
		name: 'Emby',
		image: { src: './imgs/monsters/emby/emby0.png' },
		imageFaceUp: { src: './imgs/monsters/emby/emby.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'This small predator can spit flames and is extremely agile. He grills his prey after the hunt.',
		weight: 10, // kg
		height: 42, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.fire],
		stats: {
			base: {
				hp: 50,
				att: 40,
				def: 35,
				ini: 45,
				spAtt: 39,
				spDef: 40
			},
			IV: {
				hp: 4,
				att: 3,
				def: 3,
				ini: 7,
				spAtt: 4,
				spDef: 5
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 45,
		cat: 4,
		status: { ...initialStatus },
		catchRate: 90,
		spawnChance: .01,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Fireball,
			3: allAttacks.Tackle,
			7: allAttacks.Fireball,
			10: allAttacks.Scratch,
			12: allAttacks.Cut,
		} as const,
		transformLvl: 16,
		transTo: 4
	},
	Flemby: {
		idx: 4,
		name: 'Flemby',
		image: { src: './imgs/monsters/flemby/flemby0.png' },
		imageFaceUp: { src: './imgs/monsters/flemby/flemby.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'The flame on its head can grow and cover its whole body which makes him untouchable without getting damaged.',
		weight: 45, // kg
		height: 75, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.fire],
		stats: {
			base: {
				hp: 59,
				att: 51,
				def: 45,
				ini: 60,
				spAtt: 49,
				spDef: 50
			},
			IV: {
				hp: 5,
				att: 5,
				def: 5,
				ini: 8,
				spAtt: 6,
				spDef: 6
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 1,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 70,
		cat: 3,
		status: initialStatus,
		catchRate: 70,
		spawnChance: .1,
		lvlAttacks: {
			// 2: allAttacks.Growl,
			16: allAttacks.Cut,
		} as const,
		transformLvl: 36,
		transTo: 5
	},
	Flembolon: {
		idx: 5,
		name: 'Flembolon',
		image: { src: './imgs/monsters/flembolon/flembolon0.png' },
		imageFaceUp: { src: './imgs/monsters/flembolon/flembolon.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'The flame on his head has moved to his tail. This creature is very dangerous and lightning fast.',
		weight: 100, // kg
		height: 160, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.fire],
		stats: {
			base: {
				hp: 75,
				att: 70,
				def: 70,
				ini: 100,
				spAtt: 90,
				spDef: 80
			},
			IV: {
				hp: 7,
				att: 6,
				def: 6,
				ini: 9,
				spAtt: 7,
				spDef: 7
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 3,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 110,
		cat: 2,
		status: initialStatus,
		catchRate: 45,
		spawnChance: .1,
		lvlAttacks: {
			2: allAttacks.Growl,
			16: allAttacks.Cut
		} as const,
	},
	// 3
	Sheely: {
		idx: 6,
		name: 'Sheely',
		image: { src: './imgs/monsters/sheely/sheely0.png' },
		imageFaceUp: { src: './imgs/monsters/sheely/sheely.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'This hairy monster can hurl leaves with the power of its mind alone.',
		weight: 13, // kg
		height: 45, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.grass, MonsterType.poison],
		stats: {
			base: {
				hp: 50,
				att: 45,
				def: 35,
				ini: 40,
				spAtt: 44,
				spDef: 35
			},
			IV: {
				hp: 4,
				att: 4,
				def: 3,
				ini: 5,
				spAtt: 6,
				spDef: 5
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 1,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 43,
		cat: 1,
		status: initialStatus,
		catchRate: 100,
		spawnChance: .01,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.LeafBullet,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 16,
		transTo: 7
	},
	Sheezor: {
		idx: 7,
		name: 'Sheezor',
		image: { src: './imgs/monsters/sheezor/sheezor0.png' },
		imageFaceUp: { src: './imgs/monsters/sheezor/sheezor.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Its claws are very sharp. He can easily dive under the ground with them.',
		weight: 60, // kg
		height: 160, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.grass, MonsterType.poison],
		stats: {
			base: {
				hp: 60,
				att: 51,
				def: 55,
				ini: 50,
				spAtt: 55,
				spDef: 48
			},
			IV: {
				hp: 5,
				att: 5,
				def: 6,
				ini: 6,
				spAtt: 7,
				spDef: 6
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 1,
				spDef: 1
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 64,
		cat: 1,
		status: initialStatus,
		catchRate: 75,
		lvlAttacks: {
			2: allAttacks.Growl,
			16: allAttacks.Cut
		} as const,
		transformLvl: 36,
		spawnChance: .1,
		transTo: 8
	},
	Sheerazor: {
		idx: 8,
		name: 'Sheerazor',
		image: { src: './imgs/monsters/sheerazor/sheerazor0.png' },
		imageFaceUp: { src: './imgs/monsters/sheerazor/sheerazor.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'A majestic beast that can shake entire neighborhoods or make forests flourish in a matter of seconds.',
		weight: 140, // kg
		height: 275, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.grass, MonsterType.poison],
		stats: {
			base: {
				hp: 75,
				att: 70,
				def: 80,
				ini: 75,
				spAtt: 100,
				spDef: 80
			},
			IV: {
				hp: 7,
				att: 6,
				def: 7,
				ini: 7,
				spAtt: 8,
				spDef: 7
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 2,
				spDef: 1
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 110,
		cat: 3,
		status: initialStatus,
		catchRate: 45,
		spawnChance: .1,
		lvlAttacks: {
			2: allAttacks.Growl,
			37: allAttacks.WingAttack
		} as const,
	},
	// 4
	Rookidee: {
		idx: 9,
		name: 'Rookidee',
		image: { src: './imgs/monsters/rookidee/rookidee0.png' },
		imageFaceUp: { src: './imgs/monsters/rookidee/rookidee.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'Not docile at all. If attacked, it will often fly away to protect itself rather than fight back.',
		weight: 2, // kg
		height: 15, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.normal, MonsterType.flying],
		stats: {
			base: {
				hp: 40,
				att: 45,
				def: 42,
				ini: 58,
				spAtt: 50,
				spDef: 50
			},
			IV: {
				hp: 3,
				att: 3,
				def: 3,
				ini: 4,
				spAtt: 3,
				spDef: 3
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 45,
		cat: 4,
		status: { ...initialStatus },
		catchRate: 90,
		spawnChance: .5,
		canEscape: { chance: .6 },
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 16,
		transTo: 10
	},
	Corvisquire: {
		idx: 10,
		name: 'Corvisquire',
		image: { src: './imgs/monsters/corvisquire/corvisquire0.png' },
		imageFaceUp: { src: './imgs/monsters/corvisquire/corvisquire.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'This Monster is very attentive. It can constantly stare at you for hours. Nobody knows the exact effect.',
		weight: 29, // kg
		height: 100, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.normal, MonsterType.flying],
		stats: {
			base: {
				hp: 50,
				att: 52,
				def: 52,
				ini: 65,
				spAtt: 50,
				spDef: 50
			},
			IV: {
				hp: 4,
				att: 4,
				def: 4,
				ini: 5,
				spAtt: 3,
				spDef: 3
			},
			EVY: {
				hp: 0,
				att: 1,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 75,
		cat: 4,
		status: { ...initialStatus },
		catchRate: 75,
		spawnChance: .3,
		lvlAttacks: {
			16: allAttacks.Cut,
			// 2: allAttacks.Scratch,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			// 10: allAttacks.Scratch,
		} as const,
		transformLvl: 36,
		transTo: 11
	},
	Corviknight: {
		idx: 11,
		name: 'Corviknight',
		image: { src: './imgs/monsters/corviknight/corviknight0.png' },
		imageFaceUp: { src: './imgs/monsters/corviknight/corviknight.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'Its strong talons made of carbon-like springs are feared as wicked and sharp weapons. If angry, its eyes might fire a laser beam.',
		weight: 45, // kg
		height: 150, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.steel, MonsterType.flying],
		stats: {
			base: {
				hp: 70,
				att: 70,
				def: 70,
				ini: 90,
				spAtt: 60,
				spDef: 60
			},
			IV: {
				hp: 6,
				att: 6,
				def: 6,
				ini: 7,
				spAtt: 5,
				spDef: 8
			},
			EVY: {
				hp: 0,
				att: 1,
				def: 0,
				ini: 1,
				spAtt: 1,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 110,
		cat: 3,
		status: { ...initialStatus },
		catchRate: 50,
		spawnChance: .1,
		lvlAttacks: {
			16: allAttacks.Cut,
			// 2: allAttacks.Scratch,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			// 10: allAttacks.Scratch,
		} as const
	},
	// 5
	Fletchling: {
		idx: 12,
		name: 'Fletchling',
		image: { src: './imgs/monsters/fletchling/fletchling0.png' },
		imageFaceUp: { src: './imgs/monsters/fletchling/fletchling.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'Inept at flying high. However, it is fearless, brave and protects its territory at all costs.',
		weight: 1.5, // kg
		height: 13, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.normal, MonsterType.flying],
		stats: {
			base: {
				hp: 40,
				att: 50,
				def: 42,
				ini: 58,
				spAtt: 55,
				spDef: 50
			},
			IV: {
				hp: 3,
				att: 4,
				def: 3,
				ini: 4,
				spAtt: 4,
				spDef: 3
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 45,
		cat: 3,
		status: { ...initialStatus },
		catchRate: 100,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 16,
		transTo: 13
	},
	Fletchinder: {
		idx: 13,
		name: 'Fletchinder',
		image: { src: './imgs/monsters/fletchinder/fletchinder0.png' },
		imageFaceUp: { src: './imgs/monsters/fletchinder/fletchinder.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'A Monster that dates back many years. It is tender but if it senses danger, it immediatly attacks.',
		weight: 29, // kg
		height: 90, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.normal, MonsterType.flying],
		stats: {
			base: {
				hp: 45,
				att: 60,
				def: 52,
				ini: 70,
				spAtt: 50,
				spDef: 40
			},
			IV: {
				hp: 4,
				att: 5,
				def: 3,
				ini: 5,
				spAtt: 4,
				spDef: 3
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 2,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 45,
		cat: 2,
		status: { ...initialStatus },
		catchRate: 75,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 36,
		transTo: 14
	},
	Talonflame: {
		idx: 14,
		name: 'Talonflame',
		image: { src: './imgs/monsters/talonflame/talonflame0.png' },
		imageFaceUp: { src: './imgs/monsters/talonflame/talonflame.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'This monster can produce flames that can reach a temperature of 12.000°C.',
		weight: 55, // kg
		height: 130, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.flying, MonsterType.fire],
		stats: {
			base: {
				hp: 70,
				att: 70,
				def: 65,
				ini: 100,
				spAtt: 70,
				spDef: 60
			},
			IV: {
				hp: 6,
				att: 7,
				def: 5,
				ini: 7,
				spAtt: 7,
				spDef: 6
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 3,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 110,
		cat: 2,
		status: { ...initialStatus },
		catchRate: 50,
		spawnChance: .4,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			10: allAttacks.Scratch,
		} as const,
	},
	// 6
	Yungoos: {
		idx: 15,
		name: 'Yungoos',
		image: { src: './imgs/monsters/yungoos/yungoos0.png' },
		imageFaceUp: { src: './imgs/monsters/yungoos/yungoos.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Will chew on anything with its fangs. If you see one, you can be certain that it will bite you.',
		weight: 9, // kg
		height: 35, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.normal, MonsterType.water],
		stats: {
			base: {
				hp: 30,
				att: 50,
				def: 35,
				ini: 45,
				spAtt: 30,
				spDef: 40
			},
			IV: {
				hp: 2,
				att: 5,
				def: 3,
				ini: 4,
				spAtt: 2,
				spDef: 3
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 1,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 60,
		cat: 2,
		status: initialStatus,
		catchRate: 155,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			// 7: allAttacks.LeafBullet,
			11: allAttacks.Scratch,
		} as const,
		transformLvl: 22,
		transTo: 16
	},
	Gumshoos: {
		idx: 16,
		name: 'Gumshoos',
		image: { src: './imgs/monsters/gumshoos/gumshoos0.png' },
		imageFaceUp: { src: './imgs/monsters/gumshoos/gumshoos.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'This Monster can also be found near rivers where he dives for a long time. Its skull is very hard and heavy.',
		weight: 90, // kg
		height: 150, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.normal, MonsterType.water],
		stats: {
			base: {
				hp: 45,
				att: 55,
				def: 40,
				ini: 60,
				spAtt: 35,
				spDef: 55
			},
			IV: {
				hp: 4,
				att: 5,
				def: 4,
				ini: 6,
				spAtt: 3,
				spDef: 5
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 2,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 100,
		cat: 3,
		status: initialStatus,
		catchRate: 80,
		spawnChance: .7,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			// 7: allAttacks.LeafBullet,
			11: allAttacks.Scratch,
		} as const,
	},
	// 7
	Scatterbug: {
		idx: 17,
		name: 'Scatterbug',
		image: { src: './imgs/monsters/scatterbug/scatterbug0.png' },
		imageFaceUp: { src: './imgs/monsters/scatterbug/scatterbug.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'For protection, it releases a horrible stench from the antenna on its head to drive away enemies.',
		weight: 2.5, // kg
		height: 15, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.bug],
		stats: {
			base: {
				hp: 35,
				att: 35,
				def: 42,
				ini: 40,
				spAtt: 30,
				spDef: 30
			},
			IV: {
				hp: 3,
				att: 2,
				def: 3,
				ini: 3,
				spAtt: 2,
				spDef: 2
			},
			EVY: {
				hp: 1,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 40,
		cat: 2,
		status: { ...initialStatus },
		catchRate: 150,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 16,
		transTo: 18
	},
	Spewpa: {
		idx: 18,
		name: 'Spewpa',
		image: { src: './imgs/monsters/spewpa/spewpa0.png' },
		imageFaceUp: { src: './imgs/monsters/spewpa/spewpa.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'It is waiting for the moment to evolve. At this stage, it can only hide, so it remains motionless to avoid attack.',
		weight: 9, // kg
		height: 25, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.bug],
		stats: {
			base: {
				hp: 35,
				att: 35,
				def: 50,
				ini: 40,
				spAtt: 30,
				spDef: 30
			},
			IV: {
				hp: 3,
				att: 2,
				def: 4,
				ini: 2,
				spAtt: 2,
				spDef: 2
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 2,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 50,
		cat: 4,
		status: { ...initialStatus },
		catchRate: 150,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			// 10: allAttacks.Scratch,
		} as const,
		transformLvl: 24,
		transTo: 19
	},
	Vivillon: {
		idx: 19,
		name: 'Vivillon',
		image: { src: './imgs/monsters/vivillon/vivillon0.png' },
		imageFaceUp: { src: './imgs/monsters/vivillon/vivillon.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'In battle, it flaps its wings at great speed to release highly toxic dust into the air.',
		weight: 31, // kg
		height: 75, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.bug, MonsterType.flying],
		stats: {
			base: {
				hp: 50,
				att: 40,
				def: 55,
				ini: 75,
				spAtt: 85,
				spDef: 85
			},
			IV: {
				hp: 4,
				att: 3,
				def: 3,
				ini: 5,
				spAtt: 6,
				spDef: 5
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 2,
				spDef: 1
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 110,
		cat: 3,
		status: { ...initialStatus },
		catchRate: 70,
		spawnChance: .7,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			// 10: allAttacks.Scratch,
		} as const
	},
	// 8
	Blipbug: {
		idx: 20,
		name: 'Blipbug',
		image: { src: './imgs/monsters/blipbug/blipbug0.png' },
		imageFaceUp: { src: './imgs/monsters/blipbug/blipbug.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'Its color says that this Monster is very poisonus. It hides in grass and bushes where it eats leaves.',
		weight: 3, // kg
		height: 15, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.bug, MonsterType.poison],
		stats: {
			base: {
				hp: 35,
				att: 42,
				def: 35,
				ini: 40,
				spAtt: 30,
				spDef: 30
			},
			IV: {
				hp: 3,
				att: 3,
				def: 2,
				ini: 3,
				spAtt: 2,
				spDef: 2
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 40,
		cat: 2,
		status: { ...initialStatus },
		catchRate: 150,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 16,
		transTo: 21
	},
	Dottler: {
		idx: 21,
		name: 'Dottler',
		image: { src: './imgs/monsters/dottler/dottler0.png' },
		imageFaceUp: { src: './imgs/monsters/dottler/dottler.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'Able to move only slightly. When endangered, it may stick out its stinger and poison its enemy.',
		weight: 10, // kg
		height: 40, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.bug, MonsterType.poison],
		stats: {
			base: {
				hp: 35,
				att: 40,
				def: 45,
				ini: 40,
				spAtt: 30,
				spDef: 30
			},
			IV: {
				hp: 3,
				att: 2,
				def: 3,
				ini: 3,
				spAtt: 2,
				spDef: 2
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 2,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 60,
		cat: 4,
		status: { ...initialStatus },
		catchRate: 150,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			10: allAttacks.Scratch,
		} as const,
		transformLvl: 22,
		transTo: 22
	},
	Orbeetle: {
		idx: 22,
		name: 'Orbeetle',
		image: { src: './imgs/monsters/orbeetle/orbeetle0.png' },
		imageFaceUp: { src: './imgs/monsters/orbeetle/orbeetle.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'It has some poisonous stingers. They are used to jab its enemy repeatedly.',
		weight: 30, // kg
		height: 110, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.bug, MonsterType.flying],
		stats: {
			base: {
				hp: 50,
				att: 65,
				def: 50,
				ini: 75,
				spAtt: 50,
				spDef: 70
			},
			IV: {
				hp: 4,
				att: 6,
				def: 3,
				ini: 5,
				spAtt: 3,
				spDef: 5
			},
			EVY: {
				hp: 0,
				att: 2,
				def: 0,
				ini: 0,
				spAtt: 0,
				spDef: 1
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 110,
		cat: 3,
		status: { ...initialStatus },
		catchRate: 70,
		spawnChance: .7,
		lvlAttacks: {
			1: allAttacks.Growl,
			// 2: allAttacks.Scratch,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Fireball,
			// 10: allAttacks.Scratch,
		} as const
	},
	// 9
	Bunnelby: {
		idx: 23,
		name: 'Bunnelby',
		image: { src: './imgs/monsters/bunnelby/bunnelby0.png' },
		imageFaceUp: { src: './imgs/monsters/bunnelby/bunnelby.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'It is very sensitive to smells. While foraging, they\'ll use their whiskers to check wind direction and stay downwind of predators.',
		weight: 6.8, // kg
		height: 30, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.normal],
		stats: {
			base: {
				hp: 50,
				att: 40,
				def: 40,
				ini: 60,
				spAtt: 35,
				spDef: 35
			},
			IV: {
				hp: 4,
				att: 3,
				def: 3,
				ini: 5,
				spAtt: 3,
				spDef: 3
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 50,
		cat: 3,
		status: initialStatus,
		catchRate: 80,
		spawnChance: .6,
		canEscape: { chance: .3 },
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			// 7: allAttacks.LeafBullet,
			11: allAttacks.Scratch,
		} as const,
		transformLvl: 26,
		transTo: 24
	},
	Diggersby: {
		idx: 24,
		name: 'Diggersby',
		image: { src: './imgs/monsters/diggersby/diggersby0.png' },
		imageFaceUp: { src: './imgs/monsters/diggersby/diggersby.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'When it goes on a rampage or hunt, it\'s impossible to control. It is lightning fast and its ears are a powerfull weapon.',
		weight: 70, // kg
		height: 140, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.normal],
		stats: {
			base: {
				hp: 65,
				att: 65,
				def: 65,
				ini: 100,
				spAtt: 55,
				spDef: 60
			},
			IV: {
				hp: 6,
				att: 6,
				def: 6,
				ini: 8,
				spAtt: 4,
				spDef: 5
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 2,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 110,
		cat: 3,
		status: initialStatus,
		catchRate: 50,
		spawnChance: .5,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			// 7: allAttacks.LeafBullet,
			11: allAttacks.Scratch,
		} as const
	},
	// 10
	Catus: {
		idx: 25,
		name: 'Catus',
		image: { src: './imgs/monsters/catus/catus0.png' },
		imageFaceUp: { src: './imgs/monsters/catus/catus.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'This creature appears harmless, but it can fatally poison you if you get too close.',
		weight: 2, // kg
		height: 35, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.grass, MonsterType.poison],
		stats: {
			base: {
				hp: 45,
				att: 50,
				def: 55,
				ini: 30,
				spAtt: 75,
				spDef: 65
			},
			IV: {
				hp: 8,
				att: 7,
				def: 8,
				ini: 8,
				spAtt: 10,
				spDef: 11
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 1,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 39,
		cat: 3,
		status: initialStatus,
		catchRate: 155,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.LeafBullet,
			11: allAttacks.Scratch,
		} as const,
		transformLvl: 22,
		transTo: 26
	},
	Cacatus: {
		idx: 26,
		name: 'Cacatus',
		image: { src: './imgs/monsters/cacatus/cacatus0.png' },
		imageFaceUp: { src: './imgs/monsters/cacatus/cacatus.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'The spines continuously release a viscous substance that has sweet aromas to lure and poison its prey.',
		weight: 15, // kg
		height: 95, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.grass, MonsterType.poison],
		stats: {
			base: {
				hp: 60,
				att: 65,
				def: 70,
				ini: 41,
				spAtt: 85,
				spDef: 75
			},
			IV: {
				hp: 10,
				att: 9,
				def: 13,
				ini: 9,
				spAtt: 10,
				spDef: 13
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 0,
				spDef: 2
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 75,
		cat: 2,
		status: initialStatus,
		catchRate: 110,
		spawnChance: .7,
		lvlAttacks: {
			2: allAttacks.Growl,
			23: allAttacks.Scratch,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Waterball,
			// 11: allAttacks.Scratch,
		} as const
	},
	// 11
	Mamus: {
		idx: 27,
		name: 'Mamus',
		image: { src: './imgs/monsters/mamus/mamus0.png' },
		imageFaceUp: { src: './imgs/monsters/mamus/mamus.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Lives about 4 meters underground where it feeds on plant roots. It sometimes appears above ground.',
		weight: 5, // kg
		height: 29, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.ground],
		stats: {
			base: {
				hp: 15,
				att: 55,
				def: 25,
				ini: 95,
				spAtt: 35,
				spDef: 45
			},
			IV: {
				hp: 6,
				att: 8,
				def: 7,
				ini: 10,
				spAtt: 8,
				spDef: 9
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 39,
		cat: 4,
		status: initialStatus,
		catchRate: 110,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			8: allAttacks.Dirtball,
			12: allAttacks.Scratch,
		} as const,
		transformLvl: 26,
		transTo: 28
	},
	Marlagus: {
		idx: 28,
		name: 'Marlagus',
		image: { src: './imgs/monsters/marlagus/marlagus0.png' },
		imageFaceUp: { src: './imgs/monsters/marlagus/marlagus.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'It spends more time on surface but prefer dark places and only goes underground to take a nap.',
		weight: 40, // kg
		height: 145, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.ground, MonsterType.fighting],
		stats: {
			base: {
				hp: 35,
				att: 100,
				def: 50,
				ini: 120,
				spAtt: 50,
				spDef: 70
			},
			IV: {
				hp: 10,
				att: 9,
				def: 8,
				ini: 13,
				spAtt: 11,
				spDef: 11
			},
			EVY: {
				hp: 0,
				att: 1,
				def: 0,
				ini: 1,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 89,
		cat: 3,
		status: initialStatus,
		catchRate: 55,
		spawnChance: .8,
		lvlAttacks: {
			2: allAttacks.Growl,
			// 29: allAttacks.Scratch,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Waterball,
			// 11: allAttacks.Scratch,
		} as const
	},
	// 12
	Etha: {
		idx: 29,
		name: 'Etha',
		image: { src: './imgs/monsters/etha/etha0.png' },
		imageFaceUp: { src: './imgs/monsters/etha/etha.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Etha has a body of steel, this monster can demolish even a heavy truck or control the mind of the truck driver.',
		weight: 2, // kg
		height: 30, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.steel, MonsterType.dark],
		stats: {
			base: {
				hp: 50,
				att: 70,
				def: 100,
				ini: 30,
				spAtt: 40,
				spDef: 40
			},
			IV: {
				hp: 7,
				att: 8,
				def: 11,
				ini: 7,
				spAtt: 8,
				spDef: 8
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 0,
				spDef: 1
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 39,
		cat: 2,
		status: initialStatus,
		catchRate: 130,
		spawnChance: .9,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			// 7: allAttacks.Waterball,
			// 11: allAttacks.Scratch,
		} as const,
		transformLvl: 24,
		transTo: 30
	},
	Etherea: {
		idx: 30,
		name: 'Etherea',
		image: { src: './imgs/monsters/etherea/etherea0.png' },
		imageFaceUp: { src: './imgs/monsters/etherea/etherea.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Etherea tempers its steel body by drinking highly nutritious mineral springwater.',
		weight: 20, // kg
		height: 60, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.steel, MonsterType.dark],
		stats: {
			base: {
				hp: 60,
				att: 90,
				def: 140,
				ini: 40,
				spAtt: 50,
				spDef: 50
			},
			IV: {
				hp: 9,
				att: 8,
				def: 11,
				ini: 8,
				spAtt: 9,
				spDef: 8
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 1,
				spDef: 1
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 75,
		cat: 2,
		status: initialStatus,
		catchRate: 90,
		spawnChance: .7,
		lvlAttacks: {
			2: allAttacks.Growl,
			// 19: allAttacks.Growl,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Waterball,
			// 11: allAttacks.Scratch,
		} as const,
		transformLvl: 42,
		transTo: 31
	},
	Ethereus: {
		idx: 31,
		name: 'Ethereus',
		image: { src: './imgs/monsters/ethereus/ethereus0.png' },
		imageFaceUp: { src: './imgs/monsters/ethereus/ethereus.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Etherea can only evolve into Ethereus through genetic engineering. The genome has been cracked by unknowns.',
		weight: 90, // kg
		height: 120, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.steel, MonsterType.dark],
		stats: {
			base: {
				hp: 75,
				att: 110,
				def: 180,
				ini: 50,
				spAtt: 60,
				spDef: 60
			},
			IV: {
				hp: 10,
				att: 10,
				def: 12,
				ini: 8,
				spAtt: 9,
				spDef: 9
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 3,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 120,
		cat: 2,
		status: initialStatus,
		catchRate: 70,
		spawnChance: .1,
		lvlAttacks: {
			2: allAttacks.Growl,
			// 40: allAttacks.Growl,
			// 3: allAttacks.Tackle,
			// 7: allAttacks.Waterball,
			// 11: allAttacks.Scratch,
		} as const
	},
	// 13
	Ripta: {
		idx: 32,
		name: 'Ripta',
		image: { src: './imgs/monsters/ripta/ripta0.png' },
		imageFaceUp: { src: './imgs/monsters/ripta/ripta.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Ripta can split monsters twice his size into 2 with one bite. He likes to tear and throw his victims in the air.',
		weight: 110, // kg
		height: 200, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.ground],
		stats: {
			base: {
				hp: 70,
				att: 100,
				def: 60,
				ini: 45,
				spAtt: 50,
				spDef: 75
			},
			IV: {
				hp: 7,
				att: 8,
				def: 7,
				ini: 7,
				spAtt: 8,
				spDef: 9
			},
			EVY: {
				hp: 1,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 80,
		cat: 1,
		status: initialStatus,
		catchRate: 90,
		spawnChance: .8,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.Dirtball,
			11: allAttacks.Scratch,
			16: allAttacks.Bite
		} as const,
		transformLvl: 30,
		transTo: 33
	},
	Tyrantrum: {
		idx: 33,
		name: 'Tyrantrum',
		image: { src: './imgs/monsters/tyrantrum/tyrantrum0.png' },
		imageFaceUp: { src: './imgs/monsters/tyrantrum/tyrantrum.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Rumor has it that this species was created from ancient fossils. Tyrantrum bite force can be up to 21 tons.',
		weight: 450, // kg
		height: 380, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.ground],
		stats: {
			base: {
				hp: 100,
				att: 130,
				def: 80,
				ini: 60,
				spAtt: 65,
				spDef: 90
			},
			IV: {
				hp: 9,
				att: 10,
				def: 9,
				ini: 9,
				spAtt: 9,
				spDef: 10
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 2,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 120,
		cat: 1,
		status: initialStatus,
		catchRate: 60,
		spawnChance: .3,
		lvlAttacks: {
			30: allAttacks.Bite,
		} as const
	},
	// 14
	Shabbly: {
		idx: 34,
		name: 'Shabbly',
		image: { src: './imgs/monsters/shabbly/shabbly0.png' },
		imageFaceUp: { src: './imgs/monsters/shabbly/shabbly.png' },
		// frames: { max: 4, hold: 10 },
		// animate: true,
		description: 'Long considered an extinct species until recently a small colony was found deep in the arctis.',
		weight: 12, // kg
		height: 49, // cm
		position: { x: 0, y: 0 }, // { x: -100, y: 150 },
		type: [MonsterType.ice, MonsterType.dragon],
		stats: {
			base: {
				hp: 41,
				att: 64,
				def: 45,
				ini: 50,
				spAtt: 50,
				spDef: 50
			},
			IV: {
				hp: 8,
				att: 9,
				def: 7,
				ini: 9,
				spAtt: 10,
				spDef: 10
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 1,
				spDef: 0
			},
			EV: initialStats,
			IS: initialStats,
		},
		effort: 60,
		cat: 2,
		status: { ...initialStatus },
		catchRate: 110,
		spawnChance: .4,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.Snowball,
			11: allAttacks.Scratch,
		} as const,
		transformLvl: 26,
		transTo: 35
	},
	Sharkle: {
		idx: 35,
		name: 'Sharkle',
		image: { src: './imgs/monsters/sharkle/sharkle0.png' },
		imageFaceUp: { src: './imgs/monsters/sharkle/sharkle.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'A wunderful small dragon that has the ability to summon ice cold climate conditions.',
		weight: 75, // kg
		height: 180, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.ice, MonsterType.dragon],
		stats: {
			base: {
				hp: 61,
				att: 84,
				def: 65,
				ini: 70,
				spAtt: 70,
				spDef: 70
			},
			IV: {
				hp: 9,
				att: 11,
				def: 10,
				ini: 10,
				spAtt: 10,
				spDef: 9
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 1,
				spDef: 1
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 70,
		cat: 2,
		status: initialStatus,
		catchRate: 85,
		spawnChance: .3,
		lvlAttacks: {
			2: allAttacks.Growl,
			16: allAttacks.Cut
		} as const,
		transformLvl: 52,
		transTo: 36
	},
	Shreedar: {
		idx: 36,
		name: 'Shreedar',
		image: { src: './imgs/monsters/shreedar/shreedar0.png' },
		imageFaceUp: { src: './imgs/monsters/shreedar/shreedar.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'A very rare creature which intelligence is said to outbid that of humans.',
		weight: 140, // kg
		height: 250, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.ice, MonsterType.dragon],
		stats: {
			base: {
				hp: 91,
				att: 134,
				def: 95,
				ini: 80,
				spAtt: 100,
				spDef: 100
			},
			IV: {
				hp: 11,
				att: 10,
				def: 10,
				ini: 11,
				spAtt: 12,
				spDef: 10
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 1,
				spAtt: 1,
				spDef: 1
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 110,
		cat: 2,
		status: initialStatus,
		catchRate: 45,
		spawnChance: .1,
		lvlAttacks: {
			2: allAttacks.Growl,
			38: allAttacks.Cut
		} as const
	},
	// 15
	Finagron: {
		idx: 37,
		name: 'Finagron',
		image: { src: './imgs/monsters/finagron/finagron0.png' },
		imageFaceUp: { src: './imgs/monsters/finagron/finagron.png' },
		// frames: { max: 4, hold: 25 }, // higher hold = slow frame change
		// animate: true,
		description: 'Poeple say that it has been seen by astronomers high above the atmosphere.',
		weight: 190, // kg
		height: 310, // cm
		position: { x: 0, y: 0 },
		type: [MonsterType.dark, MonsterType.ghost],
		stats: {
			base: {
				hp: 90,
				att: 130,
				def: 80,
				ini: 150,
				spAtt: 100,
				spDef: 80
			},
			IV: {
				hp: 11,
				att: 13,
				def: 12,
				ini: 11,
				spAtt: 12,
				spDef: 11
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 0,
				ini: 0,
				spAtt: 3,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 190,
		cat: 4,
		status: initialStatus,
		catchRate: 30,
		spawnChance: .01,
		lvlAttacks: {
			2: allAttacks.Growl,
			38: allAttacks.Cut
		} as const
	},
	// 16
	Carkol: {
		idx: 38,
		name: 'Carkol',
		image: { src: './imgs/monsters/carkol/carkol0.png' },
		imageFaceUp: { src: './imgs/monsters/carkol/carkol.png' },
		description: 'Commonly found near mountain trails, etc. If you step on one by accident, it gets angry.',
		weight: 145,
		height: 155,
		position: { x: 0, y: 0 },
		type: [MonsterType.rock],
		stats: {
			base: {
				hp: 55,
				att: 95,
				def: 115,
				ini: 35,
				spAtt: 45,
				spDef: 45
			},
			IV: {
				hp: 6,
				att: 11,
				def: 12,
				ini: 3,
				spAtt: 4,
				spDef: 4
			},
			EVY: {
				hp: 0,
				att: 1,
				def: 1,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 90,
		cat: 1,
		status: initialStatus,
		catchRate: 90,
		spawnChance: .3,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.Dirtball,
			11: allAttacks.Scratch,
		} as const,
		transformLvl: 33,
		transTo: 39
	},
	Coalossal: {
		idx: 39,
		name: 'Coalossal',
		image: { src: './imgs/monsters/coalossal/coalossal0.png' },
		imageFaceUp: { src: './imgs/monsters/coalossal/coalossal.png' },
		description: 'Its body is extremely hard. It can easily withstand dynamite blasts without damage.',
		weight: 310,
		height: 195,
		position: { x: 0, y: 0 },
		type: [MonsterType.rock],
		stats: {
			base: {
				hp: 85,
				att: 120,
				def: 130,
				ini: 45,
				spAtt: 55,
				spDef: 65
			},
			IV: {
				hp: 8,
				att: 12,
				def: 13,
				ini: 5,
				spAtt: 5,
				spDef: 6
			},
			EVY: {
				hp: 0,
				att: 0,
				def: 3,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 145,
		cat: 2,
		status: initialStatus,
		catchRate: 45,
		spawnChance: .2,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.Dirtball,
			11: allAttacks.Scratch,
			16: allAttacks.Cut
		} as const
	},
	// 17
	Bergmite: {
		idx: 40,
		name: 'Bergmite',
		image: { src: './imgs/monsters/bergmite/bergmite0.png' },
		imageFaceUp: { src: './imgs/monsters/bergmite/bergmite.png' },
		description: 'After fleeing a volcanic eruption, it ended up moving to an area of snowy mountains. Its ice shell is as hard as steel.',
		weight: 90,
		height: 65,
		position: { x: 0, y: 0 },
		type: [MonsterType.rock],
		stats: {
			base: {
				hp: 50,
				att: 90,
				def: 110,
				ini: 45,
				spAtt: 45,
				spDef: 45
			},
			IV: {
				hp: 6,
				att: 10,
				def: 11,
				ini: 5,
				spAtt: 4,
				spDef: 4
			},
			EVY: {
				hp: 1,
				att: 0,
				def: 1,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 90,
		cat: 2,
		status: initialStatus,
		catchRate: 90,
		spawnChance: .5,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.Dirtball,
			11: allAttacks.Scratch,
		} as const,
		transformLvl: 28,
		transTo: 41
	},
	Avalugg: {
		idx: 41,
		name: 'Avalugg',
		image: { src: './imgs/monsters/avalugg/avalugg0.png' },
		imageFaceUp: { src: './imgs/monsters/avalugg/avalugg.png' },
		description: 'With its sharp claws fully extended, it can climb right up an iceberg without slipping and drill himself with his sharp head into it.',
		weight: 330,
		height: 150,
		position: { x: 0, y: 0 },
		type: [MonsterType.rock, MonsterType.ice],
		stats: {
			base: {
				hp: 95,
				att: 110,
				def: 120,
				ini: 65,
				spAtt: 60,
				spDef: 65
			},
			IV: {
				hp: 9,
				att: 11,
				def: 12,
				ini: 6,
				spAtt: 5,
				spDef: 5
			},
			EVY: {
				hp: 0,
				att: 1,
				def: 2,
				ini: 0,
				spAtt: 0,
				spDef: 0
			},
			EV: initialStats, // Max 65,535 each stat
			IS: initialStats, // actual stats (IS) & HP derived from level, base, IV & EV
		},
		effort: 145,
		cat: 2,
		status: initialStatus,
		catchRate: 40,
		spawnChance: .2,
		lvlAttacks: {
			1: allAttacks.Growl,
			3: allAttacks.Tackle,
			7: allAttacks.Dirtball,
			11: allAttacks.Scratch,
		} as const
	},
}

/**
 * This method generates a XP-table which is stored in memory for checking if a monster has reached the next level based on the total amount of XP it has gained.
 * @returns an XP-Table for all the available growth categories (1,2,3,4) which indicates how much XP each monster needs to reach the next level.
 */
// function createXPTable() {
// 	const xpTable: number[][] = []
// 	for (const monster of Object.values(getAllMonsters())) {
// 		const xpArr: number[] = []
// 		for (let j = 0; j < 100; j++) {
// 			const xpLvl = formula.calcXPGrowth(j + 1, monster.cat)
// 			xpArr.push(xpLvl)
// 		}
// 		xpTable.push(xpArr)
// 	}
// 	console.log(xpTable)
// 	return xpTable
// }

// createXPTable()