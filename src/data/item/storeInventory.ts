import { Ball } from '../../classes/Items/Ball'
import { Potion } from '../../classes/Items/Potion'
import type { IStoreInventory } from '../../models/classes'

export function getStoreInventory({ mapID, storeID }: { mapID: number, storeID: number }) {
	if (storeID < 2) { return [] }
	return structuredClone(storeInventory[`${mapID}`].store[`${storeID}`])
}

// TODO refill item count logic once sold out
const storeInventory: IStoreInventory = {
	// Map Ids
	20: {
		store: {
			// Store IDs
			2: [
				new Potion({ name: 'Potion', count: 100 }),
				new Ball({ name: 'Ball', count: 100 })
			]
		}
	}
}