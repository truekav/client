import { Ball } from '../../classes/Items/Ball'
import { Potion } from '../../classes/Items/Potion'
import { RareCandy } from '../../classes/Items/RareCandy'
import { ICollectableItems } from '../../models/classes'

export const hiddenItems: ICollectableItems = {
	// Map IDs
	20: {
		map: {
			40: new Potion({ name: 'Potion', count: 1 }),
			41: new Potion({ name: 'Potion', count: 1 }),
		},
		house: {
			// House IDs
			5: {},
			6: {},
			7: {},
			8: {
				40: new Ball({ name: 'Ball', count: 1 }),
			},
			9: {},
			10: {},
			11: {},
		}
	},
	21: {
		map: {},
		house: {}
	},
	22: {
		map: {},
		house: {}
	}
}

export const visibleItems: ICollectableItems = {
	// Map IDs
	20: { // snab town
		map: {
			60: new Ball({ name: 'Ball', count: 1 }),
			61: new RareCandy({ name: 'Rare Candy', count: 1 }),
			62: new Potion({ name: 'Potion', count: 1 })
		},
		house: {
			// House IDs
			5: {},
			6: {},
			7: {},
			8: {},
			9: {},
			10: {},
			11: {},
		}
	},
	// andro town
	21: {
		map: {
			60: new Potion({ name: 'Potion', count: 1 })
		},
		house: {}
	},
	// snab forest
	22: {
		map: {
			60: new Potion({ name: 'Potion', count: 1 }),
			61: new Ball({ name: 'Ball', count: 1 }),
			62: new Potion({ name: 'Potion', count: 1 }),
			63: new RareCandy({ name: 'Rare Candy', count: 1 }),
		},
		house: {}
	},
	// canis town
	23: {
		map: {
			60: new Potion({ name: 'Potion', count: 1 })
		},
		house: {}
	},
	// new map
	24: {
		map: {},
		house: {}
	},
	// new map
	25: {
		map: {},
		house: {}
	},
	// new map
	26: {
		map: {},
		house: {}
	},
	// new map
	27: {
		map: {},
		house: {}
	},
	// new map
	28: {
		map: {},
		house: {}
	},
	// new map
	29: {
		map: {},
		house: {}
	},

}