import { Howl } from 'howler'
import { IMonsterAudio } from '../models/functions'

const loop = true
const html5 = true
const volume = 1
const preload = false

export function playAudio(name: string) {
	const audio = mapAudio[name] || battleAudio[name] || attackAudio[name]
	// console.log('audio: ', audio)
	if (audio && audio.state() === 'unloaded') {
		audio.load()
	}
	audio.play()
}

export const mapAudio: { [key: string]: Howl } = {
	openingDemoFirst: new Howl({
		src: './audio/map/openingDemoFirst.mp3',
		html5,
		loop,
		volume: .6,
		preload
	}),
	openingDemoSecond: new Howl({
		src: './audio/map/openingDemoSecond.mp3',
		html5,
		loop,
		volume: .3,
		preload
	}),
	opening: new Howl({
		src: './audio/map/opening.mp3',
		html5,
		loop,
		volume: .5,
		preload
	}),
	wild_battle: new Howl({
		src: './audio/battle/wild_battle.mp3',
		html5,
		loop,
		volume: .4,
		preload
	}),
	wild_victory: new Howl({
		src: './audio/battle/wild_victory.mp3',
		html5,
		volume: .4,
		loop,
		preload
	}),
	item1: new Howl({
		src: './audio/map/item1.mp3',
		html5,
		volume: .2,
		preload
	}),
	importantItem: new Howl({
		src: './audio/map/importantItem.mp3',
		html5,
		volume: .4,
		preload
	}),
	collision: new Howl({
		src: './audio/map/collision.mp3',
		html5,
		volume: .1,
		preload
	}),
	pressab: new Howl({
		src: './audio/map/pressab.mp3',
		html5,
		volume: .1,
		preload
	}),
	hpUp: new Howl({
		src: './audio/map/hpUp.mp3',
		html5,
		volume: .3,
		preload
	}),
	openMenu: new Howl({
		src: './audio/map/openMenu.mp3',
		html5,
		volume,
		preload
	}),
	closeMenu: new Howl({
		src: './audio/map/closeMenu.mp3',
		html5,
		volume: .5,
		preload
	}),
	recovery: new Howl({
		src: './audio/hospital/recovery.mp3',
		html5,
		volume: .4,
		preload
	}),
	goInside: new Howl({
		src: './audio/map/goInside.mp3',
		html5,
		volume: .1,
		preload
	}),
	goOutside: new Howl({
		src: './audio/map/goOutside.mp3',
		html5,
		volume: .2,
		preload
	}),
	pcOn: new Howl({
		src: './audio/map/pcOn.mp3',
		html5,
		volume: .4,
		preload
	})
}

export const battleAudio: { [key: string]: Howl } = {
	trainerSpawn: new Howl({
		src: './audio/battle/trainerSpawn.mp3',
		html5,
		volume: .2,
		preload
	}),
	evolution: new Howl({
		src: './audio/battle/evolution.mp3',
		html5,
		volume: .35,
		preload
	}),
	congratsEvo: new Howl({
		src: './audio/battle/congratsEvo.mp3',
		html5,
		volume: .35,
		preload
	}),
	forgetMove: new Howl({
		src: './audio/battle/forgetMove.mp3',
		html5,
		volume: .7,
		preload
	}),
	learnMove: new Howl({
		src: './audio/battle/learnMove.mp3',
		html5,
		volume: .5,
		preload
	}),
	leaveBattle: new Howl({
		src: './audio/battle/leaveBattle.mp3',
		html5,
		volume: .15,
		preload
	}),
	initBall: new Howl({
		src: './audio/ball/initBall.mp3',
		html5,
		volume: .2,
		preload
	}),
	lvlup: new Howl({
		src: './audio/battle/lvlup.mp3',
		html5,
		volume: .5,
		preload
	}),
	callback: new Howl({
		src: './audio/battle/callback.mp3',
		html5,
		volume: 1,
		preload
	}),
	ballImpact: new Howl({
		src: './audio/ball/ballImpact.mp3',
		html5,
		volume: .4,
		preload
	}),
	hitBall: new Howl({
		src: './audio/ball/hitBall.mp3',
		html5,
		volume: .2,
		preload
	}),
	throw: new Howl({
		src: './audio/ball/throw.mp3',
		html5,
		volume: .45,
		preload
	}),
	getMedal: new Howl({
		src: './audio/battle/getMedal.mp3',
		html5,
		volume: .4,
		preload
	})
}

export const monsterAudio: IMonsterAudio = {
	Draggle: new Howl({
		src: './audio/monster/draggle/draggle.mp3',
		html5,
		volume: .5,
		preload
	}),
	Dragolino: new Howl({
		src: './audio/monster/dragolino/dragolino.mp3',
		html5,
		volume: .5,
		preload
	}),
	Dragolon: new Howl({
		src: './audio/monster/dragolon/dragolon.mp3',
		html5,
		volume: .7,
		preload
	}),
	Emby: new Howl({
		src: './audio/monster/emby/emby.mp3',
		html5,
		volume,
		preload
	}),
	Flemby: new Howl({
		src: './audio/monster/flemby/flemby.mp3',
		html5,
		volume: .3,
		preload
	}),
	Flembolon: new Howl({
		src: './audio/monster/flembolon/flembolon.mp3',
		html5,
		volume: .3,
		preload
	}),
	Sheely: new Howl({
		src: './audio/monster/sheely/sheely.mp3',
		html5,
		volume: .4,
		preload
	}),
	Sheezor: new Howl({
		src: './audio/monster/sheezor/sheezor.mp3',
		html5,
		volume: .5,
		preload
	}),
	Sheerazor: new Howl({
		src: './audio/monster/sheerazor/sheerazor.mp3',
		html5,
		volume: .4,
		preload
	}),
	Rookidee: new Howl({
		src: './audio/monster/rookidee/rookidee.mp3',
		html5,
		volume: .2,
		preload
	}),
	Corvisquire: new Howl({
		src: './audio/monster/corvisquire/corvisquire.mp3',
		html5,
		volume: .5,
		preload
	}),
	Corviknight: new Howl({
		src: './audio/monster/corviknight/corviknight.mp3',
		html5,
		volume: .3,
		preload
	}),
	Fletchling: new Howl({
		src: './audio/monster/fletchling/fletchling.mp3',
		html5,
		volume: .3,
		preload
	}),
	Fletchinder: new Howl({
		src: './audio/monster/fletchinder/fletchinder.mp3',
		html5,
		volume: .2,
		preload
	}),
	Talonflame: new Howl({
		src: './audio/monster/talonflame/talonflame.mp3',
		html5,
		volume: .4,
		preload
	}),
	Yungoos: new Howl({
		src: './audio/monster/yungoos/yungoos.mp3',
		html5,
		volume: .4,
		preload
	}),
	Gumshoos: new Howl({
		src: './audio/monster/gumshoos/gumshoos.mp3',
		html5,
		volume: .4,
		preload
	}),
	Scatterbug: new Howl({
		src: './audio/monster/scatterbug/scatterbug.mp3',
		html5,
		volume: .2,
		preload
	}),
	Spewpa: new Howl({
		src: './audio/monster/spewpa/spewpa.mp3',
		html5,
		volume: .2,
		preload
	}),
	Vivillon: new Howl({
		src: './audio/monster/vivillon/vivillon.mp3',
		html5,
		volume: .4,
		preload
	}),
	Blipbug: new Howl({
		src: './audio/monster/blipbug/blipbug.mp3',
		html5,
		volume: .4,
		preload
	}),
	Dottler: new Howl({
		src: './audio/monster/dottler/dottler.mp3',
		html5,
		volume: .6,
		preload
	}),
	Orbeetle: new Howl({
		src: './audio/monster/orbeetle/orbeetle.mp3',
		html5,
		volume: .3,
		preload
	}),
	Bunnelby: new Howl({
		src: './audio/monster/bunnelby/bunnelby.mp3',
		html5,
		volume: .3,
		preload
	}),
	Diggersby: new Howl({
		src: './audio/monster/diggersby/diggersby.mp3',
		html5,
		volume: .5,
		preload
	}),
	Catus: new Howl({
		src: './audio/monster/catus/catus.mp3',
		html5,
		volume: .5,
		preload
	}),
	Cacatus: new Howl({
		src: './audio/monster/cacatus/cacatus.mp3',
		html5,
		volume: .2,
		preload
	}),
	Mamus: new Howl({
		src: './audio/monster/mamus/mamus.mp3',
		html5,
		volume: .5,
		preload
	}),
	Marlagus: new Howl({
		src: './audio/monster/marlagus/marlagus.mp3',
		html5,
		volume: .5,
		preload
	}),
	Etha: new Howl({
		src: './audio/monster/etha/etha.mp3',
		html5,
		volume: .4,
		preload
	}),
	Etherea: new Howl({
		src: './audio/monster/etherea/etherea.mp3',
		html5,
		volume: .4,
		preload
	}),
	Ethereus: new Howl({
		src: './audio/monster/ethereus/ethereus.mp3',
		html5,
		volume: .4,
		preload
	}),
	Ripta: new Howl({
		src: './audio/monster/ripta/ripta.mp3',
		html5,
		volume: .5,
		preload
	}),
	Tyrantrum: new Howl({
		src: './audio/monster/tyrantrum/tyrantrum.mp3',
		html5,
		volume: .3,
		preload
	}),
	Shabbly: new Howl({
		src: './audio/monster/shabbly/shabbly.mp3',
		html5,
		volume: .5,
		preload
	}),
	Sharkle: new Howl({
		src: './audio/monster/sharkle/sharkle.mp3',
		html5,
		volume: .4,
		preload
	}),
	Shreedar: new Howl({
		src: './audio/monster/shreedar/shreedar.mp3',
		html5,
		volume: .2,
		preload
	}),
	Finagron: new Howl({
		src: './audio/monster/finagron/nice.mp3',
		html5,
		volume: .25,
		preload
	}),
	Carkol: new Howl({
		src: './audio/monster/carkol/carkol.mp3',
		html5,
		volume: .5,
		preload
	}),
	Coalossal: new Howl({
		src: './audio/monster/coalossal/coalossal.mp3',
		html5,
		volume: .5,
		preload
	}),
	Bergmite: new Howl({
		src: './audio/monster/bergmite/bergmite.mp3',
		html5,
		volume: .5,
		preload
	}),
	Avalugg: new Howl({
		src: './audio/monster/avalugg/avalugg.mp3',
		html5,
		volume: .5,
		preload
	}),
}

export const attackAudio: { [key: string]: Howl } = {
	tackleHit: new Howl({
		src: './audio/attack/tackleHit.mp3',
		html5,
		volume: .4,
		preload
	}),
	growl: new Howl({
		src: './audio/attack/growl.mp3',
		html5,
		volume: .1,
		preload
	}),
	initFireball: new Howl({
		src: './audio/attack/initFireball.mp3',
		html5,
		volume: .9,
		preload
	}),
	fireballHit: new Howl({
		src: './audio/attack/fireballHit.mp3',
		html5,
		volume: .25,
		preload
	}),
	initWaterball: new Howl({
		src: './audio/attack/initWaterball.mp3',
		html5,
		volume: .3,
		preload
	}),
	waterballHit: new Howl({
		src: './audio/attack/waterballHit.mp3',
		html5,
		volume: .3,
		preload
	}),
	initSnowball: new Howl({
		src: './audio/attack/initSnowball.mp3',
		html5,
		volume: .2,
		preload
	}),
	snowballHit: new Howl({
		src: './audio/attack/snowballHit.mp3',
		html5,
		volume: .3,
		preload
	}),
	initDirtball: new Howl({
		src: './audio/attack/initDirtball.mp3',
		html5,
		volume: .4,
		preload
	}),
	dirtballHit: new Howl({
		src: './audio/attack/dirtballHit.mp3',
		html5,
		volume: .3,
		preload
	}),
	initLeafBullet: new Howl({
		src: './audio/attack/initLeafBullet.mp3',
		html5,
		volume: .1,
		preload
	}),
	leafBulletHit: new Howl({
		src: './audio/attack/leafBulletHit.mp3',
		html5,
		volume: .8,
		preload
	}),
	scratch: new Howl({
		src: './audio/attack/scratch.mp3',
		html5,
		volume: .8,
		preload
	}),
	cut: new Howl({
		src: './audio/attack/cut.mp3',
		html5,
		volume: .4,
		preload
	})
}