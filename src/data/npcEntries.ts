import { player } from '..'
import { Ball } from '../classes/Items/Ball'
import { dialog } from '../classes/Dialog'
import { flags } from '../classes/Flags'
import { getMonster } from './monster/allMonsters'
import { Monster } from '../classes/Monster'
import { Potion } from '../classes/Items/Potion'
import { monsterAudio, playAudio } from './audio'
import { NPC } from '../classes/NPC'
import type { TNonTrainerNPCs, TTrainerNPCs, TAllNPCs } from '../models/functions'
import { getMap } from '../classes/Movables'
import { initFirstMonListHTML } from '../util/html/battle'
import { getUniqueID } from '../util/helper'
import { base64NPCIdleSprite } from './base64Imgs'
import { data } from '../classes/Data'
import { viewport } from '../classes/Viewport'
import { getMonsterTypeNameById } from './monster/types'

const frames = { max: 4, hold: 210 } // idle NPC
const moveFrames = { max: 4, hold: 10 } // movable NPC
/* npc audio */
const trainerAudio = {
	look: {
		src: './audio/map/look1.mp3',
		html5: true,
		loop: true,
		volume: .4,
		preload: false
	},
	battle: {
		src: './audio/map/trainerBattle.mp3',
		html5: true,
		loop: true,
		volume: .4,
		preload: false
	},
	victory: {
		src: './audio/battle/trainer_victory.mp3',
		html5: true,
		volume: .4,
		loop: true,
		preload: false
	}
}
const rivalAudio = {
	look: {
		src: './audio/map/lookRival.mp3',
		html5: true,
		loop: true,
		volume: .4,
		preload: false
	},
	battle: {
		src: './audio/map/rivalBattle.mp3',
		html5: true,
		loop: true,
		volume: .4,
		preload: false
	},
	victory: {
		src: './audio/battle/trainer_victory.mp3',
		html5: true,
		volume: .4,
		loop: true,
		preload: false
	}
}
const profAudio = {
	look: {
		src: './audio/map/lookProf.mp3',
		html5: true,
		loop: true,
		volume: .4,
		preload: false
	},
	battle: {
		src: './audio/map/rivalBattle.mp3',
		html5: true,
		loop: true,
		volume: .4,
		preload: false
	},
	victory: {
		src: './audio/battle/trainer_victory.mp3',
		html5: true,
		volume: .4,
		loop: true,
		preload: false
	}
}
// const fiatGangAudio = {}
// const fiatGangBossAudio = {}
const gymAudio = {
	battle: {
		src: './audio/battle/gym_battle.mp3',
		html5: true,
		loop: true,
		volume: .4,
		preload: false
	},
	victory: {
		src: './audio/battle/gym_victory.mp3',
		html5: true,
		volume: .4,
		loop: true,
		preload: false
	}
}

/* non-trainer entries */
const nonTrainerNPCs: TNonTrainerNPCs = {
	Antopoulos: {
		name: 'antopoulos',
		image: { src: base64NPCIdleSprite.ANTOPOULOS },
		audio: profAudio,
		sprites: {
			up: { src: './imgs/npcs/villagers/prof_antopoulos/up.png' },
			right: { src: './imgs/npcs/villagers/prof_antopoulos/right.png' },
			down: { src: './imgs/npcs/villagers/prof_antopoulos/down.png' },
			left: { src: './imgs/npcs/villagers/prof_antopoulos/left.png' }
		},
	},
	Mother: {
		name: 'mother',
		image: { src: base64NPCIdleSprite.MOTHER },
		sprites: {
			up: { src: './imgs/npcs/villagers/mother/up.png' },
			right: { src: './imgs/npcs/villagers/mother/right.png' },
			down: { src: './imgs/npcs/villagers/mother/down.png' },
			left: { src: './imgs/npcs/villagers/mother/left.png' }
		},
	},
	Neighbor: {
		name: 'neighbor',
		image: { src: base64NPCIdleSprite.NEIGHBOR },
		sprites: {
			up: { src: './imgs/npcs/villagers/neighbor/up.png' },
			right: { src: './imgs/npcs/villagers/neighbor/right.png' },
			down: { src: './imgs/npcs/villagers/neighbor/down.png' },
			left: { src: './imgs/npcs/villagers/neighbor/left.png' }
		},
	},
	Villager0: {
		name: 'villager0',
		image: { src: base64NPCIdleSprite.VILLAGER0 },
		sprites: {
			up: { src: './imgs/npcs/villagers/villager0/up.png' },
			right: { src: './imgs/npcs/villagers/villager0/right.png' },
			down: { src: './imgs/npcs/villagers/villager0/down.png' },
			left: { src: './imgs/npcs/villagers/villager0/left.png' }
		},
	},
	Villager1: {
		name: 'villager1',
		image: { src: base64NPCIdleSprite.VILLAGER1 },
		sprites: {
			up: { src: './imgs/npcs/villagers/villager1/up.png' },
			right: { src: './imgs/npcs/villagers/villager1/right.png' },
			down: { src: './imgs/npcs/villagers/villager1/down.png' },
			left: { src: './imgs/npcs/villagers/villager1/left.png' }
		},
	},
	Villager2: {
		name: 'villager2',
		image: { src: base64NPCIdleSprite.VILLAGER2 },
		sprites: {
			up: { src: './imgs/npcs/villagers/villager2/up.png' },
			right: { src: './imgs/npcs/villagers/villager2/right.png' },
			down: { src: './imgs/npcs/villagers/villager2/down.png' },
			left: { src: './imgs/npcs/villagers/villager2/left.png' }
		},
	},
	Villager3: {
		name: 'villager3',
		image: { src: base64NPCIdleSprite.VILLAGER3 },
		headImgs: {
			up: { src: './imgs/npcs/villagers/villager3/headUp.png' },
			right: { src: './imgs/npcs/villagers/villager3/headRight.png' },
			down: { src: './imgs/npcs/villagers/villager3/headDown.png' },
			left: { src: './imgs/npcs/villagers/villager3/headLeft.png' }
		},
		sprites: {
			up: { src: './imgs/npcs/villagers/villager3/up.png' },
			right: { src: './imgs/npcs/villagers/villager3/right.png' },
			down: { src: './imgs/npcs/villagers/villager3/down.png' },
			left: { src: './imgs/npcs/villagers/villager3/left.png' }
		},
	},
	Villager4: {
		name: 'villager4',
		image: { src: base64NPCIdleSprite.VILLAGER4 },
		sprites: {
			up: { src: './imgs/npcs/villagers/villager4/up.png' },
			right: { src: './imgs/npcs/villagers/villager4/right.png' },
			down: { src: './imgs/npcs/villagers/villager4/down.png' },
			left: { src: './imgs/npcs/villagers/villager4/left.png' }
		},
	},
	Villager5: {
		name: 'villager5',
		image: { src: base64NPCIdleSprite.VILLAGER5 },
		sprites: {
			up: { src: './imgs/npcs/villagers/villager5/up.png' },
			right: { src: './imgs/npcs/villagers/villager5/right.png' },
			down: { src: './imgs/npcs/villagers/villager5/down.png' },
			left: { src: './imgs/npcs/villagers/villager5/left.png' }
		},
	},
	Villager6: {
		name: 'villager6',
		image: { src: base64NPCIdleSprite.VILLAGER6 },
		headImgs: {
			up: { src: './imgs/npcs/villagers/villager6/headUp.png' },
			right: { src: './imgs/npcs/villagers/villager6/headRight.png' },
			down: { src: './imgs/npcs/villagers/villager6/headDown.png' },
			left: { src: './imgs/npcs/villagers/villager6/headLeft.png' }
		},
		sprites: {
			up: { src: './imgs/npcs/villagers/villager6/up.png' },
			right: { src: './imgs/npcs/villagers/villager6/right.png' },
			down: { src: './imgs/npcs/villagers/villager6/down.png' },
			left: { src: './imgs/npcs/villagers/villager6/left.png' }
		},
	},
	Promoter: {
		name: 'promoter',
		image: { src: base64NPCIdleSprite.PROMOTER },
		sprites: {
			up: { src: './imgs/npcs/villagers/promoter/up.png' },
			right: { src: './imgs/npcs/villagers/promoter/right.png' },
			down: { src: './imgs/npcs/villagers/promoter/down.png' },
			left: { src: './imgs/npcs/villagers/promoter/left.png' }
		},
	},
	Walker: {
		name: 'walker',
		image: { src: './imgs/npcs/villagers/walker/idle.png' },
		sprites: {
			up: { src: './imgs/npcs/villagers/walker/up.png' },
			right: { src: './imgs/npcs/villagers/walker/right.png' },
			down: { src: './imgs/npcs/villagers/walker/down.png' },
			left: { src: './imgs/npcs/villagers/walker/left.png' }
		},
	},
	Nurse: {
		name: 'nurse',
		image: { src: './imgs/npcs/villagers/nurse/down.png' },
	},
	Seller: {
		name: 'seller',
		image: { src: base64NPCIdleSprite.SELLER },
		sprites: {
			up: { src: './imgs/npcs/villagers/seller/up.png' },
			right: { src: './imgs/npcs/villagers/seller/right.png' },
			down: { src: './imgs/npcs/villagers/seller/down.png' },
			left: { src: './imgs/npcs/villagers/seller/left.png' }
		},
	},
	Guard: {
		name: 'guard',
		image: { src: base64NPCIdleSprite.GUARD },
		sprites: {
			up: { src: './imgs/npcs/villagers/guard/up.png' },
			right: { src: './imgs/npcs/villagers/guard/right.png' },
			down: { src: './imgs/npcs/villagers/guard/down.png' },
			left: { src: './imgs/npcs/villagers/guard/left.png' }
		},
	},
	Gyminfo: {
		name: 'gyminfo',
		image: { src: './imgs/npcs/villagers/gym_info/idle.png' },
		sprites: {
			up: { src: './imgs/npcs/villagers/gym_info/up.png' },
			right: { src: './imgs/npcs/villagers/gym_info/right.png' },
			down: { src: './imgs/npcs/villagers/gym_info/down.png' },
			left: { src: './imgs/npcs/villagers/gym_info/left.png' }
		},
	}
}

/* trainer entries */
const trainerNPCs: TTrainerNPCs = {
	Rival: {
		name: 'rival',
		image: { src: './imgs/npcs/trainers/Rival/down.png' },
		audio: rivalAudio,
		sprites: {
			up: { src: './imgs/npcs/trainers/Rival/up.png' },
			right: { src: './imgs/npcs/trainers/Rival/right.png' },
			down: { src: './imgs/npcs/trainers/Rival/down.png' },
			left: { src: './imgs/npcs/trainers/Rival/left.png' }
		},
		battleSpriteSrc: { src: './imgs/npcs/trainers/Rival/battle.png' },
		effort: 180,
		isTrainer: false, // false because it is a rival, a specific trainer
		isRival: true,
		isGym: false
	},
	Environmental_activist: {
		name: 'environmental activist',
		image: { src: './imgs/npcs/trainers/Environmental_activist/right.png' },
		audio: trainerAudio,
		sprites: {
			up: { src: './imgs/npcs/trainers/Environmental_activist/up.png' },
			right: { src: './imgs/npcs/trainers/Environmental_activist/right.png' },
			down: { src: './imgs/npcs/trainers/Environmental_activist/down.png' },
			left: { src: './imgs/npcs/trainers/Environmental_activist/left.png' }
		},
		battleSpriteSrc: { src: './imgs/npcs/trainers/Environmental_activist/battle.png' },
		effort: 55,
		isTrainer: true,
		isGym: false
	},
	Trader: {
		name: 'trader',
		image: { src: './imgs/npcs/trainers/Trader/left.png' },
		audio: trainerAudio,
		sprites: {
			up: { src: './imgs/npcs/trainers/Trader/up.png' },
			right: { src: './imgs/npcs/trainers/Trader/right.png' },
			down: { src: './imgs/npcs/trainers/Trader/down.png' },
			left: { src: './imgs/npcs/trainers/Trader/left.png' }
		},
		battleSpriteSrc: { src: './imgs/npcs/trainers/Trader/battle.png' },
		effort: 69,
		isTrainer: true,
		isGym: false
	},
	Banker: {
		name: 'banker',
		image: { src: './imgs/npcs/trainers/Banker/down.png' },
		audio: trainerAudio,
		sprites: {
			up: { src: './imgs/npcs/trainers/Banker/up.png' },
			right: { src: './imgs/npcs/trainers/Banker/right.png' },
			down: { src: './imgs/npcs/trainers/Banker/down.png' },
			left: { src: './imgs/npcs/trainers/Banker/left.png' }
		},
		battleSpriteSrc: { src: './imgs/npcs/trainers/Banker/battle.png' },
		effort: 66,
		isTrainer: true,
		isGym: false
	},
	Chain_analyst: {
		name: 'chain analyst',
		image: { src: './imgs/npcs/trainers/Chain_analyst/up.png' },
		audio: trainerAudio,
		sprites: {
			up: { src: './imgs/npcs/trainers/Chain_analyst/up.png' },
			right: { src: './imgs/npcs/trainers/Chain_analyst/right.png' },
			down: { src: './imgs/npcs/trainers/Chain_analyst/down.png' },
			left: { src: './imgs/npcs/trainers/Chain_analyst/left.png' }
		},
		battleSpriteSrc: { src: './imgs/npcs/trainers/Chain_analyst/battle.png' },
		effort: 42,
		isTrainer: true,
		isGym: false
	},
	Peter_sniff: {
		name: 'peter Sniff',
		image: { src: './imgs/npcs/trainers/Peter_sniff/down.png' },
		audio: gymAudio,
		sprites: {
			up: { src: './imgs/npcs/trainers/Peter_sniff/up.png' },
			right: { src: './imgs/npcs/trainers/Peter_sniff/right.png' },
			down: { src: './imgs/npcs/trainers/Peter_sniff/down.png' },
			left: { src: './imgs/npcs/trainers/Peter_sniff/left.png' },
		},
		battleSpriteSrc: { src: './imgs/npcs/trainers/Peter_sniff/battle.png' },
		effort: 260,
		isTrainer: false,
		isGym: true
	},
	Gold_miner: {
		name: 'gold miner',
		image: { src: './imgs/npcs/trainers/Gold_miner/down.png' },
		audio: trainerAudio,
		sprites: {
			up: { src: './imgs/npcs/trainers/Gold_miner/up.png' },
			right: { src: './imgs/npcs/trainers/Gold_miner/right.png' },
			down: { src: './imgs/npcs/trainers/Gold_miner/down.png' },
			left: { src: './imgs/npcs/trainers/Gold_miner/left.png' },
		},
		battleSpriteSrc: { src: './imgs/npcs/trainers/Gold_miner/battle.png' },
		effort: 160,
		isTrainer: true,
		isGym: false
	}
}

/* trainer and non-trainer data needed for all maps & houses */
export const allNPCs: TAllNPCs = [
	// ID 20 - SNAB-TOWN
	{
		id: 20,
		npcs: [
			/* non-trainers */
			// Prof. Antopoulos
			{
				...structuredClone(nonTrainerNPCs['Antopoulos']),
				image: { src: './imgs/npcs/villagers/prof_antopoulos/down.png' },
				id: 110,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => {
						dialog.show(`PROF. ${npc.name}: Hello ${player.name}...`)
					},
					() => dialog.show('I see you can\'t wait to start your journey!'),
					() => dialog.show('The road to the MONSTER-LEAGUE in AGROLAND is very dangerous!'),
					() => dialog.show('You will not survive without any MONSTER to protect you.'),
					() => dialog.show('Luckily, I can offer you ONE of my THREE favorite MONSTER. You just have to choose...'),
					() => {
						// open monster list
						dialog.updateInnerHTML('Choose your FIRST MONSTER!')
						initFirstMonListHTML()
					},
					() => {
						getMap().playAudio()
						dialog.show(`I see! You prefer the ${getMonsterTypeNameById(player.monsters[0].type[0]).toUpperCase()} type...`)
					},
					() => dialog.show(`${player.monsters[0].name} is a good choice! It has much potential and I know it is in good hands now!`),
					() => {
						const rival = data.getNPCByID({ npcID: 106, mapID: player.currentMapId })
						if (!rival) { return }
						dialog.show(`However, ${rival.name} will know which MONSTER-TYPE you have.`)
					},
					() => dialog.show(`OH! He just sent me a message! Good luck, ${player.name}!`),
					() => {
						getMap().audio.pause()
						npc.playLookAudio()
						dialog.hide()
						npc.shouldWalkAway = true
					}
				],
				// monsters where player has to choose his first monster for the journey
				monsters: [
					new Monster({
						...getMonster('Draggle'),
						uid: getUniqueID(),
						audio: monsterAudio['Draggle'],
						level: 5,
						catchLvl: 5,
						catchLocation: 'SNAB-TOWN'
					}),
					new Monster({
						...getMonster('Emby'),
						uid: getUniqueID(),
						audio: monsterAudio['Emby'],
						level: 5,
						catchLvl: 5,
						catchLocation: 'SNAB-TOWN'
					}),
					new Monster({
						...getMonster('Sheely'),
						uid: getUniqueID(),
						audio: monsterAudio['Sheely'],
						level: 5,
						catchLvl: 5,
						catchLocation: 'SNAB-TOWN'
					})
				]
			},
			// Mother
			{
				...structuredClone(nonTrainerNPCs['Mother']),
				image: { src: './imgs/npcs/villagers/mother/down.png' },
				isMovingAround: true,
				maxRange: 250 * viewport.getScaleFactor(false),
				id: 100,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show(`${npc.name}: Hello my Son...`),
					() => dialog.show('You have to visit our NEIGHBOR, his SON has something for you!'),
					() => dialog.show('Also, I wanted to give you something on your way aswell...'),
					() => {
						// item from npc to player
						if (!npc.itemGift) { return }
						getMap().audio.pause()
						flags.setIsAnimating(true)
						playAudio('item1')
						dialog.show(`${player.name} received x${npc.itemGift.count} ${npc.itemGift.name}!`)
						player.addItem(new Ball({ name: npc.itemGift.name, count: npc.itemGift.count }))
						npc.removeItemGift()
						const t = setTimeout(() => {
							flags.setIsAnimating(false)
							clearTimeout(t)
						}, 2000)
					},
					() => {
						getMap().playAudio()
						dialog.show('It\'s not much but I hope it will be helpful')
					},
					() => dialog.show('Please take care, Son!'),
					npc.finishDialog
				],
				itemGift: new Ball({ name: 'Ball', count: 5 }),
				secondDialog: (npc: NPC) => [
					() => dialog.show(`Have you cleaned your room and turned off your PC, ${player.name}?`),
					() => dialog.show('I miss you already!'),
					npc.finishDialog
				]
			},
			// Neighbor
			{
				...structuredClone(nonTrainerNPCs['Neighbor']),
				id: 101,
				animate: true,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show(`${npc.name}: Hello ${player.name}...`),
					() => {
						const log = !player.monsterDex.available ?
							'My SON wants to give you something! You will find him inside.' :
							'I wish you good luck!'
						dialog.show(log)
					},
					npc.finishDialog
				],
			},
			// Walker
			{
				...structuredClone(nonTrainerNPCs['Walker']),
				id: 102,
				animate: false,
				isMovingAround: false,
				maxRange: 75 * viewport.getScaleFactor(false),
				frames: moveFrames,
				isBlocker: true,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Water... I need water... I am walking for such a long time... Do you have water for me?'),
					() => npc.rejectPlayerAfterBlocking({ direction: 'a' })
				],
				// gift that will not be given to player and is just to determine if first or second dialog will be fired
				itemGift: new Potion({ name: 'Potion', count: 1 }),
				secondDialog: (npc: NPC) => [
					() => dialog.show('Ahhh Someone gave me water... I feel so strong again!'),
					npc.finishDialog
				]
			},
			// Random villager
			{
				...structuredClone(nonTrainerNPCs['Seller']),
				image: { src: './imgs/npcs/villagers/seller/right.png' },
				id: 107,
				animate: false,
				isMovingAround: true,
				maxRange: 250 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('You can find MONSTER from type WATER like YUNGOOS in high grass right next to the water aswell!'),
					() => dialog.show('I was able to catch one yesterday while looking for a BLIPBUG! Lucky me!'),
					npc.finishDialog
				],
			},
			// Random high grass villager
			{
				...structuredClone(nonTrainerNPCs['Villager3']),
				image: { src: './imgs/npcs/villagers/villager3/down.png' },
				id: 108,
				animate: false,
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('PSSST! Don\'t be so loud...'),
					() => dialog.show('I am trying to find a rare ROOKIDEE!'),
					() => dialog.show('Everytime I find one, he manages to escape me! 5 times already!'),
					() => dialog.show('Leave me alone!'),
					npc.finishDialog
				],
			},
			// Promoter
			{
				...structuredClone(nonTrainerNPCs['Promoter']),
				image: { src: './imgs/npcs/villagers/promoter/right.png' },
				id: 109,
				animate: false,
				isMovingAround: true,
				maxRange: 200 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Hey! I am a PROMOTER!'),
					() => dialog.show('You should visit the SHOP in SNAB-TOWN, we have a lot of ITEMs that might help you survive in the SNAB-FOREST!'),
					() => dialog.show('It\'s not far away... just go a few steps to the SOUTH.'),
					() => dialog.show('Here, I\'ll give you a promotional gift!'),
					() => {
						// item from npc to player
						if (!npc.itemGift) { return }
						getMap().audio.pause()
						flags.setIsAnimating(true)
						playAudio('item1')
						dialog.show(`${player.name} received x${npc.itemGift.count} ${npc.itemGift.name}!`)
						player.addItem(new Ball({ name: npc.itemGift.name, count: npc.itemGift.count }))
						npc.removeItemGift()
						const t = setTimeout(() => {
							flags.setIsAnimating(false)
							clearTimeout(t)
						}, 2000)
					},
					() => {
						getMap().playAudio()
						dialog.show('I hope to have convinced you! Good luck anyway!')
					},
					npc.finishDialog
				],
				itemGift: new Ball({ name: 'Ball', count: 1 }),
				secondDialog: (npc: NPC) => [
					() => dialog.show('I have to stay here for a while...'),
					npc.finishDialog
				]
			},
			// snab-town villager
			{
				...structuredClone(nonTrainerNPCs['Villager4']),
				image: { src: './imgs/npcs/villagers/villager4/right.png' },
				id: 111,
				animate: false,
				isMovingAround: true,
				maxRange: 100 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('I see you carry MONSTER with you... I raise monsters too!'),
					() => dialog.show('They can protect me when they grow up and evolve!'),
					npc.finishDialog
				],
			},
			// snab town peer villager
			{
				...structuredClone(nonTrainerNPCs['Villager0']),
				id: 113,
				animate: true,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('I am trying to teach my GUMSHOOS how to carry me through the water so we can expore the wide ocean!'),
					() => dialog.show('But it seems like he does not want to learn anything! I am frustrated!'),
					npc.finishDialog
				],
			},
			// snab town lady
			{
				...structuredClone(nonTrainerNPCs['Villager5']),
				image: { src: './imgs/npcs/villagers/villager5/up.png' },
				id: 112,
				animate: false,
				isMovingAround: true,
				maxRange: 200 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Have you heard from the FIAT-GANG?'),
					() => dialog.show('It is a ruthless sect that attacks everyone and protects their interests at all costs!'),
					() => dialog.show('They were also in my house and stole my MONSTERs! So be careful if you encounter them!'),
					npc.finishDialog
				],
			},
			// forest lady
			{
				...structuredClone(nonTrainerNPCs['Villager6']),
				image: { src: './imgs/npcs/villagers/villager6/up.png' },
				id: 114,
				animate: false,
				isMovingAround: true,
				maxRange: 300 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('The SNAB-FOREST is a very scary place full of TRAINER and MONSTER!'),
					() => dialog.show('Make sure to heal your MONSTER and carry some BALLs and POTIONs with you before you go there!'),
					npc.finishDialog
				],
			},
			// random
			{
				...structuredClone(nonTrainerNPCs['Villager6']),
				image: { src: './imgs/npcs/villagers/villager6/up.png' },
				id: 115,
				animate: false,
				isMovingAround: true,
				maxRange: 100 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('There are so many different MONSTERS in this world.'),
					() => dialog.show('According to books, there are currently 210 different species... Some of them can also evolve!'),
					() => dialog.show('But there could be many more undiscovered...'),
					npc.finishDialog
				],
			},
			// random
			{
				...structuredClone(nonTrainerNPCs['Villager4']),
				image: { src: './imgs/npcs/villagers/villager4/up.png' },
				id: 116,
				animate: false,
				isMovingAround: true,
				maxRange: 75 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Some MONSTERs are friendly and others are not. But it is possible to train them all.'),
					npc.finishDialog
				],
			},
			// twin high grass villager
			{
				...structuredClone(nonTrainerNPCs['Villager3']),
				image: { src: './imgs/npcs/villagers/villager3/down.png' },
				id: 117,
				animate: false,
				isMovingAround: true,
				maxRange: 75 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('My brother thinks he can catch a ROOKIDEE...'),
					() => dialog.show('He\'s been searching obsessively for days!'),
					() => dialog.show('I have a FLETCHLING, its much easier to catch.'),
					npc.finishDialog
				],
			},
			{
				...structuredClone(nonTrainerNPCs['Villager5']),
				image: { src: './imgs/npcs/villagers/villager5/right.png' },
				id: 118,
				animate: false,
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('I travel a lot and I always carry some POTIONs with me... They helped me a lot!'),
					() => dialog.show('I don\'t know what all these ingredients are in there but POTIONs can be bought in SHOPs, so I guess they are harmless...'),
					npc.finishDialog
				],
			},
			{
				...structuredClone(nonTrainerNPCs['Villager1']),
				image: { src: './imgs/npcs/villagers/villager1/down.png' },
				id: 119,
				animate: false,
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('I train my MONSTERs every day!'),
					npc.finishDialog
				],
			},
			{
				...structuredClone(nonTrainerNPCs['Villager1']),
				image: { src: './imgs/npcs/villagers/villager1/idle.png' },
				id: 120,
				animate: false,
				isMovingAround: false,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('This building is a SHOP!'),
					() => dialog.show('You can recognize it by the SATOSHI symbol on top of the door...'),
					() => dialog.show('All the SHOPS in AGROLAND should accept SATS as a payment for GOODs and SERVICEs!'),
					() => dialog.show('It\'s not LEGAL but i guess they can\'t stop everyone at the same time to do so.'),
					() => dialog.show('Not everybody is happy about it... Espacially the FIAT-GANG!'),
					npc.finishDialog
				],
			},
			{
				...structuredClone(nonTrainerNPCs['Villager1']),
				image: { src: './imgs/npcs/villagers/villager1/idle.png' },
				id: 121,
				animate: false,
				isMovingAround: false,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('This building is a MONSTER HOSPITAL!'),
					() => dialog.show('You can recognize it by the RED CROSS symbol on top of the door...'),
					() => dialog.show('Where does this symbol actually come from?'),
					npc.finishDialog
				],
			},
			{
				...structuredClone(nonTrainerNPCs['Villager0']),
				id: 122,
				animate: true,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('My friends are surfing with their MONSTERs!'),
					() => dialog.show('I am scared of water so i just stay here and watch them. Looks funny.'),
					npc.finishDialog
				],
			},
			{
				...structuredClone(nonTrainerNPCs['Villager0']),
				id: 123,
				animate: true,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('People trust each other in small towns like this.'),
					() => dialog.show('Almost every door is open. The poeple who close their doors certainly have something to hide...'),
					npc.finishDialog
				],
			},
			/* trainers */
			// rival
			{
				...structuredClone(trainerNPCs['Rival']),
				id: 106,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc) => [
					() => dialog.show(`${npc.name}: Hey! ${player.name}!`),
					() => dialog.show(`I have chosen ${npc.monsters?.[0].name || ''} as my first MONSTER!`),
					() => dialog.show('I can see the FEAR in your eyes!'),
					() => dialog.show('Are you on your way to the MONSTER-LEAGUE as well?'),
					() => dialog.show('Just forget it! You will need to have at least the first MEDAL!'),
					() => dialog.show('Otherwise the guard won\'t even let you through!'),
					() => dialog.show('Let me see how fast I can beat you!')
				],
				looseDialog: 'That is not fair! You have chosen the strongest MONSTER!',
				winDialog: 'HAHA I won faster than I thought! I am the BEST!',
				afterBattleDialog: () => [
					() => {
						dialog.show('There are many strong TRAINERS in the MONSTER-LEAGUE!')
						// release the way that is blocked by NPC 102 by deleting his gift only if player has already gained his MONSTER-DEX
						const blockingNPC = data.getNPCByID({ npcID: 102, mapID: player.currentMapId }) // getMap().getNPCById(102)
						if (blockingNPC && player.monsterDex.available) {
							blockingNPC.releaseWay()
						}
					},
					() => dialog.show('I need to find a way to BEAT them ALL!'),
					() => dialog.show('Stop being lazy and get some practice if you want to have a chance!'),
					() => dialog.show('Next time I see you, I will have no mercy! HAHAHA!')
				],
				monsters: [], // rivals first monster depends on player first monster choose
				isBeaten: false
			},
		],
		houses: {
			5: { // house 5 (player house)
				npcs: [
					{
						...structuredClone(nonTrainerNPCs['Villager6']),
						image: { src: './imgs/npcs/villagers/villager6/up.png' },
						id: 115,
						animate: false,
						isMovingAround: true,
						maxRange: 100 * viewport.getScaleFactor(false),
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show(`${player.name}!`),
							() => dialog.show('Good luck on your way!'),
							npc.finishDialog
						],
					},
				]
			},
			6: { // house 6 (neighbor house)
				npcs: [
					{
						...structuredClone(nonTrainerNPCs['Villager0']),
						image: { src: './imgs/npcs/villagers/villager0/down.png' },
						isMovingAround: true,
						maxRange: 250 * viewport.getScaleFactor(false),
						id: 100,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show(`Hello ${player.name}...`),
							() => dialog.show('I have been keeping something for you for a long time...'),
							() => dialog.show('I developed this device during college and used it myself for a while.'),
							() => dialog.show('Unfortunately I just gave up after not beeing able to get more than the FIRST MEDAL...'),
							() => dialog.show('But you have the potential to fill it with DATA from more far-off worlds!'),
							() => {
								// item from npc to player
								if (!npc.itemGift) { return }
								getMap().audio.pause()
								flags.setIsAnimating(true)
								playAudio('importantItem')
								dialog.show(`${player.name} received MONSTER-DEX!`)
								player.monsterDex.unlockMonsterDex()
								npc.removeItemGift()
								/*
									NPCs outside of house are not available through getMap().getNPCById(n) since we are currently in a house where NPC-ID 102 is not available.
									We can get Map NPCs while beeing in a house by calling the map data directly with: data.get... instead of getMap().get... 
								*/
								const blockingNPC = data.getNPCByID({ npcID: 102, mapID: 20 }) // data.getNPCById(player.currentMapId, 102)
								const rival = data.getNPCByID({ npcID: 106, mapID: 20 }) // data.getRival(player.currentMapId)
								// release the way that is blocked by NPC 102 by deleting his gift only if player has already beaten Rival.
								if (blockingNPC && !rival) {
									blockingNPC.releaseWay()
								}
								const t = setTimeout(() => {
									flags.setIsAnimating(false)
									clearTimeout(t)
								}, 2500)
							},
							() => {
								getMap().playAudio()
								dialog.show('But before you can use it, it needs to be repaired!')
							},
							() => dialog.show('You have to visit my friend in the SHOP of SNAB-TOWN not too far away.'),
							() => dialog.show(`He will be able to help you! I wish you a great time, ${player.name}!`),
							npc.finishDialog
						],
						itemGift: new Ball({ name: 'Ball', count: 1 }),
						secondDialog: (npc: NPC) => [
							() => {
								const log = player.monsterDex.isBroken ?
									'You should go now, find my friend in the STORE and repair your MONSTER-DEX!' :
									`OHH! ${player.name}! The MONSTER-DEX is looking amazing! COOL, you already have CATCHED ${player.monsterDex.getCatchedCount()} MONSTER!`
								dialog.show(log)
							},
							npc.finishDialog
						]
					},
				]
			},
			7: { // house 7 - monster hospital
				npcs: [
					{ // nurse
						...structuredClone(nonTrainerNPCs['Nurse']),
						id: 100,
						animate: false,
						frames,
						firstDialog: () => [],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Neighbor']),
						image: { src: './imgs/npcs/villagers/neighbor/left.png' },
						isMovingAround: true,
						maxRange: 250 * viewport.getScaleFactor(false),
						id: 101,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('My MONSTERs can have a break and regenerate in HOSPITAL like these!'),
							() => dialog.show('The technology nowadays is very impressive! I am able to store and manage ITEMs or MONSTERs via PCs!'),
							() => dialog.show('Try it yourself with the one over there on the table!'),
							npc.finishDialog
						],
					},
					{
						...structuredClone(nonTrainerNPCs['Villager6']),
						image: { src: './imgs/npcs/villagers/villager6/up.png' },
						id: 102,
						animate: false,
						isMovingAround: true,
						maxRange: 50 * viewport.getScaleFactor(false),
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('I am waiting for my MONSTER to regenerate...'),
							() => dialog.show('I could have used POTIONs but I am scared of side effects!'),
							npc.finishDialog
						],
					},
				]
			},
			8: { // house 8 - friendly shop
				npcs: [
					{ // seller
						...structuredClone(nonTrainerNPCs['Seller']),
						image: { src: './imgs/npcs/villagers/seller/right.png' },
						id: 100,
						animate: false,
						frames,
						firstDialog: () => [],
					},
					{ // repair monster-dex
						...structuredClone(nonTrainerNPCs['Villager1']),
						image: { src: './imgs/npcs/villagers/villager1/down.png' },
						isMovingAround: true,
						maxRange: 150 * viewport.getScaleFactor(false),
						id: 101,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show(`Oh hey! You are ${player.name}, right?`),
							() => dialog.show('I heard that your MONSTER-DEX throws some ERROR...'),
							() => dialog.show('Let me fix this quick!'),
							() => {
								// item from npc to player
								if (!npc.itemGift) { return }
								getMap().audio.pause()
								flags.setIsAnimating(true)
								playAudio('importantItem')
								dialog.show(`${player.name} just repaired the MONSTER-DEX!`)
								player.monsterDex.repairMonsterDex()
								npc.removeItemGift()
								// release the way that is blocked by NPC
								const blockingNPC = data.getNPCByID({ npcID: 100, mapID: 22 }) // getMap().getNPCById(102)
								if (blockingNPC) {
									blockingNPC.releaseWay()
								}
								const t = setTimeout(() => {
									flags.setIsAnimating(false)
									clearTimeout(t)
								}, 2500)
							},
							() => {
								getMap().playAudio()
								dialog.show('Allright, it was just a small BUG that needed to be fixed!')
							},
							() => dialog.show('Now it will work again and automatically add new MONSTER entries if you see or catch some.'),
							npc.finishDialog
						],
						itemGift: new Ball({ name: 'Ball', count: 1 }),
						secondDialog: (npc: NPC) => [
							() => dialog.show('I wish you good luck on your journey and much FUN with your MONSTER-DEX!'),
							npc.finishDialog
						]
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager2']),
						id: 102,
						animate: true,
						frames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('Awesome how much stuff you can buy in SHOPs like this!'),
							() => dialog.show('Every SHOP I visited so far has different ITEMs!'),
							npc.finishDialog
						],
					},
					{
						...structuredClone(nonTrainerNPCs['Villager5']),
						image: { src: './imgs/npcs/villagers/villager5/up.png' },
						id: 103,
						animate: false,
						isMovingAround: true,
						maxRange: 50 * viewport.getScaleFactor(false),
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('I\'m glad this place wasn\'t visited by the FIAT-GANG!'),
							() => dialog.show('I heard they try to force some SHOPs not to accept SATs!'),
							npc.finishDialog
						],
					},
				]
			},
			9: { // house 9 - entry to league maps
				npcs: [
					{
						...structuredClone(nonTrainerNPCs['Guard']),
						image: { src: './imgs/npcs/villagers/guard/left.png' },
						id: 100,
						animate: false,
						isMovingAround: false,
						maxRange: 75 * viewport.getScaleFactor(false),
						frames: moveFrames,
						isBlocker: true,
						firstDialog: (npc: NPC) => [
							() => dialog.show('Only experienced TRAINER are allowed to pass here...'),
							() => dialog.show('And you don\'t have the GROUND MEDAL yet!'),
							() => dialog.show('I am sorry, I can\'t let you through!'),
							() => npc.rejectPlayerAfterBlocking({ direction: 's' })
						],
						// gift that will not be given to player and is just to determine if first or second dialog will be fired
						itemGift: new Potion({ name: 'Potion', count: 1 }),
						secondDialog: (npc: NPC) => [
							() => dialog.show('WOW! The GROUND MEDAL is already yours! I see you are very ambitious!'),
							npc.finishDialog
						]
					},
					{
						...structuredClone(nonTrainerNPCs['Seller']),
						image: { src: './imgs/npcs/villagers/seller/right.png' },
						id: 101,
						animate: false,
						isMovingAround: true,
						maxRange: 100 * viewport.getScaleFactor(false),
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('The guard is a very serious person...'),
							() => dialog.show('I guess I will never be able to pass him without the GROUND MEDAL...'),
							npc.finishDialog
						],
					},
				]
			},
			10: {
				npcs: []
			},
			11: {
				npcs: []
			}
		}
	},
	// ID 21 - ANDRO-TOWN
	{
		id: 21,
		npcs: [
			{
				...structuredClone(nonTrainerNPCs['Villager0']),
				id: 100,
				animate: true,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('This GYM is always closed... I wonder who the GYM-LEADER is...'),
					() => dialog.show('Nobody lives in this town anymore...'),
					() => dialog.show('One day I saw some FIAT-GANG members walking around here. I immediately hid.'),
					npc.finishDialog
				],
			},
			{
				...structuredClone(nonTrainerNPCs['Villager4']),
				image: { src: './imgs/npcs/villagers/villager4/right.png' },
				id: 101,
				animate: false,
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('The name of this town should be changed to GHOST-TOWN!'),
					npc.finishDialog
				],
			},
		],
		houses: {}
	},
	// ID 22 - SNAB-FOREST
	{
		id: 22,
		npcs: [
			// forest blocker
			{
				...structuredClone(nonTrainerNPCs['Villager3']),
				image: { src: './imgs/npcs/villagers/villager3/left.png' },
				id: 100,
				animate: false,
				isMovingAround: false,
				maxRange: 100 * viewport.getScaleFactor(false),
				frames: moveFrames,
				isBlocker: true,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Your MONSTER-DEX still doesn\'t work!'),
					() => dialog.show('You will miss a lot of MONSTER ENTRIES!'),
					() => dialog.show('Why don\'t you go back and REPAIR it in the STORE of SNAB-TOWN...?'),
					() => npc.rejectPlayerAfterBlocking({ direction: 's' })
				],
				// gift that will not be given to player and is just to determine if first or second dialog will be fired
				itemGift: new Potion({ name: 'Potion', count: 1 }),
				secondDialog: (npc: NPC) => [
					() => dialog.show('I know some of the TRAINERs in this FOREST!'),
					npc.finishDialog
				]
			},
			// forest ball advertiser
			{
				...structuredClone(nonTrainerNPCs['Villager6']),
				image: { src: './imgs/npcs/villagers/villager6/up.png' },
				id: 101,
				animate: false,
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Make sure to carry enough BALLs with you!'),
					() => dialog.show('Otherwise you will not be able to CATCH the beautiful MONSTERs of this WORLD!'),
					npc.finishDialog
				],
			},
			// environmental activist
			{
				...structuredClone(trainerNPCs['Environmental_activist']),
				id: 102,
				animate: false,
				frames: moveFrames,
				firstDialog: () => [() => dialog.show('HEY YOU! Are you a MINER?! BITCOIN MINER are destroying the planet!')],
				looseDialog: 'Well, I need to get stronger to beat all the MINER...',
				winDialog: 'HAHA I WON! This is how I will shut down all of the MINER!',
				afterBattleDialog: () => [() => dialog.show('Leave me alone you CRIMINAL!')],
				monsters: [
					new Monster({
						...getMonster('Blipbug'),
						uid: 0,
						audio: monsterAudio['Blipbug'],
						level: 6,
						isEnemy: true
					}),
					new Monster({
						...getMonster('Scatterbug'),
						uid: 0,
						audio: monsterAudio['Scatterbug'],
						level: 6,
						isEnemy: true
					}),
				],
				isBeaten: false
			},
			// environmental activist
			{
				...structuredClone(trainerNPCs['Environmental_activist']),
				image: { src: './imgs/npcs/trainers/Environmental_activist/down.png' },
				id: 103,
				animate: false,
				frames: moveFrames,
				firstDialog: () => [
					() => dialog.show('You have MONSTERs?!'),
					() => dialog.show('Let\'s fight!')
				],
				looseDialog: 'Ahhh, my bugs were not strong enough!',
				winDialog: 'Yesss, my bugs were strong enough to beat you!',
				afterBattleDialog: () => [() => dialog.show('I love bugs!')],
				monsters: [
					new Monster({
						...getMonster('Blipbug'),
						uid: 0,
						audio: monsterAudio['Blipbug'],
						level: 6,
						isEnemy: true
					}),
					new Monster({
						...getMonster('Spewpa'),
						uid: 0,
						audio: monsterAudio['Spewpa'],
						level: 7,
						isEnemy: true
					}),
					new Monster({
						...getMonster('Catus'),
						uid: 0,
						audio: monsterAudio['Catus'],
						level: 7,
						isEnemy: true
					}),
				],
				isBeaten: false
			},
			// banker
			{
				...structuredClone(trainerNPCs['Banker']),
				image: { src: './imgs/npcs/trainers/Banker/up.png' },
				id: 104,
				animate: false,
				frames: moveFrames,
				firstDialog: () => [() => dialog.show('I am on my way to SNAB-TOWN but I think I am lost in this FOREST!')],
				looseDialog: 'I just wanted to do some ADVERTISING for the new FINANCE-TOOLS of BANKS...',
				winDialog: 'You are so WEAK!',
				afterBattleDialog: () => [() => dialog.show('I hope someone can guide me out of this FOREST!')],
				monsters: [
					new Monster({
						...getMonster('Scatterbug'),
						uid: 0,
						audio: monsterAudio['Scatterbug'],
						level: 9,
						isEnemy: true
					}),
				],
				isBeaten: false
			},
			// environmental activist
			{
				...structuredClone(trainerNPCs['Environmental_activist']),
				image: { src: './imgs/npcs/trainers/Environmental_activist/left.png' },
				id: 105,
				animate: false,
				frames: moveFrames,
				firstDialog: () => [() => dialog.show('Hey, wait a moment! Why the rush?!')],
				looseDialog: 'Ufff! You are so good...',
				winDialog: 'Yesss, my bugs were strong enough to beat you!',
				afterBattleDialog: () => [() => dialog.show('The forest is beautiful... Sometimes i sleep here in my tent.')],
				monsters: [
					new Monster({
						...getMonster('Blipbug'),
						uid: 0,
						audio: monsterAudio['Blipbug'],
						level: 8,
						isEnemy: true
					}),
					new Monster({
						...getMonster('Dottler'),
						uid: 0,
						audio: monsterAudio['Dottler'],
						level: 7,
						isEnemy: true
					}),
					new Monster({
						...getMonster('Scatterbug'),
						uid: 0,
						audio: monsterAudio['Scatterbug'],
						level: 8,
						isEnemy: true
					}),
				],
				isBeaten: false
			},
			// rival
			{
				...structuredClone(trainerNPCs['Rival']),
				id: 106,
				animate: false,
				frames: moveFrames,
				firstDialog: () => [
					() => dialog.show(`HEY ${player.name}! You again!`),
					() => dialog.show('I just defeated the GROUND GYM LEADER!'),
					() => dialog.show('Do you think you can do the same!?'),
					() => dialog.show('have your MONSTERs gotten stronger?')
				],
				looseDialog: 'You are so lucky!',
				winDialog: 'HAHA! You are not prepaired for the GROUND GYM LEADER!',
				afterBattleDialog: () => [
					() => dialog.show('You are so lucky!'),
					() => dialog.show('Get out of my way, I have important things to do!')
				],
				monsters: [
					// rivals first monster depends on player first monster choose
					new Monster({
						...getMonster('Fletchling'),
						uid: 0,
						audio: monsterAudio['Fletchling'],
						level: 9,
						isEnemy: true
					}),
				],
				isBeaten: false
			},
		],
		houses: {}
	},
	// ID 23 - CANIS-TOWN
	{
		id: 23,
		npcs: [
			{ // random villager
				...structuredClone(nonTrainerNPCs['Villager5']),
				image: { src: './imgs/npcs/villagers/villager5/left.png' },
				isMovingAround: true,
				maxRange: 75 * viewport.getScaleFactor(false),
				id: 100,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('The GYM-LEADER PETER SNIFF knows very much about ROCKS!'),
					() => dialog.show('He also gave me some great FINANCIAL ADVICEs!'),
					npc.finishDialog
				],
			},
			{ // random villager
				...structuredClone(nonTrainerNPCs['Villager2']),
				image: { src: './imgs/npcs/villagers/villager2/left.png' },
				isMovingAround: true,
				maxRange: 100 * viewport.getScaleFactor(false),
				id: 101,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Hey! I am spreading PROTECT+ to keep WILD MONSTERs away...'),
					npc.finishDialog
				],
			},
			{ // random villager
				...structuredClone(nonTrainerNPCs['Villager4']),
				image: { src: './imgs/npcs/villagers/villager4/down.png' },
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				id: 102,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Nothing makes me more happy than to raise my MONSTERs!'),
					npc.finishDialog
				],
			},
			{ // random villager
				...structuredClone(nonTrainerNPCs['Villager6']),
				image: { src: './imgs/npcs/villagers/villager6/down.png' },
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				id: 103,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('CANIS-TOWN is very beautiful and quiet... That\'s why I moved here!'),
					npc.finishDialog
				],
			},
			{ // random villager
				...structuredClone(nonTrainerNPCs['Villager3']),
				image: { src: './imgs/npcs/villagers/villager3/left.png' },
				isMovingAround: true,
				maxRange: 150 * viewport.getScaleFactor(false),
				id: 104,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('I am trying to catch all the MONSTERs and fulfill my MONSTER-DEX... No matter how!'),
					npc.finishDialog
				],
			},
			{ // random villager
				...structuredClone(nonTrainerNPCs['Villager0']),
				image: { src: './imgs/npcs/villagers/villager0/idle.png' },
				id: 105,
				animate: true,
				frames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('I watch every FIGHT in this GYM through the window.'),
					() => dialog.show('PETER SNIFFs MONSTERs can be very BRUTAL!'),
					npc.finishDialog
				],
			},
			{ // random villager
				...structuredClone(nonTrainerNPCs['Villager1']),
				image: { src: './imgs/npcs/villagers/villager1/up.png' },
				isMovingAround: true,
				maxRange: 200 * viewport.getScaleFactor(false),
				id: 106,
				animate: false,
				frames: moveFrames,
				firstDialog: (npc: NPC) => [
					() => dialog.show('Some poeple offer the possibility to exchange MONSTERs or to TRAIN them for you!'),
					npc.finishDialog
				],
			},
			// trainer
			{
				...structuredClone(trainerNPCs['Trader']),
				image: { src: './imgs/npcs/trainers/Trader/up.png' },
				id: 107,
				animate: false,
				frames: moveFrames,
				firstDialog: () => [() => dialog.show('HEY! You are bothering me! My current TRADE position will make me RICH! LET\'S GO!')],
				looseDialog: 'OH NO! I just lost everything in a SHITCOIN FLASH CRASH...',
				winDialog: 'Bye bye, LOOSER... I am a MARKET MAKER! ',
				afterBattleDialog: () => [() => dialog.show('Trust me, normally I can predict the MARKET...')],
				monsters: [
					new Monster({
						...getMonster('Bunnelby'),
						uid: 0,
						audio: monsterAudio['Bunnelby'],
						level: 10,
						isEnemy: true
					}),
				],
				isBeaten: false
			},
		],
		houses: {
			5: { // house 5 -  Gym
				npcs: [
					{ // Gym leader Peter sniff
						...structuredClone(trainerNPCs['Peter_sniff']),
						image: { src: './imgs/npcs/trainers/Peter_sniff/down.png' },
						id: 100,
						animate: false,
						frames,
						firstDialog: () => [
							() => dialog.show('Welcome to the GROUND GYM!'),
							() => dialog.show('I am the GYM LEADER PETER SNIFF...'),
							() => dialog.show('I have dedicated my life to STONES and their history.'),
							() => dialog.show('In particular to SHINY ROCKS also known as GOLD!'),
							() => dialog.show('History shows, there is almost nothing more valuable than SHINY YELLOW ROCKS.'),
							() => dialog.show('Especially not BITCOIN! I advice you to forget that IDEA!'),
							() => dialog.show('That\'s why I train MONSTERS from TYPE GROUND AND ROCK.'),
							() => dialog.show('They represent the hardness and rarity of the SHINY ROCKS that I love so much.'),
							() => dialog.show('Anyway, let me show you what I mean...')
						],
						looseDialog: 'I may have underestimated you...',
						winDialog: 'My advices are rooted in my understanding of MONSTERs and ECONOMICs!',
						afterBattleDialog: (npc: NPC) => [
							() => dialog.show('You are very persistent and well informed...'),
							() => dialog.show('You deserved the GROUND MEDAL even if I don\'t like to admit it!'),
							() => {
								getMap().audio.pause()
								playAudio('getMedal')
								flags.setIsAnimating(true)
								dialog.show(`${player.name} received the GROUND MEDAL!`)
								player.unlockMedalByName('Ground')
								const gymInfoNPC = data.getNPCByID({ npcID: 102, mapID: player.currentMapId, houseID: player.currentHouseId })
								if (!gymInfoNPC) { return }
								gymInfoNPC.removeItemGift()
								const t = setTimeout(() => {
									flags.setIsAnimating(false)
									clearTimeout(t)
								}, 6000)
							},
							() => {
								getMap().playAudio()
								dialog.show(`CONGRATULATIONS, ${player.name}! You are one step closer to your GOAL...`)
							},
							() => dialog.show('The GROUND MEDAL also allows you to access a small SMALL VILLAGE near SNAB-TOWN...'),
							() => dialog.show('It is under construction now and is not available in this DEMO VERSION!'),
							() => dialog.show('More MAPS and FEATURES are being built for a FULL GAME RELEASE...'),
							() => dialog.show(`Bye, ${player.name}!`),
							npc.finishDialog
						],
						secondDialog: (npc: NPC) => [
							() => dialog.show('I wish I had bought BITCOIN at 1 CENT. I have nothing to say anymore...'),
							npc.finishDialog
						],
						monsters: [
							new Monster({
								...getMonster('Ripta'),
								uid: 0,
								audio: monsterAudio['Blipbug'],
								level: 12,
								isEnemy: true
							}),
							new Monster({
								...getMonster('Carkol'),
								uid: 0,
								audio: monsterAudio['Carkol'],
								level: 14,
								isEnemy: true
							}),
						],
						isBeaten: false
					},
					{ // gold miner
						...structuredClone(trainerNPCs['Gold_miner']),
						image: { src: './imgs/npcs/trainers/Gold_miner/right.png' },
						id: 101,
						animate: false,
						frames: moveFrames,
						firstDialog: () => [
							() => dialog.show('You think you can challenge the GROUND GYM LEADER?'),
							() => dialog.show('Did a stone fall on your head?'),
							() => dialog.show('You have to get past me first and I will have no mercy!')
						],
						looseDialog: 'OK! OK! You are better than me!',
						winDialog: 'This was easier than I thought!',
						afterBattleDialog: () => [() => dialog.show('Digging rocks is kind of interesting... I use my MONSTERs to help me')],
						monsters: [
							new Monster({
								...getMonster('Mamus'),
								uid: 0,
								audio: monsterAudio['Mamus'],
								level: 11,
								isEnemy: true
							}),
							new Monster({
								...getMonster('Bergmite'),
								uid: 0,
								audio: monsterAudio['Bergmite'],
								level: 11,
								isEnemy: true
							}),
						],
						isBeaten: false
					},
					{ // gym info
						...structuredClone(nonTrainerNPCs['Gyminfo']),

						id: 102,
						animate: false,
						frames: { ...frames, val: 0 },
						firstDialog: (npc: NPC) => [
							() => dialog.show('Hello TRAINER!'),
							() => dialog.show('The GROUND GYM LEADER loves MONSTER from type ROCK and GROUND!'),
							() => dialog.show('Your chances are higher if you have MONSTERs from type GRASS!'),
							() => dialog.show('Before challenging the LEADER you will have to beat his best student!'),
							() => dialog.show('Good luck!'),
							npc.finishDialog
						],
						// gift that will not be given to player and is just to determine if first or second dialog will be fired
						itemGift: new Potion({ name: 'Potion', count: 1 }),
						secondDialog: (npc: NPC) => [
							() => dialog.show('WOW! You have the GROUND MEDAL!'),
							npc.finishDialog
						]
					},
				]
			},
			6: {
				npcs: []
			},
			7: { // house 7 - monster hospital
				npcs: [
					{ // nurse
						...structuredClone(nonTrainerNPCs['Nurse']),
						id: 100,
						animate: false,
						frames,
						firstDialog: () => [],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager2']),
						id: 101,
						animate: true,
						frames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('My grandchild is very young but she already knows a lot about MONSTERs...'),
							() => dialog.show('However, I feel like we are too often in this HOSPTIAL!'),
							npc.finishDialog
						],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager5']),
						image: { src: './imgs/npcs/villagers/villager5/left.png' },
						isMovingAround: true,
						maxRange: 75 * viewport.getScaleFactor(false),
						id: 102,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('My grandpa always accompanies me!'),
							() => dialog.show('He teaches me a lot about how to raise MONSTERs...'),
							npc.finishDialog
						],
					},
				]
			},
			8: { // house 8 - friendly shop
				npcs: [
					{ // seller
						...structuredClone(nonTrainerNPCs['Seller']),
						image: { src: './imgs/npcs/villagers/seller/right.png' },
						id: 100,
						animate: false,
						frames,
						firstDialog: () => [],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager6']),
						id: 101,
						animate: true,
						frames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('I spend so much money on energy drinks...'),
							() => dialog.show('I ask myself why... but i don\'t know...'),
							npc.finishDialog
						],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager5']),
						image: { src: './imgs/npcs/villagers/villager5/left.png' },
						isMovingAround: true,
						maxRange: 100 * viewport.getScaleFactor(false),
						id: 102,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('Some BALLs are more effective than others...'),
							npc.finishDialog
						],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager2']),
						image: { src: './imgs/npcs/villagers/villager2/left.png' },
						isMovingAround: true,
						maxRange: 75 * viewport.getScaleFactor(false),
						id: 103,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('One day, I found a SHINY ROCK and sold it here for a lot of SATs!'),
							() => dialog.show('I find SATs more valuable than ROCKs...'),
							npc.finishDialog
						],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager4']),
						image: { src: './imgs/npcs/villagers/villager4/left.png' },
						isMovingAround: true,
						maxRange: 75 * viewport.getScaleFactor(false),
						id: 104,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('Can we really trust the GOVERNMENT? Why are we using their CURRENCY in most places...?'),
							() => dialog.show('It\'s SAFE they say... And it seems to be SAFE in the short therm...'),
							npc.finishDialog
						],
					},
				]
			},
			9: {
				npcs: [
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager4']),
						image: { src: './imgs/npcs/villagers/villager4/left.png' },
						isMovingAround: true,
						maxRange: 75 * viewport.getScaleFactor(false),
						id: 100,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('We are working here on a DATABASE for the classification of MONSTERs'),
							npc.finishDialog
						],
					},
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager3']),
						image: { src: './imgs/npcs/villagers/villager3/left.png' },
						isMovingAround: true,
						maxRange: 75 * viewport.getScaleFactor(false),
						id: 101,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('We are working here on a DATABASE for the classification of MONSTERs'),
							npc.finishDialog
						],
					},
				]
			},
			10: {
				npcs: [
					{ // random villager
						...structuredClone(nonTrainerNPCs['Villager6']),
						image: { src: './imgs/npcs/villagers/villager6/left.png' },
						isMovingAround: true,
						maxRange: 75 * viewport.getScaleFactor(false),
						id: 100,
						animate: false,
						frames: moveFrames,
						firstDialog: (npc: NPC) => [
							() => dialog.show('My friends in the neighbor house are working on a DATABASE...'),
							() => dialog.show('I work on the FRONTEND for a nice DATA VISUALIZATION!'),
							npc.finishDialog
						],
					},
				]
			}
		}
	},
	// new map
	{
		id: 24,
		npcs: [],
		houses: {}
	},
	// new map
	{
		id: 25,
		npcs: [],
		houses: {}
	},
	// new map
	{
		id: 26,
		npcs: [],
		houses: {}
	},
	// new map
	{
		id: 27,
		npcs: [],
		houses: {}
	},
	// new map
	{
		id: 28,
		npcs: [],
		houses: {}
	},
	// new map
	{
		id: 29,
		npcs: [],
		houses: {}
	},
]

// trader
// {
// 	...structuredClone(trainerNPCs['Trader']),
// 	id: 103,
// 	animate: false,
// 	frames: moveFrames,
// 	firstDialog: () => [() => dialog.show('HEY YOU! My current TRADE position will make me RICH! LET\'S GO!')],
// 	looseDialog: 'OH NO! I just lost all my BITCOIN in a SHITCOIN FLASH CRASH...',
// 	winDialog: 'Bye bye, LOOSER... I am a MARKET MAKER! ',
// 	afterBattleDialog: () => [() => dialog.show('Trust me, normally I can predict the MARKET...')],
// 	monsters: [
// 		new Monster({
// 			...getMonster('Emby'),
// 			uid: 0,
// 			audio: {
// 				init: monsterAudio['Emby'].init,
// 				faint: monsterAudio['Emby'].faint
// 			},
// 			level: 10,
// 			isEnemy: true
// 		}),
// 	],
// 	isBeaten: false
// },
// banker
// {
// 	...structuredClone(trainerNPCs['Banker']),
// 	id: 104,
// 	animate: false,
// 	frames: moveFrames,
// 	firstDialog: () => [() => dialog.show('NATIONS can not survive without a strong CENTRAL BANK!')],
// 	looseDialog: 'Did you just HACKED my BANK account and LEAKED my personal info?!',
// 	winDialog: 'You are so WEAK because you don\'t TRUST the GOVERNMENT.',
// 	afterBattleDialog: () => [() => dialog.show('I guess I will just use another one of the 12 CREDIT CARD I own...')],
// 	monsters: [
// 		new Monster({
// 			...getMonster('Mamus'),
// 			uid: 0,
// 			audio: {
// 				init: monsterAudio['Mamus'].init,
// 				faint: monsterAudio['Mamus'].faint
// 			},
// 			level: 10,
// 			isEnemy: true
// 		}),
// 	],
// 	isBeaten: false
// },
// chain analyst
// {
// 	...structuredClone(trainerNPCs['Chain_analyst']),
// 	id: 105,
// 	animate: false,
// 	frames: moveFrames,
// 	firstDialog: () => [() => dialog.show('I can track all the TRANSACTIONS on the BITCOIN BLOCKCHAIN. You can\'t hide!')],
// 	looseDialog: 'I hate the BITCOIN LIGHTNING NETWORK and PRIVACY COINS!',
// 	winDialog: 'You can not beat me. If you do, someone else will track you.',
// 	afterBattleDialog: () => [() => dialog.show('I am glad that CUSTODIAL KYC WALLET providers exist.')],
// 	monsters: [
// 		new Monster({
// 			...getMonster('Ripta'),
// 			uid: 0,
// 			audio: {
// 				init: monsterAudio['Ripta'].init,
// 				faint: monsterAudio['Ripta'].faint
// 			},
// 			level: 10,
// 			isEnemy: true
// 		}),

// 	],
// 	isBeaten: false
// },