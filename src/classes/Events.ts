import gsap from 'gsap'
import { battle, player } from '..'
import { playAudio } from '../data/audio'
import { interactions } from '../data/interactions'
import { hiddenItems, visibleItems } from '../data/item/collectables'
import { config } from '../config'
import { changeFocus } from '../util/focus'
import { clearArr, elHasClassName, elHasStyleVal, focusFirstChildAfterTimeout, getElByQuery } from '../util/helper'
import {
	attacksSection,
	creditsPage,
	dexEntryEl,
	dexListEl,
	evoSection,
	initQAWrap,
	inputItemToDrop,
	introWrap,
	inventoryItemList,
	inventoryPlayerBag,
	inventorySection,
	itemListEl,
	monsterListEl,
	monsterStatusEl,
	noBtn,
	optionsPage,
	pauseMenuEl,
	pauseOptionsPage,
	playerOvEl,
	QAWrap,
	useDropWrap,
	yesBtn
} from '../util/html/elements'
import { initDisclaimer, submitPlayerName, submitRival } from '../util/html/intro'
import { cancelInventoryModal, openInventoryMenu } from '../util/html/inventory'
import { initPauseMenu } from '../util/html/pause'
import { Boundary } from './Boundary'
import { canvas } from './Canvas'
import { data } from './Data'
import { dialog } from './Dialog'
import { flags } from './Flags'
import { getMap } from './Movables'
import { reaction } from './ReactionQueue'
import { getSavedCanvasSizes } from '../util/localStorage'

class Events {
	static #instance: Events
	#lastKeyPressed
	#keys: { [key: string]: { pressed: boolean } }
	#doResize: ReturnType<typeof setTimeout> | undefined
	#arrowWrap
	#roundBtnWrap
	#touchUp
	#touchRight
	#touchDown
	#touchLeft
	#touchStart
	#touchA
	#touchB
	#lastFocusEl: HTMLElement | null

	/***** Getter *****/

	/**
	 * @returns a string that represents the last key pressed
	 */
	get lastKeyPressed() { return this.#lastKeyPressed }
	/**
	 * @returns an object containing the following properties:
	 * - w: an object that has a boolean "pressed" property 
	 * - a: an object that has a boolean "pressed" property 
	 * - s: an object that has a boolean "pressed" property 
	 * - d: an object that has a boolean "pressed" property
	 */
	get keys() { return this.#keys }

	/***** Setter *****/

	set lastFocusEl(el: HTMLElement | null) { this.#lastFocusEl = el }
	/**
	 * @param val - A string that represents the last key pressed
	 */
	public setLastKeyPressed(val: string) { this.#lastKeyPressed = val }

	/**
	 * This method initiates the singleton Events object 
	 * @returns the Events object
	 */
	public static newEvents() {
		if (!this.#instance) { return new Events() }
		return this.#instance
	}

	/***********************/
	/***** CONSTRUCTOR *****/
	/***********************/

	/** This class handles and adds the initial game events to the document */
	private constructor() {
		this.#lastKeyPressed = ''
		this.#keys = {
			w: { pressed: false },
			a: { pressed: false },
			s: { pressed: false },
			d: { pressed: false }
		}
		this.#doResize = undefined
		this.#arrowWrap = getElByQuery('#arrowWrap')
		this.#roundBtnWrap = getElByQuery('#roundBtnWrap')
		this.#touchUp = getElByQuery('#ctrl-top')
		this.#touchRight = getElByQuery('#ctrl-right')
		this.#touchDown = getElByQuery('#ctrl-bottom')
		this.#touchLeft = getElByQuery('#ctrl-left')
		this.#touchStart = getElByQuery('#ctrl-start')
		const touchStartHalfWidth = 30
		this.#touchStart.style.left = `${(window.innerWidth / 2) - touchStartHalfWidth}px`
		this.#touchA = getElByQuery('#ctrl-a')
		this.#touchB = getElByQuery('#ctrl-b')
		this.#lastFocusEl = null
		// init resize event
		addEventListener('resize', () => {
			clearTimeout(this.#doResize)
			this.#doResize = setTimeout(this.#handleResize, 250)
		})
	}

	/***********************/
	/******* METHODS *******/
	/***********************/

	/**
	 * This method handles the keydown event for the key "backspace/escape"
	 */
	public handleBackBtn = () => {
		// If inventory item list is open
		if (elHasClassName(inventoryItemList, 'display') || elHasClassName(inventoryPlayerBag, 'display')) {
			playAudio('closeMenu')
			console.log('close inventory/player bag item list on PC')
			if (elHasStyleVal(inputItemToDrop)) {
				cancelInventoryModal(elHasClassName(inventoryPlayerBag, 'display'), dialog.showsStr('DROP'))
				return
			}
			inventoryItemList.classList.remove('display')
			inventoryPlayerBag.classList.remove('display')
			inventorySection.style.display = 'grid'
			focusFirstChildAfterTimeout(inventorySection)
			dialog.show('Choose a MENU')
			return
		}
		// If inventory page is open
		if (elHasStyleVal(inventorySection)) {
			playAudio('closeMenu')
			console.log('close inventory page')
			inventorySection.removeAttribute('style')
			dialog.hide()
			const t = setTimeout(() => {
				flags.setIsInteracting(false)
				clearTimeout(t)
			}, 500)
			this.#lastFocusEl = null
			return
		}
		if (flags.getIsInteracting()) { return }
		// If attack learning window is open
		if (elHasStyleVal(document.getElementById('forgetAttackUI'))) {
			playAudio('closeMenu')
			document.getElementById('forgetAttackUI')?.removeAttribute('style')
			initQAWrap()
			dialog.display()
			return
		}
		// If attack learning question is showing
		if (dialog.isShowing() && dialog.showsStr('forget an attack and learn')) {
			playAudio('closeMenu')
			noBtn.click()
			return
		}
		if (!battle.isInitiated) {
			// go back from options page in intro
			if (elHasStyleVal(optionsPage)) {
				playAudio('closeMenu')
				optionsPage.removeAttribute('style')
				const optionsBtn = document.getElementById('introOptionsBtn')
				optionsBtn?.focus()
				events.lastFocusEl = optionsBtn
				return
			}
			// go back from credits page in intro
			if (elHasStyleVal(creditsPage)) {
				playAudio('closeMenu')
				creditsPage.removeAttribute('style')
				const creditsBtn = document.getElementById('introCreditsBtn')
				creditsBtn?.focus()
				events.lastFocusEl = creditsBtn
				return
			}
			// hide pause menu options
			if (elHasStyleVal(pauseOptionsPage)) {
				playAudio('closeMenu')
				pauseOptionsPage.removeAttribute('style')
				focusFirstChildAfterTimeout(pauseMenuEl)
				return
			}
			// cancel first monster choose
			if (dialog.showsStr('Are you sure that you want')) {
				playAudio('closeMenu')
				console.log('cancel first monster choose')
				// cancel choice
				QAWrap.style.display = 'none'
				dialog.updateInnerHTML('Choose your FIRST MONSTER!')
				monsterListEl.classList.add('display')
				focusFirstChildAfterTimeout(monsterListEl)
				return
			}
			// cancel save progress
			if (elHasStyleVal(QAWrap)) {
				playAudio('closeMenu')
				console.log('cancel save progress')
				// if evolution section is open, early return
				if (elHasStyleVal(evoSection)) {
					return
				}
				const saveWindowInfo = getElByQuery('#saveWindowId')
				saveWindowInfo.removeAttribute('style')
				events.dontSaveGameProgress()
				return
			}
			// If monster status page is open while choosing first monster from prof.
			if (elHasStyleVal(monsterStatusEl)) {
				playAudio('closeMenu')
				console.log('close monster status page')
				monsterStatusEl.removeAttribute('style')
				if (battle.isInitiated) {
					battle.showUI()
				}
				const prof = data.getNPCByID({ npcID: 110, mapID: player.currentMapId })
				// player is choosing his first monster
				if (prof && !prof.isOutOfViewport() && prof.monsters?.length === 3) {
					dialog.show('Choose your FIRST MONSTER!')
				}
				focusFirstChildAfterTimeout(monsterListEl)
				return
			}
			// cancel item usage on monster list 
			if (dialog.el.innerText.includes('Which MONSTER should get it?')) {
				playAudio('closeMenu')
				console.log('cancel item usage outside of battle')
				clearArr(player.selectedItems)
				monsterListEl.classList.remove('display')
				itemListEl.classList.add('display')
				focusFirstChildAfterTimeout(itemListEl)
				dialog.hide()
				dialog.el.innerText = ''
				return
			}
			// If Pause Menu is open
			if (!dialog.isShowing() && flags.getIsPaused()) {
				playAudio('closeMenu')
				console.log('handle pause')
				this.#handlePause()
				return
			}
		}
		if (battle.isInitiated) {
			// cancel evolution
			if (dialog.showsStr('is about to EVOLVE')) {
				playAudio('closeMenu')
				console.log('cancel evolution')
				flags.setIsAnimating(false)
				dialog.show(`${player.name} canceled the evolution!`)
				this.#lastFocusEl = null
				return
			}
			// If swicth monster question is open
			if (dialog.showsStr('switch the MONSTER?') && elHasStyleVal(QAWrap)) {
				playAudio('closeMenu')
				noBtn.click()
				return
			}
			// If monster dex entry is open
			if (elHasStyleVal(dexEntryEl)) {
				playAudio('closeMenu')
				this.#lastFocusEl = null
				if (reaction.queue.length) {
					reaction.execute()
				}
				return
			}
			// If Monster List within Battle is open to choose next monster after loss against enemy trainer
			if (dialog.showsStr('Which MONSTER should fight?')) {
				// UIWrap.style.display = 'grid'
				return
			}
			// If Monster status page is open OR Monster status page is open after player-monster faints before player sends new monster
			if ((dialog.showsStr('What will ') && elHasStyleVal(monsterStatusEl)) || elHasStyleVal(monsterStatusEl)) {
				playAudio('closeMenu')
				// remove the status page
				monsterStatusEl.removeAttribute('style')
				focusFirstChildAfterTimeout(monsterListEl)
				return
			}
			// If Monster List within Battle is open without item selected before
			if (elHasClassName(monsterListEl, 'display') && !dialog.cantSkip() && player.selectedItems.length < 1) {
				playAudio('closeMenu')
				battle.showUI()
				monsterListEl.classList.remove('display')
				focusFirstChildAfterTimeout(attacksSection)
				return
			}
			// If Monster list within battle is open for a item usage
			if (elHasClassName(monsterListEl, 'display') && player.selectedItems.length > 0) {
				playAudio('closeMenu')
				clearArr(player.selectedItems)
				dialog.hide()
				monsterListEl.classList.remove('display')
				itemListEl.classList.add('display')
				// battle.showUI()
				focusFirstChildAfterTimeout(itemListEl)
				// UIWrap.style.display = 'grid'
				return
			}
			// If Item List within battle is open
			if (elHasClassName(itemListEl, 'display')) {
				playAudio('closeMenu')
				itemListEl.classList.remove('display')
				battle.showUI()
				focusFirstChildAfterTimeout(attacksSection)
			}
		}
	}

	/**
	 *  This method handles the keydown event for the key "space/enter"
	 */
	public handleInteraction = () => {
		playAudio('pressab')
		// handle click on focused element
		if (this.isTouchDevice() && this.#lastFocusEl && document.activeElement?.id !== 'appBody') {
			this.#lastFocusEl.click()
			return
		}
		// handle player or rival name submit with pressing keyboard enter
		if (document.activeElement?.id === 'enterPlayerName') {
			submitPlayerName()
			return
		}
		if (document.activeElement?.id === 'enterRivalName') {
			submitRival()
			return
		}
		// If is paused and dialog is open (while or after item usage)
		if (flags.getIsPaused()) {
			if (elHasClassName(itemListEl, 'display') && dialog.isShowing()) {
				dialog.hide()
				focusFirstChildAfterTimeout(itemListEl)
				return
			}
			if (!battle.isInitiated && elHasClassName(monsterListEl, 'display') && dialog.isShowing()) {
				reaction.execute()
			}
			return
		}
		const attackBtnsWrap = getElByQuery('#forgetAttackBtns')
		if (attackBtnsWrap && elHasStyleVal(attackBtnsWrap)) { return }
		// If a dialog is displayed outside of pause menu
		if (dialog.isShowing()) {
			if (battle.isInitiated && flags.getIsEndOfBattle()) {
				return
			}
			dialog.handleEvent()
			return
		}
		if (flags.getIsInteracting() || flags.getIsDetected() || battle.isInitiated || document.getElementById('intro')) { return }
		// check for single collision
		if (getMap().pc && !flags.getIsPaused()) {
			// check for Inventar PC in house
			this.#checkForPC()
		}
		// check for interactions
		this.#checkForInteractions()
		// check for hidden items
		this.#checkForCollectableItems()
		// console.log('can interact with npc')
		// check for NPC interaction
		getMap().canInteractWithNPC()
	}

	/**
	 * @returns a boolean which indicates if the device is a touch device
	 */
	public isTouchDevice() {
		return /(iphone|ipod|ipad|android|iemobile|blackberry|bada)/.test(navigator.userAgent.toLowerCase())
	}

	/**
	 * This method checks if the device is a touch device and displays the touch controls depending on the output of the check
	 */
	public shouldDisplayTouchCtrls() {
		this.#arrowWrap.style.display = this.isTouchDevice() ? 'block' : 'none'
		this.#roundBtnWrap.style.display = this.isTouchDevice() ? 'block' : 'none'
		this.#touchStart.style.display = this.isTouchDevice() ? 'block' : 'none'
	}

	/**
	 * This method hides the touch controls
	 */
	public hideTouchCtrls() {
		this.#arrowWrap.style.display = 'none'
		this.#roundBtnWrap.style.display = 'none'
		this.#touchStart.style.display = 'none'
	}

	/**
	 * This method handles the cancellation of the saving progress,
	 * in case that the player has clicked on the save menu button, but then decides to cancel.
	 */
	public dontSaveGameProgress() {
		pauseMenuEl.style.display = 'grid'
		focusFirstChildAfterTimeout(pauseMenuEl)
		QAWrap.removeAttribute('style')
		dialog.hide()
	}

	/**
	 * This method is the entry point of the app and starts the main animation loop depending on the available player data.
	 * It also initiates the main DOM events and fades out the start screen.
	 */
	public attachStartIntroEvent() {
		const introBtn = getElByQuery('#startIntroBtn')
		introBtn.focus()
		this.lastFocusEl = introBtn
		this.shouldDisplayTouchCtrls()
		if (this.isTouchDevice()) {
			this.initTouchEvents()
		}
		this.#initMainEvents()
		introBtn.onclick = () => {
			// intro
			introBtn.remove()
			if (config.withIntro) {
				playAudio('openingDemoFirst')
				initDisclaimer()
				return
			}
			// DEBUG without intro:
			getMap().animate()
			gsap.to(introWrap, {
				opacity: 0,
				duration: 2.5,
				onComplete: () => {
					introWrap.remove()
					this.lastFocusEl = null
				}
			})
			this.resizeSavedCanvas()
		}
	}

	public resizeSavedCanvas() {
		const savedCanvas = getSavedCanvasSizes()
		// only resize if the saved canvas size !== the current canvas size
		if (savedCanvas && (savedCanvas.height !== canvas.element.height || savedCanvas.width !== canvas.element.width)) {
			this.#handleResize(savedCanvas)
		}
	}

	/**
	 * This method attachs the initial touch events to the corresponding touch buttons
	 */
	public initTouchEvents() {
		document.body.addEventListener('touchmove', e => {
			const loc = e.changedTouches[0]
			const target = document.elementFromPoint(loc.clientX, loc.clientY)
			if (target?.id === 'ctrl-top') {
				this.#cancelAllKeysPressed()
				if (flags.getAreKeysBlocked()) { return }
				this.keys.w.pressed = true
				this.setLastKeyPressed('w')
			}
			if (target?.id === 'ctrl-right') {
				this.#cancelAllKeysPressed()
				if (flags.getAreKeysBlocked()) { return }
				this.keys.d.pressed = true
				this.setLastKeyPressed('d')
			}
			if (target?.id === 'ctrl-bottom') {
				this.#cancelAllKeysPressed()
				if (flags.getAreKeysBlocked()) { return }
				this.keys.s.pressed = true
				this.setLastKeyPressed('s')
			}
			if (target?.id === 'ctrl-left') {
				this.#cancelAllKeysPressed()
				if (flags.getAreKeysBlocked()) { return }
				this.keys.a.pressed = true
				this.setLastKeyPressed('a')
			}
		})
		// up touch start
		this.#touchUp.addEventListener('touchstart', e => {
			e.preventDefault()
			if (flags.getAreKeysBlocked() || this.#lastFocusEl?.id === 'startIntroBtn') { return }
			console.log('this.lastFocus: ', this.#lastFocusEl)
			this.#handlePressAnyBtn()
			// if focus is lost
			if (document.activeElement !== this.#lastFocusEl) {
				this.#lastFocusEl?.focus()
			}
			if (this.#lastFocusEl) {
				changeFocus({ direction: 'w' })
				return
			}
			this.keys.w.pressed = true
			this.setLastKeyPressed('w')
		}, { passive: false })
		// right touch start
		this.#touchRight.addEventListener('touchstart', e => {
			e.preventDefault()
			if (flags.getAreKeysBlocked() || this.#lastFocusEl?.id === 'startIntroBtn') { return }
			console.log('this.lastFocus: ', this.#lastFocusEl)
			this.#handlePressAnyBtn()
			// if focus is lost
			if (document.activeElement !== this.#lastFocusEl) {
				this.#lastFocusEl?.focus()
			}
			if (this.#lastFocusEl) {
				changeFocus({ direction: 'd' })
				return
			}
			this.keys.d.pressed = true
			this.setLastKeyPressed('d')
		}, { passive: false })
		// down touch start
		this.#touchDown.addEventListener('touchstart', e => {
			e.preventDefault()
			if (flags.getAreKeysBlocked() || this.#lastFocusEl?.id === 'startIntroBtn') { return }
			console.log('this.lastFocus: ', this.#lastFocusEl)
			this.#handlePressAnyBtn()
			// if focus is lost
			if (document.activeElement !== this.#lastFocusEl) {
				this.#lastFocusEl?.focus()
			}
			if (this.#lastFocusEl) {
				changeFocus({ direction: 's' })
				return
			}
			this.keys.s.pressed = true
			this.setLastKeyPressed('s')
		}, { passive: false })
		// left touch start
		this.#touchLeft.addEventListener('touchstart', e => {
			e.preventDefault()
			if (flags.getAreKeysBlocked() || this.#lastFocusEl?.id === 'startIntroBtn') { return }
			console.log('this.lastFocus: ', this.#lastFocusEl)
			this.#handlePressAnyBtn()
			// if focus is lost
			if (document.activeElement !== this.#lastFocusEl) {
				this.#lastFocusEl?.focus()
			}
			if (this.#lastFocusEl) {
				changeFocus({ direction: 'a' })
				return
			}
			this.keys.a.pressed = true
			this.setLastKeyPressed('a')
		}, { passive: false })
		// up touch end
		this.#touchUp.addEventListener('touchend', () => this.#cancelAllKeysPressed())
		// right touch end
		this.#touchRight.addEventListener('touchend', () => this.#cancelAllKeysPressed())
		// down touch end
		this.#touchDown.addEventListener('touchend', () => this.#cancelAllKeysPressed())
		// left touch end
		this.#touchLeft.addEventListener('touchend', () => this.#cancelAllKeysPressed())
		// start-button
		this.#touchStart.addEventListener('touchstart', e => {
			e.preventDefault()
			this.#handlePressAnyBtn()
			this.#handlePause()
		})
		// A-button
		this.#touchA.addEventListener('touchstart', e => {
			e.preventDefault()
			this.#handlePressAnyBtn()
			this.handleInteraction()
		})
		// B-button
		this.#touchB.addEventListener('touchstart', e => {
			e.preventDefault()
			this.#handlePressAnyBtn()
			this.handleBackBtn()
			player.run()
		})
		this.#touchB.addEventListener('touchend', e => {
			e.preventDefault()
			player.dontRun()
		})

	}

	/***** Private Methods *****/

	/**
	 * This method adds the events on the initial game start
	 */
	#initMainEvents() {

		addEventListener('keydown', e => {
			// console.log(e.key)
			this.#handlePressAnyBtn()
			if (flags.getAreKeysBlocked()) { return }
			switch (e.key) {
				case 'w':
				case 'W':
				case 'ArrowUp':
					this.#keys.w.pressed = true
					this.#lastKeyPressed = 'w'
					if (!this.#lastFocusEl) { return }
					changeFocus({ direction: 'w' })
					break

				case 'a':
				case 'A':
				case 'ArrowLeft':
					this.#keys.a.pressed = true
					this.#lastKeyPressed = 'a'
					if (!this.#lastFocusEl) { return }
					changeFocus({ direction: 'a' })
					break

				case 's':
				case 'S':
				case 'ArrowDown':
					this.#keys.s.pressed = true
					this.#lastKeyPressed = 's'
					if (!this.#lastFocusEl) { return }
					changeFocus({ direction: 's' })
					break

				case 'd':
				case 'D':
				case 'ArrowRight':
					this.#keys.d.pressed = true
					this.#lastKeyPressed = 'd'
					if (!this.#lastFocusEl) { return }
					changeFocus({ direction: 'd' })
					break

				case 'Shift':
					e.preventDefault()
					player.run()
					break

				case 'p':
				case 'P':
					if (e.repeat) { break }
					this.#handlePause()
					break

				case ' ': // space
					if (e.repeat) { return }
					this.handleInteraction()
					break

				case 'Enter':
					if (e.repeat) { return }
					this.handleInteraction()
					break

				case 'Backspace':
					if (e.repeat) { return }
					this.handleBackBtn()
					break

				case 'Escape':
					if (e.repeat) { return }
					this.handleBackBtn()
					break

				default:
					break
			}
		})

		addEventListener('keyup', e => {
			switch (e.key) {
				case 'w':
				case 'W':
				case 'ArrowUp':
					this.#keys.w.pressed = false
					break

				case 'a':
				case 'A':
				case 'ArrowLeft':
					this.#keys.a.pressed = false
					break

				case 's':
				case 'S':
				case 'ArrowDown':
					this.#keys.s.pressed = false
					break

				case 'd':
				case 'D':
				case 'ArrowRight':
					this.#keys.d.pressed = false
					break

				case 'Shift':
					player.dontRun()
					break

				default:
					break
			}
		})

		addEventListener('click', e => {
			console.log(e.target)
			const target = e.target as HTMLElement
			// avoid focus lost while click on the following elements
			if (
				target.id === 'htmlEl' ||
				target.id === 'canvasEl' ||
				target.id === 'appBody' ||
				target.id === 'dropCount' ||
				target.id === 'monsterList' ||
				target.id === 'introSequence' ||
				target.id === 'intro' ||
				elHasClassName(target.parentElement, 'sequenceWrap') ||
				elHasClassName(target, 'gameLogo')
			) {
				if (this.#lastFocusEl) {
					this.#lastFocusEl?.focus()
				}
			}
		})

		canvas.element.addEventListener('mousedown', function (e) {
			getCursorPosition(e)
		})

	}

	/**
	 * This method handles the keydown event for the key "p"
	 */
	#handlePause = () => {
		if (flags.getIsDetected() || flags.getIsAnimating() || flags.getIsInteracting() || battle.isInitiated) { return }
		if (document.getElementById('intro')) { return }
		if (elHasStyleVal(document.getElementById('saveWindowId'))) { return }
		if (dialog.isShowing() && elHasClassName(itemListEl, 'display')) {
			dialog.hide()
			focusFirstChildAfterTimeout(itemListEl)
			return
		}
		// If monster-dex is open
		if (elHasStyleVal(dexListEl)) {
			player.monsterDex.closeList()
			return
		}
		// If monster dex-entry is open outside battle
		if (!battle.isInitiated && elHasStyleVal(dexEntryEl)) {
			player.monsterDex.closeEntry()
			return
		}
		// If dex-entry is open while in battle (after monster catch)
		if (battle.isInitiated && elHasStyleVal(dexEntryEl)) {
			if (reaction.queue.length) {
				reaction.execute()
			}
			return
		}
		// If Monster List outside of Battle is open
		if (elHasClassName(monsterListEl, 'display') && !dialog.isShowing()) {
			monsterListEl.classList.remove('display')
			pauseMenuEl.style.display = 'grid'
			focusFirstChildAfterTimeout(pauseMenuEl)
			return
		}
		// If Monster List outside of Battle is open for item usage (open dialog)
		if (elHasClassName(monsterListEl, 'display') && dialog.isShowing()) {
			dialog.hide()
			monsterListEl.classList.remove('display')
			itemListEl.classList.add('display')
			focusFirstChildAfterTimeout(itemListEl)
			return
		}
		// If item list outside of battle is open
		if (elHasClassName(itemListEl, 'display')) {
			// item use/drop window closing
			if (elHasStyleVal(useDropWrap)) {
				// drop count modal
				if (elHasStyleVal(inputItemToDrop)) {
					const dropTxt = getElByQuery('#dropTxt')
					dropTxt.innerText = ''
					inputItemToDrop.removeAttribute('style')
					focusFirstChildAfterTimeout(useDropWrap)
					return
				}
				useDropWrap.removeAttribute('style')
				Array.from(itemListEl.children).forEach(button => button.removeAttribute('style'))
				focusFirstChildAfterTimeout(itemListEl)
				return
			}
			itemListEl.classList.remove('display')
			pauseMenuEl.style.display = 'grid'
			focusFirstChildAfterTimeout(pauseMenuEl)
			return
		}
		// If player overview is open
		if (elHasStyleVal(playerOvEl)) {
			playerOvEl.removeAttribute('style')
			pauseMenuEl.style.display = 'grid'
			focusFirstChildAfterTimeout(pauseMenuEl)
			return
		}
		// If pause menu is open
		if (elHasStyleVal(pauseMenuEl)) {
			pauseMenuEl.removeAttribute('style')
			flags.setIsPaused(false)
			this.#lastFocusEl = null
			return
		}
		// else, open menu
		playAudio('openMenu')
		initPauseMenu()
		flags.setIsPaused(true)
		pauseMenuEl.style.display = 'grid'
		focusFirstChildAfterTimeout(pauseMenuEl)
	}

	#cancelAllKeysPressed() {
		for (const key in this.keys) {
			if (Object.prototype.hasOwnProperty.call(this.keys, key)) {
				const k = this.keys[key]
				k.pressed = false
			}
		}
	}

	#checkForCollectableItems() {
		const boundaries = getMap().collectables
		for (let i = 0; i < boundaries.length; i++) {
			const collectable = boundaries[i]
			if (getMap().isBoundaryFarAway(collectable.boundary)) { continue }
			if (getMap().isColliding(this.#getInteractionCollisionObj(collectable.boundary))) {
				const collectables = collectable.isVisible() ? visibleItems : hiddenItems
				const collectableItem = player.currentHouseId > 0
					?
					collectables[`${player.currentMapId}`].house[`${player.currentHouseId}`][`${collectable.id}`]
					:
					collectables[`${player.currentMapId}`].map[`${collectable.id}`]
				if (collectableItem) {
					getMap().audio.pause()
					playAudio('item1')
					dialog.show(`${player.name} found x${collectableItem.count} ${collectableItem.name}`)
					flags.setIsInteracting(true)
					player.sprite.animate = false
					player.sprite.frames.val = 0
					player.addItem(collectableItem)
					getMap().deleteCollectableItem(collectable.id)
					reaction.queue.push(() => {
						flags.setIsInteracting(false)
						player.sprite.animate = true
						dialog.hide()
						getMap().playAudio()
					})
				}
			}
		}
	}

	#checkForInteractions() {
		reaction.clearQueue()
		const boundaries = getMap().interactions
		for (let i = 0; i < boundaries.length; i++) {
			const boundary = boundaries[i]
			if (getMap().isBoundaryFarAway(boundary)) { continue }
			if (getMap().isColliding(this.#getInteractionCollisionObj(boundary))) {
				/* 
					early return if collision has already been detected,
					occurs if player stands between 2 boundaries with the same ID and interacts with them,
					which causes this block to fire twice
				*/
				if (reaction.queue.length > 0) { return }
				const interaction = player.currentHouseId > 0
					?
					interactions[`${player.currentMapId}`].house[`${player.currentHouseId}`][`${boundary.id}`]
					:
					interactions[`${player.currentMapId}`].map[`${boundary.id}`]
				if (interaction && interaction.direction.includes(events.lastKeyPressed)) {
					player.sprite.frames.val = 0
					flags.setIsInteracting(true)
					player.sprite.animate = false
					// handle healing in house-ID 7 (hospital)
					if (player.currentHouseId === 7 && boundary.id === 201) {
						this.#handleHealing(interaction.txt)
						return
					}
					// handle shop seller interaction in house-ID 8 (friendly-shop)
					if (player.currentHouseId === 8 && boundary.id === 201) {
						dialog.show(interaction.txt)
						reaction.queue.push(() => this.#endDialog())
						return
					}
					// handle the rest of possible interactions
					dialog.show(interaction.txt)
					reaction.queue.push(() => this.#endDialog())
				}
			}
		}
	}

	#getInteractionCollisionObj(boundary: Boundary) {
		return {
			rectangle1: player.sprite,
			rectangle2: new Boundary({
				id: boundary.id,
				position: {
					x: boundary.position.x - (getMap().movingByPx * 3),
					y: boundary.position.y - (getMap().movingByPx * 3)
				},
				width: boundary.width + getMap().movingByPx * 6,
				height: boundary.height + getMap().movingByPx * 6
			})
		}
	}

	#checkForPC() {
		const pc = getMap().pc
		if (pc && events.lastKeyPressed === 'w' && getMap().isColliding({
			rectangle1: player.sprite,
			rectangle2: new Boundary({
				id: pc.id,
				position: {
					x: pc.position.x,
					y: pc.position.y + getMap().movingByPx * 2
				},
				width: pc.width,
				height: pc.height
			})
		})) {
			if (!elHasStyleVal(inventorySection) && !dialog.isShowing()) {
				openInventoryMenu()
			}
		}
	}

	#handleHealing(txt: string) {
		// trigger the healing interaction
		reaction.queue.push(
			() => dialog.show(txt),
			() => {
				dialog.show('We offer the best possible HEALING methods!')
				const t = setTimeout(() => {
					initQAWrap()
					clearTimeout(t)
				}, 1000)
				noBtn.onclick = () => {
					events.lastFocusEl = null
					// hide "yes or no" btns
					QAWrap.removeAttribute('style')
					dialog.show('Please visit us again soon!')
					reaction.queue.push(() => this.#endDialog())
				}
				yesBtn.onclick = () => {
					events.lastFocusEl = null
					QAWrap.removeAttribute('style')
					dialog.hide()
					dialog.show('Let us take care of your MONSTERS...')
					reaction.queue.push(
						() => {
							getMap().audio.pause()
							playAudio('recovery')
							flags.setIsAnimating(true)
							dialog.show('Healing MONSTERS...')
							player.monsters.forEach(monster => monster.regenerate())
							const t = setTimeout(() => {
								flags.setIsAnimating(false)
								clearTimeout(t)
							}, 3000)
						},
						() => {
							getMap().playAudio()
							dialog.show('Your MONSTERS are now fully regenerated!')
						},
						() => dialog.show('Please visit us again soon!'),
						() => this.#endDialog()
					)
				}
			},
		)
		reaction.execute()
	}

	#endDialog() {
		player.sprite.animate = true
		flags.setIsInteracting(false)
		dialog.hide()
	}

	/**
	 * This method handles the window resize event
	 */
	#handleResize = (savedCanvasSizes?: { width: number, height: number }) => {
		// no resize if it was trigerred by soft mobile keyboard appearing
		if (this.isTouchDevice() && document.activeElement?.tagName === 'INPUT') {
			return
		}
		// update current map offset and boundaries
		const oldWidth = savedCanvasSizes?.width || canvas.element.width
		const oldHeight = savedCanvasSizes?.height || canvas.element.height
		canvas.element.width = canvas.setSizes().x
		canvas.element.height = canvas.setSizes().y
		canvas.alignGameWrap()
		const oldOffset = getMap().getCorrespondingOffset()
		const newOffset = data.calcNewOffset(oldOffset, oldWidth, oldHeight)
		// set offset of current map
		getMap().setOffset({ x: newOffset.x, y: newOffset.y })
		// set boundaries position of current map
		getMap().placeAllBoundaries(newOffset.x, newOffset.y, oldOffset, true)
		// update all other boundaries
		data.updateAllBoundariesPos(getMap().mapID, getMap().houseID, oldWidth, oldHeight, true)
		// update all other offsets
		data.updateAllOffsets(getMap().mapID, getMap().houseID, oldWidth, oldHeight)
		// update map player pos
		player.resetPos()
		// update battle interface sprites position
		if (battle.isInitiated) {
			battle.setRenderedSpritesPos()
			player.resetAllMonsterPos()
		}
		// update the "start" button of the touch controls
		if (this.isTouchDevice()) { this.#touchStart.style.left = `${(window.innerWidth / 2) - 30}px` }
	}

	#handlePressAnyBtn() {
		if (!this.#lastFocusEl && elHasStyleVal(document.getElementById('introSequence'))) {
			const pressAny = document.getElementById('pressAnyBtn')
			if (pressAny) {
				pressAny.click()
			}
		}
	}

}

/**
 * This function is for debug purposes and indicates the cursor position on the canvas
 * @param e - The mouse click event
 */
function getCursorPosition(e: MouseEvent) {
	const rect = canvas.element.getBoundingClientRect()
	const x = e.clientX - rect.left
	const y = e.clientY - rect.top
	console.log(`x: ${x} - y: ${y}`)
}

export const events = Events.newEvents()