class Flags {

	static #instance: Flags
	#isMapSoundOn
	#isAnimating
	#isPaused
	#isInteracting
	#isPlayerDetected
	#playerSpokeToTrainer
	#canWalkThrough
	#areKeysBlocked
	#isEndOfBattle

	public static newFlags() {
		if (!this.#instance) { return new Flags() }
		return this.#instance
	}

	/**
	 * This class handles basic boolean flags that are updated during the game procedure
	 */
	private constructor() {
		this.#isMapSoundOn = false
		this.#isAnimating = false
		this.#isPaused = false
		this.#isInteracting = false
		this.#isPlayerDetected = false
		this.#playerSpokeToTrainer = false
		this.#canWalkThrough = false
		this.#areKeysBlocked = false
		this.#isEndOfBattle = false
	}

	/***** Getter *****/

	/**
	 * @returns a boolean indicating if the map sound is on or not 
	 */
	public getIsMapSoundOn() {
		return this.#isMapSoundOn
	}

	public getPlayerSpokeToTrainer() {
		return this.#playerSpokeToTrainer
	}

	/**
	 * @returns a boolean indicating if an animation is currently performing or not to avoid skipping 
	 */
	public getIsAnimating() {
		return this.#isAnimating
	}

	/**
	 * @returns a boolean indicating if the pause menu is open or not
	 */
	public getIsPaused() {
		return this.#isPaused
	}

	/**
	 * @returns a boolean indicating if the player is currently in a dialog with any NPC
	 */
	public getIsInteracting() {
		return this.#isInteracting
	}

	public getIsDetected() {
		return this.#isPlayerDetected
	}

	public getCanWalkThrough() {
		return this.#canWalkThrough
	}

	public getAreKeysBlocked() {
		return this.#areKeysBlocked
	}

	public getIsEndOfBattle() {
		return this.#isEndOfBattle
	}

	/***** Setter *****/

	/**
	 * Updates the flag that indicates if the map sound is on or not 
	 * @param val a boolean to update the map sound flag
	 */
	public setIsMapSoundOn(val: boolean) {
		this.#isMapSoundOn = val
	}

	public setPlayerSpokeToTrainer(val: boolean) {
		return this.#playerSpokeToTrainer = val
	}

	/**
	 * Updates the flag that indicates if an animation is currently performing
	 * @param val a boolean to update the animation flag
	 */
	public setIsAnimating(val: boolean) {
		this.#isAnimating = val
	}

	/**
	 * Updates the flag that indicates if the pause menu is open or not
	 * @param val a boolean to update the pause flag
	 */
	public setIsPaused(val: boolean) {
		this.#isPaused = val
	}

	/**
	 * Updates the flag that indicates if the player is currently in a dialog with any NPC
	 * @param val a boolean to update the NPC-interaction flag
	 */
	public setIsInteracting(val: boolean) {
		this.#isInteracting = val
	}

	public setIsDetected(val: boolean) {
		this.#isPlayerDetected = val
	}

	public setCanWalkThrough(val: boolean) {
		this.#canWalkThrough = val
	}

	public setAreKeysBlocked(val: boolean) {
		this.#areKeysBlocked = val
	}

	public setIsEndOfBattle(val: boolean) {
		this.#isEndOfBattle = val
	}
}

export const flags = Flags.newFlags()