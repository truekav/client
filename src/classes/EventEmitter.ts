// import { battle, formula, flags, reaction, player } from '..'
// import { endBattleAnimation } from '../animation/battle'
// import { audio } from '../data/audio'
// import { getMonster, playerMonSpawnPos } from '../data/monsters'
// import { IBall, IBallEvents, IIsCatched } from '../models/classes'
// import { displayDialog } from '../util/helper'
// import { formatMonName } from '../util/helper/fmt'
// import { dialogWindow, enemyMonInfoEl, monsterInfoEl, dexEntryEl } from '../util/htmlElements'
// import { Monster } from './Monster'
// import { Sprite } from './Sprite'

export class EventEmitter {
	// eslint-disable-next-line @typescript-eslint/ban-types
	#callbacks: { [key: string]: Function[] }
	public constructor() { this.#callbacks = {} }

	// eslint-disable-next-line @typescript-eslint/ban-types
	on(event: string | number, cb: Function) {
		if (!this.#callbacks[event]) { this.#callbacks[event] = [] }
		this.#callbacks[event].push(cb)
	}

	emit(event: string | number, ...data: unknown[]) {
		const cbs = this.#callbacks[event]
		if (cbs) {
			// eslint-disable-next-line @typescript-eslint/ban-types
			cbs.forEach((cb: Function) => { cb(data) })
		}
	}
}

// export class Ball extends EventEmitter implements IBall {

// 	#name: string
// 	#ball = new Sprite({
// 		position: { x: 450, y: 450 },
// 		image: { src: './imgs/ball/ball.png' }
// 	})
// 	#smoke = new Sprite({
// 		position: { x: 670, y: 120 },
// 		image: { src: './imgs/ball/ballSmoke1.png' },
// 		animate: true,
// 		frames: { max: 6, hold: 8 },
// 	})

// 	/**
// 	 * @returns the name of the ball object
// 	 */
// 	get name() { return this.#name }

// 	/**
// 	 * This class contains the main methods for catching a monster with a ball
// 	 * @param name - A string that represents the name of the ball object
// 	 */
// 	constructor(name: string) {
// 		super()
// 		this.#name = name
// 	}

// 	/***** Public Methods *****/

// 	public on = <TKey extends keyof IBallEvents>(event: TKey, listener: IBallEvents[TKey]): this =>
// 		this._untypedOn(event, listener)

// 	public emit = <TKey extends keyof IBallEvents>(event: TKey, ...args: Parameters<IBallEvents[TKey]>): boolean =>
// 		this._untypedEmit(event, ...args)

// 	/**
// 	 * This method is a wrapper that calls the ball-throw animation method
// 	 * @param enemy - The monster object that might be catched
// 	 */
// 	public throw(enemy: Monster) {
// 		this.#animateBallThrow(enemy)
// 		this.emit('ballThrow', enemy)
// 	}

// 	/***** Private Methods *****/

// 	/**
// 	 * This method animates the ball throw and the ball shake sequence
// 	 * @param enemy - The monster object that might be catched
// 	 */
// 	#animateBallThrow = (enemy: Monster) => {
// 		// reset previous sprite changes
// 		this.#ball.position = { x: 450, y: 450 }
// 		this.#smoke.opacity = 1
// 		this.#smoke.position = { x: 670, y: 120 }
// 		this.#smoke.frames.val = 0
// 		battle.pushToRenderedSprites(this.#ball)
// 		audio.throw.play()
// 		// catch result
// 		const catched = formula.isCatched(this.#name, enemy)
// 		// misses target entirely
// 		if (catched.ballShakeCount < 0) {
// 			this.#missedTarget(enemy)
// 			return
// 		}
// 		const timeline = gsap.timeline()
// 		// throw over viewport
// 		timeline.to(this.#ball.position, {
// 			x: 575,
// 			y: -50,
// 			duration: .5
// 			// over viewport move to right
// 		}).to(this.#ball.position, {
// 			x: 675,
// 			y: -200,
// 			duration: .5
// 			// move down
// 		}).to(this.#ball.position, {
// 			x: 750,
// 			y: 200,
// 			duration: 1,
// 			yoyo: true,
// 			ease: Power1.easeIn,
// 			// catch animation
// 			onComplete: () => {
// 				audio.ballImpact.play()
// 				audio.initBall.play()
// 				// high brightness
// 				enemy.sprite.brightness = 250
// 				// add smoke
// 				battle.pushToRenderedSprites(this.#smoke)
// 				// enemy opacity
// 				gsap.to(enemy.sprite, {
// 					opacity: 0,
// 					duration: .7,
// 					yoyo: true,
// 					ease: Bounce.easeInOut,
// 					onComplete: () => {
// 						// Ball bounce audio
// 						audio.ballImpact.play()
// 					}
// 				})
// 				// ball bounce on monster
// 				gsap.to(this.#ball.position, {
// 					y: 150,
// 					duration: .3,
// 					yoyo: true,
// 					ease: Power1.easeOut,
// 					onComplete: () => {
// 						// Ball bounce audio
// 						const tb = setTimeout(() => {
// 							audio.ballImpact.play()
// 							clearTimeout(tb)
// 						}, 750)
// 						gsap.to(this.#ball.position, {
// 							y: 250,
// 							yoyo: true,
// 							ease: Bounce.easeOut,
// 							duration: 1,
// 							onComplete: () => {
// 								// Ball actually stands still with monster in it
// 								this.#shakeAnimation(enemy, catched)
// 							}
// 						})
// 					}
// 				})
// 				// smoke opacity
// 				gsap.to(this.#smoke, {
// 					opacity: 0,
// 					duration: 2,
// 				})
// 			}
// 		})
// 	}

// 	/**
// 	 * This method animates the ball shakes and handles the catch sequence
// 	 * @param enemy - The monster object that might be catched
// 	 */
// 	#shakeAnimation = (enemy: Monster, catched: IIsCatched) => {
// 		// misses target entirely
// 		if (catched.ballShakeCount < 0) {
// 			this.#missedTarget(enemy)
// 			return
// 		}
// 		// else, start shake animation
// 		audio.hitBall.play()
// 		// First vibration
// 		gsap.to(this.#ball.position, {
// 			x: 750 + 11,
// 			yoyo: true,
// 			repeat: 5,
// 			duration: .1,
// 			onComplete: () => {
// 				audio.hitBall.stop()
// 				const bt = setTimeout(() => {
// 					// second vibration
// 					if (catched.brokeFree && catched.ballShakeCount > 1) {
// 						// breaks free after more than 1 shake
// 						audio.hitBall.play()
// 						gsap.to(this.#ball.position, {
// 							x: 750 + 8,
// 							yoyo: true,
// 							repeat: 5,
// 							duration: .08,
// 							onComplete: () => {
// 								audio.hitBall.stop()
// 								const bt = setTimeout(() => {
// 									if (catched.brokeFree && catched.ballShakeCount > 2) {
// 										// breaks free after 3 shakes
// 										audio.hitBall.play()
// 										// Last vibration
// 										gsap.to(this.#ball.position, {
// 											x: 750 + 8,
// 											yoyo: true,
// 											repeat: 6,
// 											duration: .15,
// 											onComplete: () => {
// 												audio.hitBall.stop()
// 												const bt = setTimeout(() => {
// 													if (!catched.brokeFree) {
// 														// monster catched
// 														this.#catchSuccess(enemy)
// 														return
// 													}
// 													this.#breakFree(enemy)
// 													clearTimeout(bt)
// 												}, 800)
// 											}
// 										})
// 										return
// 									}
// 									if (catched.brokeFree && catched.ballShakeCount === 2) {
// 										this.#breakFree(enemy)
// 										return
// 									}
// 									clearTimeout(bt)
// 								}, 1100)
// 							}
// 						})
// 						return
// 					}
// 					if (catched.brokeFree && catched.ballShakeCount === 1) {
// 						this.#breakFree(enemy)
// 						return
// 					}
// 					clearTimeout(bt)
// 				}, 900)
// 			}
// 		})
// 	}

// 	/**
// 	 * This method handles the catch sequence
// 	 * @param enemy - The monster object that might be catched
// 	 */
// 	#catchSuccess = (enemy: Monster) => {
// 		flags.setIsAnimating(false)
// 		// catched
// 		enemyMonInfoEl.style.display = 'none'
// 		dialogWindow.removeAttribute('style')
// 		displayDialog(`WUNDERFUL! <bold class="bold">${enemy.name}</bold> has been caught!`)
// 		audio.wild_battle.stop()
// 		audio.catched.play()
// 		// add to monster-dex as catched
// 		player.monsterDex.setIsCatched(enemy.idx)
// 		// TODO add XP for battleMonsters

// 		reaction.queue.push(
// 			() => {
// 				displayDialog(`The <bold class="bold">MONSTER-DEX</bold> shows some information about <bold class="bold">${enemy.name}</bold>!`)
// 			},
// 			() => {
// 				monsterInfoEl.style.display = 'none'
// 				dialogWindow.removeAttribute('style')
// 				player.monsterDex.showEntry(formatMonName(enemy.name))
// 				const t = setTimeout(() => {
// 					dexEntryEl.querySelector('button')?.focus()
// 					clearTimeout(t)
// 				}, 25)
// 			},
// 			() => {
// 				dexEntryEl.removeAttribute('style')
// 				monsterInfoEl.style.display = 'block'
// 				const txt = player.monsters.length < 6 ?
// 					`<bold class="bold">${enemy.name}</bold> is now a part of your TEAM!` :
// 					`<bold class="bold">${enemy.name}</bold> has been pushed into your STORAGE!`
// 				displayDialog(txt)
// 				if (player.monsters.length < 6) {
// 					player.addMonster(new Monster({
// 						...getMonster(formatMonName(enemy.name)),
// 						level: enemy.level,
// 						position: { ...playerMonSpawnPos },
// 						audio: enemy.audio
// 					}))
// 					return
// 				}
// 				console.log('add storage')
// 			},
// 			() => {
// 				audio.catched.stop()
// 				endBattleAnimation()
// 			}
// 		)
// 	}

// 	/**
// 	 * This method animates the monster evading the ball throw
// 	 * @param enemy - The monster object that might be catched
// 	 */
// 	#missedTarget = (enemy: Monster) => {
// 		const timeline = gsap.timeline()
// 		timeline.to(this.#ball.position, {
// 			x: 575,
// 			y: -50,
// 			duration: .5
// 			// over viewport move to right
// 		}).to(this.#ball.position, {
// 			x: 675,
// 			y: -200,
// 			duration: .5,
// 			onComplete: () => {
// 				// move enemy aside
// 				gsap.to(enemy.sprite.position, {
// 					x: enemy.sprite.position.x + 120,
// 					ease: Power0.easeInOut,
// 					duration: .7
// 				})
// 			}
// 			// Move down
// 		}).to(this.#ball.position, {
// 			x: 750,
// 			y: 250,
// 			duration: 1,
// 			yoyo: true,
// 			ease: Power1.easeIn,
// 			onComplete: () => {
// 				audio.ballImpact.play()
// 			}
// 		}).to(this.#ball.position, {
// 			x: 800,
// 			y: -50,
// 			duration: .7,
// 			onComplete: () => {
// 				gsap.to(enemy.sprite.position, {
// 					x: enemy.sprite.position.x - 120,
// 					ease: Power0.easeInOut,
// 					duration: .4,
// 					onComplete: () => {
// 						dialogWindow.removeAttribute('style')
// 						displayDialog(`WOW! <bold class="bold">${enemy.name}</bold> dodged!`)
// 						flags.setIsAnimating(false)
// 						// remove ball
// 						battle.popRenderedSprite()
// 						reaction.queue.push(() => enemy.randomAttack(battle.getMonsterParticipated()[0]))
// 					}
// 				})
// 			}
// 		})
// 	}

// 	/**
// 	 * This method animates the monster breaking free from the ball
// 	 * @param enemy - The monster object that might be catched
// 	 */
// 	#breakFree = (enemy: Monster) => {
// 		// console.log(battleMonsters[0])
// 		// remove ball
// 		battle.getRenderedSprites().splice(battle.getRenderedSprites().length - 2, 1)
// 		audio.initBall.play()
// 		// animation
// 		const timeline = gsap.timeline()
// 		timeline.to(this.#smoke, {
// 			opacity: 1,
// 			duration: .1
// 		}).to(this.#smoke, {
// 			opacity: 0,
// 			duration: 2
// 		})
// 		// enemy opacity
// 		gsap.to(enemy.sprite, {
// 			opacity: 1,
// 			duration: .5,
// 			onComplete: () => {
// 				// remove smoke
// 				const st = setTimeout(() => {
// 					battle.popRenderedSprite()
// 					clearTimeout(st)
// 				}, 1000)
// 				enemy.sprite.brightness = 100
// 				// remove previous dialog
// 				dialogWindow.removeAttribute('style')
// 				displayDialog(`<bold class="bold">${enemy.name}</bold> broke out of the BALL!`)
// 				flags.setIsAnimating(false)
// 				// Enemy attacks after breaking out
// 				reaction.queue.push(() => enemy.randomAttack(battle.getMonsterParticipated()[0]))
// 			}
// 		})
// 	}

// 	// eslint-disable-next-line @typescript-eslint/unbound-method
// 	private _untypedEmit = this.emit
// 	// eslint-disable-next-line @typescript-eslint/unbound-method
// 	private _untypedOn = this.on

// }

// const ball = new Ball('ballName')

// ball.on('ballThrow', (enemy: Monster) => {
// 	animateBallThrow(enemy, ball.name)
// })

// ball.throw(new Monster({...getMonster('Draggle'), level: 5}))
