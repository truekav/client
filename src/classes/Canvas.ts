import { getElByQuery } from '../util/helper'
import { Sprite } from './Sprite'
import { viewport } from './Viewport'

class Canvas {
	static #instance: Canvas
	#element
	#ctx

	/***** Getter *****/

	/**@returns the canvas element */
	get element() { return this.#element }
	/**@returns the canvas 2D context API */
	get ctx() {
		if (!this.#ctx) { return }
		return this.#ctx
	}

	public static newCanvas() {
		if (!this.#instance) { return new Canvas() }
		return this.#instance
	}

	/** This class initializes the HTML canvas and has the canvas element and context API as properties */
	private constructor() {
		/* if (process.env.NODE_ENV === 'test') {
			this.#element = document.createElement('canvas')
		} else { */ this.#element = getElByQuery<HTMLCanvasElement>('canvas') //}

		this.#element.width = this.setSizes().x
		this.#element.height = this.setSizes().y
		// console.log({ canvasWidh: this.#element.width, canvasHeight: this.#element.height })
		this.#ctx = this.#element.getContext('2d', { alpha: false })
		this.alignGameWrap()
		if (!this.#ctx) { return }
		this.#ctx.imageSmoothingEnabled = false
		this.sharpen()
	}

	/**
	 * This method handles the canvas size
	 */
	public setSizes() {
		if (viewport.isSmartphone()) {
			return {
				x: window.innerWidth,
				y: window.innerHeight / 2.4
			}
		}
		if (viewport.isTurnedSmartphone()) {
			return {
				x: window.innerWidth / 1.6,
				y: window.innerHeight
			}
		}
		if (viewport.isTablet()) {
			return {
				x: window.innerWidth,
				y: window.innerHeight / 2
			}
		}
		if (viewport.isSmallDesktop()) {
			return {
				x: window.innerWidth / 1.3,
				y: window.innerHeight / 1.5
			}
		}
		// if bigger than small desktop, set canvas to a fixed x & y size
		return { x: 1024, y: 576 }
	}

	public sharpen() {
		this.#element.style.imageRendering = 'crisp-edges'
	}

	public reserSharpen() {
		this.#element.removeAttribute('style')
	}

	/**
	 * This method aligns the canvas window depending on the screen size
	 */
	public alignGameWrap() {
		const gameWrap = getElByQuery('#gameWrap')
		if (viewport.isSmartphone() || viewport.isTablet()) {
			gameWrap.style.marginLeft = `${(window.innerWidth - this.#element.width) / 2}px`
			gameWrap.style.marginTop = '0'
			return
		}
		if (viewport.isTurnedSmartphone()) {
			gameWrap.style.marginLeft = `${(window.innerWidth - this.#element.width) / 2}px`
			gameWrap.style.marginTop = `${(window.innerHeight - this.#element.height) / 10}px`
			return
		}
		gameWrap.style.marginLeft = `${(window.innerWidth - this.#element.width) / 2}px`
		gameWrap.style.marginTop = `${(window.innerHeight - this.#element.height) / 2}px`
	}

	public getEnemyPosXBeforeFade() {
		return - this.#element.width
	}

	public getEnemyPosXAfterFade(sprite: Sprite, trainer = false) {
		const width = trainer ? sprite.image.width * viewport.getTrainerScaleFactor() : sprite.enemyWidth
		return this.#element.width - (this.#element.width * .24) - (width / 2)
	}

	public getEnemyPosY(sprite?: Sprite, isTrainer = false) {
		if (!sprite) { return 0 }
		const height = isTrainer ? sprite.image.height * viewport.getTrainerScaleFactor() : sprite.enemyHeight
		const distanceToEnemyGrass = this.#element.height * .44
		return distanceToEnemyGrass - height
	}

	public getFriendlyPosX() {
		if (viewport.isTurnedSmartphone()) {
			return this.#element.width / 6
		}
		// smartphone
		return this.#element.width / 10
	}

	public getFriendlyPosY(sprite: Sprite) {
		const dialogHeight = this.#element.height / 4
		return this.#element.height - dialogHeight - sprite.playerMonHeight
	}

	public toJSON() {
		return {
			width: this.#element.width,
			height: this.#element.height
		}
	}
}

export const canvas = Canvas.newCanvas()