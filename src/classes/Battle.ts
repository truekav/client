import gsap from 'gsap'
import { player } from '..'
import { endBattleAnimation, fadeInSide } from '../animation/battle'
import { battleAudio, mapAudio, monsterAudio, playAudio } from '../data/audio'
import { monsterLocation } from '../data/monster/monsterLocation'
import type { IStats } from '../models/classes'
import type { IAttack } from '../models/functions'
import { initAttackForgetUI, initBattleHTML } from '../util/html/battle'
import { clearArr, getElByQuery, getRandomInt, getUniqueID } from '../util/helper'
import { monsterStatsEl, monsterLvlEl, monHPTxtEl, playerMonsterHPBar, monXPBar, monsterInfoEl, dexListEl, yesBtn, noBtn, QAWrap, evoSection, initQAWrap } from '../util/html/elements'
import { canvas } from './Canvas'
import { dialog } from './Dialog'
import { events } from './Events'
import { Img } from './Img'
import { Monster } from './Monster'
import { reaction } from './ReactionQueue'
import { Sprite } from './Sprite'
import { xpTable } from '../data/monster/xpTable'
import { flags } from './Flags'
import { getMonsterByIdx } from '../data/monster/allMonsters'
import { NPC } from './NPC'
import { attackSequences } from './Attack'
import { statusSequence } from './Status'
import { getMap } from './Movables'
import { formula } from './Formula'
import { config } from '../config'

export class Battle {

	static #instance: Battle
	#animationID
	#battleBackgroundSprite
	#battlegroundUI
	#chance
	#isInitiated
	#turn = 0
	#renderedSprites: (Sprite | NPC | Monster)[]
	#monsterParticipated: Monster[]
	#enemyCurrentMon?: Monster

	public static newBattle({ mapID }: { mapID: number }) {
		if (!this.#instance) { return new Battle({ mapID }) }
		return this.#instance
	}

	/***** Getter *****/

	/**
	 * @returns the current battle-animation-ID for canceling the animation
	 */
	get animationID() { return this.#animationID }
	/**
	 * @returns the sprite image of the battle background
	 */
	get background() { return this.#battleBackgroundSprite }
	/**
	 * @returns the battle turn integer which indicates which round the battle currently is.
	 * It gets incremented after each monster has performed its attack
	 */
	get turn() { return this.#turn }
	/**
	 * @returns 0.01 which is the chance to trigger a wild battle and is compared with the output of Math.random()
	 */
	get chance() { return this.#chance }
	/**
	 * @returns a boolean which indicates if a battle has started
	 */
	get isInitiated() { return this.#isInitiated }
	/**
	 * @returns an array of monsters belonging to the player that have participated in the battle
	 */
	get monsterParticipated() { return this.#monsterParticipated }
	/**
	 * @returns an array of sprites that are rendered in the battle animation
	 */
	get renderedSprites() { return this.#renderedSprites }
	/**
	 * @returns the current monster of trainer that is in battle
	 */
	get enemyCurrentMon() { return this.#enemyCurrentMon }


	/***** Setter *****/

	public setEnemyCurrentMon(monster?: Monster) {
		this.#enemyCurrentMon = monster
	}

	public resetRound() {
		this.#turn = 0
	}

	/**
	 * This methods sets the position of the sprites in a battle
	 */
	public setRenderedSpritesPos() {
		// resize in battle
		if (this.isInitiated) {
			this.#renderedSprites.forEach(sprite => {
				if (sprite instanceof Monster) {
					// sprites already faded, so we need the position on grass and not the one outside viewport
					sprite.sprite.setPos(sprite.getMonsterPos())
				}
				// check if we have a player sprite in renderedSprites
				if (sprite instanceof Sprite) {
					sprite.setPos(player.getBattleSpritePos(true))
				}
				// check if opponent trainer is in rendered sprites
				if (sprite instanceof NPC) {
					sprite.battleSprite.setPos(sprite.getBattleTrainerPos(true))
				}
			})
		}
	}

	/**
	 * @param val - a boolean that indicates if a battle has started
	 */
	public setIsBattleInitiated(val: boolean) {
		this.#isInitiated = val
	}

	/**
	 * @param sprites - an array of sprite objects and/or monster-objects where the monster-object is used to trigger a corresponding monster method 
	 */
	public setRenderedSprites(sprites: (Sprite | NPC | Monster)[]) {
		this.#renderedSprites = sprites
	}

	/**
	 * Removes the last element of the rendered sprites array
	 */
	public popRenderedSprite() {
		this.#renderedSprites.pop()
	}

	/**
	 * @param item - pushes a new sprite object or a monster object into the rendered sprites array
	 */
	public pushToRenderedSprites(item: Sprite | NPC | Monster) {
		this.#renderedSprites.push(item)
	}

	/**
	 * This method resets the monster count that participated in a battle to avoid XP split after
	 * the opponent trainer monster has fainted and before he sends his next monster out
	 */
	public resetMonsParticipatedAlive() {
		this.#monsterParticipated = [this.#monsterParticipated[0]]
	}

	/***********************/
	/***** CONSTRUCTOR *****/
	/***********************/

	/**
	 * A battle object that contains some battle-related HTML elements and the main methods for the battle sequence
	 */
	private constructor({ mapID }: { mapID: number }) {
		this.#animationID = -1
		this.#monsterParticipated = []
		this.#battleBackgroundSprite = new Sprite({
			position: { x: 0, y: -(canvas.element.height / 20) },
			image: { src: `./imgs/battle/background${mapID}.png` },
			isBattleBg: true
		})
		this.#battlegroundUI = getElByQuery('#battlegroundUI')
		this.#chance = config.battleChance
		this.#isInitiated = false
		this.#renderedSprites = []
	}

	/***********************/
	/******* METHODS *******/
	/***********************/

	/**
	 * @returns a number that indicated how many monster have participated in the battle and have survived
	 */
	public getMonsterCountParticipatedAlive() {
		return this.#monsterParticipated.filter(monster => monster.stats.IS.hp > 0).length
	}

	/**
	 * @returns an array of monsters that have participated in the battle and have survived
	 */
	public getMonsterParticipatedAlive() {
		return this.#monsterParticipated.filter(monster => monster.stats.IS.hp > 0)
	}

	/**
	 * Starts the battle animation after initializing a random wild enemy monster and its HTML elements
	 */
	public start = ({ wildEnemy, trainer }: { wildEnemy?: Monster, trainer?: NPC }) => {
		canvas.sharpen()
		flags.setIsEndOfBattle(false)
		// set battle background
		this.#setBackgroundImg()
		// clear battle monsters array
		this.clearMonsParticipated()
		// clear reaction Queue
		reaction.clearQueue()
		// in battle, hide touch controls, user is able to touch the interface buttons
		// events.hideTouchCtrls()
		// if wild enemy is undefined, start trainer battle
		if (trainer) {
			// init html elements (wildEnemy prop will not be used if trainer is available)
			initBattleHTML({ wildEnemy, trainer })
			// console.log('trainer in start battle: ', trainer)
			// animation
			this.#animate()
			// // fade first sprites from side
			this.#renderedSprites?.forEach(sprite => {
				fadeInSide(sprite)
			})
			return
		}
		// else: animate wild battle
		initBattleHTML({ wildEnemy }) // init html elements
		// animation
		this.#animate()
		// fade first sprites from side
		this.#renderedSprites?.forEach(sprite => {
			fadeInSide(sprite)
		})
	}

	/**
	 * This methods clears the array with the monster that participated in battle
	 */
	public clearMonsParticipated() {
		clearArr(this.#monsterParticipated)
	}

	/**
	 * Displays the HTML element that contains the user interface including attack and menu buttons 
	 */
	public showUI() {
		this.#battlegroundUI.style.display = 'flex'
	}

	/**
	 * Hides the HTML element that contains the user interface including attack and menu buttons by removing its style attribute 
	 */
	public hideUI() {
		this.#battlegroundUI.removeAttribute('style')
	}

	/**
	 * Generates a message string that is displayed in the dialog after a monster attacks
	 * @param type1 - 0 | 1 | 2 | 0.5 which indicates the effectivness multiplicator for the damage calculation 
	 * @param type2 - 0 | 1 | 2 | 0.5 which indicates the effectivness multiplicator for the damage calculation
	 * @returns string | undefined
	 */
	public generateTypeEffectiveTxt(type1: number, type2: number) {
		if (type1 === 0 || type2 === 0) {
			return 'Nothing happened...'
		}
		if (type1 === .5 || type2 === .5) {
			return 'The attack was not very effective...'
		}
		if (type1 === 2 || type2 === 2) {
			return 'The attack was very effective!'
		}
	}

	/**
	 * Checks the monster initiative to determine which monster attacks first and handles the reaction-queue for that sequence 
	 * @param monster - the monster object from the player that will attack 
	 * @param enemy - the monster object that is the enemy and recipient of the attack
	 * @param attack - the attack from the player monster that will be used 
	 * @param attackButton - HTMLButtonElement for updating the remaining attack executions on the attack-button
	 */
	public playerMonAttackSequence(monster: Monster, enemy: Monster, attack: IAttack, attackButton: HTMLButtonElement, trainer?: NPC) {
		this.#turn++
		// Check for player Monster initiative for first Attack execution
		if (monster.stats.IS.ini > enemy.stats.IS.ini) {
			// trigger the selected attack of player monster
			monster.attack({
				attack,
				recipient: enemy
			})
			// update attack remaining executions
			attackButton.innerHTML = `${attack.name.toUpperCase()}<p class="attackExec">${attack.remExec}/${attack.maxExec}</p>`
			// enemy defeated
			if (enemy.stats.IS.hp <= 0) {
				reaction.queue.push(() => enemy.faint(monster, trainer))
				return
			}
			// the attack has affected the enemy status
			if (attackSequences.statusChangedFirst) {
				attackSequences.setStatusChangedFirst(false)
				reaction.queue.push(
					...statusSequence.generateDamagingStatusQueue(enemy, monster, trainer),
					...statusSequence.generateNonDamagingStatusQueue(enemy)
				)
			}
			// enemy attack response
			reaction.queue.push(() => {
				this.opponentAttackSequence(enemy, attack, monster, attackButton, trainer)
			})
			return
		}
		// Else: enemy has more initiative and attacks first,
		this.opponentAttackSequence(enemy, attack, monster, attackButton, trainer, true)
	}

	/**
	 * This method handles the attack sequence of the opponent monster
	 * @param enemy - The opponent monster object that will perform an attack
	 * @param attack - The attack object used by the opponent
	 * @param monster - The monster object belonging to the player
	 * @param attackButton - The attack button to update the remaining attack executions
	 * @param trainer - The NPC trainer object to handle the queue if the opponents faints
	 * @param playerMonAttacksAfter - A boolean which indicates if the player monster should attack after the opponent attacks (in case of the opponent initiative is higher)
	 */
	public opponentAttackSequence(enemy: Monster, attack: IAttack, monster: Monster, attackButton: HTMLButtonElement, trainer?: NPC, playerMonAttacksAfter = false) {
		// check for attack preventing status
		const attPreventStatus = statusSequence.hasAttackPreventingStatus(enemy)
		// check if enemy is not frozen anymore
		if (statusSequence.isThawed(enemy)) {
			attPreventStatus.isAffected = false
			dialog.show(`${enemy.name} is not FROZEN anymore!`)
			// enemy still has a damaging status
			if (reaction.queue.length > 1) {
				this.opponentStatusDamageSequence(enemy, attack, monster, attPreventStatus, attackButton, trainer, playerMonAttacksAfter)
				return
			}
			// is not frozen anymore and has no other damaging status
			reaction.queue.push(() => {
				enemy.randomAttack(monster, trainer)
				if (playerMonAttacksAfter) {
					monster.attackSequence(attack, enemy, attackButton, trainer)
				}
			})
			return
		}
		// can not attack due to status
		if (attPreventStatus.isAffected) {
			statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: enemy })
			dialog.show(`${enemy.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
			if (playerMonAttacksAfter) {
				monster.attackSequence(attack, enemy, attackButton, trainer)
			}
			return
		}
		// else: opponent can attack
		enemy.randomAttack(monster, trainer)
		if (playerMonAttacksAfter) {
			monster.attackSequence(attack, enemy, attackButton, trainer)
		}
	}

	/**
	 * This method executes the queue which contains a status-damage sequence at this moment and
	 * creates the corresponding following reaction queue for the attack sequence of the monster belonging to the player
	 * @param monster - The monster object that will perform an attack
	 * @param attack - The attack object used by the monster
	 * @param enemy - The opponent monster object that is the target for the attack 
	 * @param attPreventStatus - An object that indicates if the player-monster status allows an attack
	 * @param button - The attack button to update the remaining attack executions
	 * @param trainer - The NPC trainer object to handle the queue if the opponents faints
	 */
	public PlayerMonsterStatusDamageSequence(
		monster: Monster,
		attack: IAttack,
		enemy: Monster,
		attPreventStatus: { statusName: string, isAffected: boolean },
		button: HTMLButtonElement,
		trainer?: NPC
	) {
		reaction.execute()
		reaction.queue.push(
			() => {
				// player monster can not attack due to any attack-preventing status
				if (attPreventStatus.isAffected) {
					statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: monster })
					dialog.show(`${monster.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
					// no attack sequence, enemy just reacts with his attack
					reaction.queue.push(() => enemy.randomAttack(monster, trainer))
					return
				}
				// else: player monster can attack
				this.playerMonAttackSequence(monster, enemy, attack, button, trainer)
			},
			...statusSequence.generateDamagingStatusQueue(monster, enemy, trainer, true)
		)
	}

	/**
	 * This method executes the queue which contains a status-damage sequence at this moment and
	 * creates the corresponding following reaction queue for the attack sequence of the opponent monster
	 * @param enemy - The opponent monster object that will perform an attack
	 * @param attack - The attack object used by the opponent
	 * @param monster - The monster object belonging to the player
	 * @param attPreventStatus - An object that indicates if the opponent monster status allows an attack
	 * @param attackButton - The attack button to update the remaining attack executions
	 * @param trainer - The NPC trainer object to handle the queue if the opponents faints
	 * @param playerMonAttacksAfter - A boolean which indicates if the player monster should attack after the opponent attacks (in case of the opponent initiative is higher)
	 */
	public opponentStatusDamageSequence(
		enemy: Monster,
		attack: IAttack,
		monster: Monster,
		attPreventStatus: { statusName: string, isAffected: boolean },
		attackButton: HTMLButtonElement,
		trainer?: NPC,
		playerMonAttacksAfter = false
	) {
		reaction.execute()
		reaction.queue.push(
			() => {
				// enemy can not attack due to any attack-preventing status
				if (attPreventStatus.isAffected) {
					statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: enemy })
					dialog.show(`${enemy.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
					// player monster attack response
					if (playerMonAttacksAfter) {
						monster.attackSequence(attack, enemy, attackButton, trainer)
					}
					return
				}
				// else: enemy can attack
				enemy.randomAttack(monster, trainer)
				// player monster attack response
				if (playerMonAttacksAfter) {
					monster.attackSequence(attack, enemy, attackButton, trainer)
				}
			},
			// other status damage
			...statusSequence.generateDamagingStatusQueue(enemy, monster, trainer, true)
		)
	}

	/**
	 * @param accuracy - a number (0-1 or between)
	 * @returns a boolean which indicates if the attack has missed or was successful and can also be used to determine if a hit was critical
	 */
	public isHit(accuracy: number) {
		return Math.random() < accuracy
	}

	/**
	 * Wrapper function that stops the battle audio and ends the battle animation
	 */
	public escape() {
		mapAudio.wild_battle.stop()
		playAudio('leaveBattle')
		// fade out battleground
		endBattleAnimation()
	}

	/**
	 * This method generates the relevant reaction-queue for all monster that participated in a battle
	 * after defeating an enemy and is called explicitly in a loop over all battle participants 
	 * @param monster - The monster object in the current loop iteration
	 * @param enemy - The enemy monster object which is the same on each iteration
	 * @param xp - The calculated amout of XP for the monster in the current iteration
	 * @param isFirstIteration - A boolean which indicates the first loop iteration (i === 0) 
	 * @param isLastIteration - A boolean which indicates the last loop iteration (i === loop length - 1)
	 * @returns the reaction needed for each monster that participated in the battle
	 */
	public generateXPQueue(monster: Monster, enemy: Monster, xp: number, trainer?: NPC, isFirstIteration = false, isLastIteration = false) {
		// save current values for calculating difference after level-up
		const currentmaxHP = monster.maxHP
		const currentLvl = monster.level
		const currentStats: IStats = formula.calcStatsIS(monster.stats, currentLvl)
		// add XP to corresponding monster
		monster.increaseXP(xp)
		if (monster.isLevelUp()) {
			// TODO check for multiple level-ups
			monster.increaseLvl(1)
			// calculate new stats
			monster.increaseStats()
		}
		// create queue
		const monQueue: (() => void)[] = []
		// show xp gain
		monQueue.push(
			() => {
				monsterStatsEl.removeAttribute('style')
				dialog.show(`${monster.name} gained ${xp} XP!`)
				// animate xp bar
				if (isLastIteration) {
					gsap.to(monXPBar, {
						value: +monXPBar.value + xp,
						duration: 1
					})
				}
				// Check if is wild monster for victory audio, else Check for Trainer next monster
				if (enemy.isWild && isFirstIteration) {
					mapAudio.wild_battle.stop()
					playAudio('wild_victory')
				}
			}
		)
		// Show level up
		if (currentLvl !== monster.level) {
			monQueue.push(
				() => {
					// Prompt Player
					dialog.show(`${monster.name} has reached LEVEL ${monster.level}!`)
					mapAudio.wild_victory.volume(.03)
					playAudio('lvlup')
					const t = setTimeout(() => {
						mapAudio.wild_victory.volume(.4)
						clearTimeout(t)
					}, 2200)
					// update HTML ONLY of current rendered monster which is always last iteration
					if (isLastIteration) {
						monsterLvlEl.innerText = `Lv. ${monster.level}`
						monHPTxtEl.innerText = `HP: ${monster.stats.IS.hp}/${monster.maxHP}`
						playerMonsterHPBar.max = monster.maxHP
						playerMonsterHPBar.value = monster.stats.IS.hp
						// update xp bar
						monXPBar.setAttribute('min', '0') // ${xpTable[monster.idx][monster.level - 1]}
						monXPBar.setAttribute('max', `${xpTable[monster.cat - 1][monster.level] - xpTable[monster.cat - 1][monster.level - 1]}`)
						monXPBar.setAttribute('value', '0')
						gsap.to(monXPBar, {
							value: monster.xp - xpTable[monster.cat - 1][monster.level - 1]
						})
					}
				},
				// Show stats after level up
				() => {
					monsterStatsEl.replaceChildren()
					// Show stats after level up, show increased difference
					for (let i = 0; i < Object.entries(monster.stats.IS).length; i++) {
						const stat = Object.entries(monster.stats.IS)[i]
						const p = document.createElement('p')
						p.classList.add('statParagraph')
						if (stat[0] === 'hp') {
							p.innerHTML = `<span>MAX HP: </span><span>${monster.maxHP} (+${monster.maxHP - currentmaxHP})</span>`
							monsterStatsEl.appendChild(p)
							continue
						}
						p.innerHTML = `<span>${stat[0].toUpperCase()}:</span><span>${stat[1]} (+${stat[1] - currentStats[stat[0]]})</span>`
						monsterStatsEl.appendChild(p)
					}
					monsterStatsEl.style.display = 'grid'
				}
			)
			// Check for new attack learning
			if (monster.lvlAttacks[monster.level]) {
				// append new attack
				if (monster.attacks.length < 4) {
					monQueue.push(() => this.#learnAttack(monster))
					return monQueue
				}
				// else: user chooses which attack to replace
				monQueue.push(
					() => {
						monsterStatsEl.removeAttribute('style')
						dialog.show(`${monster.name} is about to learn ${monster.lvlAttacks[monster.level].name.toUpperCase()}...`)
					},
					() => dialog.show(`but ${monster.name} already has 4 attacks...`),
					() => {
						dialog.show(`Should ${monster.name} forget an attack and learn ${monster.lvlAttacks[monster.level].name.toUpperCase()}?`)
						this.#handleAttackReplace({ monster, trainer, newAttack: monster.lvlAttacks[monster.level] })
					},
				)
				return monQueue
			}
			return monQueue
		}
		return monQueue
	}

	/**
	 * This method creates an enemy monster object with a random level inbetween the given level range
	 * @param lvlRange - an array of 2 numbers which indicates the monster level-range in which a level gets randomly generated
	 * @returns a monster object
	 */
	public initWildMonster(lvlRange: number[]) {
		let randomMon = this.#getWildRandomMon()
		while (randomMon && randomMon.spawnChance < Math.random()) {
			// console.log('get a wild random monster in do-while loop...')
			randomMon = this.#getWildRandomMon()
			// console.log({ monsterName: randomMon?.name, spawnChance: randomMon?.spawnChance })
		}
		if (!randomMon) {
			console.warn('A random wild monster could not be initiated!')
			return
		}
		const randomLvl = getRandomInt(lvlRange[0], lvlRange[1])
		return new Monster({
			...randomMon,
			uid: getUniqueID(),
			level: randomLvl,
			isEnemy: true,
			isWild: true,
			audio: monsterAudio[randomMon.name],
		})
	}

	/**
	 * This method handles the evolution animation in an interval function, also
	 * checks if the evolved monster has a new attack to learn and handles the dialog queue.
	 * @param monster - The monster object that will evolve
	 * @param evoMon - The evolved monster object 
	 */
	public evolutionSequence(monster: Monster, evoMon: Monster, trainer?: NPC) {
		const currentMonImg = new Img(monster.sprite.imageFaceUp.src)
		currentMonImg.classList.add('currentMonEvoImg')
		const evoMonImg = new Img(evoMon.sprite.imageFaceUp.src)
		evoMonImg.classList.add('evoMonImg')
		evoSection.appendChild(currentMonImg)
		evoMonImg.style.filter = 'contrast(0%)'
		evoMonImg.style.display = 'none'
		evoSection.appendChild(evoMonImg)
		evoSection.style.display = 'block'
		// handle evolution animation and queue
		let isAppended = false
		let iteration = 0
		// Wow, so many time-outs...
		// Much code to create a basic evolution animation
		// and handle the corresponding queue. Please help make it better? :)
		const t = setTimeout(() => {
			const t1 = setTimeout(() => {
				// append a filter to hide the evolving monster-img after few milliseconds
				currentMonImg.style.filter = 'contrast(0%)'
				const t2 = setTimeout(() => {
					// start interval animation after a few milliseconds
					const interval = setInterval(() => {
						// check for evolution cancelation (by pressing escape or backspace which I handle in the Event class)
						if (!flags.getIsAnimating()) {
							// set the flag back to true so user is not able to click away the dialog as long as the monster img has a filter
							flags.setIsAnimating(true)
							currentMonImg.style.display = 'block'
							// remove the filter and let user skip the dialog
							const t3 = setTimeout(() => {
								currentMonImg.removeAttribute('style')
								flags.setIsAnimating(false)
								battleAudio.evolution.stop()
								trainer ? trainer.playVictoryAudio() : playAudio('wild_victory')
								clearTimeout(t3)
							}, 1250)
							// hide the evolved monster image
							evoMonImg.style.display = 'none'
							iteration = 0
							clearInterval(interval)
							return
						}
						// successfully evolved: stop the interval and start handling evolution queue
						if (iteration === 40) {
							// hide old monster image
							currentMonImg.style.display = 'none'
							// show evolved monster image
							evoMonImg.style.display = 'block'
							iteration = 0
							clearInterval(interval)
							// handle successfull evolution
							const t3 = setTimeout(() => {
								battleAudio.evolution.stop()
								playAudio('congratsEvo')
								// remove filter from evolved monster image
								evoMonImg.removeAttribute('style')
								// prompt user
								dialog.show(`CONGRATULATIONS! ${monster.name} just evolved to... ${evoMon.name}!`)
								// add to dex
								player.monsterDex.addNewEntry(evoMon)
								// set is catched
								player.monsterDex.setIsCatched(evoMon.idx)
								// replace player monster with evolved
								const playerOldMOnIdx = player.monsters.findIndex(playerMon => playerMon.name === monster.name)
								player.monsters.splice(playerOldMOnIdx, 1, evoMon)
								const t = setTimeout(() => {
									flags.setIsAnimating(false)
									clearTimeout(t)
								}, 3500)
								// show dex-entry
								reaction.queue.splice(
									0,
									0,
									() => {
										trainer ? trainer.playVictoryAudio() : playAudio('wild_victory')
										dialog.show(`The MONSTER-DEX shows some information about ${evoMon.name}.`)
									},
									() => {
										dialog.hide()
										evoSection.removeAttribute('style')
										monsterInfoEl.style.display = 'none'
										player.monsterDex.showEntry(evoMon)
									}
								)
								// Check for attack learning of new evolved monster
								if (evoMon.lvlAttacks[evoMon.level]) {
									// append new attack
									if (evoMon.attacks.length < 4) {
										reaction.queue.splice(
											reaction.queue.length - 1,
											0,
											() => this.#learnAttack(evoMon)
										)
										return
									}
									// else: user chooses which attack to replace
									reaction.queue.splice(
										reaction.queue.length - 1,
										0,
										() => {
											monsterStatsEl.removeAttribute('style')
											player.monsterDex.closeEntry()
											dexListEl.removeAttribute('style')
											evoSection.style.display = 'block'
											dialog.show(`${evoMon.name} is about to learn ${evoMon.lvlAttacks[evoMon.level].name.toUpperCase()}...`)
										},
										() => dialog.show(`but ${evoMon.name} already has 4 attacks...`),
										() => {
											dialog.show(`Should ${evoMon.name} forget an attack and learn ${evoMon.lvlAttacks[evoMon.level].name.toUpperCase()}?`)
											this.#handleAttackReplace({ monster, evoMon, trainer, newAttack: evoMon.lvlAttacks[evoMon.level] })
										},

									)
								}
								clearTimeout(t3)
							}, 2000)
							return
						}
						// This is basically the evolution animation
						iteration++
						currentMonImg.style.display = 'none'
						evoMonImg.style.display = 'block'
						if (isAppended) {
							currentMonImg.style.display = 'block'
							evoMonImg.style.display = 'none'
							isAppended = false
							return
						}
						isAppended = true
					}, 120)
					clearTimeout(t2)
				}, 1500)
				clearTimeout(t1)
			}, 1500)
			clearTimeout(t)
		}, 1500)
	}

	public forgetAttack = (monster: Monster, attack: IAttack, trainer?: NPC) => {
		// replace monster attack in monster attacks object
		const attackIdx = monster.attacks.findIndex(attackIdx => attack.name === attackIdx.name)
		monster.attacks.splice(attackIdx, 1, { ...monster.lvlAttacks[monster.level] })
		// prompt user
		trainer ? trainer.playVictoryAudio() : mapAudio.wild_victory.pause()
		playAudio('forgetMove')
		dialog.show('1... 2... Voilà!')
	}

	/***** Private Methods *****/

	/**
	 * requestAnimationFrame for the battle sequence. Draws the sprites that are inside the rendered-sprites array.
	 * Updating the rendered-sprites array while beeing in a battle results in additional sprite-draw or sprite-removal.  
	 */
	#animate = () => {
		if (!canvas.ctx) { return }
		this.#animationID = window.requestAnimationFrame(this.#animate)
		// console.log('battle animation id: ', this.#animationID)
		// clear canvas
		canvas.ctx.clearRect(0, 0, canvas.element.width, canvas.element.height)
		// start new Path
		canvas.ctx.beginPath()
		this.#battleBackgroundSprite.draw()
		this.#renderedSprites.forEach(sprite => {
			if (sprite instanceof Monster) {
				sprite.sprite.draw()
			}
			if (sprite instanceof Sprite) {
				sprite.draw()
			}
			if (sprite instanceof NPC) {
				sprite.battleSprite.draw()
			}
		})
	}

	#setBackgroundImg() {
		const isInGym = this.#isGym(player.currentMapId, player.currentHouseId)
		this.#battleBackgroundSprite.image.src = `./imgs/battle/background${isInGym ? 'Gym' : player.currentMapId}.png`
	}

	#isGym(mapID: number, houseID: number) {
		return mapID === 23 && houseID === 5
	}

	#getWildRandomMon() {
		const randomMonsterInt = getRandomInt(0, monsterLocation[getMap().mapID][player.battlezoneLocation].length - 1)
		const randomMonsterIdx = monsterLocation[getMap().mapID][player.battlezoneLocation][randomMonsterInt]
		return getMonsterByIdx(randomMonsterIdx)
	}

	#learnAttack = (monster: Monster) => {
		playAudio('learnMove')
		monsterStatsEl.removeAttribute('style')
		monster.attacks.push(monster.lvlAttacks[monster.level])
		dialog.show(`${monster.name} just learned ${monster.lvlAttacks[monster.level].name.toUpperCase()}!`)
	}

	#handleAttackReplace({ monster, trainer, evoMon, newAttack }: { monster: Monster, evoMon?: Monster, trainer?: NPC, newAttack: IAttack }) {
		const to = setTimeout(() => {
			initQAWrap()
			// events.lastFocusEl = QAWrap.querySelector('button')
			noBtn.onclick = () => {
				events.lastFocusEl = null
				// hide "yes or no" btns
				QAWrap.removeAttribute('style')
				const actualMon = evoMon || monster
				// prompt user
				dialog.show(`${actualMon.name} did not learn ${actualMon.lvlAttacks[actualMon.level].name.toUpperCase()}!`)
			}
			yesBtn.onclick = () => {
				events.lastFocusEl = null
				QAWrap.removeAttribute('style')
				dialog.hide()
				initAttackForgetUI(monster, newAttack, evoMon, trainer)
			}
			clearTimeout(to)
		}, 1500)
	}

}