import gsap from 'gsap'
import { Howl } from 'howler'
import { battle, player } from '..'
import { base64MapImgs } from '../data/base64Imgs'
import { config } from '../config'
import type { INPCArgs } from '../models/classes'
import { getRandomInt, getRandomSpriteDirection, removeArrEntry } from '../util/helper'
import { Boundary } from './Boundary'
import { canvas } from './Canvas'
import { dialog } from './Dialog'
import { events } from './Events'
import { flags } from './Flags'
import { getMap } from './Movables'
import type { Player } from './Player'
import { reaction } from './ReactionQueue'
import { Sprite } from './Sprite'
import { viewport } from './Viewport'

export class NPC {
	// npc general props
	id
	name
	image
	#frames
	#sprites
	#animate
	sprite
	#headImgs
	spriteHead?: Sprite
	position
	boundary
	initialStaticPos
	isBlocker
	spritesLoaded
	firstDialog
	secondDialog
	itemGift
	// npc random movement props
	movingByPx
	maxRange
	npcMoving = true
	movesAround
	initialPos
	randomMoveMaxVal
	timeOnLastMove = Date.now()
	randomTimeUntilNextMove
	// npc determined movement
	halfway = { reached: false, pos: 0 }
	directionToGo: { checked: boolean, direction: 'up' | 'down' | 'left' | 'right' }
	shouldWalkAway = false
	// npc trainer props
	audio
	#battleSpriteSrc
	battleSprite: Sprite
	winDialog
	looseDialog
	afterBattleDialog
	monsters
	items
	effort
	#isBeaten
	isTrainer
	isGym
	// npc rival props
	isRival
	rivalFirstMonIdx

	get isBeaten() { return this.#isBeaten }
	get frames() { return this.#frames }
	get animate() { return this.#animate }
	get sprites() { return this.#sprites }
	get headImgs() { return this.#headImgs }
	get battleSpriteSrc() { return this.#battleSpriteSrc }

	/**
	 * This class contains the main methods for the NPC object
	 */
	public constructor({
		id,
		name,
		image,
		headImgs,
		audio,
		frames,
		sprites,
		animate,
		position,
		initialStaticPos,
		boundary,
		itemGift,
		isTrainer,
		isRival,
		rivalFirstMonIdx,
		isBlocker,
		battleSpriteSrc,
		firstDialog,
		secondDialog,
		winDialog,
		looseDialog,
		afterBattleDialog,
		isGym,
		monsters,
		items,
		effort,
		isBeaten,
		isMovingAround,
		maxRange
	}: INPCArgs) {
		this.id = id
		this.name = name.toUpperCase()
		this.image = image
		this.#frames = frames
		this.#sprites = sprites
		this.#animate = animate
		this.audio = audio ? {
			look: audio.look ? new Howl({ ...audio.look }) : undefined,
			battle: new Howl({ ...audio.battle }),
			victory: new Howl({ ...audio.victory })
		} : undefined
		this.sprite = new Sprite({ position })
		this.#headImgs = headImgs
		this.spriteHead = undefined
		this.position = position
		this.boundary = boundary
		this.initialPos = { x: position.x, y: position.y }
		this.initialStaticPos = initialStaticPos || { ...position }
		this.#battleSpriteSrc = battleSpriteSrc
		this.battleSprite = new Sprite({ position: this.getBattleTrainerPos() })
		this.spritesLoaded = false
		this.itemGift = itemGift
		this.isTrainer = isTrainer
		this.isRival = isRival
		this.rivalFirstMonIdx = rivalFirstMonIdx || -1
		this.isBlocker = isBlocker
		this.directionToGo = { checked: false, direction: 'left' }
		this.isGym = isGym
		this.firstDialog = firstDialog
		this.secondDialog = secondDialog
		this.winDialog = winDialog
		this.looseDialog = looseDialog
		this.afterBattleDialog = afterBattleDialog
		this.monsters = monsters
		this.items = items
		this.effort = effort
		this.#isBeaten = isBeaten
		this.movingByPx = config.npcMovingByPx * viewport.getScaleFactor(false)
		this.movesAround = isMovingAround
		this.maxRange = maxRange
		this.randomMoveMaxVal = 0
		this.randomTimeUntilNextMove = { time: 0, isSet: false }
	}

	/**
	 * This method sets the "isBeaten" property of the NPC object to "true" to avoid multiple fights
	 * @param val - A boolean that indicates if the trainer has been beaten
	 */
	public setIsBeaten() {
		this.#isBeaten = true
	}

	public playLookAudio() {
		if (this.audio?.look?.state() === 'unloaded') {
			this.audio.look.load()
		}
		this.audio?.look?.play()
	}

	public playBattleAudio() {
		if (this.audio?.battle.state() === 'unloaded') {
			this.audio.battle.load()
		}
		this.audio?.battle.play()
	}

	public playVictoryAudio() {
		if (this.audio?.victory?.state() === 'unloaded') {
			this.audio.victory.load()
		}
		this.audio?.victory.play()
	}

	/**
	 * This method removes the item gift from the npc property to avoid double collection 
	 */
	public removeItemGift() {
		delete this.itemGift
	}

	/**
	 * This method changes the src of the sprite image of the npc depending on where the player has started the interaction
	 * @param player - The player object that is needed to determine last player facing direction
	 * @param trainer - The optional trainer npc object since trainer have different image source handling, else it will handle a non-trainer image source change
	 * @returns void - undefined
	 */
	public changeSpriteDirections(player: Player, trainer?: NPC) {
		const xDiff = this.sprite.position.x - player.sprite.position.x
		const yDiff = this.sprite.position.y - player.sprite.position.y
		// cases
		if (this.sprite.position.x > player.sprite.position.x && xDiff > player.sprite.enemyWidth / 2) {
			// player left of NPC
			if (events.lastKeyPressed !== 'd') {
				// player dont look in NPC direction
				player.sprite.image = player.sprite.sprites.right
				player.spriteHead.image = player.spriteHead.sprites.right
			}
			if (trainer) {
				trainer.sprite.image.src = trainer.sprite.sprites.left.src
				if (trainer.spriteHead) {
					trainer.spriteHead.image.src = trainer.spriteHead.sprites.left.src
				}
				return
			}
			// was moving around
			if (this.movesAround) {
				this.sprite.image = this.sprite.sprites['left']
				// moves around in high grass
				if (this.spriteHead) {
					this.spriteHead.image.src = this.spriteHead.sprites['left'].src
				}
				return
			}
			// is idle
			this.sprite.frames.val = 2
			return
		}
		if (this.sprite.position.x < player.sprite.position.x && - xDiff > player.sprite.enemyWidth / 2) {
			// player right of NPC
			if (events.lastKeyPressed !== 'a') {
				player.sprite.image = player.sprite.sprites.left
				player.spriteHead.image = player.spriteHead.sprites.left
			}
			if (trainer) {
				trainer.sprite.image.src = trainer.sprite.sprites.right.src
				if (trainer.spriteHead) {
					trainer.spriteHead.image.src = trainer.spriteHead.sprites.right.src
				}
				return
			}
			// was moving around
			if (this.movesAround) {
				this.sprite.image = this.sprite.sprites['right']
				// moves around in high grass
				if (this.spriteHead) {
					this.spriteHead.image.src = this.spriteHead.sprites['right'].src
				}
				return
			}
			this.sprite.frames.val = 3
			return
		}
		if (this.sprite.position.y < player.sprite.position.y && - yDiff > player.sprite.enemyHeight / 4) {
			// player down of NPC
			if (events.lastKeyPressed !== 'w') {
				player.sprite.image = player.sprite.sprites.up
				player.spriteHead.image = player.spriteHead.sprites.up
			}
			if (trainer) {
				trainer.sprite.image.src = trainer.sprite.sprites.down.src
				if (trainer.spriteHead) {
					trainer.spriteHead.image.src = trainer.spriteHead.sprites.down.src
				}
				return
			}
			// was moving around
			if (this.movesAround) {
				this.sprite.image = this.sprite.sprites['down']
				// moves around in high grass
				if (this.spriteHead) {
					this.spriteHead.image.src = this.spriteHead.sprites['down'].src
				}
				return
			}
			this.sprite.frames.val = 0
			return
		}
		if (this.sprite.position.y > player.sprite.position.y && yDiff > player.sprite.enemyHeight / 4) {
			// player up of NPC
			if (events.lastKeyPressed !== 's') {
				player.sprite.image = player.sprite.sprites.down
				player.spriteHead.image = player.spriteHead.sprites.down
			}
			if (trainer) {
				trainer.sprite.image.src = trainer.sprite.sprites.up.src
				if (trainer.spriteHead) {
					trainer.spriteHead.image.src = trainer.spriteHead.sprites.up.src
				}
				return
			}
			// was moving around
			if (this.movesAround) {
				this.sprite.image = this.sprite.sprites['up']
				// moves around in high grass
				if (this.spriteHead) {
					this.spriteHead.image.src = this.spriteHead.sprites['up'].src
				}
				return
			}
			this.sprite.frames.val = 1
		}
	}

	/**
	 * This method indicates if the player has run into the view of a trainer in the 4 possible
	 * directions "up, right, bottom, down" to handle the trainer-battle activation
	 * @returns a boolean that indicates if the player has walked into the view of a trainer
	 */
	public isDetectingPlayer() {
		const maxDistance = 270 * viewport.getScaleFactor(false)
		// if trainer is facing to the right
		if (this.image.src.includes('right.') || (this.image.src.includes('idle') && this.sprite.frames.val === 3)) {
			// get distance between player and npc trainer
			const distance = player.sprite.position.x - this.sprite.position.x
			if (distance > 0 && distance < maxDistance && this.#isAlignedY()) { return true }
		}
		// if trainer is facing to the left
		if (this.image.src.includes('left.') || (this.image.src.includes('idle') && this.sprite.frames.val === 2)) {
			const distance = this.sprite.position.x - player.sprite.position.x
			if (distance > 0 && distance < maxDistance && this.#isAlignedY()) { return true }
		}
		// if trainer is facing to the bottom
		if (this.image.src.includes('down.') || (this.image.src.includes('idle') && this.sprite.frames.val === 0)) {
			const distance = player.sprite.position.y - this.sprite.position.y
			if (distance > 0 && distance < maxDistance && this.#isAlignedX()) { return true }
		}
		// if trainer is facing to the top
		if (this.image.src.includes('up.') || (this.image.src.includes('idle') && this.sprite.frames.val === 1)) {
			const distance = this.sprite.position.y - player.sprite.position.y
			if (distance > 0 && distance < maxDistance && this.#isAlignedX()) { return true }
		}
		return false
	}

	/**
	 * This method handles the sequence right after the player has been detected by a trainer
	 * @param forRegularTrainer - A boolean that indicates which exclamation bubble sprite should be used.
	 * If set to false: bubble can indicate any 4 direction. If set to true, bubble can only indicate to the bottom (over regular trainer head)  
	 */
	public detectSequence(forRegularTrainer: boolean) {
		// stop player from moving
		flags.setIsDetected(true)
		// initiate the exclamation bubble sprite
		const detectedSprite = this.#getDetectedSprite(forRegularTrainer)
		// pause map sound
		if (!this.isGym) {
			getMap().audio.pause()
		}
		// play trainer detected sound
		this.audio?.look?.play()
		// show exclamation bubble sprite
		getMap().renderedDetection.push(detectedSprite)
		// set id of detected trainer for use in the animation loop
		getMap().detectedTrainer.id = this.id
		// clear the current queue before starting the trainer dialog queue
		reaction.clearQueue()
	}

	/**
	 * This method handles the player getting blocked to avoid running to certain locations and
	 * fires the dialog of the npc that detected the player
	 */
	public blockPlayerFromWalking() {
		player.sprite.animate = false
		player.sprite.frames.val = 0
		flags.setIsInteracting(true)
		flags.setIsDetected(true)
		reaction.queue.push(...this.firstDialog(this))
		reaction.execute()
	}

	/**
	 * This method handles the auto movement of the player while getting rejected and blocks any movement key event
	 * @param param0 - An object with a direction property to determine in which direction the player should get rejected
	 */
	public rejectPlayerAfterBlocking({ direction }: { direction: 'w' | 'a' | 's' | 'd' }) {
		flags.setAreKeysBlocked(true)
		flags.setCanWalkThrough(true)
		flags.setIsInteracting(false)
		flags.setIsDetected(false)
		dialog.hide()
		this.sprite.frames.val = 0
		events.keys[direction].pressed = true
		events.setLastKeyPressed(direction)
		const t = setTimeout(() => {
			flags.setCanWalkThrough(false)
			flags.setAreKeysBlocked(false)
			events.keys[direction].pressed = false
			clearTimeout(t)
		}, 500)
	}

	/**
	 * This method handles the movement of the trainer NPC that has detected the player and
	 * moves the npc straight to the trainer depending on its facing direction
	 * @returns void - undefined
	 */
	public moveStraightToPlayer() {
		// dont move trainer if player talked to him by himself
		if (flags.getPlayerSpokeToTrainer()) { return }
		this.sprite.animate = true
		// if trainer is facing to the right
		if (this.sprite.image.src.includes('right.')) {
			this.#walk('right')
		}
		// if trainer is facing to the left
		if (this.sprite.image.src.includes('left.')) {
			this.#walk('left')
		}
		// if trainer is facing to the bottom
		if (this.sprite.image.src.includes('down.')) {
			this.#walk('down')
		}
		// if trainer is facing to the top
		if (this.sprite.image.src.includes('up.')) {
			this.#walk('up')
		}
	}

	/**
	 * This method moves the NPC in a given radius in random directions
	 * @returns void - undefined
	 */
	public moveAround() {
		if (!this.maxRange) { return }
		if (flags.getIsInteracting()) {
			this.sprite.animate = false
			return
		}
		// determine max moving radius
		const maxPosRight = this.initialPos.x + this.maxRange
		const maxPosLeft = this.initialPos.x - this.maxRange
		const maxPosDown = this.initialPos.y + this.maxRange
		const maxPosUp = this.initialPos.y - this.maxRange
		// set random int if not already done to determine how far npc can go. This way, it will not always go to the end of radius.
		if (this.randomMoveMaxVal === 0) {
			this.randomMoveMaxVal = getRandomInt(1, this.maxRange)
		}
		// if trainer is facing to the right
		if (this.sprite.image.src.includes('right.')) {
			// console.log('this.sprite.position.x >= maxPosRight: ', this.sprite.position.x >= maxPosRight)
			// console.log('this.sprite.position.x >= this.initialPos.x + this.randomMoveMaxVal: ', this.sprite.position.x >= this.initialPos.x + this.randomMoveMaxVal)
			// console.log('this.#isNPCColliding({ direction: \'right\' }): ', this.#isNPCColliding({ direction: 'right' }))
			// check for end of npc movement
			if (
				// npc position has reached the max radius OR
				this.sprite.position.x >= maxPosRight ||
				// npc has walked the random amout of px OR
				this.sprite.position.x >= this.initialPos.x + this.randomMoveMaxVal ||
				// npc is colliding with player or with boundary
				this.#isNPCColliding({ direction: 'right' })
			) {
				// console.log('npc is colliding!: ', this.name)
				// stop npc from walking and change direction
				this.npcMoving = false
				this.#changeMovingDirection(this.maxRange)
				return
			}
			// else: move npc
			if (this.npcMoving) {
				// console.log('npc moves to the right: ', this.name)
				this.sprite.animate = true
				this.#walk('right')
			}
		}
		// if trainer is facing to the left
		if (this.sprite.image.src.includes('left.')) {
			// second check for end of npc movement
			if (
				// npc position has reached the max radius OR
				this.sprite.position.x <= maxPosLeft ||
				// npc has walked the random amout of px
				this.sprite.position.x <= this.initialPos.x - this.randomMoveMaxVal ||
				// npc is colliding with player or with boundary
				this.#isNPCColliding({ direction: 'left' })
			) {
				// console.log('npc is colliding!: ', this.name)
				// stop npc from walking and change direction
				this.npcMoving = false
				this.#changeMovingDirection(this.maxRange)
				return
			}
			// else: move npc
			if (this.npcMoving) {
				this.sprite.animate = true
				this.#walk('left')
			}
		}
		// if trainer is facing to the bottom
		if (this.sprite.image.src.includes('down.')) {
			// check for end of npc movement
			if (
				// npc position has reached the max radius OR
				this.sprite.position.y >= maxPosDown ||
				// npc has walked the random amout of px
				this.sprite.position.y >= this.initialPos.y + this.randomMoveMaxVal ||
				// npc is colliding with player or with boundary
				this.#isNPCColliding({ direction: 'down' })
			) {
				// console.log('npc is colliding!: ', this.name)
				// stop npc from walking and change direction
				this.npcMoving = false
				this.#changeMovingDirection(this.maxRange)
				return
			}
			// else: move npc
			if (this.npcMoving) {
				// console.log('npc moves down: ', this.name)
				this.sprite.animate = true
				this.#walk('down')
			}
		}
		// if trainer is facing to the top
		if (this.sprite.image.src.includes('up.')) {
			// check for end of npc movement
			if (
				// npc position has reached the max radius OR
				this.sprite.position.y <= maxPosUp ||
				// npc has walked the random amout of px OR
				this.sprite.position.y <= this.initialPos.y - this.randomMoveMaxVal ||
				// npc is colliding with player or with boundary
				this.#isNPCColliding({ direction: 'up' })
			) {
				// console.log('npc is colliding!: ', this.name)
				// stop npc from walking
				this.npcMoving = false
				this.#changeMovingDirection(this.maxRange)
				return
			}
			// else: move npc
			if (this.npcMoving) {
				// console.log('npc moves up: ', this.name)
				this.sprite.animate = true
				this.#walk('up')
			}
		}
	}

	/**
	 * This method moves the NPC to the player that might not be aligned with the NPC (Rival detection for example)
	 * @param param0 - An object with a map ID property that indicates the moving logic that should be triggered
	 * @returns void - undefined
	 */
	public moveToPlayer({ mapID }: { mapID: number }) {
		this.sprite.animate = true
		if (mapID === 20 || mapID === 22) {
			if (this.sprite.image.src.includes('down')) {
				this.#walk('down')
				if (this.#isRivalHalfway({ mapID }) && !this.halfway.reached) {
					this.halfway.reached = true
					this.#changeDirectionX()
				}
				return
			}
			if (this.sprite.image.src.includes('right')) {
				this.#walk('right')
				if (this.#isAlignedX()) {
					this.#changeDirectionY()
					return
				}
			}
			if (this.sprite.image.src.includes('left')) {
				this.#walk('left')
				if (this.#isAlignedX()) {
					this.#changeDirectionY()
				}
			}
		}
	}

	/**
	 * This method handles the NPC movement after it has finished its interaction with the player
	 * @param param0 - An object with a map ID property that indicates the moving logic that should be triggered
	 * @returns void - undefined
	 */
	public moveAway({ mapID }: { mapID: number }) {
		this.sprite.animate = true
		flags.setIsInteracting(true)
		// movement on first map
		if (mapID === 20 || mapID === 22) {
			// move rival
			if (this.id === 106) {
				// check which direction the rival should walk
				if (!this.directionToGo.checked) { this.#checkWhichDirectionToGo() }
				const direction = this.directionToGo.direction
				// check if rival has walked right or left beyond player
				if (
					// has walked left beyond player
					(direction === 'left' && this.sprite.position.x < player.sprite.position.x - player.sprite.enemyWidth)
					||
					// has walked right beyond player
					(direction === 'right' && this.sprite.position.x > player.sprite.position.x + player.sprite.enemyWidth)
				) {
					// walk down until out of viewport
					if (!this.sprite.image.src.includes('down')) {
						this.sprite.image.src = this.sprite.sprites['down'].src
					}
					this.#walk('down')
					// is out of viewport
					if (this.sprite.position.y > canvas.element.height) {
						// reset rival related flags
						this.#handleNPCAfterMovingAway()
						return
					}
					return
				}
				this.sprite.image.src = this.sprite.sprites[direction].src
				// move rival to the right
				if (direction === 'right') {
					this.#walk('right')
					return
				}
				// else: move rival to the left
				this.#walk('left')
				return
			}
			// move prof
			if (this.id === 110) {
				if (!this.sprite.image.src.includes('up')) {
					this.sprite.image.src = this.sprite.sprites.up.src
				}
				this.#walk('up')
				// is out of viewport
				if (this.sprite.position.y + this.sprite.enemyHeight < 0) {
					// reset related flags
					this.#handleNPCAfterMovingAway()
				}
			}
		}
	}

	/**
	 * This method determines if the NPC is out of the canvas viewport to avoid its random radius movement while not beeing seen by player
	 * @returns a boolean that indicates if the NPC sprite is out of the canvas viewport 
	 */
	public isOutOfViewport() {
		return this.sprite.position.y + this.sprite.enemyHeight < 0 ||
			this.sprite.position.y > canvas.element.height ||
			this.sprite.position.x > canvas.element.width ||
			this.sprite.position.x + this.sprite.enemyWidth < 0
	}

	/**
	 * This method indicates at what coordinates on the canvas the NPC should match to start its detection sequence (for rival detection for example)
	 * @param param0 - An object with a map ID property that indicates which coordinates should be used depending on the player current map 
	 * @returns a boolean that indicates the NPC should start its detection sequence
	 */
	public isAppearing({ mapID }: { mapID: number }) {
		// snab town
		if (mapID === 20) {
			// rival
			if (this.id === 106) {
				const minDistanceXBetween = -26
				return this.#isNPCAppearingTop() && this.sprite.position.x - player.sprite.position.x > (minDistanceXBetween * viewport.getScaleFactor(false))
			}
			// prof
			if (this.id === 110) {
				return this.#isNPCAppearingTop()
			}
		}
		// snab-forest
		if (mapID === 22) {
			// rival
			if (this.id === 106) {
				const minDistanceXBetween = -100
				return this.#isNPCAppearingTop() && this.sprite.position.x - player.sprite.position.x > (minDistanceXBetween * viewport.getScaleFactor(false))
			}
		}
		return false
	}

	/**
	 * This method returns the value for the fame object of the cropped image by image source
	 * @param src - The source of the sprite image
	 * @returns a number that indicates the frame value of the cropped image
	 */
	public getFrameValByImgSrc(src: string) {
		if (src.includes('right')) { return 3 }
		if (src.includes('left')) { return 2 }
		if (src.includes('down')) { return 0 }
		return 1
	}

	/**
	 * This method sets the coordinates where the NPC has to stop its movement once reached the player position after a detection
	 * @returns a position object with "x" and "y" coordinates
	 */
	public getPlayerCollisionPos() {
		// if trainer is facing to the right
		if (this.image.src.includes('right.')) {
			return {
				x: player.sprite.position.x - (5 * viewport.getScaleFactor(false)),
				y: player.sprite.position.y
			}
		}
		// if trainer is facing to the left
		if (this.image.src.includes('left.')) {
			return {
				x: player.sprite.position.x + (5 * viewport.getScaleFactor(false)),
				y: player.sprite.position.y
			}
		}
		// if trainer is facing to the bottom
		if (this.image.src.includes('down.')) {
			return {
				x: player.sprite.position.x,
				y: player.sprite.position.y + (30 * viewport.getScaleFactor(false))
			}
		}
		// if trainer is facing to the top
		if (this.image.src.includes('up.')) {
			return {
				x: player.sprite.position.x,
				y: player.sprite.position.y + (48 * viewport.getScaleFactor(false))
			}
		}
		return { x: 0, y: 0 }
	}

	/**
	 * This method handles the position of the battle-sprite before it fades into viewport or after the fade is done.
	 * @param isFaded - A boolean that indicates if the position object is used before or after the sprite fade animation
	 * @returns a position object
	 */
	public getBattleTrainerPos(isFaded = false) {
		return {
			x: isFaded ? canvas.getEnemyPosXAfterFade(this.battleSprite) : canvas.getEnemyPosXBeforeFade(),
			y: canvas.getEnemyPosY(this.battleSprite, true)
		}
	}

	/**
	 * This method fades the battle-sprite into the viewport
	 * @param afterWin - A boolean that indicates if the fade happened after player has beaten all the monster of trainer.
	 * If true, the loose-dialog of NPC will be shown
	 */
	public fadeBattleSpriteIntoViewport(afterWin?: boolean) {
		this.battleSprite.position = { ...this.getBattleTrainerPos(), x: canvas.element.width + 200 }
		battle.pushToRenderedSprites(this)
		gsap.to(this.battleSprite.position, {
			x: canvas.getEnemyPosXAfterFade(this.battleSprite),
			duration: 1,
			onComplete: () => {
				if (afterWin) {
					dialog.show(`${this.name}: ${this.winDialog || ''}`)
				}
				flags.setIsAnimating(false)
			}
		})
	}

	/**
	 * This method places the npc sprite (in map) to its initial value (before detection) after the npc has beaten the player 
	 */
	public placeTrainerBackToInitialPos() {
		this.sprite.position = {
			x: this.initialStaticPos.x,
			y: this.initialStaticPos.y // - (16 * viewport.getScaleFactor(false))
		}
		if (this.spriteHead) {
			this.spriteHead.position = {
				x: this.initialStaticPos.x,
				y: this.initialStaticPos.y
			}
		}
		this.boundary.position = {
			x: this.initialStaticPos.x,
			y: this.initialStaticPos.y + (16 * viewport.getScaleFactor(false))
		}
	}

	/**
	 * This method sets the movement flags after the last NPC dialog so NPC and player can move again 
	 */
	public finishDialog = () => {
		// let player move again
		flags.setIsInteracting(false)
		dialog.hide()
		// let idle npc move again
		this.sprite.animate = true
	}

	/**
	 * This method changes the NPC properties that blocked a player from walking to a location so the player can walk in that direction
	 */
	public releaseWay() {
		this.removeItemGift()
		this.isBlocker = false
		this.sprite.image.src = this.sprite.sprites.down.src
		if (this.spriteHead) {
			this.spriteHead.image.src = this.spriteHead.sprites.down.src
		}
		this.movesAround = true
	}

	/**
	 * @returns an object with the properties needed for the save process
	 */
	public toJSON() {
		return {
			name: this.name,
			id: this.id,
			image: this.image,
			animate: this.animate,
			frames: this.frames,
			boundary: this.boundary,
			headImgs: this.#headImgs,
			position: this.sprite.position,
			initialStaticPos: this.initialStaticPos,
			isTrainer: this.isTrainer,
			isRival: this.isRival,
			rivalFirstMonIdx: this.rivalFirstMonIdx,
			isBlocker: this.isBlocker,
			movesAround: this.movesAround,
			isBeaten: this.isBeaten,
			itemGift: this.itemGift,
			monsters: this.monsters,
			items: this.items,
		}
	}

	/**
	 * This method moves the NPC in a given direction
	 */
	#walk(direction: 'up' | 'left' | 'down' | 'right') {
		if (direction === 'up') {
			this.sprite.position.y -= this.movingByPx
			this.boundary.position.y -= this.movingByPx
			return
		}
		if (direction === 'left') {
			this.sprite.position.x -= this.movingByPx
			this.boundary.position.x -= this.movingByPx
			return
		}
		if (direction === 'down') {
			this.sprite.position.y += this.movingByPx
			this.boundary.position.y += this.movingByPx
			return
		}
		if (direction === 'right') {
			this.sprite.position.x += this.movingByPx
			this.boundary.position.x += this.movingByPx
		}
	}

	/**
	 * This method handles the moment the NPC leaves the viewport after an interaction
	 */
	#handleNPCAfterMovingAway() {
		this.directionToGo.checked = false
		this.shouldWalkAway = false
		this.audio?.look?.stop()
		// remove rival and its boundary from array
		this.#removeNPC()
		flags.setIsDetected(false)
		flags.setIsInteracting(false)
		getMap().detectedTrainer.moving = true
		getMap().playAudio()
	}

	/**
	 * @return a boolean that indicates if the npc is appearing into the viewport from the top
	 */
	#isNPCAppearingTop() {
		return this.sprite.position.y + this.sprite.enemyHeight >= 0
	}

	/**
	 * This method initiates the detection bubble sprite
	 * @param forRegularTrainer - A boolean that indicates which exclamation bubble sprite should be used.
	 * @returns a sprite object
	 */
	#getDetectedSprite(forRegularTrainer: boolean) {
		const yPos = (forRegularTrainer ? -this.sprite.enemyHeight : this.sprite.enemyHeight) * 1.3
		return new Sprite({
			image: { src: forRegularTrainer ? base64MapImgs.detectedDown : base64MapImgs.detectedUp },
			position: {
				x: this.sprite.position.x - this.sprite.enemyWidth / 3,
				y: this.sprite.position.y + yPos
			},
			scaleFactor: viewport.getScaleFactor(false)
		})
	}

	/**
	 * This method checks if the moving NPC is colliding with any of the given boundaries
	 * @param param0 - An object with a direction property that indicates in which direction the npc is facing
	 * @returns a boolean that indicates if the moving NPC is colliding with a boundary
	 */
	#isNPCColliding({ direction }: { direction: string }) {
		for (let i = 0; i < getMap().boundaries.length; i++) {
			const boundary = getMap().boundaries[i]
			if (getMap().isBoundaryFarAway(boundary, this)) { continue }
			if (
				// npc is colliding with player
				getMap().isWalkingNPCColliding({
					rectangle1: this.sprite,
					rectangle2: new Boundary({
						id: boundary.id,
						position: this.#getCollidingObjPos({ direction }),
						width: boundary.width,
						height: boundary.height
					})
				}) ||
				// npc is colliding with collision boundary
				(getMap().isBoundaryIDCollision(boundary.id) &&
					getMap().isWalkingNPCColliding({
						rectangle1: this.sprite,
						rectangle2: new Boundary({
							id: boundary.id,
							position: this.#getCollidingObjPos({ direction, obj: boundary }),
							width: boundary.width,
							height: boundary.height
						})
					}))
			) {
				return true
			}
		}
		return false
	}

	/**
	 * @param param0 - An object with the properties "direction" and "obj"
	 * @returns a new boundary that is created to determine if a collision will happen
	 */
	#getCollidingObjPos({ direction, obj }: { direction: string, obj?: Boundary }) {
		const objNeeded = obj || player.sprite
		if (direction === 'right') { return { x: objNeeded.position.x - this.movingByPx, y: objNeeded.position.y } }
		if (direction === 'left') { return { x: objNeeded.position.x + this.movingByPx, y: objNeeded.position.y } }
		if (direction === 'down') { return { x: objNeeded.position.x, y: objNeeded.position.y - this.movingByPx } }
		// 40 for Sprite to avoid the npc colliding like a player over the boundary, which causes
		// the player sprite to not be able to move while npc collides with him
		if (direction === 'up') { return { x: objNeeded.position.x, y: objNeeded.position.y + (obj ? this.movingByPx : 20) } }
		return { x: 0, y: 0 }
	}

	/**
	 * This method checks if there is more space to the left or right of the player if a NPC needs to walk around the player to move away (after rival fight for example)
	 * @returns void - undefined
	 */
	#checkWhichDirectionToGo() {
		this.directionToGo.checked = true
		for (let i = 0; i < getMap().boundaries.length; i++) {
			const boundary = getMap().boundaries[i]
			if (getMap().isBoundaryFarAway(boundary, this) || boundary.id !== 1) { continue }
			if (
				// all boundaries left of rival
				this.sprite.position.x > boundary.position.x &&
				// only boundaries approximately same y axis
				boundary.position.y - this.sprite.position.y < (48 * viewport.getScaleFactor(false)) &&
				boundary.position.y - this.sprite.position.y >= 0 &&
				// space between left boundary & rival is too small
				boundary.position.x + (96 * viewport.getScaleFactor(false)) > this.sprite.position.x
			) {
				// rival will collide with left boundary
				this.directionToGo.direction = 'right'
				return
			}
		}
		this.directionToGo.direction = 'left'
	}

	/**
	 * This method changes the direction of a moving NPC after a random time range and moves it with the new direction to a random distance 
	 * @param maxRange - a number that indicates the maximum movement radius
	 */
	#changeMovingDirection(maxRange: number) {
		// determine a random time before npc changes direction
		if (!this.randomTimeUntilNextMove.isSet) {
			// flag
			this.randomTimeUntilNextMove.isSet = true
			this.timeOnLastMove = Date.now()
			// get random int from 1sec - 5sec
			this.randomTimeUntilNextMove.time = getRandomInt(1000, 5000)
		}
		// this.sprite.animate = false
		this.sprite.frames.val = 0
		// check if random time elapsed is reached
		if (Date.now() >= this.timeOnLastMove + this.randomTimeUntilNextMove.time) {
			// set the random int to a new random value
			this.randomMoveMaxVal = getRandomInt(1, maxRange)
			// change npc sprite direction to a random direction
			const randomDirection = getRandomSpriteDirection()
			this.sprite.image = this.sprite.sprites[randomDirection]
			if (this.spriteHead) {
				this.spriteHead.image.src = this.spriteHead.sprites[randomDirection].src
			}
			// reset flag to be able to set a new random time for next direction change
			this.randomTimeUntilNextMove.isSet = false
		}
	}

	/**
	 * This method checks if the NPC that has detected the player has already moved half its way so it can change its direction to get aligned with the player
	 * @param param0 - An object with a map ID property that indicates the moving logic that should be triggered
	 * @returns a boolean
	 */
	#isRivalHalfway({ mapID }: { mapID: number }) {
		if (mapID === 20 || mapID === 22) {
			if (this.sprite.image.src.includes('down')) {
				if (this.halfway.pos === 0) {
					this.halfway.pos = (player.sprite.position.y - this.sprite.position.y - this.sprite.enemyHeight) / 2
				}
				return player.sprite.position.y - this.sprite.position.y - this.sprite.enemyHeight <= this.halfway.pos
			}
		}
	}

	/**
	 * @returns a boolean that indicates if player and NPC are aligned on the Y axis 
	 */
	#isAlignedY() {
		return player.sprite.position.y - this.sprite.position.y > 0 && player.sprite.position.y - this.sprite.position.y < this.movingByPx * 2.5
	}

	/**
	 * @returns a boolean that indicates if player and NPC are aligned on the X axis 
	 */
	#isAlignedX() {
		return player.sprite.position.x - this.sprite.position.x > 0 && player.sprite.position.x - this.sprite.position.x < this.movingByPx * 2.5
	}

	/**
	 * This method changes the NPC image source depending on its position relative to the player and is used for the NPC movement
	 */
	#changeDirectionX() {
		// rival stands right of player
		if (this.sprite.position.x > player.sprite.position.x) {
			this.sprite.image.src = this.sprite.sprites.left.src
		}
		// rival stands left of player
		if (this.sprite.position.x < player.sprite.position.x) {
			this.sprite.image.src = this.sprite.sprites.right.src
		}
	}

	/**
	 * This method changes the NPC image source depending on its position relative to the player and is used for the NPC movement
	 */
	#changeDirectionY() {
		// rival stands right of player
		if (this.sprite.position.y > player.sprite.position.y) {
			this.sprite.image.src = this.sprite.sprites.up.src
		}
		// rival stands left of player
		if (this.sprite.position.y < player.sprite.position.y) {
			this.sprite.image.src = this.sprite.sprites.down.src
		}
	}

	/**
	 * This method removes the rival npc from the npc & boundary array after he has been beaten and walked beyond viewport
	 */
	#removeNPC() {
		const npcIdx = getMap().npcs.findIndex(x => x.id === this.id)
		removeArrEntry(getMap().npcs, npcIdx)
	}

}
