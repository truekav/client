import { battle } from '..'
import { getMonsterTypeMultiplicator, getMonsterTypeNameById, MonsterType } from '../data/monster/types'
import type { IMonsterStats } from '../models/classes'
import type { IAttack } from '../models/functions'
import { assertNonNullish, getRandomInt } from '../util/helper'
import { Monster } from './Monster'

class Formula {

	static #instance: Formula

	public static newFormula() {
		if (!this.#instance) { return new Formula() }
		return this.#instance
	}

	/**
	 * This class contains the main game formulas. Source: https://m.bulbapedia.bulbagarden.net/wiki/
	 */
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	private constructor() {}

	/***** Public Methods *****/

	/**
	 * This method calculates the damage caused by an attack. Source: https://m.bulbapedia.bulbagarden.net/wiki/Damage
	 * @param attacker - a monster object
	 * @param attack - the performed attack object
	 * @param recipient - a monster object
	 * @returns an object with the following properties:
	 * - damage: the calculated damage that will be used to substrack from monster HP
	 * - isCritical: a boolean which indicates if the attack hit was critical
	 * - type1: 0 | .5 | 1 | 2 which indicates the attack-to-monster type multiplicator
	 * - type2: 0 | .5 | 1 | 2 which indicates the attack-to-monster second type multiplicator
	 */
	public calcDamage(attacker: Monster, attack: IAttack, recipient: Monster) {
		// Critical hit based on attack property
		const isCritical = battle.isHit(attack.criticalChance) ? 2 : 1
		// formula params
		const level = attacker.level
		assertNonNullish(level, 'level is not available during damage calculation')
		const critical = isCritical
		const attackV = attack.isSpecial ? attacker.stats.IS.spAtt : attacker.stats.IS.att
		const defenseV = attack.isSpecial ? recipient.stats.IS.spDef : recipient.stats.IS.spDef
		const power = attack.power
		const stab = this.#isSTAB(attack.type, attacker.type)
		const typeMultiplicator = getMonsterTypeMultiplicator(attack.type, recipient.type)
		const type1 = typeMultiplicator.type1
		const type2 = typeMultiplicator.type2
		const random = (getRandomInt(217, 255)) / 255
		// calculation
		const part1 = ((2 * level * critical) / 5) + 2
		const part2 = power * (attackV / defenseV)
		const part3 = ((part1 * part2) / 50) + 2
		return {
			damage: power === 0 ? 0 : Math.floor(part3 * stab * type1 * type2 * random),
			isCritical: isCritical === 2 ? true : false,
			type1,
			type2
		}
	}

	/**
	 * This method is called in a loop that creates a XP-Table and calculates how much XP a monster needs to reach the given level.
	 * Source: https://m.bulbapedia.bulbagarden.net/wiki/Experience
	 * @param lvl - the monster level
	 * @param cat - a number (1 | 2 | 3 | 4) which indicates the XP-growth-rate of a monster
	 * - 1 = slow: 1,250,000 XP needed for level 100
	 * - 2 = medium-slow: 1,059,860 XP needed for level 100
	 * - 3 = medium-fast: 1,000,000 XP needed for level 100
	 * - 4 = fast: 800,000 XP needed for level 100
	 * @returns the XP needed to reach the given level
	 */
	public calcXPGrowth(lvl: number, cat: number) {
		// slow
		if (cat === 1) {
			return Math.floor((5 * Math.pow(lvl, 3)) / 4)
		}
		// medium slow
		if (cat === 2) {
			return Math.floor(((6 / 5) * Math.pow(lvl, 3)) - (15 * (Math.pow(lvl, 2))) + 100 * lvl - 140)
		}
		// medium fast
		if (cat === 3) {
			return Math.floor(Math.pow(lvl, 3))
		}
		// fast
		return Math.floor((4 * Math.pow(lvl, 3)) / 5)
	}

	/**
	 * This method calculates the amount of XP that a monster gains after winning a battle,
	 * considers the difference between both monster levels and
	 * that multiple monsters might have participated in the battle
	 * source: https://bulbapedia.bulbagarden.net/wiki/Experience
	 * @param winner - the monster object that has won the battle
	 * @param enemy - the monster object that has lost the battle
	 * @returns a number which indicates the XP gained
	 */
	public calcScaledXPGain(winner: Monster, enemy: Monster) {
		const a = enemy.isWild ? 1 : 1.5
		const t = 1
		const b = enemy.effort
		const e = 1
		const L = enemy.level
		const p = 1
		const s = battle.getMonsterCountParticipatedAlive()
		const Lp = winner.level
		const result = Math.floor(((((a * b * L) / (5 * s)) * ((2 * L + 10) / (L + Lp + 10)) ^ 2.5))) * t * e * p
		return result > 100000 ? 100000 : result
	}

	/**
	 * This method calculates the "IS"-stats of a monster based on the given initial stats object of that monster.
	 * Source: https://bulbapedia.bulbagarden.net/wiki/Stat
	 * @param s - the stats object of the monster
	 * @param lvl - the level of the monster
	 * @returns the calculated stats based on the monster level. the "IS"-stats object has the following properties:
	 * - hp: The HP of the monster
	 * - att: The attack stat of the monster
	 * - def: The defense stat of the monster
	 * - initiativ: The initiativ of the monster
	 * - spAtt: The secial-attack of the monster
	 * - spDef: The special-defense of the monster
	 */
	public calcStatsIS(s: IMonsterStats, lvl: number) {
		// assertNonNullish(lvl, 'level not available while calculating stats')
		// console.log('level in calc: ', lvl)
		return {
			hp: this.#calcStat(s.base.hp, s.IV.hp, s.EV.hp, lvl, true),
			att: this.#calcStat(s.base.att, s.IV.att, s.EV.att, lvl),
			def: this.#calcStat(s.base.def, s.IV.def, s.EV.def, lvl),
			ini: this.#calcStat(s.base.ini, s.IV.ini, s.EV.ini, lvl),
			spAtt: this.#calcStat(s.base.spAtt, s.IV.spAtt, s.EV.spAtt, lvl),
			spDef: this.#calcStat(s.base.spDef, s.IV.spDef, s.EV.spDef, lvl),
		}
	}

	/**
	 * This method calculates the amount of effort-values that a winning monster gains.
	 * Due to effort values, trained monsters are usually stronger than wild monster, even those of the same level.
	 * Effort values are distributed evenly across all monster that participated in the battle.
	 * Source: https://m.bulbapedia.bulbagarden.net/wiki/Effort_values
	 * @param enemy - the monster object that has lost the battle
	 */
	public calcEVGain(enemy: Monster) {
		const maxEV = 255 // per stats
		const maxTotalEV = 510 // all EV stats added together
		let currentTotalEV = 0
		const battleMons = battle.getMonsterParticipatedAlive()
		battleMons.forEach(monster => {
			Object.keys(monster.stats.EV).forEach(key => {
				currentTotalEV += monster.stats.EV[key]
			})
		})
		battleMons.forEach(monster => {
			Object.keys(monster.stats.EV).forEach(key => {
				monster.stats.EV[key] += (monster.stats.EV[key] >= maxEV || currentTotalEV >= maxTotalEV) ? 0 : enemy.stats.EVY[key]
			})
		})
	}

	/**
	 * This method calculates if a monster has been catched or not.
	 * Source: https://bulbapedia.bulbagarden.net/wiki/Catch_rate#Capture_method_.28Generation_I.29
	 * @param ballName - The name of the ball that has been used
	 * @param enemy - The monster object that might get catched
	 * @returns an object containing the following properties:
	 * - brokeFree: a boolean that indicates if the target broke free from the ball
	 * - ballShakeCount: -1 | 1 | 2 | 3 which indicates how many shakes before braking free where -1 misses the target entirely
	 */
	public isCatched(ballName: string, enemy: Monster) {
		let brokeFree = false
		/**** Ball Value ****/
		let ballMaxRange = 255 // 255 BALL
		if (ballName === 'TERABALL') { ballMaxRange = 200 }
		if (ballName === 'PETABALL') { ballMaxRange = 175 }
		if (ballName === 'EXABALL') { ballMaxRange = 155 }
		/**** Status threshold ****/
		let status = 0
		if (enemy.status.sleeping || enemy.status.frozen) { status = 25 }
		if (enemy.status.poisoned || enemy.status.burning || enemy.status.paralyzed) { status = 12 }
		const randomInt = getRandomInt(0, ballMaxRange)
		// console.log('first random int: ', randomInt)
		if (randomInt < 25 && (enemy.status.sleeping || enemy.status.frozen)) {
			brokeFree = true
		}
		if (randomInt < 12 && (enemy.status.poisoned || enemy.status.burning || enemy.status.paralyzed)) {
			brokeFree = true
		}
		if (randomInt - status > enemy.catchRate) {
			brokeFree = true
		}
		let secondBallVal = 12 // 12
		if (ballName === 'TERABALL') { secondBallVal = 10 }
		if (ballName === 'PETABALL') { secondBallVal = 8 }
		if (ballName === 'EXABALL') { secondBallVal = 6 }

		const M = getRandomInt(0, 255)
		// console.log('random M: ', M)
		let f = Math.floor((enemy.maxHP * 255 * 4) / (enemy.stats.IS.hp * secondBallVal))
		// console.log('f: ', f)
		if (f < 1) { f = 1 }
		if (f > 255) { f = 255 }
		if (f >= M) { brokeFree = false }

		let ballShakeCount = 3
		let s = 0
		if (enemy.status.sleeping || enemy.status.frozen) { s = 10 }
		if (enemy.status.paralyzed || enemy.status.poisoned || enemy.status.burning) { s = 5 }
		const d = Math.floor((enemy.catchRate * 100) / ballMaxRange)
		if (d >= 256) { ballShakeCount = 3 }
		const x = Math.floor((d * f) / 255) + s
		if (x < 10) { ballShakeCount = -1 }
		if (x < 30 && x >= 10) { ballShakeCount = 1 }
		if (x < 70 && x >= 30) { ballShakeCount = 2 }
		return {
			brokeFree,
			ballShakeCount
		}
	}

	public calcTrainerPayout(effort: number, lastMonLvl: number) {
		return effort * lastMonLvl
	}

	/***** Private Methods *****/

	/**
	 * This method checks if the monster type and attack are the same to generate a damage multiplicator (1 or 1.5)
	 * @param attackType - the type of the used attack
	 * @param monsterTypes - the types array of the monster object that has used the attack
	 * @returns 1 | 1.5 as a damage multiplicator
	 */
	#isSTAB(attackType: MonsterType, monsterTypes?: MonsterType[]) {
		assertNonNullish(monsterTypes, 'MonsterTypes not available')
		const attType = getMonsterTypeNameById(attackType)
		for (let i = 0; i < monsterTypes.length; i++) {
			const id = monsterTypes[i]
			const monsterType = getMonsterTypeNameById(id)
			if (attType === monsterType) { return 1.5 }
		}
		return 1
	}

	/**
	 * This method calculates the value of the IS-stat object of the monster based on its base-stats, IV-stats and EV-stats
	 * source: https://pokemon.fandom.com/wiki/Statistics
	 * @param base - The base-stats object of the monster
	 * @param IV - The IV-stats object of the monster
	 * @param EV - The EV-stats object of the monster
	 * @param lvl - The level of the monster
	 * @param isHP - A boolean that indicates if the calculation is for the HP-property of the IS-stats 
	 * @returns a number which indicates the value of the IS-stat property
	 */
	#calcStat(base: number, IV: number, EV: number, lvl: number, isHP = false) {
		if (isHP) {
			return Math.floor(.01 * (2 * base + IV + Math.floor(.25 * EV)) * lvl) + lvl + 10
		}
		return Math.floor(.01 * (2 * base + IV + Math.floor(.25 * EV)) * lvl) + 5
	}

}

export const formula = Formula.newFormula()