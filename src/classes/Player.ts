
import { getMap, setMap } from './Movables'
import { animateHouseToMapTransition, animateMapTransition } from '../animation/map'
import { playAudio } from '../data/audio'
import { base64BattleImgs } from '../data/base64Imgs'
import type { IMoveParams, IPlayerArgs, IPosition } from '../models/classes'
import { assertNonNullish, newItemInstance } from '../util/helper'
import { calcOffset, getInitialPlayerObj, getSavedPlayer } from '../util/localStorage'
import { Boundary } from './Boundary'
import { canvas } from './Canvas'
import { events } from './Events'
import { flags } from './Flags'
import { Item } from './Items/Item'
import { Monster } from './Monster'
import { Sprite } from './Sprite'
import { viewport } from './Viewport'
import { NPC } from './NPC'
import { dialog } from './Dialog'
import { data } from './Data'
import { config } from '../config'
import { locationStrEl } from '../util/html/elements'
import { getLocationStr } from '../data/map/location'

export class Player {

	static #instance: Player
	#name
	#sprite
	#spriteHead
	#battleSprite
	#monsters
	#items
	#monsterDex
	#inventory
	#isPlayingCollision
	#selectedItems: Item[]
	#selectedMonToSwitch?: Monster
	#satoshis
	#startTime
	#playTime
	#battlezoneLocation
	#currentMapId
	#currentHouseId
	#maxBagItems = 30
	#badges
	#lastHospitalData
	#locationStrTimeout: ReturnType<typeof setTimeout> | undefined

	/***** Getter *****/

	/** @returns the player name */
	get name() { return this.#name }
	/** @returns the player sprite object */
	get sprite() { return this.#sprite }
	/** @returns the player head sprite object */
	get spriteHead() { return this.#spriteHead }
	/** @returns the player battleSprite object */
	get battleSprite() { return this.#battleSprite }
	/** @returns the player monster array */
	get monsters() { return this.#monsters }
	/** @returns the player item array */
	get items() {
		this.#items.forEach(item => {
			if (item.count <= 0) {
				this.removeItem(item)
			}
		})
		return this.#items
	}
	/** @returns the player monster-dex object */
	get monsterDex() { return this.#monsterDex }
	/** @returns the player selected items array */
	get selectedItems() { return this.#selectedItems }
	/** @returns the player selected monster to switch after trainer sends out his next monster */
	get selectedMonToSwitch() { return this.#selectedMonToSwitch }
	/** @returns the player currency */
	get satoshis() { return this.#satoshis }
	/** @returns the player inventory (Storage PC) */
	get inventory() { return this.#inventory }
	/** @returns the time played value in seconds */
	get time() { return this.#startTime }
	/** @returns the location value */
	get battlezoneLocation() { return this.#battlezoneLocation }
	/** @returns the ID which indicates in which house the player currently is */
	get currentHouseId() { return this.#currentHouseId }
	/** @returns the ID which indicates in which map the player currently is */
	get currentMapId() { return this.#currentMapId }
	/** @returns the maximum amount of items in the players bag */
	get maxBagItems() { return this.#maxBagItems }
	/** @returns the medals object of the player */
	get badges() { return this.#badges }
	/** @returns the object that includes the offset and map ID of the last visited hospital */
	get lastHospitalData() { return this.#lastHospitalData }
	get locationStrTimeout() { return this.#locationStrTimeout }

	public static newPlayer() {
		if (!this.#instance) {
			return new Player(getSavedPlayer())
		}
		return this.#instance
	}

	/***********************/
	/***** CONSTRUCTOR *****/
	/***********************/

	/** This class contains the main methods for the player object and some methods for the monsters belonging to the player */
	private constructor({
		name,
		monsters,
		items,
		playtime,
		satoshis,
		monsterDex,
		inventory,
		location,
		currentHouseId = 0,
		currentMapId = 0,
		badges,
		lastHospitalData
	}: IPlayerArgs) {
		const playerImgWidth = 96
		const playerImgHeight = 34
		this.#name = name.toUpperCase()
		this.#sprite = new Sprite({
			position: {
				x: canvas.element.width / 2 - playerImgWidth / 4 / 2,
				y: canvas.element.height / 2 - playerImgHeight / 2
			},
			image: { src: './imgs/players/standard/playerDown.png' },
			frames: { max: 4, hold: 10 },
			sprites: {
				up: { src: './imgs/players/standard/playerUp.png' },
				right: { src: './imgs/players/standard/playerRight.png' },
				down: { src: './imgs/players/standard/playerDown.png' },
				left: { src: './imgs/players/standard/playerLeft.png' }
			},
			scaleFactor: viewport.getScaleFactor(false)
		})
		// for high grass illusion
		this.#spriteHead = new Sprite({
			position: {
				x: canvas.element.width / 2 - playerImgWidth / 4 / 2,
				y: canvas.element.height / 2 - playerImgHeight / 2
			},
			image: { src: './imgs/players/standard/headDown.png' },
			frames: { max: 4, hold: 10000 },
			sprites: {
				up: { src: './imgs/players/standard/headUp.png' },
				right: { src: './imgs/players/standard/headRight.png' },
				down: { src: './imgs/players/standard/headDown.png' },
				left: { src: './imgs/players/standard/headLeft.png' }
			},
			scaleFactor: viewport.getScaleFactor(false)
		})
		this.#battleSprite = this.#initBattleSprite(this.getBattleSpritePos())
		this.#monsters = monsters
		this.#items = items
		this.#badges = badges
		this.#monsterDex = monsterDex
		this.#inventory = inventory
		this.#isPlayingCollision = false
		this.#selectedItems = []
		this.#selectedMonToSwitch = undefined
		this.#satoshis = satoshis || 1500
		this.#startTime = Math.floor(Date.now())
		this.#playTime = playtime || 0
		this.#battlezoneLocation = location
		this.#currentHouseId = currentHouseId
		this.#currentMapId = currentMapId
		this.#lastHospitalData = lastHospitalData
	}

	/***********************/
	/******* METHODS *******/
	/***********************/

	/**
	 * @returns a number indicating the amount of badges that the player has earned 
	 */
	public getBadgesCount() {
		let count = 0
		for (const key in this.#badges) {
			if (Object.prototype.hasOwnProperty.call(this.#badges, key)) {
				const medal = this.#badges[key]
				if (medal.inPossession) { count++ }
			}
		}
		return count
	}

	/**
	 * This method unlocks the medal won
	 */
	public unlockMedalByName(name: string) {
		// 'Ground' 'Plant' 'Water' 'Fire' 'Lightning' 'Psy' 'Ghost' 'Master'
		for (const key in this.#badges) {
			if (Object.prototype.hasOwnProperty.call(this.#badges, key)) {
				const medal = this.#badges[key]
				if (key === name) {
					medal.inPossession = true
					return
				}
			}
		}
	}

	/**
	 * This method only returns the best ball in the player item array if the better balls are pushed in front of the item array
	 * @returns the first ball item in the player item array
	 */
	public getBestBall() {
		// this works only if the balls that are better are pushed in front of the item array
		return this.items.find(item => item.name.includes('BALL'))
	}

	/**
	 * This method checks which monster index contains a monster with HP > 0
	 * @returns the index of the first monster that is alive belonging to the player 
	 */
	public getMonsterAliveIdx() {
		let monIdx = -1
		for (let i = 0; i < this.monsters.length; i++) {
			const monster = this.monsters[i]
			if (monster.stats.IS.hp > 0) {
				monIdx = i
				break
			}
		}
		return monIdx
	}

	/**
	 * This method handles the position of the player sprite in battle
	 * @param isFaded - A boolean that indicates if the sprite has already faded into the viewport
	 * @returns a position object for the player sprite in battle
	 */
	public getBattleSpritePos(isFaded = false) {
		const battleSpriteH = 230 * viewport.getScaleFactor(false)
		const dialogH = canvas.element.height * .25
		const yPos = canvas.element.height - battleSpriteH - dialogH
		if (viewport.isSmartphone()) {
			return {
				x: isFaded ? 0 : canvas.element.width * 1.5,
				y: yPos
			}
		}
		return {
			x: isFaded ? canvas.element.width / 7 : canvas.element.width * 1.5,
			y: yPos
		}
	}

	/**
	 * This method formats the time played from milliseconds to hh:mm
	 * @returns a string indicating the time played in hh:mm format
	 */
	public getTimeStr() {
		return new Date(this.#getTimePlayed()).toISOString().substring(11, 16)
	}

	/**
	 * This method sets the player properties back to initial
	 */
	public resetPlayerToInitial() {
		const initialPlayer = getInitialPlayerObj()
		this.#badges = initialPlayer.badges
		this.#monsters = initialPlayer.monsters
		this.#items = initialPlayer.items
		this.#monsterDex = initialPlayer.monsterDex
		this.#inventory = initialPlayer.inventory
		this.#battlezoneLocation = 0
		this.#currentHouseId = initialPlayer.currentHouseId
		this.#currentMapId = initialPlayer.currentMapId
		this.#playTime = 0
	}

	/**
	 * This method sets the player name to the given string
	 */
	public setName(name: string) {
		this.#name = name.toUpperCase()
	}

	/**
	 * This method sets the property "selectedMonToSwitch" for a monster switch after a NPC trainer sends out his next monster
	 * @param mon - The monster object that has been chosen by player
	 */
	public setSelectedMonToSwitch(mon?: Monster) {
		this.#selectedMonToSwitch = mon
	}

	/**
	 * This method saves the house ID in which the player currently is in
	 * @param id - A number that indicates the ID of the house where the player currently is in
	 */
	public setCurrentHouseId(id: number) {
		this.#currentHouseId = id
	}

	/**
	 * This method saves the map ID in which the player currently is in
	 * @param id - A number that indicates the ID of the map where the player currently is in
	 */
	public setCurrentMapId(id: number) {
		this.#currentMapId = id
	}

	/**
	 * This method increases the money of the player by a given number
	 * @param sats - A number that is used to increase the money of the player
	 */
	public addSats(sats: number) {
		this.#satoshis += sats
	}

	/**
	 * This method decreases the money of the player by a given number
	 * @param sats - A number that is used to decrease the money of the player
	 */
	public decreaseSats(sats: number) {
		this.#satoshis -= sats
	}

	/**
	 * This method sets the properties needed for the play to move faster
	 */
	public run() {
		const maxSpeed = 6 * viewport.getScaleFactor(false)
		const runFrames = 7
		if (this.#currentHouseId > 0 || getMap().movingByPx === maxSpeed) { return }
		getMap().setMovingByPx(getMap().movingByPx * 2)
		this.#sprite.frames.hold = runFrames
	}

	/**
	 * This method sets the propeties needed to move faster back to default
	 */
	public dontRun() {
		const defaultFrames = 10
		getMap().setMovingByPx(config.movingByPx * viewport.getScaleFactor(false))
		this.#sprite.frames.hold = defaultFrames
	}

	/**
	 * This method handles the movables position, the player sprite facing directions and checks for collisions while user input
	 * @param moving - A boolean that indicates if the player is moving or not 
	 */
	public move = ({ moving, animationID, inHouse }: IMoveParams) => {
		if (!this.sprite.sprites || !this.spriteHead.sprites) { return }
		if (flags.getIsPaused()) {
			// reset player sprite frame to 0 if standing still
			this.sprite.frames.val = 0
			return
		}
		const collisions = getMap().boundaries
		const allMovables = getMap().movables
		// check user input
		if (events.keys.w.pressed && events.lastKeyPressed === 'w') {
			// professor encounter
			this.#npcDetectionCheck(110)
			// rival encounter
			this.#npcDetectionCheck(106)
			// console.time()
			this.#handleSpriteDirection({ direction: 'up' })
			const check = this.isPlayerColliding({ collisions, direction: 'up' })
			if (check.colliding && !config.walkThroughBoundaries) {
				// save last visited hospital
				if (check.boundary?.id === 7) {
					this.#lastHospitalData = {
						mapID: this.currentMapId,
						offset: viewport.isSmallDevice() ? getMap().offset.smartphone : getMap().offset.desktop
					}
					// update the initial static position of trainer npcs
					for (let i = 0; i < allMovables.length; i++) {
						const movable = allMovables[i]
						if (movable instanceof NPC && (movable.isTrainer || movable.isRival || movable.isGym || movable.movesAround)) {
							movable.initialStaticPos = { x: movable.initialPos.x, y: movable.initialPos.y }
						}
					}
				}
				// check for entries while beeing outside of house
				if (!inHouse) {
					// house door locked
					if (check.boundary?.id === 203) {
						flags.setIsInteracting(true)
						this.sprite.animate = false
						this.sprite.frames.val = 0
						dialog.show('This door is LOCKED...!')
						const t = setTimeout(() => {
							dialog.hide()
							flags.setIsInteracting(false)
							clearTimeout(t)
						}, 2000)
						return
					}
					// check for house door entries
					if (check.boundary && getMap().isHouseEntry(check.boundary.id)) {
						flags.setIsInteracting(true)
						// stop map animation
						cancelAnimationFrame(animationID)
						// play door sound
						playAudio('goInside')
						// set house id
						this.setCurrentHouseId(check.boundary.id)
						// set player sprites values
						this.sprite.animate = false
						getMap().audio.pause()
						// initialize house object
						setMap({
							mapID: this.#currentMapId,
							houseID: check.boundary.id
						})
						// start map to house transition
						animateMapTransition()
						return
					}
					// check for map entries
					if (check.boundary && getMap().isMapEntry(check.boundary.id)) {
						this.#handleMapEntry({ animationID, mapID: check.boundary.id })
						return
					}
				}
				this.#playCollision()
				moving = false
			}
			if (moving) {
				this.#isPlayingCollision = false
				canvas.reserSharpen()
				// move the movables depending on user input
				getMap().moveMovables('w')
				// update offset
				getMap().updateMapOffset()
			}
			// console.timeEnd()
			return
		}
		if (events.keys.a.pressed && events.lastKeyPressed === 'a') {
			// rival encounter
			this.#npcDetectionCheck(106)
			// console.time()
			this.#handleSpriteDirection({ direction: 'left' })
			// loop over collision boundaries to check for collisions
			const check = this.isPlayerColliding({ collisions, direction: 'left' })
			if (check.colliding && !config.walkThroughBoundaries) {
				// check for entries while beeing outside of house
				if (!inHouse) {
					// check for map entries
					if (check.boundary && getMap().isMapEntry(check.boundary.id)) {
						this.#handleMapEntry({ animationID, mapID: check.boundary.id })
						return
					}
				}
				this.#playCollision()
				moving = false
			}
			if (moving) {
				this.#isPlayingCollision = false
				canvas.reserSharpen()
				// move the movables depending on user input
				getMap().moveMovables('a')
				// update offset
				getMap().updateMapOffset()
			}
			// console.timeEnd()
			return
		}
		if (events.keys.s.pressed && events.lastKeyPressed === 's') {
			// rival encounter
			this.#npcDetectionCheck(106)
			// console.time()
			this.#handleSpriteDirection({ direction: 'down' })
			// loop over collision boundaries to check for collisions
			const check = this.isPlayerColliding({ collisions, direction: 'down' })
			if (check.colliding && !config.walkThroughBoundaries) {
				// check for house door exit while inside house
				if (inHouse && check.boundary?.id === 5) {
					flags.setIsInteracting(true)
					// set player current house id to 0 to indicate player is not in house anymore
					this.setCurrentHouseId(0)
					// play door sound
					playAudio('goOutside')
					// update player sprites val
					this.sprite.animate = false
					this.sprite.frames.val = 0
					// cancel house animation
					window.cancelAnimationFrame(animationID)
					getMap().audio.pause()
					// start house to map transition
					animateHouseToMapTransition()
					return
				}
				// check for entries while beeing outside of house
				if (!inHouse) {
					// check for map entries
					if (check.boundary && getMap().isMapEntry(check.boundary.id)) {
						this.#handleMapEntry({ animationID, mapID: check.boundary.id })
						return
					}
				}
				this.#playCollision()
				moving = false
			}
			if (moving) {
				canvas.reserSharpen()
				this.#isPlayingCollision = false
				// move the movables depending on user input
				getMap().moveMovables('s')
				// update offset
				getMap().updateMapOffset()
			}
			// console.timeEnd()
			return
		}
		if (events.keys.d.pressed && events.lastKeyPressed === 'd') {
			// rival encounter
			this.#npcDetectionCheck(106)
			// console.time()
			this.#handleSpriteDirection({ direction: 'right' })
			// loop over collision boundaries to check for collisions
			const check = this.isPlayerColliding({ collisions, direction: 'right' })
			if (check.colliding && !config.walkThroughBoundaries) {
				// check for entries while beeing outside of house
				if (!inHouse) {
					// check for map entries
					if (check.boundary && getMap().isMapEntry(check.boundary.id)) {
						this.#handleMapEntry({ animationID, mapID: check.boundary.id })
						return
					}
				}
				this.#playCollision()
				moving = false
			}
			// console.timeEnd()
			if (moving) {
				canvas.reserSharpen()
				this.#isPlayingCollision = false
				// move the movables depending on user input
				getMap().moveMovables('d')
				// update offset
				getMap().updateMapOffset()
			}
			return
		}
		// reset player sprite frame to 0 if standing still
		this.sprite.frames.val = 0
	}

	/**
	 * @param param0 the boundary array to check for collisions and a direction string "up" | "right" | "bottom" | "left"
	 * @returns an object with the boundary ID which the player is colliding with and a boolean that indicates a collision happened
	 */
	public isPlayerColliding({ collisions, direction }: { collisions: Boundary[], direction: string }) {
		const npcCols = getMap().npcs.map(npc => npc.boundary)
		const allCols = [...collisions, ...npcCols]
		// loop over collision boundaries to check for collisions
		for (let i = 0; i < allCols.length; i++) {
			// console.log('map offset: ', getMap().getMapOffset())
			const boundary = allCols[i]
			if (getMap().isBoundaryFarAway(boundary)) { continue }
			// hiden items (ids between 40 & 50)
			if (boundary.id >= 40 && boundary.id <= 50) { continue }
			// check for collisions
			if (
				getMap().isColliding({
					rectangle1: this.sprite,
					rectangle2: new Boundary({
						id: boundary.id,
						position: this.#getCollidingObjPos({ direction, boundary }),
						width: boundary.width,
						height: boundary.height
					})
				})
			) {
				// check for battlezones
				if (boundary.id > 244) {
					// set battlezoneLocation as a player prop. to determine a location-based random wild enemy
					if (this.#battlezoneLocation !== boundary.id) {
						this.#battlezoneLocation = boundary.id
					}
					continue
				}
				return { boundary, colliding: true }
			}
		}
		return { colliding: false }
	}

	/**
	 * @returns a boolean that indicates if the player bag has reached its maximum capacity
	 */
	public hasMaxItems() {
		return this.#items.length >= this.#maxBagItems
	}

	/**
	 * This method adds a new item to the player item array or increases its count if the given item is already in array
	 * @param item - The item object that should be added to the player item array
	 */
	public addItem(item: Item, count?: number) {
		if (!(item instanceof Item)) { return }
		// max bag items // TODO prompt user
		if (this.hasMaxItems()) { return }
		const idx = this.#items.findIndex(x => x.name === item.name)
		if (idx === -1) {
			this.#items.push(newItemInstance(item, count))
			return
		}
		this.#items[idx].increaseCount(count || item.count)
	}

	/**
	 * This method removes an item from the player item array or decreases its count
	 * @param item - The item object that should be removed from the player item array
	 */
	public removeItem(item: Item) {
		const idx = this.#items.findIndex(x => x.name === item.name)
		if (idx === -1) { return }
		// remove from count
		if (item.count >= this.#items[idx].count) {
			this.#items.splice(idx, 1)
			return
		}
		this.#items[idx].decreaseCount(item.count)
		// this.#items[idx].count -= item.count
	}

	/**
	 * This method adds a new monster object into the player monster array
	 * @param monster - A monster object that should be added to the player monster array
	 */
	public addMonster(monster: Monster) {
		if (!(monster instanceof Monster)) { return }
		if (this.#monsters.length === 6) { return }
		this.#monsters.push(monster)
	}

	/**
	 * This method removes a monster object from the player monster array
	 * @param monsterIdx - A number indicating the monster idx that should be removed from the player monster array
	 */
	public removeMonster(monsterIdx: number) {
		this.#monsters.splice(monsterIdx, 1)
	}

	/**
	 * This method switches 2 monsters in position in the player monster array 
	 * @param oldIdx - A number which indicates the old index of a monster in the player monster array
	 * @param newIdx - A number which indicates the new index of a monster in the player monster array
	 */
	public replaceMonsters(oldIdx: number, newIdx: number) {
		const oldMon = this.#monsters[oldIdx]
		const newMon = this.#monsters[newIdx]
		this.#monsters[oldIdx] = newMon
		this.#monsters[newIdx] = oldMon
	}

	/**
	 * This method spawns the player infront of the nearest hospital with regenerated monsters and resets all the boundaries based on new map offset values
	 */
	public hospitalSpawn = () => {
		assertNonNullish(this.sprite.sprites, 'Player sprites not available!')
		this.sprite.image = this.sprite.sprites.down
		this.spriteHead.image = this.spriteHead.sprites.down
		const firstMapID = 20
		// regenerate player monsters
		this.monsters.forEach(monster => monster.regenerate())
		// reset boundaries and offset of current map to initial only if player is not in the first map and
		// is not in the same map as the last visited hospital map
		if (this.#currentMapId !== firstMapID && (this.#currentMapId !== this.#lastHospitalData?.mapID || this.#currentHouseId > 0)) {
			const offset = calcOffset(data.getInitialOffsetByID(this.#currentMapId, this.#currentHouseId))
			// current boundaries position
			getMap().placeAllBoundaries(offset.x, offset.y, getMap().getCorrespondingOffset())
			// current offset
			getMap().setOffset({ x: offset.x, y: offset.y })
			// if player was in house while fainting, reset its currentHouseID
			if (this.#currentHouseId > 0) { this.#currentHouseId = 0 }
		}
		// spawn player in front of last visited hospital or in front of his house in first map, if no hospital visited yet
		const mapID = this.#lastHospitalData?.mapID || firstMapID
		setMap({ mapID })
		// reset player map id
		this.setCurrentMapId(mapID)
		const offset = this.#lastHospitalData?.offset || calcOffset(data.getInitialOffsetByID(firstMapID))
		// boundaries position of new map if last hospital spawn map !== current map
		getMap().placeAllBoundaries(offset.x, offset.y, getMap().getCorrespondingOffset())
		// offset of new map if last hospital spawn map !== current map
		getMap().setOffset({ x: offset.x, y: offset.y })
	}

	/**
	 * This method resets all the monster positions of the player and is basically used in a resize event
	 */
	public resetAllMonsterPos() {
		this.#monsters.forEach(mon => mon.sprite.position = mon.getMonsterPos())
	}

	/**
	 * This method resets the player sprite object position to its initial position
	 */
	public resetPos() {
		const newPos = {
			x: canvas.element.width / 2 - this.sprite.enemyWidth / (viewport.isSmallDevice() ? 2 : 4),
			y: canvas.element.height / 2 - this.sprite.enemyHeight / (viewport.isSmallDevice() ? 2 : 4)
		}
		this.#sprite.position = newPos
		this.#spriteHead.position = newPos
	}

	public showLocationStr() {
		locationStrEl.innerText = getLocationStr(this.#currentMapId)
		locationStrEl.style.display = 'block'
		clearTimeout(this.#locationStrTimeout)
		this.#locationStrTimeout = setTimeout(() => {
			locationStrEl.removeAttribute('style')
			clearTimeout(this.#locationStrTimeout)
		}, 3000)
	}

	/**
	 * This method allows to serialize the Player object for saving purposes
	 * @returns a JSON string for saving progress purposes
	 */
	public toJSON() {
		return {
			name: this.#name,
			monsters: this.#monsters,
			items: this.#items,
			badges: this.#badges,
			monsterDex: this.#monsterDex,
			inventory: this.#inventory,
			satoshis: this.#satoshis,
			playtime: this.#getTimePlayed(),
			location: this.#battlezoneLocation,
			currentHouseId: this.#currentHouseId,
			currentMapId: this.#currentMapId,
			lastHospitalData: this.#lastHospitalData,
		}
	}

	/**
	 * @returns a sprite object for rendering player in battle sequence
	 */
	#initBattleSprite(position: IPosition) {
		// player image in battle sequence
		return new Sprite({
			position,
			image: { src: base64BattleImgs.player },
			scaleFactor: viewport.getScaleFactor(false)
		})
	}

	/**
	 * This method checks if the NPC with the given npcID is appearing and starts the interaction sequence with player
	 * @param npcID - A number representing the ID of the NPC
	 */
	#npcDetectionCheck(npcID: number) {
		const npc = data.getNPCByID({ npcID, mapID: this.currentMapId, houseID: this.currentHouseId }) // getMap().getNPCById(npcID)
		if (npc?.isAppearing({ mapID: this.currentMapId })) {
			npc.detectSequence(false)
		}
	}

	/**
	 * This method plays the collision audio
	 */
	#playCollision() {
		if (!this.#isPlayingCollision) {
			playAudio('collision')
			this.#isPlayingCollision = true
		}
	}

	#getCollidingObjPos({ direction, boundary }: { direction: string, boundary: Boundary }) {
		if (direction === 'right') { return { x: boundary.position.x - getMap().movingByPx, y: boundary.position.y } }
		if (direction === 'left') { return { x: boundary.position.x + getMap().movingByPx, y: boundary.position.y } }
		if (direction === 'down') { return { x: boundary.position.x, y: boundary.position.y - getMap().movingByPx } }
		if (direction === 'up') { return { x: boundary.position.x, y: boundary.position.y + getMap().movingByPx } }
		return { x: 0, y: 0 }
	}

	#handleSpriteDirection({ direction }: { direction: 'up' | 'right' | 'down' | 'left' }) {
		this.sprite.animate = true
		this.sprite.image = this.sprite.sprites[direction]
		this.spriteHead.image = this.spriteHead.sprites[direction]
	}

	#handleMapEntry({ animationID, mapID }: { animationID: number, mapID: number }) {
		playAudio('goOutside')
		// stop map animation
		cancelAnimationFrame(animationID)
		// player map ID
		this.#currentMapId = mapID
		// set player sprites values
		this.sprite.animate = false
		getMap().audio.pause()
		// initialize map object
		setMap({ mapID })
		// start map transition
		animateMapTransition()
	}

	/**
	 * This method calculates the time played
	 * @returns a number indicating the milliseconds played since game start
	 */
	#getTimePlayed() {
		const now = Math.floor(Date.now())
		return (now - this.#startTime) + this.#playTime
	}

}