import { data } from './Data'
import {
	isColliding as wasmColliding,
	isNPCColliding as wasmNPCColliding,
	calculateOverlappingArea as wasmCalcOverlapping
} from '../../public/js/release'
import { Boundary } from './Boundary'
import { Sprite } from './Sprite'
import type { ICollisionArgs, IMovablesArgs, IPosition } from '../models/classes'
import { reaction } from './ReactionQueue'
import { flags } from './Flags'
import { canvas } from './Canvas'
import { battle, player } from '..'
import { playAudio } from '../data/audio'
import { getSavedPlayer } from '../util/localStorage'
import { events } from './Events'
import { animateMapToBattleTransition } from '../animation/map'
import { dialog } from './Dialog'
import { Howl } from 'howler'
import { NPC } from './NPC'
import { Collectable } from './Items/Collectable'
import { visibleItems } from '../data/item/collectables'
import { viewport } from './Viewport'
import { elHasStyleVal, removeArrEntry } from '../util/helper'
import { config } from '../config'

class Movables {

	#mapData
	#mapID
	#houseID
	#animationID
	#background
	#foreground
	#highGrass
	#movingByPixel
	#movables
	#rival
	#prof
	#battlezoneBoundaries
	#interactions: Boundary[] = []
	#collectables: Collectable[] = []
	#pc
	#renderedDetection: Sprite[] = []
	#detectedTrainer = { id: 0, moving: true }
	#audio

	/***** Getter *****/

	/**
	 * @returns the ID of the current map
	 */
	get mapID() { return this.#mapID }
	/**
	 * @returns the ID of the current house
	 */
	get houseID() { return this.#houseID }
	/**
	 * @returns the current X and Y offset of the map
	 */
	get offset() { return this.#mapData.offset }
	/**
	 * @returns all the boundary objects of a map
	 */
	get boundaries() { return this.#mapData.boundaries }
	/**
	 * @returns the amount of pixel that should be added to the movables position object while player moves 
	 */
	get movingByPx() { return this.#movingByPixel }
	/**
	 * @returns an array with all movable objects in it 
	 */
	get movables() { return this.#movables }
	/**
	 * @returns the sprite object of the map background
	 */
	get background() { return this.#background }
	/**
	 * @returns the sprite object of the map foreground
	 */
	get foreground() { return this.#foreground }
	/**
	 * @returns the sprite object of the high grass
	 */
	get highgrass() { return this.#highGrass }
	/**
	 * @returns an array containing all NPC objects (non-trainer & trainer)
	 */
	get npcs() { return this.#mapData.npcs }
	/**
	* @returns an array containing all collectables
	*/
	get collectables() { return this.#collectables }
	/**
	 * @returns all the interaction boundaries
	 */
	get interactions() { return this.#interactions }
	/**
	 * @returns the boundary to interact with the PC-Inventory
	 */
	get pc() { return this.#pc }
	/**
	 * @returns the exclamation mark sprite for trainer detection
	 */
	get renderedDetection() { return this.#renderedDetection }
	/**
	 * returns an object with the id of the trainer that has detected the player and a moving property that indicates if the trainer is currently walking
	 */
	get detectedTrainer() { return this.#detectedTrainer }
	/**
	 * @returns the correspondng audio Howl 
	 */
	get audio() { return this.#audio }

	/***********************/
	/***** CONSTRUCTOR *****/
	/***********************/

	/** This class contains all the movables and the main methods to handle the movable objects */
	public constructor({ mapID, houseID }: IMovablesArgs) {
		this.#mapID = mapID
		this.#houseID = houseID || 0
		this.#mapData = data.getDataByMapID(mapID, houseID)
		this.#animationID = 0
		this.#movingByPixel = config.movingByPx * viewport.getScaleFactor(false)
		this.#background = new Sprite({
			position: { x: this.getCorrespondingOffset().x, y: this.getCorrespondingOffset().y },
			image: { src: `./imgs/maps/map${mapID}/${houseID ? `houses/house${houseID}` : ''}/background.png` },
			scaleFactor: viewport.getScaleFactor(false),
		})
		this.#foreground = new Sprite({
			position: { x: this.getCorrespondingOffset().x, y: this.getCorrespondingOffset().y },
			image: { src: `./imgs/maps/map${mapID}/${houseID ? `houses/house${houseID}` : ''}/foreground.png` },
			scaleFactor: viewport.getScaleFactor(false)
		})
		if (this.#isMapWithHighGrass()) {
			this.#highGrass = new Sprite({
				position: { x: this.getCorrespondingOffset().x, y: this.getCorrespondingOffset().y },
				image: { src: `./imgs/maps/map${mapID}/highGrass.png` },
				scaleFactor: viewport.getScaleFactor(false)
			})
		}
		this.#battlezoneBoundaries = this.boundaries.filter(x => x.id > 244)
		for (let i = 0; i < this.#mapData.boundaries.length; i++) {
			const boundary = this.#mapData.boundaries[i]
			if (boundary.id === 35) { this.#pc = boundary }
			if (boundary.id > 39 && boundary.id < 80) {
				this.#collectables.push(new Collectable({
					id: boundary.id,
					backgroundSrc: { src: './imgs/ball/ballItem.png' },
					foregroundSrc: { src: './imgs/ball/ballItemHead.png' },
					position: { ...boundary.position },
					item: houseID ? visibleItems[mapID].house[houseID][boundary.id] : visibleItems[mapID].map[boundary.id],
					boundary
				}))
			}
			if (boundary.id > 199 && boundary.id < 245) {
				this.#interactions.push(boundary)
			}
		}
		// init the sprite of the NPCs in current map
		this.#initMapNPCImgs()
		this.#movables = [
			this.#background,
			this.#foreground,
			this.#highGrass,
			...this.#mapData.boundaries,
			...this.#mapData.npcs,
			...this.#collectables
		]
		this.#audio = new Howl({ ...this.#mapData.audio })
		this.#rival = data.getNPCByID({ npcID: 106, mapID })
		this.#prof = data.getNPCByID({ npcID: 110, mapID })
	}

	/***********************/
	/******* METHODS *******/
	/***********************/

	/**
	 * This method calls the requestAnimationFrame API and starts the movables animation
	 */
	public animate = () => {
		// console.log({ animationID: this.#animationID })
		this.#animationID = requestAnimationFrame(this.animate)
		// clear canvas
		canvas.ctx?.clearRect(0, 0, canvas.element.width, canvas.element.height)
		// start new Path
		canvas.ctx?.beginPath()
		if (!elHasStyleVal(canvas.element)) { canvas.sharpen() }
		/***** draw objects *****/
		// render map background
		this.background.draw()
		// render player
		player.sprite.draw()
		// render npcs
		for (let i = 0; i < this.npcs.length; i++) {
			const npc = this.npcs[i]
			// dont draw the rival until player goes in the detection area
			if (npc.isRival && !flags.getIsDetected() && !npc.isBeaten) { continue }
			// DEBUG: npc boundary
			if (config.drawBoundaries) { npc.boundary.draw() }
			npc.sprite.draw()
			// move npc if it is an npc that moves around
			if (npc.movesAround && !npc.isOutOfViewport()) {
				npc.sprite.animate = false
				npc.npcMoving = true
				npc.moveAround()
			}
		}
		// render visible collectable items
		for (let i = 0; i < this.#collectables.length; i++) {
			if (this.#collectables[i].isVisible()) {
				this.#collectables[i].background.draw()
			}
		}
		// render high grass layer
		this.highgrass?.draw()
		// render player head
		player.spriteHead.draw()
		if (this.#isMapWithHighGrass()) {
			// render npcs head (for high grass illusion)
			for (let i = 0; i < this.npcs.length; i++) {
				const npc = this.npcs[i]
				if (npc.spriteHead) { npc.spriteHead.draw() }
			}
		}
		// render collectable visible item foreground
		for (let i = 0; i < this.#collectables.length; i++) {
			if (this.#collectables[i].isVisible()) {
				this.#collectables[i].foreground.draw()
			}
		}
		// render map foregrounds
		this.foreground.draw()
		// DEBUG: render collisions boundaries
		if (config.drawBoundaries) {
			for (let i = 0; i < this.boundaries.length; i++) {
				this.boundaries[i].draw()
			}
		}
		// render trainer exclamation mark sprite if available
		if (this.renderedDetection.length > 0) {
			this.renderedDetection[0].draw()
		}
		// check if npc should walk away after conversation or after beeing beaten
		if (this.#rival?.shouldWalkAway) {
			this.#rival.moveAway({ mapID: this.mapID })
			return
		}
		if (this.#prof?.shouldWalkAway) {
			this.#prof.moveAway({ mapID: this.mapID })
			return
		}
		/***** early return if player is interacting *****/ // and no return if player spoke to trainer so we can trigger the trainer detect sequence
		if (flags.getIsInteracting() && !flags.getPlayerSpokeToTrainer()) { return }
		/***** Player movement and collision monitoring *****/
		const moving = true
		player.sprite.animate = false
		// check for wild battlezone collision
		if (!flags.getIsDetected() && this.#isDirectionBtnPressed()) {
			for (let i = 0; i < this.#battlezoneBoundaries.length; i++) {
				const battleZoneBoundary = this.#battlezoneBoundaries[i]
				// calculate how much % player overlaping battlezone area
				const overlappingArea = this.calculateOverlappingArea(player.sprite, battleZoneBoundary)
				// check for battlezone collision
				if (
					!flags.getIsPaused() &&
					this.isColliding({
						rectangle1: player.sprite,
						rectangle2: battleZoneBoundary
					}) &&
					overlappingArea > (player.sprite.enemyWidth * player.sprite.enemyHeight) / 2 &&
					Math.random() < battle.chance
				) {
					// set battle flag to true
					battle.setIsBattleInitiated(true)
					// pause Map sound & play initBattle sound
					this.audio.pause()
					playAudio('wild_battle')
					const wildEnemy = battle.initWildMonster(this.#getWildLvlRange())
					wildEnemy?.loadAudio()
					// cancel current map animation loop
					cancelAnimationFrame(this.#animationID)
					// map to battle sequence transition
					animateMapToBattleTransition({ wildEnemy })
					break
				}
			}
		}
		// check for Trainer NPC fight activation
		if (!flags.getIsDetected() && this.#isDirectionBtnPressed()) {
			for (let i = 0; i < this.npcs.length; i++) {
				const npc = this.npcs[i]
				if (npc.isTrainer && !npc.isBeaten && npc.isDetectingPlayer()) {
					npc.detectSequence(true)
					break
				}
				// check for Non-trainer NPC detection to block player from walking through
				if (!flags.getCanWalkThrough() && !npc.isTrainer && npc.isBlocker && npc.isDetectingPlayer()) {
					npc.blockPlayerFromWalking()
				}
			}
		}
		// Trainer NPC has detected player
		if (flags.getIsDetected()) {
			if (!this.detectedTrainer.moving) { return }
			// get trainer by id because we are outside of the trainer detection loop
			// console.log('this.detectedTrainer.id: ', this.detectedTrainer.id)
			const trainer = data.getNPCByID({ npcID: this.detectedTrainer.id, mapID: player.currentMapId, houseID: player.currentHouseId })
			// console.log('trainer: ', trainer)
			// return if detected npc is not a trainer
			if (!trainer) { return }
			// reset player sprite to standing position
			player.sprite.frames.val = 0
			// once detected by trainer, handle trainer-player sequence after 1 second
			const t = setTimeout(() => {
				if (!this.detectedTrainer.moving) { return }
				if (this.renderedDetection.length > 0) { this.renderedDetection.pop() }
				// let trainer npc walk straight to player
				if (this.#detectedTrainer.id !== 106 && this.#detectedTrainer.id !== 110) {
					trainer.moveStraightToPlayer()
				} else {
					// let rival walk to player
					trainer.moveToPlayer({ mapID: this.mapID })
				}
				// stop trainer once reached player or if player talked to trainer by himself
				if (
					flags.getPlayerSpokeToTrainer() ||
					this.isColliding({
						rectangle1: trainer.sprite,
						rectangle2: new Boundary({
							id: 0,
							position: trainer.getPlayerCollisionPos(),
							width: 48 * viewport.getScaleFactor(false),
							height: 48 * viewport.getScaleFactor(false)
						})
					})) {
					// new battle start, reset flag that avoids skipping last battle dialog 
					flags.setIsEndOfBattle(false)
					// stop npc walk once reached destination
					trainer.changeSpriteDirections(player)
					trainer.sprite.frames.val = 0
					this.detectedTrainer.moving = false
					trainer.sprite.animate = false
					// reset the walk prop that indicates when the rival has reached halfway to player 
					trainer.halfway.reached = false
					// show dialog
					reaction.queue.push(...trainer.firstDialog(trainer))
					reaction.execute()
					// start battle sequence
					if (trainer.isRival || trainer.isTrainer || trainer.isGym) {
						reaction.queue.push(
							() => {
								// reset player spoke to trainer by himself
								flags.setPlayerSpokeToTrainer(false)
								// stop map animation
								cancelAnimationFrame(this.#animationID)
								dialog.hide()
								trainer.audio?.look?.stop()
								if (trainer.isGym) {
									this.#audio.pause()
								}
								trainer.playBattleAudio()
								flags.setIsInteracting(false)
								animateMapToBattleTransition({ trainer })
							}
						)
						// set battle flag to true
						battle.setIsBattleInitiated(true)
						return
					}
				}
				clearTimeout(t)
			}, 1000)
			return
		}
		// check for collisions, change player facing direction and move the movables depending on user input
		player.move({ moving, animationID: this.#animationID, inHouse: this.houseID > 0 })
	}

	public setMovingByPx(val: number) {
		this.#movingByPixel = val
	}

	public playAudio() {
		if (this.#audio.state() === 'unloaded') {
			this.#audio.load()
		}
		this.#audio.play()
	}

	public moveMovables(direction: 'w' | 'a' | 's' | 'd') {
		for (let i = 0; i < this.#movables.length; i++) {
			const movable = this.#movables[i]
			if (movable) {
				if (direction === 'w') {
					if (movable instanceof NPC) {
						movable.sprite.position.y += this.#movingByPixel
						movable.boundary.position.y += this.#movingByPixel
						movable.initialPos.y += this.#movingByPixel
					} else if (movable instanceof Sprite || movable instanceof Boundary) {
						movable.position.y += this.#movingByPixel
					} else if (movable instanceof Collectable) {
						movable.background.position.y += this.#movingByPixel
						movable.foreground.position.y += this.#movingByPixel
					}
				}
				if (direction === 'a') {
					if (movable instanceof NPC) {
						movable.sprite.position.x += this.#movingByPixel
						movable.boundary.position.x += this.#movingByPixel
						movable.initialPos.x += this.#movingByPixel
					} else if (movable instanceof Sprite || movable instanceof Boundary) {
						movable.position.x += this.#movingByPixel
					} else if (movable instanceof Collectable) {
						movable.background.position.x += this.#movingByPixel
						movable.foreground.position.x += this.#movingByPixel
					}
				}
				if (direction === 's') {
					if (movable instanceof NPC) {
						movable.sprite.position.y -= this.#movingByPixel
						movable.boundary.position.y -= this.#movingByPixel
						movable.initialPos.y -= this.#movingByPixel
					} else if (movable instanceof Sprite || movable instanceof Boundary) {
						movable.position.y -= this.#movingByPixel
					} else if (movable instanceof Collectable) {
						movable.background.position.y -= this.#movingByPixel
						movable.foreground.position.y -= this.#movingByPixel
					}
				}
				if (direction === 'd') {
					if (movable instanceof NPC) {
						movable.sprite.position.x -= this.#movingByPixel
						movable.boundary.position.x -= this.#movingByPixel
						movable.initialPos.x -= this.#movingByPixel
					} else if (movable instanceof Sprite || movable instanceof Boundary) {
						movable.position.x -= this.#movingByPixel
					} else if (movable instanceof Collectable) {
						movable.background.position.x -= this.#movingByPixel
						movable.foreground.position.x -= this.#movingByPixel
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param boundaryId 
	 */
	public deleteCollectableItem(boundaryId: number) {
		// remove boundary
		const hiddenItemBoundaryIdx = this.#mapData.boundaries.findIndex(x => x.id === boundaryId)
		removeArrEntry(this.#mapData.boundaries, hiddenItemBoundaryIdx)
		// remove visible sprite if available
		const visibleSpriteIdx = this.#collectables.findIndex(x => x.id === boundaryId)
		removeArrEntry(this.#collectables, visibleSpriteIdx)
	}

	/**
	 * @return the corresponding map offset depending on device size 
	 */
	public getCorrespondingOffset() {
		return viewport.isSmallDevice() ? this.#mapData.offset.smartphone : this.#mapData.offset.desktop
	}

	/**
	 * This method updates the mapOffset object after the player moves 
	 */
	public updateMapOffset() {
		const mapImg = this.#movables[0]
		if (mapImg instanceof Sprite) {
			if (viewport.isSmallDevice()) {
				this.#mapData.offset.smartphone = mapImg.position
				return
			}
			this.#mapData.offset.desktop = mapImg.position

		}
	}

	/**
	 * This method sets the offset data
	 * @param offset - A new offset object
	 */
	public setOffset(offset: IPosition) {
		if (viewport.isSmallDevice()) {
			this.#mapData.offset.smartphone = offset
			return
		}
		this.#mapData.offset.desktop = offset
	}

	/**
	 * This method checks if the player collides with an NPC boundary to execute the NPC interaction
	 */
	public canInteractWithNPC = () => {
		if (flags.getIsInteracting() || battle.isInitiated) { return }
		// clear reaction queue
		reaction.clearQueue()
		for (let i = 0; i < this.#mapData.npcs.length; i++) {
			const npc = this.#mapData.npcs[i]
			// check for npc collision
			if (
				this.isColliding({
					rectangle1: player.sprite,
					rectangle2: new Boundary({
						id: npc.id,
						position: {
							x: npc.sprite.position.x,
							y: npc.sprite.position.y
						},
						width: 48 * viewport.getScaleFactor(false),
						height: 48 * viewport.getScaleFactor(false)
					}),
					withNPC: true
				})
			) {
				// freeze player movement
				player.sprite.frames.val = 0
				player.sprite.animate = false
				// set anti-spam flag
				flags.setIsInteracting(true)
				// freeze npc direction
				npc.sprite.animate = false
				// interact with non-trainer NPC
				if (!npc.isTrainer) {
					if (npc.isGym && !npc.isBeaten) {
						// trigger fight
						flags.setPlayerSpokeToTrainer(true)
						npc.changeSpriteDirections(player, npc)
						npc.detectSequence(true)
						break
					}
					// change sprite direction
					npc.changeSpriteDirections(player)
					// activate npc correspondig dialog
					if (npc.secondDialog && !npc.itemGift) {
						reaction.queue.push(...npc.secondDialog(npc))
						reaction.execute()
						break
					}
					reaction.queue.push(...npc.firstDialog(npc))
					reaction.execute()
					break
				}
				// else: interact with Trainer
				if (!npc.isBeaten) {
					// trigger fight
					flags.setPlayerSpokeToTrainer(true)
					npc.changeSpriteDirections(player, npc)
					npc.detectSequence(true)
					break
				}
				npc.changeSpriteDirections(player)
				if (npc.afterBattleDialog) {
					reaction.queue.push(...npc.afterBattleDialog(npc))
					reaction.execute()
					reaction.queue.push(npc.finishDialog)
				}
				break
			}
			// no collision
		}
	}

	/**
	 * This method calculates if a player sprite position is colliding with any given boundary
	 * @param {rectangle1, rectangle2, withNPC}
	 * - Rectangle1: The player sprite object
	 * - Rectangle2: The boundary object
	 * - withNPC: indicates if the calculation should perfom for a NPC collision
	 * @returns a boolean which indicates if the player is colliding with any given boundary 
	 */
	public isColliding({ rectangle1, rectangle2, withNPC }: ICollisionArgs) {
		return wasmColliding(
			rectangle1.position.x,
			withNPC ? rectangle1.position.y - 30 : rectangle1.position.y,
			rectangle1.enemyWidth,
			withNPC ? rectangle1.enemyHeight + 30 : rectangle1.enemyHeight,
			withNPC ? rectangle2.position.x - 7 : rectangle2.position.x,
			rectangle2.position.y,
			withNPC ? rectangle2.width + 15 : rectangle2.width,
			rectangle2.height,
			viewport.getScaleFactor(false) < 1
		)
	}

	/**
	 * This method calculates if a NPC is colliding with any given boundary
	 * @param {rectangle1, rectangle2}
	 * - Rectangle1: The NPC sprite object
	 * - Rectangle2: The boundary object
	 * @returns a boolean which indicates if the NPC is colliding with any given boundary 
	 */
	public isWalkingNPCColliding({ rectangle1, rectangle2 }: ICollisionArgs) {
		return wasmNPCColliding(
			rectangle1.position.x,
			rectangle1.position.y,
			rectangle1.enemyWidth,
			rectangle1.enemyHeight,
			rectangle2.position.x,
			rectangle2.position.y,
			rectangle2.width,
			rectangle2.height
		)
	}

	/**
	 * This method calculates if the player sprite position is overlapping any battlezone boundary by a certain percentage
	 * @param player - The player sprite object
	 * @param battleZoneBoundary - The battlezone boundary object
	 * @returns a boolean which indicates if the player ist overlapping any battlezone boundary
	 */
	public calculateOverlappingArea(player: Sprite, battleZoneBoundary: Boundary) {
		return wasmCalcOverlapping(
			player.position.x,
			player.enemyWidth,
			battleZoneBoundary.position.x,
			battleZoneBoundary.width,
			player.position.y,
			player.enemyHeight,
			battleZoneBoundary.position.y,
			battleZoneBoundary.height
		)
	}

	/**
	 * @param boundary - the boundary object
	 * @returns a boolean that indicates if the boundary position is far from the player current position
	 * to avoid object initializaion in the collision loop 
	 */
	public isBoundaryFarAway(boundary: Boundary, npc?: NPC) {
		const obj = npc || player
		return (boundary.position.x < obj.sprite.position.x - 96
			||
			boundary.position.x > obj.sprite.position.x + 96) &&
			(boundary.position.y < obj.sprite.position.y - 96
				||
				boundary.position.y > obj.sprite.position.y + 96)
	}

	/**
	 * @param id - The boundary ID
	 * @returns a boolean that indicates if the boundary is a collision or if the player can walk through it
	 */
	public isBoundaryIDCollision(id: number) {
		return id < 40 || (id > 199 && id < 245) || (id > 59 && id < 80)
	}

	/**
	 * This method replaces the map-offset and re-renders the new placed movables
	 * @param x - The X coordinates of the map offset
	 * @param y - The Y coordinates of the map offset
	 * @param oldOffset - The offset before resizing
	 */
	public placeAllBoundaries(x: number, y: number, oldOffset: IPosition, updateNPCStaticPos = false) {
		const offsetXDiff = x - oldOffset.x
		const offsetYDiff = y - oldOffset.y
		for (let i = 0; i < this.#movables.length; i++) {
			const movable = this.#movables[i]
			// place map image
			if (movable instanceof Sprite) {
				movable.position = { x, y }
			}
			// place boundaries
			if (movable instanceof Boundary) {
				movable.position = {
					x: movable.position.x + offsetXDiff,
					y: movable.position.y + offsetYDiff
				}
			}
			// place npcs
			if (movable instanceof NPC) {
				data.updateNPCPos(movable, { x: offsetXDiff, y: offsetYDiff }, updateNPCStaticPos)
			}
			// place collectable items
			if (movable instanceof Collectable) {
				movable.background.position = {
					x: movable.background.position.x + offsetXDiff,
					y: movable.background.position.y + offsetYDiff
				}
				movable.foreground.position = {
					x: movable.foreground.position.x + offsetXDiff,
					y: movable.foreground.position.y + offsetYDiff
				}
			}
		}
	}

	/**
	 * @param boundaryID - The boundary ID
	 * @returns a boolean that indicates if the boundary id represents an entry into a map
	 */
	public isMapEntry(boundaryID: number) {
		return boundaryID > 19 && boundaryID < 30
	}

	/**
	 * @param boundaryID - The boundary ID
	 * @returns a boolean that indicates if the boundary id represents an entry into a house
	 */
	public isHouseEntry(boundaryID: number) {
		return boundaryID > 4 && boundaryID < 20
	}

	/***************************/
	/***** Private Methods *****/
	/***************************/

	#initMapNPCImgs() {
		for (let i = 0; i < this.#mapData.npcs.length; i++) {
			const npc = this.#mapData.npcs[i]
			if (npc.spritesLoaded) { continue }
			npc.sprite = new Sprite({
				position: npc.sprite.position,
				image: npc.image,
				frames: npc.frames,
				sprites: npc.sprites,
				animate: npc.animate,
				scaleFactor: viewport.getScaleFactor(false)
			})
			npc.spriteHead = npc.headImgs ? new Sprite({
				position: npc.spriteHead?.position || npc.sprite.position,
				image: npc.headImgs[npc.image.src.substring(npc.image.src.lastIndexOf('/') + 1, npc.image.src.lastIndexOf('.')) as 'down' | 'up' | 'left' | 'right'],
				frames: npc.frames,
				sprites: {
					up: npc.headImgs.up,
					right: npc.headImgs.right,
					down: npc.headImgs.down,
					left: npc.headImgs.left
				},
				scaleFactor: viewport.getScaleFactor(false)
			}) : undefined
			npc.battleSprite = new Sprite({
				image: npc.battleSpriteSrc,
				position: npc.getBattleTrainerPos(),
				scaleFactor: viewport.getTrainerScaleFactor()
			})
			npc.spritesLoaded = true
		}
	}

	/**
	 * @returns a boolean that indicates if the current map has high grass battlezones
	 */
	#isMapWithHighGrass() {
		return this.#houseID === 0 &&
			(
				this.#mapID === 20 ||
				this.#mapID === 22 ||
				this.#mapID === 23
			)
	}

	/**
	 * @returns a boolean that indicates if the user is pressing any of the 4 direction buttons
	 */
	#isDirectionBtnPressed() {
		return events.keys.w.pressed || events.keys.a.pressed || events.keys.s.pressed || events.keys.d.pressed
	}

	/**
	 * @returns an array of 2 numbers indicating the range of a wild monster level determined by the map ID 
	 */
	#getWildLvlRange() {
		const ranges: { [key: string]: number[] } = {
			// map 20
			20: [2, 5],
			// map 21
			21: [3, 6],
			// map 22
			22: [3, 6],
			// map 23
			23: [5, 8]
		}
		return config.wildLvl.use ? config.wildLvl.range : ranges[this.#mapID]
	}

}

let map = new Movables({ mapID: getSavedPlayer().currentMapId, houseID: getSavedPlayer().currentHouseId })

export function getMap() { return map }

export function setMap({ mapID, houseID }: IMovablesArgs) { map = new Movables({ mapID, houseID }) }