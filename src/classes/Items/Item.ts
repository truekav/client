import type { IItemArgs } from '../../models/classes'
import type { Monster } from '../Monster'

const itemPrices: { [key: string]: number } = {
	Ball: 200,
	GigaBall: 300,
	TeraBall: 450,
	PetaBall: 600,
	ExaBall: 950,
	Potion: 300,
	GigaPotion: 500,
	TeraPotion: 750,
	PetaPotion: 1000,
	ExaPotion: 1200
}

export abstract class Item {
	protected _name
	protected _count
	protected _price

	/** @returns The name of the item */
	get name() { return this._name }
	/** @returns The count of an item */
	get count() { return this._count }
	/** @returns The price of an item */
	get price() { return this._price }

	/**
	 * This class contains the main methods for the player items
	 */
	public constructor({
		name,
		count,
		price
	}: IItemArgs) {
		this._name = name.toUpperCase()
		this._count = count
		this._price = price || itemPrices[name]
	}

	/**
	 * This method handles the item usage
	 * @param monster - the monster object for which the item is destinated or undefined if the item usage is not for a player monster
	 */
	public abstract use(monster?: Monster): void

	/**
	 * This method increases the count of an item by a given number 
	 * @param count - a number that indicates the item count to increase by
	 */
	public increaseCount(count: number) {
		this._count += count
	}

	/**
	 * This method decreases the count of an item by a given number 
	 * @param count - a number that indicates the item count to decrease by
	 */
	public decreaseCount(count: number) {
		this._count -= count
	}

	/**
	 * This method allows to serialize the Item object for saving purposes
	 * @returns a JSON string for saving progress purposes
	 */
	public toJSON() {
		return { count: this._count, name: this._name, price: this._price }
	}
}
