import { IItemArgs } from '../../models/classes'
import { Monster } from '../Monster'
import { Item } from './Item'

export class RareCandy extends Item {
	/**
	 * This class contains the main methods for the player item -> balls
	 */
	public constructor(args: IItemArgs) { super(args) }
	/**
	 * This method handles the item usage
	 */
	public use(monster: Monster) {
		this.decreaseCount(1)
		monster.increaseLvl(1)
		monster.increaseStats()
		monster.setInitialXPOfLvl()
	}

	/**
	* This method allows to serialize the Item object for saving purposes
	* @returns a JSON string for saving progress purposes
	*/
	public toJSON() {
		return { count: this._count, name: this._name, price: this._price }
	}
}