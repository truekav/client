import { ICollectableArgs } from '../../models/classes'
import { Sprite } from '../Sprite'
import { viewport } from '../Viewport'

export class Collectable {
	#id
	#background
	#foreground
	#item
	#boundary

	get id() { return this.#id }
	get background() { return this.#background }
	get foreground() { return this.#foreground }
	get item() { return this.#item }
	get boundary() { return this.#boundary }

	constructor({
		id,
		backgroundSrc,
		foregroundSrc,
		position,
		item,
		boundary
	}: ICollectableArgs) {
		this.#id = id
		this.#background = new Sprite({
			image: backgroundSrc,
			position: { ...position },
			scaleFactor: viewport.getScaleFactor(false)
		})
		this.#foreground = new Sprite({
			image: foregroundSrc,
			position: { ...position },
			scaleFactor: viewport.getScaleFactor(false)
		})
		this.#item = item
		this.#boundary = boundary
	}

	public isVisible() {
		// IDs from 40-59 are hidden items
		return this.#id > 59 && this.#id < 80
	}

}