import type { IItemArgs } from '../../models/classes'
import { Item } from './Item'

export class Ball extends Item {
	protected _catchProbability = 255
	/**
	 * This class contains the main methods for the player item -> balls
	 */
	public constructor(args: IItemArgs) { super(args) }
	/**
	 * This method handles the item usage
	 */
	public use() { this.decreaseCount(1) }

	/**
	* This method allows to serialize the Item object for saving purposes
	* @returns a JSON string for saving progress purposes
	*/
	public toJSON() {
		return { count: this._count, name: this._name, price: this._price, catchProbability: this._catchProbability }
	}
}
// export class SuperBall extends Ball { protected _catchProbability = 200 }
// export class MegaBall extends Ball { protected _catchProbability = 175 }
export class GigaBall extends Ball { protected _catchProbability = 200 }
export class TeraBall extends Ball { protected _catchProbability = 150 }
export class PetaBall extends Ball { protected _catchProbability = 100 }
export class ExaBall extends Ball { protected _catchProbability = 50 }