import type { IItemArgs } from '../../models/classes'
import { Item } from './Item'
import { Monster } from '../Monster'

// LOGIC
export class Potion extends Item {
	protected _recoverHP: number
	/**
	 * This class contains the main methods for the player item Potion
	 */
	public constructor(args: IItemArgs, recoverHP = 20) {
		super(args)
		this._recoverHP = recoverHP
	}

	/**
	 * This method handles the item usage
	 * @param monster - the monster object for which the item is destinated or undefined if the item usage is not for a player monster
	 */
	public use(monster?: Monster) {
		if (monster) {
			monster.increaseHP(this._recoverHP)
			this.decreaseCount(1)
		}
	}
	/**
	* This method allows to serialize the Item object for saving purposes
	* @returns a JSON string for saving progress purposes
	*/
	public toJSON() {
		return { count: this._count, name: this._name, price: this._price, recoverHP: this._recoverHP }
	}
}
// export class SuperPotion extends Potion { protected _recoverHP = 40 }
// export class MegaPotion extends Potion { protected _recoverHP = 60 }
export class GigaPotion extends Potion { protected _recoverHP = 40 }
export class TeraPotion extends Potion { protected _recoverHP = 60 }
export class PetaPotion extends Potion { protected _recoverHP = 100 }
export class ExaPotion extends Potion { protected _recoverHP = Number.MAX_VALUE }