import { MonsterNames } from './data/monster/allMonsters'

/**
 * A config object with values changed regulary and used to DEBUG faster
 */
export const config = {						// PRODUCTION:		// DEBUG:			// DESCRIPTION:
	movingByPx: 3,							// 3				// any number		// indicates the player "walking speed"	
	npcMovingByPx: 3,						// 3				// any number		// indicates the NPCs "walking speed"
	drawBoundaries: false,					// false 			// true 			// draws the collision boundaries into the canvas
	walkThroughBoundaries: false,			// false			// true				// disable the player movement collision detection
	battleChance: .009, 					// .009				// .1				// starts a wild battle more frequently
	playerMapID: 20,						// 20				// any map ID		// starts the game animation in a given map id
	playerHouseID: 5,						// 5				// any house ID		// set to 0 if you want to start outside of house in any map ID
	withIntro: true,						// true				// boolean			// starts the game with/without intro sequence
	withBattleZone: false,					// false			// boolean			// starts the game with a battlezone directly in front of house in map 20
	withProf: true,							// true				// boolean			// starts the game with professor interaction
	withRival: true,						// true				// boolean			// starts the game with rival interaction
	withBlockingNPCs: true,					// true				// boolean			// starts the game with the npcs that usually block player from walking to some location
	playerWithMonster: false,					// false			// boolean			// player starts the game with debug monsters declared in "src/util/localStorage.ts"
	playerMonster: [						// -				// {name, lv}		// starting monster of player and is only used if "playerWithMonster" is set to true
		{
			name: MonsterNames.Sheely,
			lv: 30 // test evolution by winning a wild battle while making this value >= than monster property "transformLvl"
		},
		// {
		// 	name: MonsterNames.Emby,
		// 	lv: 7
		// },
		// {
		// 	name: MonsterNames.Sheely,
		// 	lv: 7
		// },
	],
	dex: {
		available: false,					// false			// true				// indicates if the player already starts with a monster-dex 
		isBroken: true,						// true				// false			// "
		entries: [							// []				// Dex-entry obj	// adds an entry to the monster-dex
			// {
			// 	idx: MonsterNames.Draggle,
			// },
		] as { idx: number }[]
	},
	wildMonster: [							// []				// MonsterName[]	// starts a wild battle in zone 255 with a given monster only in the first map-ID 20
		// MonsterNames.Scatterbug,
	],
	wildLvl: {								// {use: false}		// {use: true} 		// determines the wild monster level range
		use: false,
		range: [10, 20]
	},
}