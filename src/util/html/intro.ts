import gsap from 'gsap'
import { player } from '../..'
import { data } from '../../classes/Data'
import { dialog } from '../../classes/Dialog'
import { events } from '../../classes/Events'
import { flags } from '../../classes/Flags'
import { getMap, setMap } from '../../classes/Movables'
import { reaction } from '../../classes/ReactionQueue'
import { mapAudio, playAudio } from '../../data/audio'
import { isValidName } from '../helper'
import { hasProgress, removeFromLocalStorage } from '../localStorage'
import {
	confirmNewGame,
	controlsOverviewWrap,
	creditsPage,
	disclaimerPage,
	exampleMon1,
	exampleMon2,
	introSequence,
	introWrap,
	nameError,
	noNewGame,
	optionsPage,
	playerNameInput,
	playerNameWrap,
	profIntroWrap,
	rivalNameError,
	rivalNameInput,
	rivalNameWrap,
	submitName,
	submitRivalName,
	yesNewGame
} from './elements'

export function initDisclaimer() {
	// wrapper
	const wrap = document.createElement('div')
	wrap.classList.add('sequenceWrap')
	wrap.style.padding = '12px'
	const header = document.createElement('span')
	header.innerText = 'DISCLAIMER'
	header.style.margin = '0 0 2px 0'
	const txt0 = document.createElement('p')
	txt0.className = 'disclaimerTxt txtCenter'
	txt0.innerHTML = 'AGROLAND is currently in the DEMO-PHASE, so you can expect some functionality that is not working or even an entire CRASH of the program.'
	const txt1 = 'The following program contains satirical depictions about poeple involved in the cryptocurrency industry or in the BITCOIN ecosystem. These stereotypes and some dialogues might be seen as wrong.'
	const txt2 = 'All characters appearing in this work are entirely fictional. Any resemblance to real persons, living or dead, is purely coincidental.'
	const txt3 = 'This game is not a financial advice.'
	// Rather than remove this content, I want to acknowledge the harmfull impact it might have on some individuals, learn from it and spark conversation to create a more inclusive future together.
	const okBtn = document.createElement('button')
	okBtn.classList.add('okBtn')
	okBtn.innerText = 'O.K!'
	okBtn.onclick = () => {
		if (txt0.innerText.includes('AGROLAND is currently in the DEMO-PHASE')) {
			txt0.innerText = txt1
			return
		}
		if (txt0.innerText.includes('The following program contains')) {
			txt0.innerText = txt2
			return
		}
		if (txt0.innerText.includes('All characters appearing')) {
			txt0.innerText = txt3
			return
		}
		if (txt0.innerText.includes('This game is not a financial advice.')) {
			okBtn.remove()
			events.lastFocusEl = null
			mapAudio.openingDemoFirst.stop()
			playAudio('openingDemoSecond')
			gsap.to(disclaimerPage, {
				opacity: 0,
				duration: 2,
				onComplete: () => {
					disclaimerPage.remove()
					initIntro()
				}
			})
		}
	}
	wrap.appendChild(header)
	wrap.appendChild(txt0)
	disclaimerPage.appendChild(wrap)
	disclaimerPage.style.display = 'flex'
	gsap.to(disclaimerPage, {
		opacity: 1,
		duration: 1,
		onComplete: () => {
			gsap.to(wrap, {
				opacity: 1,
				duration: 2,
				onComplete: () => {
					wrap.appendChild(okBtn)
					okBtn.focus()
					events.lastFocusEl = okBtn
				}
			})
		}
	})
}

export function initIntro() {
	// wrapper
	const wrap = document.createElement('div')
	wrap.classList.add('sequenceWrap')
	const imgWrap = document.createElement('div')
	imgWrap.classList.add('introImgWrap')
	// init game logo image
	const gameLogoImg = document.createElement('img')
	gameLogoImg.src = './imgs/appLogo.png'
	gameLogoImg.classList.add('gameLogo')
	// init demo txt
	const demoTxt = document.createElement('p')
	demoTxt.classList.add('demoTxt')
	demoTxt.innerText = 'DEMO'
	imgWrap.appendChild(gameLogoImg)
	imgWrap.appendChild(demoTxt)
	// init version txt
	// const versionTxt = document.createElement('p')
	// versionTxt.innerText = 'version 0.0.1'
	// versionTxt.classList.add('versionTxt')
	// init start journey btn
	const clickHere = document.createElement('p')
	clickHere.id = 'pressAnyBtn'
	clickHere.innerText = 'PRESS ANY BUTTON'
	clickHere.classList.add('clickHere')
	// maintainer
	const maintainer = document.createElement('p')
	maintainer.classList.add('maintainer')
	maintainer.innerHTML = 'THE POKÉMON CLONE FOR BROWSERS CREATED WITH PASSION BY <a href="https://gitlab.com/FirstTerraner" target="_blank">AGRON KADRIAJ</a>'
	// start journey click event
	clickHere.onclick = () => {
		clickHere.remove()
		initGameMenu(imgWrap)
	}
	// appen elements to wrap 
	wrap.appendChild(imgWrap)
	// wrap.appendChild(demoTxt)
	// wrap.appendChild(versionTxt)
	wrap.appendChild(maintainer)
	// append all
	introSequence.appendChild(wrap)
	introSequence.style.display = 'flex'
	gsap.to(introSequence, {
		opacity: 1,
		duration: 1,
		onComplete: () => {
			gsap.to(wrap, {
				opacity: 1,
				duration: 2,
				onComplete: () => {
					demoTxt.insertAdjacentElement('afterend', clickHere)
					let displayed = true
					setInterval(() => {
						if (displayed) {
							clickHere.style.visibility = 'hidden'
							displayed = false
							return
						}
						clickHere.style.visibility = 'visible'
						displayed = true
					}, 500)
				}
			})
		}
	})
}

export function initGameMenu(wrap: HTMLElement) {
	const gameMenuWrap = document.createElement('div')
	gameMenuWrap.id = 'gameMenuWrap'
	gameMenuWrap.classList.add('gameMenuWrap')
	// continue game
	const continueGameBtn = document.createElement('p')
	continueGameBtn.id = 'continueSavedGameBtn'
	if (hasProgress()) {
		continueGameBtn.tabIndex = 0
		continueGameBtn.innerText = 'CONTINUE GAME'
		continueGameBtn.className = 'gameMenuOpts pointer'
		gameMenuWrap.appendChild(continueGameBtn)
		continueGameBtn.onclick = () => continueGame()
		continueGameBtn.onkeydown = e => continueGame(e)
	}
	// new game
	const newGameBtn = document.createElement('p')
	newGameBtn.id = 'startNewGameBtn'
	newGameBtn.tabIndex = 0
	newGameBtn.innerText = 'NEW GAME'
	newGameBtn.className = 'gameMenuOpts pointer'
	gameMenuWrap.appendChild(newGameBtn)
	newGameBtn.onclick = () => initNewGame()
	newGameBtn.onkeydown = e => initNewGame(e)
	// options
	const optionsBtn = document.createElement('p')
	optionsBtn.id = 'introOptionsBtn'
	optionsBtn.tabIndex = 0
	optionsBtn.innerText = 'OPTIONS'
	optionsBtn.className = 'gameMenuOpts pointer'
	gameMenuWrap.appendChild(optionsBtn)
	optionsBtn.onclick = () => openOptions()
	optionsBtn.onkeydown = e => openOptions(e)
	// source code
	// const srcCodeBtnWrap = document.createElement('p')
	// const srcCodeBtn = document.createElement('a')
	// srcCodeBtn.tabIndex = 0
	// srcCodeBtn.classList.add('gameMenuOpts')
	// srcCodeBtn.innerText = 'SOURCE CODE'
	// srcCodeBtn.href = 'https://gitlab.com/agroland/client'
	// srcCodeBtn.target = '_blank'
	// srcCodeBtn.onclick = () => playMapAudio('pressab')
	// srcCodeBtnWrap.appendChild(srcCodeBtn)
	// gameMenuWrap.appendChild(srcCodeBtnWrap)
	// credits
	const creditsBtn = document.createElement('p')
	creditsBtn.id = 'introCreditsBtn'
	creditsBtn.tabIndex = 0
	creditsBtn.innerText = 'CREDITS'
	creditsBtn.className = 'gameMenuOpts pointer'
	gameMenuWrap.appendChild(creditsBtn)
	creditsBtn.onclick = () => openCredits()
	creditsBtn.onkeydown = e => openCredits(e)
	wrap.appendChild(gameMenuWrap)
	const t = setTimeout(() => {
		const nextFocus = hasProgress() ? continueGameBtn : newGameBtn
		nextFocus.focus()
		events.lastFocusEl = nextFocus
		clearTimeout(t)
	}, 100)
}

export function submitPlayerName() {
	const inputVal = playerNameInput.value
	if (!isValidName(inputVal)) {
		nameError.style.color = 'red'
		nameError.innerText = 'Invalid name!'
		return
	}
	submitName.remove()
	player.setName(inputVal)
	gsap.to(playerNameWrap, {
		opacity: 0,
		duration: 1,
		onComplete: () => {
			events.shouldDisplayTouchCtrls()
			playerNameWrap.remove()
			const txt = `ALLRIGHT ${player.name}! Press ${events.isTouchDevice() ? 'the "A" button' : '"ENTER/SPACE"'} to skip the DIALOG...`
			dialog.show(txt)
			reaction.queue.push(
				() => {
					controlsOverviewWrap.style.display = 'flex'
					dialog.show('Here is a quick overview about the controls if you are playing with a keyboard.')
				},
				() => {
					controlsOverviewWrap.style.display = 'none'
					dialog.show('If you are using a touch device, you can simply use the touch-controls...')
				},
				() => {
					profIntroWrap.style.display = 'flex'
					flags.setIsAnimating(true)
					gsap.to(profIntroWrap, {
						opacity: 1,
						duration: 1,
						onComplete: () => flags.setIsAnimating(false)
					})
					dialog.show('WELCOME to AGROLAND!')
				},
				() => dialog.show('My name is PROF. ANTOPOULOS and I will guide you through this world...'),
				() => dialog.show('AGROLAND is an OPEN-SOURCE POKÉMON CLONE...'),
				() => dialog.show(`Anyone can help improve the SOURCE CODE! Yes, you as well, ${player.name}!`),
				() => dialog.show('You can even contribute with no code experience at all!'),
				() => dialog.show('Make sure to have a look in the following LINK if you are interested...'),
				() => dialog.updateInnerHTML('<a href="https://gitlab.com/agroland/client#contributing" target="_blank">https://gitlab.com/agroland/client#contributing</a>'),
				// () => dialog.show('Anyway, if you don\'t know what POKÉMON is, I will explain this game in a little more detail.'),
				// () => dialog.show('You should explore every corner of the map, walk into buildings, interact with villagers...'),
				// () => dialog.show('Read the signs, collect visible and hidden ITEMs that will help you through your journey...'),
				// () => dialog.show('You will also have to solve riddles. Hints and tips are placed almost everywhere!'),
				() => {
					flags.setIsAnimating(true)
					gsap.to(exampleMon1, {
						opacity: 1,
						duration: .7
					})
					gsap.to(exampleMon2, {
						opacity: 1,
						duration: .7,
						onComplete: () => flags.setIsAnimating(false)
					})
					dialog.show('This world is full of creatures that we simply call MONSTER...')
				},
				() => dialog.show('On your road, you might encounter WILD MONSTER that will attack you...'),
				() => dialog.show('With some luck, you will be able to CATCH & TRAIN them!'),
				() => dialog.show('They get bigger and stronger with every battle you win!'),
				() => dialog.show('Your path will be a long one full of obstacles, so be prepaired to become the best TRAINER!'),
				() => dialog.show('Prove your skills against other TRAINER and earn BADGES by beating the GYM-LEADERs...'),
				() => {
					flags.setIsAnimating(true)
					gsap.to(profIntroWrap, {
						opacity: 0,
						duration: 1.5,
						onComplete: () => {
							profIntroWrap.style.display = 'none'
							flags.setIsAnimating(false)
						}
					})
					dialog.show('Ohh... And I almost forgot...')
				},
				() => {
					flags.setIsAnimating(true)
					rivalNameWrap.style.display = 'flex'
					dialog.show('A RIVAL gives you some great competition... What\'s his NAME again?')
					events.hideTouchCtrls()
					rivalNameInput.focus()
				},
			)
		}
	})
}

export function submitRival() {
	const inputVal = rivalNameInput.value
	if (!isValidName(inputVal)) {
		rivalNameError.style.color = 'red'
		rivalNameError.innerText = 'Invalid name!'
		return
	}
	submitRivalName.remove()
	data.updateRivalName(inputVal.toUpperCase())
	const rival = data.getNPCByID({ npcID: 106, mapID: 20 })
	if (!rival) { return }
	gsap.to(rivalNameWrap, {
		opacity: 0,
		duration: 1,
		onComplete: () => {
			events.shouldDisplayTouchCtrls()
			rivalNameWrap.remove()
			dialog.show(`Oh yes! His name was ${rival.name}!`)
			profIntroWrap.style.display = 'flex'
			exampleMon1.style.display = 'none'
			exampleMon2.style.display = 'none'
			profIntroWrap.style.opacity = '1'
			flags.setIsAnimating(false)
			reaction.queue.push(
				() => dialog.show(`${rival.name} has the same goals as you and wants to be the NUMBER 1 TRAINER!`),
				() => dialog.show('You might encounter him multiple times on your way to the MONSTER-LEAGUE!'),
				() => dialog.show('Anyway! It\'s time to fall down the RABBIT HOLE into AGROLAND!'),
				() => dialog.show(`GOOD LUCK, ${player.name}!`),
				() => {
					events.lastFocusEl = null
					dialog.hide()
					gsap.to(profIntroWrap, {
						opacity: 0,
						duration: 2,
						onComplete: () => {
							profIntroWrap.remove()
							mapAudio.opening.stop()
							gsap.to(introWrap, {
								opacity: 0,
								duration: 2,
								onComplete: () => {
									getMap().playAudio()
									introWrap.remove()
								}
							})
							startGame(true)
						}
					})
				}
			)
		}
	})
}

playerNameInput.oninput = () => {
	nameError.style.color = 'black'
	nameError.innerText = '2-8 chars. No special chars.'
}

rivalNameInput.oninput = () => {
	if (dialog.isShowing()) { dialog.hide() }
	rivalNameError.style.color = 'black'
	rivalNameError.innerText = '2-8 chars. No special chars.'
}

submitName.onclick = submitPlayerName

submitRivalName.onclick = submitRival

noNewGame.onclick = () => {
	flags.setIsAnimating(false)
	confirmNewGame.style.display = 'none'
	const btn = document.getElementById('continueSavedGameBtn')
	btn?.focus()
	events.lastFocusEl = btn
}

yesNewGame.onclick = () => {
	introWrap.style.backgroundColor = '#fafafa'
	introSequence.style.display = 'none'
	removeFromLocalStorage('player')
	removeFromLocalStorage('mapdata')
	removeFromLocalStorage('canvas')
	gsap.to(confirmNewGame, {
		opacity: 0,
		duration: 1,
		onComplete: () => {
			confirmNewGame.remove()
			startNewGame()
		}
	})
}

function startNewGame() {
	events.hideTouchCtrls()
	introWrap.style.backgroundColor = '#fafafa'
	mapAudio.openingDemoSecond.stop()
	playAudio('opening')
	optionsPage.style.display = 'none'
	creditsPage.style.display = 'none'
	gsap.to(introSequence, {
		opacity: 0,
		duration: 1.5,
		onComplete: () => {
			introSequence.remove()
			// reset flag that avoided double click on new game or continue game btns
			flags.setIsAnimating(false)
			playerNameWrap.style.opacity = '0'
			playerNameWrap.style.display = 'flex'
			playerNameInput.focus()
			gsap.to(playerNameWrap, {
				opacity: 1,
				duration: 1
			})
		}
	})
}

function startGame(resetPlayer?: boolean) {
	if (resetPlayer) {
		player.resetPlayerToInitial()
	}
	// data.setMapData()
	setMap({
		mapID: player.currentMapId,
		houseID: player.currentHouseId > 0 ? player.currentHouseId : undefined
	})
	events.resizeSavedCanvas()
	// start animation
	getMap().animate()
}

function continueGame(e?: KeyboardEvent) {
	if (e && e.key !== 'Enter' && e.key !== ' ') {
		return
	}
	events.lastFocusEl = null
	// avoid double click
	if (flags.getIsAnimating()) { return }
	flags.setIsAnimating(true)
	mapAudio.openingDemoSecond.stop()
	gsap.to(introSequence, {
		opacity: 0,
		duration: 2,
		onComplete: () => {
			gsap.to(introWrap, {
				opacity: 0,
				duration: 1,
				onComplete: () => {
					flags.setIsAnimating(false)
					getMap().playAudio()
					introWrap.remove()
				}
			})
			introSequence.remove()
		}
	})
	startGame()
}

function initNewGame(e?: KeyboardEvent) {
	if (e && e.key !== 'Enter' && e.key !== ' ') {
		return
	}
	events.lastFocusEl = null
	// avoid double click
	if (flags.getIsAnimating()) { return }
	flags.setIsAnimating(true)
	if (!hasProgress()) {
		startNewGame()
		return
	}
	console.log('confirm new game window')
	confirmNewGame.style.display = 'flex'
	const noBtn = document.getElementById('noNewGame')
	const t = setTimeout(() => {
		noBtn?.focus()
		clearTimeout(t)
	}, 50)
	events.lastFocusEl = noBtn
}

function openOptions(e?: KeyboardEvent) {
	if (e && e.key !== 'Enter' && e.key !== ' ') {
		return
	}
	optionsPage.style.display = 'block'
	events.lastFocusEl = null
}

function openCredits(e?: KeyboardEvent) {
	if (e && e.key !== 'Enter' && e.key !== ' ') {
		return
	}
	creditsPage.style.display = 'block'
	// focus first credit link
	const firstLink = document.getElementsByClassName('linkFocus')?.[0] as HTMLLinkElement | null
	const t = setTimeout(() => {
		firstLink?.focus()
		events.lastFocusEl = firstLink
		clearTimeout(t)
	}, 50)

}