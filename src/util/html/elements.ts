import { focusFirstChildAfterTimeout, getElByQuery } from '../helper'

export const introWrap = getElByQuery('#intro')
export const disclaimerPage = getElByQuery('#disclaimerPage')
export const introSequence = getElByQuery('#introSequence')
export const creditsPage = getElByQuery('#creditsPage')
export const optionsPage = getElByQuery('#optionsPage')
export const playerNameWrap = getElByQuery('#playerNameWrap')
export const playerNameInput = getElByQuery<HTMLInputElement>('#enterPlayerName')
export const nameError = getElByQuery('#nameError')
export const submitName = getElByQuery('#submitName')
export const profIntroWrap = getElByQuery('#profIntroWrap')
export const exampleMon1 = getElByQuery('#exampleMon1')
export const exampleMon2 = getElByQuery('#exampleMon2')
export const controlsOverviewWrap = getElByQuery('#controlsOverviewWrap')
export const rivalNameWrap = getElByQuery('#rivalNameWrap')
export const rivalNameInput = getElByQuery<HTMLInputElement>('#enterRivalName')
export const rivalNameError = getElByQuery('#rivalNameError')
export const submitRivalName = getElByQuery('#submitRivalName')
export const confirmNewGame = getElByQuery('#confirmNewGame')
export const yesNewGame = getElByQuery('#yesNewGame')
export const noNewGame = getElByQuery('#noNewGame')
export const pauseMenuEl = getElByQuery<HTMLDivElement>('#pauseMenu')
// location window
export const locationStrEl = getElByQuery('#locationWindow')
// pokedex
export const dexListEl = getElByQuery('#dexList')
export const dexEntryEl = getElByQuery('#monsterDexEntry')
// Battle User Interface
export const UIWrap = getElByQuery('#UIWrap')
// Enemy Trainer Info Section
export const enemyTrainerInfoEl = getElByQuery('#enemyTrainerInfoSection')
// Enemy monster Info Section
export const enemyMonInfoEl = getElByQuery('#enemyInfoSection')
// Player Info Section
export const playerInfoEl = getElByQuery('#playerInfoSection')
// Player Monster Info Section
export const monsterInfoEl = getElByQuery('#playerMonsterInfoSection')
// Monster List Menu
export const monsterListEl = getElByQuery('#monsterList')
// Player Monster Attacks Section
export const attacksSection = getElByQuery('#attacksMenu')
// Player Monster Level
export const monsterLvlEl = getElByQuery('#playerMonsterLvl')
// Player Monster HP Text
export const monHPTxtEl = getElByQuery('#playerHpTxt')
// Player Monster HP Bar
export const playerMonsterHPBar = getElByQuery<HTMLProgressElement>('#playerHealthBar')
// Player Monster XP Bar
export const monXPBar = getElByQuery<HTMLProgressElement>('#monsterXP')
// Player Monster Stats Section
export const monsterStatsEl = getElByQuery('#monsterStats')
// Item List Menu
export const itemListEl = getElByQuery('#itemList')
// status page
export const monsterStatusEl = getElByQuery('#monsterStatus')
export const statusName2El = getElByQuery('#statusName2')
// status HP
export const statusHPEl = getElByQuery('#statusHP')
// status HP progress bar
export const statusHPBarEl = getElByQuery('#statusHPBar')
// status monster ID
export const statusIDEl = getElByQuery('#statusID')
// status XP
export const statusXPEl = getElByQuery('#statusXP')
// status XP progress bar
export const statusXPBar = getElByQuery('#statusXPBar')
// status OT
export const statusOTEl = getElByQuery('#statusOT')
export const statusUIDEl = getElByQuery('#statusUID')
export const statusItemEl = getElByQuery('#statusItem')
// statusType
export const statusTypeEl = getElByQuery('#statusType')
// statusATT
export const statusATTEl = getElByQuery('#statusATT')
// statusDEF
export const statusDEFEl = getElByQuery('#statusDEF')
// statusINI
export const statusINIEl = getElByQuery('#statusINI')
// statusSPATT
export const statusSPATTEl = getElByQuery('#statusSPATT')
// statusSPDEF
export const statusSPDEFEl = getElByQuery('#statusSPDEF')
// total xp gained
export const statusTotalXP = getElByQuery('#statusTotalXP')
// player overview window
export const playerOvEl = getElByQuery('#playerOverview')
// player name
export const playerNameEl = getElByQuery('#playerOvName')
// player sats
export const playerMoneyEl = getElByQuery('#playerMoney')
// player time played
export const playerTimeEl = getElByQuery('#timePlayed')
// player overview dex entries
export const ovDexEntries = getElByQuery('#saveDex')
// use/drop item window
export const useDropWrap = getElByQuery('#useDropItemWrap')
// item count to drop window
export const inputItemToDrop = getElByQuery('#dropCount')
// input count
export const inputCount = getElByQuery<HTMLInputElement>('#itemCountToDrop')
// drop btns
export const dropBackBtn = getElByQuery('#dropBack')
export const dropAllBtn = getElByQuery('#dropAll')
export const dropXBtn = getElByQuery('#dropX')
// Q&A Wrap and buttons
export const QAWrap = getElByQuery('#QAWrap')
// btns
export const yesBtn = getElByQuery('#firstQ')
export const noBtn = getElByQuery('#secondQ')
// evolution section
export const evoSection = getElByQuery('#transformSection')
// inventory section
export const inventorySection = getElByQuery('#inventoryPC')
export const inventoryItemBtn = getElByQuery('#itemInventory')
export const inventoryItemList = getElByQuery('#inventoryItems')
export const inventoryPlayerBag = getElByQuery('#inventoryPlayerBag')
export const playerItemBagBtn = getElByQuery('#playerItemBag')
export const dropInventoryItemBtn = getElByQuery('#dropInventoryItem')
export const closeInventoryBtn = getElByQuery('#closeInventory')
// options page
export const pauseOptionsPage = getElByQuery('#pauseOptionsPage')
export const srcCodeBtnWrap = getElByQuery('#optionsBtnWrap')
export const srcCodeBtn = getElByQuery('#sourceCodeBtn')
// badges
export const badgesWrapEl = getElByQuery('#badgesWrap')

export function initQAWrap() {
	yesBtn.innerText = 'YES'
	noBtn.innerText = 'NO'
	QAWrap.style.display = 'grid'
	focusFirstChildAfterTimeout(QAWrap)
}