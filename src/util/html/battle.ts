import gsap from 'gsap'
import { battle, player } from '../..'
import { animateBallThrow } from '../../animation/ball'
import { monsterFadeFromBall, trainerMonsterFade } from '../../animation/battle'
import { attackSequences, getAttackByName, TAttackNames } from '../../classes/Attack'
import { dialog } from '../../classes/Dialog'
import { flags } from '../../classes/Flags'
import { Img } from '../../classes/Img'
import { Item } from '../../classes/Items/Item'
import { Monster } from '../../classes/Monster'
import { reaction } from '../../classes/ReactionQueue'
import { xpTable } from '../../data/monster/xpTable'
import { getMonsterTypeNameById } from '../../data/monster/types'
import { assertNonNullish, clearArr, elHasClassName, focusFirstChildAfterTimeout, getElByQuery, getMonsterNameEffectiveType, getTypesStr } from '../helper'
import { formatMonIdx, formatName } from '../fmt'
// HTML Elements
import {
	monsterStatsEl,
	attacksSection,
	UIWrap,
	enemyMonInfoEl,
	playerInfoEl,
	monsterInfoEl,
	playerMonsterHPBar,
	monsterLvlEl,
	monHPTxtEl,
	monsterListEl,
	itemListEl,
	monsterStatusEl,
	statusHPBarEl,
	statusHPEl,
	statusIDEl,
	statusXPEl,
	statusXPBar,
	statusOTEl,
	statusTypeEl,
	statusATTEl,
	statusDEFEl,
	statusINIEl,
	statusSPATTEl,
	statusSPDEFEl,
	statusTotalXP,
	monXPBar,
	useDropWrap,
	inputItemToDrop,
	inputCount,
	dropBackBtn,
	dropAllBtn,
	dropXBtn,
	enemyTrainerInfoEl,
	QAWrap,
	yesBtn,
	noBtn,
	statusName2El,
	statusUIDEl,
	statusItemEl,
	initQAWrap
} from './elements'
import { playAudio } from '../../data/audio'
import { events } from '../../classes/Events'
import { IAttack, IMonListParams } from '../../models/functions'
import { NPC } from '../../classes/NPC'
import { statusSequence } from '../../classes/Status'
import { data } from '../../classes/Data'

// TODO add HTML PREVIEW comments 

// Initialize Battle HTML Elements
export const initBattleHTML = ({ wildEnemy, trainer }: { wildEnemy?: Monster, trainer?: NPC }) => {
	if (!wildEnemy && !trainer) {
		console.warn('battle can not be initiated without wild enemy & without trainer')
		return
	}
	// set right position (only needed if device orientation is changed at game begin before having any monster)
	player.resetAllMonsterPos()
	// reset battle round
	battle.resetRound()
	// Trainer or wild monster HTML
	trainer ? initTrainerHTML(trainer) : initWildEnemyHTML(wildEnemy)
	// reset player battle sprite position
	player.battleSprite.position = player.getBattleSpritePos()
	// reset battleground elements
	monsterStatsEl.removeAttribute('style')
	attacksSection.removeAttribute('style')
	UIWrap.style.display = 'block'
	enemyMonInfoEl.removeAttribute('style')
	playerInfoEl.removeAttribute('style')
	monsterInfoEl.removeAttribute('style')
	playerMonCountEl.replaceChildren()
	// player HTML
	battlePlayerNameEl.innerText = player.name
	// player-monster count
	player.monsters.forEach(monster => {
		monster.loadAudio()
		const ballImg = new Img('./imgs/ball/ball.png')
		ballImg.classList.add('monBallCount')
		const ballDeadImg = new Img('./imgs/ball/ballDead.png')
		ballDeadImg.classList.add('monBallCount')
		playerMonCountEl.appendChild(monster.stats.IS.hp > 0 ? ballImg : ballDeadImg)
	})
	// push to render arr for use in animateBattle() - If trainer available, push trainer, else, push wild enemy
	if (trainer) {
		battle.setRenderedSprites([trainer, player.battleSprite]) //  enemy first due to layering
	}
	if (wildEnemy) {
		battle.setRenderedSprites([wildEnemy, player.battleSprite]) //  enemy first due to layering
	}
	// init monster list - opponent monster is needed to trigger his attack if player opens monster list to switch monster 
	initMonsterListHTML({ enemy: trainer?.monsters?.[0] || wildEnemy })
	/*
		init battle menu buttons (Buttons: ball, monster, item, escape)
		- opponent monster is needed to assosiate button action
		- the trainer object is needed to handle the reaction queue in a trainer battle
	*/
	initBattleMenuBtns(trainer?.monsters?.[0] || wildEnemy, trainer)
}
// Initialize Player Monster HTML Elements
export const initPlayerMonHTML = (monster: Monster, enemy: Monster, trainer?: NPC) => {
	monsterStatsEl.removeAttribute('style')
	// player monster name
	monsterNameEl.innerHTML = monster.name
	// healthbar 
	playerMonsterHPBar.max = monster.maxHP
	playerMonsterHPBar.value = monster.stats.IS.hp
	playerMonsterHPBar.className = 'HPBar'
	if (monster.stats.IS.hp <= monster.maxHP * .5) { playerMonsterHPBar.classList.add('HPBarYellow') }
	if (monster.stats.IS.hp <= monster.maxHP * .15) { playerMonsterHPBar.classList.replace('HPBarYellow', 'HPBarRed') }
	// XP progress bar
	monXPBar.setAttribute('min', '0')
	monXPBar.setAttribute('max', `${xpTable[monster.cat - 1][monster.level] - xpTable[monster.cat - 1][monster.level - 1]}`)
	monXPBar.setAttribute('value', `${monster.xp - xpTable[monster.cat - 1][monster.level - 1]}`)
	// show monster level
	assertNonNullish(monster.level, 'monster.level not available')
	monsterLvlEl.innerText = `Lv. ${monster.level}`
	// show monster HP
	monHPTxtEl.innerText = `HP: ${monster.stats.IS.hp}/${monster.maxHP}`
	// attack btns section
	// replace attack btns
	attacksSection.replaceChildren()
	/********************************************/
	/****** display monster attack buttons ******/
	/********************************************/
	initPlayerMonAttackBtns(monster, enemy, trainer)
}
// Initialize Player Monster Attack Button Elements
export const initPlayerMonAttackBtns = (monster: Monster, enemy: Monster, trainer?: NPC) => {
	for (let i = 0; i < monster.attacks.length; i++) {
		const attack = monster.attacks[i]
		// HTML
		const button = document.createElement('button')
		button.id = `attack${i + 1}`
		button.classList.add('attackBtn')
		button.innerHTML = `${attack.name.toUpperCase()}<p class="attackExec">${attack.remExec}/${attack.maxExec}</p>`
		button.setAttribute('attackName', attack.name)
		// Attack Buttons Event
		button.addEventListener('click', e => {
			monsterListEl.classList.remove('display')
			itemListEl.classList.remove('display')
			if (!(e.currentTarget instanceof HTMLButtonElement)) { return }
			// Check if attack is available
			if (attack.remExec === 0) {
				// Prompt player if he chooses empty attack
				dialog.show(`${monster.name} can not use ${attack.name} anymore!`)
				return
			}
			button.blur()
			// check for damaging status
			reaction.queue.push(...statusSequence.generateDamagingStatusQueue(monster, enemy, trainer))
			// check for attack preventing status
			const attPreventStatus = statusSequence.hasAttackPreventingStatus(monster)
			// check if monster is not frozen anymore
			if (statusSequence.isThawed(monster)) {
				attPreventStatus.isAffected = false
				dialog.show(`${monster.name} is not FROZEN anymore!`)
				// monster still has a damaging status
				if (reaction.queue.length > 0) {
					battle.PlayerMonsterStatusDamageSequence(monster, attack, enemy, attPreventStatus, button, trainer)
					return
				}
				// is not frozen anymore and has no other damaging status
				reaction.queue.push(() => battle.playerMonAttackSequence(monster, enemy, attack, button, trainer))
				return
			}
			// monster still has a damaging status
			if (reaction.queue.length > 0) {
				battle.PlayerMonsterStatusDamageSequence(monster, attack, enemy, attPreventStatus, button, trainer)
				return
			}
			// can not attack due to status
			if (attPreventStatus.isAffected) {
				statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: monster })
				dialog.show(`${monster.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
				reaction.queue.push(() => enemy.randomAttack(monster, trainer))
				return
			}
			// no status changes, just attack 
			battle.playerMonAttackSequence(monster, enemy, attack, button, trainer)
		})
		attacksSection.append(button)
	}
}
// Initialize Player Monster List
export const initMonsterListHTML = ({ enemy, forItem, inMap, switchBeforeFaint, switchAfterFaint, trainer, nextMonster }: IMonListParams) => {
	monsterListEl.replaceChildren()
	if (player.monsters.length === 0) {
		const msg = document.createElement('p')
		msg.classList.add('noMonsMsg')
		msg.innerText = `${player.name} has no MONSTERs!`
		monsterListEl.appendChild(msg)
		return
	}
	let idxChosen: number | null = null
	for (let i = 0; i < player.monsters.length; i++) {
		const monster = player.monsters[i]
		// List entry
		const mainWrap = document.createElement(forItem ? 'button' : 'div')
		mainWrap.classList.add('mainWrapMonList')
		// Add event to entire element if is monster list for item
		if (forItem) {
			// console.log('monster click for item')
			mainWrap.addEventListener('click', () => {
				mainWrap.blur()
				if (player.selectedItems[0].name === 'POTION') {
					usePotion(monster, i, enemy, trainer)
				}
				if (player.selectedItems[0].name === 'RARE CANDY') {
					useRareCandy(monster, i, enemy, trainer)
				}
			})
		} else {
			// console.log('monster click NOT for item')
			mainWrap.removeEventListener('click', () => usePotion(monster, i, enemy, trainer))
		}
		// Child 1: Image
		const img = monster.sprite.imageFaceUp
		img.classList.add('monListImg')
		img.removeAttribute('style')
		// Child 2: InfoWrap
		const infoWrap = document.createElement('div')
		infoWrap.classList.add('monListEntryWrap')
		const nameWrap = document.createElement('div')
		nameWrap.classList.add('listNameWrap')
		const nameEl = document.createElement('div')
		nameEl.innerText = monster.name
		const lvlEl = document.createElement('div')
		lvlEl.id = `monListLevel${i}`
		lvlEl.innerText = `Lv. ${monster.level}`
		nameWrap.appendChild(nameEl)
		nameWrap.appendChild(lvlEl)
		infoWrap.appendChild(nameWrap)
		const hpBarEl = document.createElement('progress')
		hpBarEl.id = `monListProgressBar${i}`
		hpBarEl.classList.add('HPBar')
		if (monster.stats.IS.hp <= monster.maxHP * .5) { hpBarEl.classList.add('HPBarYellow') }
		if (monster.stats.IS.hp <= monster.maxHP * .15) { hpBarEl.classList.replace('HPBarYellow', 'HPBarRed') }
		hpBarEl.max = monster.maxHP
		hpBarEl.value = monster.stats.IS.hp < 0 ? 0 : monster.stats.IS.hp
		infoWrap.appendChild(hpBarEl)
		const statusWrap = document.createElement('div')
		statusWrap.classList.add('listStatusWrap')
		const status = document.createElement('div')
		// TODO show status (poisoned, paralyzed, ...)
		const hps = document.createElement('div')
		hps.id = `monListHP${i}`
		hps.innerText = `HP: ${monster.stats.IS.hp < 0 ? 0 : monster.stats.IS.hp}/${monster.maxHP}`
		statusWrap.appendChild(status)
		statusWrap.appendChild(hps)
		infoWrap.appendChild(statusWrap)
		mainWrap.appendChild(img)
		mainWrap.appendChild(infoWrap)
		// Child 3: Monster List Event Buttons
		if (!forItem) {
			const btnsWrap = document.createElement('div')
			btnsWrap.classList.add('monListBtnWrap')
			const switchBtn = document.createElement('button')
			switchBtn.classList.add('monListBtn')
			if (monster.stats.IS.hp < 1 && (!inMap || inMap)) {
				switchBtn.style.pointerEvents = 'none'
				switchBtn.style.color = '#b3b3b3'
				img.style.filter = 'grayscale(100)'
				img.style.opacity = '.5'
			}
			if (monster === battle.monsterParticipated[0]) {
				switchBtn.style.pointerEvents = 'none'
				switchBtn.style.color = '#b3b3b3'
			}
			switchBtn.innerHTML = inMap ? 'SWITCH' : 'FIGHT'
			switchBtn.name = 'monBtn1'
			// switch monster event
			switchBtn.onclick = () => {
				// switch monster list order
				if (inMap) {
					if (idxChosen === null) {
						idxChosen = i
						switchBtn.style.pointerEvents = 'none'
						switchBtn.style.color = '#b3b3b3'
						return
					}
					if (idxChosen >= 0) {
						// switch monster
						player.replaceMonsters(idxChosen, i)
						// reset idxChosen & btn style
						idxChosen = null
						switchBtn.removeAttribute('style')
						// render new list
						monsterListEl.classList.remove('display')
						displayMonList({ inMap: true })
					}
					return
				}
				// else: switch monster fighting
				if (monster.stats.IS.hp < 1) { return }
				if (monster === battle.monsterParticipated[0]) { return }
				// close monster list
				monsterListEl.classList.remove('display')
				events.lastFocusEl = null
				// switch monster after trainer sends out his next monster
				if (switchBeforeFaint && trainer && nextMonster) {
					// console.log('switch after trainer sends out his next monster after his previous fainted')
					flags.setIsAnimating(true)
					enemyTrainerInfoEl.removeAttribute('style')
					const cancelBtn = getElByQuery('#cancelMonSwitch')
					if (cancelBtn) { cancelBtn.remove() }
					player.setSelectedMonToSwitch(monster)
					dialog.show(`${trainer.name} sends out ${nextMonster.name}!`)
					const t = setTimeout(() => {
						const playerMon = battle.renderedSprites[0]
						if (playerMon instanceof Monster) {
							initPlayerMonAttackBtns(playerMon, nextMonster, trainer)
						}
						trainerMonsterFade(nextMonster, trainer)
						clearTimeout(t)
					}, 1000)
				}
				else if (switchAfterFaint) {
					// console.log('switch player monster after previous fainted')
					switchMonster(monster, trainer, false, true)
				}
				else {
					// console.log('switch player monster before previous fainted')
					switchMonster(monster, trainer, true)
				}
			}
			const statusBtn = document.createElement('button')
			statusBtn.name = 'monBtn2'
			statusBtn.classList.add('monListBtn')
			statusBtn.innerHTML = 'REPORT'
			statusBtn.onclick = () => {
				statusBtn.blur()
				// init status page
				initStatusPageHTML(monster)
				monsterStatusEl.style.display = 'flex'
				focusFirstChildAfterTimeout(getElByQuery('#statusTabBtnsWrap'))
				if (battle.isInitiated) {
					battle.hideUI()
				}
			}
			btnsWrap.appendChild(switchBtn)
			btnsWrap.appendChild(statusBtn)
			mainWrap.appendChild(btnsWrap)
		}
		monsterListEl.appendChild(mainWrap)
	}
}
// Displays the monster list and hides the item list 
export const displayMonList = ({ enemy, forItem, inMap, switchBeforeFaint, switchAfterFaint, trainer, nextMonster }: IMonListParams) => {
	// if (battleDialog.hasAttribute('style')) { return }
	initMonsterListHTML({ enemy, forItem, inMap, switchBeforeFaint, switchAfterFaint, trainer, nextMonster })
	if (elHasClassName(itemListEl, 'display')) {
		itemListEl.classList.remove('display')
	}
	monsterListEl.classList.toggle('display')
	focusFirstChildAfterTimeout(monsterListEl)
}
// Initialize Player Item List FRONTEND
export const initItemListHTML = (enemy?: Monster) => {
	itemListEl.replaceChildren()
	for (let i = 0; i < player.items.length; i++) {
		const item = player.items[i]
		const button = document.createElement('button')
		if (i === 0) { button.id = 'firstItemEl' }
		button.classList.add('itemWrap')
		// item click event
		button.onclick = () => {
			// display use/drop btns only outside of battle
			if (!battle.isInitiated) {
				useDropWrap.style.display = 'grid'
				focusFirstChildAfterTimeout(useDropWrap)
				const useBtn = getElByQuery('#useItemBtn')
				const dropBtn = getElByQuery('#dropItemBtn')
				// use
				useBtn.onclick = () => {
					useDropWrap.removeAttribute('style')
					button.removeAttribute('style')
					itemEvent(item, enemy)
				}
				// drop
				dropBtn.onclick = () => {
					// drop single item entirely
					if (item.count === 1) {
						item.decreaseCount(1)
						useDropWrap.removeAttribute('style')
						button.removeAttribute('style')
						// rerender
						initItemListHTML()
						focusFirstChildAfterTimeout(itemListEl)
						return
					}
					// choose count to drop
					const dropTxt = getElByQuery('#dropTxt')
					dropTxt.innerText = `HOW MANY ${item.name}s DO YOU WANT TO DROP?`
					dropAllBtn.innerText = 'DROP ALL'
					inputItemToDrop.style.display = 'flex'
					inputCount.max = `${item.count}`
					inputCount.value = '1'
					dropXBtn.innerText = `DROP x${inputCount.value}`
					dropXBtn.focus()
					events.lastFocusEl = dropXBtn
					// inputCount.focus()
					inputCount.onchange = () => {
						dropXBtn.innerText = `DROP x${inputCount.value}`
					}
					inputCount.oninput = () => {
						dropXBtn.innerText = `DROP x${inputCount.value}`
					}
					// go back
					dropBackBtn.onclick = () => {
						dropTxt.innerText = ''
						inputCount.setAttribute('value', '1')
						inputItemToDrop.removeAttribute('style')
						focusFirstChildAfterTimeout(useDropWrap)
					}
					// drop all
					dropAllBtn.onclick = () => {
						dropTxt.innerText = ''
						item.decreaseCount(item.count)
						// close modal
						inputItemToDrop.removeAttribute('style')
						// close use/drop window
						useDropWrap.removeAttribute('style')
						button.removeAttribute('style')
						inputCount.setAttribute('value', '1')
						// rerender item list
						initItemListHTML()
						focusFirstChildAfterTimeout(itemListEl)
					}
					// drop amount x
					dropXBtn.onclick = () => {
						dropTxt.innerText = ''
						const inputVal = inputCount.value
						if (!inputVal) { return }
						item.decreaseCount(+inputVal)
						// close modal
						inputItemToDrop.removeAttribute('style')
						// close use/drop window
						useDropWrap.removeAttribute('style')
						button.removeAttribute('style')
						inputCount.setAttribute('value', '1')
						// rerender item list
						initItemListHTML()
						focusFirstChildAfterTimeout(itemListEl)
					}
				}
				Array.from(itemListEl.children).forEach(item => item.removeAttribute('style'))
				button.style.backgroundColor = '#FF9900'
				return
			}
			// else: use item directly
			button.blur()
			itemEvent(item, enemy)
		}
		const iName = document.createElement('p')
		iName.innerHTML = item.name
		button.appendChild(iName)
		const iCount = document.createElement('p')
		iCount.innerHTML = `x ${item.count}`
		button.appendChild(iCount)
		itemListEl.appendChild(button)
	}
	const backBtn = document.createElement('button')
	backBtn.classList.add('itemWrap')
	backBtn.innerHTML = '<p>BACK</p>'
	backBtn.onclick = () => events.handleBackBtn()
	itemListEl.appendChild(backBtn)
}
// Initialize Yes/No Buttons for player monster switching after trainer monster faints
export const initYesNoAfterTrainerMonFaints = (trainer: NPC, nextMonster: Monster) => {
	initQAWrap()
	function dontSwitch() {
		events.lastFocusEl = null
		flags.setIsAnimating(true)
		enemyTrainerInfoEl.removeAttribute('style')
		QAWrap.removeAttribute('style')
		dialog.show(`${trainer.name} sends out ${nextMonster.name}!`)
		const t = setTimeout(() => {
			const playerMon = battle.renderedSprites[0]
			if (playerMon instanceof Monster) {
				initPlayerMonAttackBtns(playerMon, nextMonster, trainer)
			}
			trainerMonsterFade(nextMonster, trainer)
			clearTimeout(t)
		}, 1000)
	}
	const cancelBtn = document.createElement('button')
	// switch monster after trainer sends out his next monster
	yesBtn.onclick = () => {
		QAWrap.removeAttribute('style')
		dialog.updateInnerHTML('Which MONSTER should fight?')
		displayMonList({ switchBeforeFaint: true, trainer, nextMonster })
		cancelBtn.classList.add('cancelSwitch')
		cancelBtn.id = 'cancelMonSwitch'
		cancelBtn.innerText = 'CANCEL'
		cancelBtn.onclick = () => {
			cancelBtn.remove()
			monsterListEl.classList.remove('display')
			dontSwitch()
		}
		UIWrap.appendChild(cancelBtn)
	}
	// continue with same monster against trainer next monster
	noBtn.onclick = () => {
		cancelBtn.remove()
		dontSwitch()
	}
}
// Initialize Enemy Trainer HTML Elements
export const initTrainerHTML = (trainer: NPC) => {
	// trainer battle sprite position
	trainer.battleSprite.position = trainer.getBattleTrainerPos(false)
	// trainer name
	enemyTrainerNameEl.innerText = trainer.name
	// monster count
	trainerMonCountEl.replaceChildren()
	trainer.monsters?.forEach(monster => {
		monster.loadAudio()
		const ballImg = new Img('./imgs/ball/ball.png')
		ballImg.classList.add('monBallCount')
		const ballDeadImg = new Img('./imgs/ball/ballDead.png')
		ballDeadImg.classList.add('monBallCount')
		trainerMonCountEl.appendChild(monster.stats.IS.hp > 0 ? ballImg : ballDeadImg)
	})
	enemyTrainerInfoEl.removeAttribute('style')
}
// Initialize Enemy HTML Elements
export const initTrainerMonHTML = (enemy: Monster) => {
	// reset position due to first fade effect
	enemy.sprite.setPos(enemy.getMonsterPos())
	// enemy name
	const enemyNameEl = getElByQuery('#enemyName')
	enemyNameEl.innerHTML = enemy.name
	// health points progress bars
	const enemyHPBar = getElByQuery<HTMLProgressElement>('#enemyHealthBar')
	enemyHPBar.max = enemy.maxHP
	enemyHPBar.value = enemy.stats.IS.hp
	// change hp progress color back to green
	enemyHPBar.className = 'HPBar'
	const enemyLvl = getElByQuery<HTMLDivElement>('#enemyLvl')
	enemyLvl.innerText = `Lv. ${enemy.level}`
}
// Switch Monster Event
export const switchMonster = (monster: Monster, trainer?: NPC, switchBeforeFaint = false, switchAfterFaint = false) => {
	// Check if a monster is already in battle
	if (battle.monsterParticipated[0].stats.IS.hp > 0) {
		// anti-spam
		flags.setIsAnimating(true)
		// show dialog
		dialog.show(`Enough, ${battle.monsterParticipated[0].name}! Come back!`) // Array idx 0 is always current monster in battle
		playAudio('callback')
		gsap.to(battle.monsterParticipated[0].sprite.position, {
			y: battle.monsterParticipated[0].sprite.position.y + 100,
			duration: 1,
			onComplete: () => {
				// remove current monster from renderedSprites
				battle.popRenderedSprite()
				flags.setIsAnimating(false)
				battle.monsterParticipated[0].playAudio()
				// reset pos
				battle.monsterParticipated[0].sprite.position.y -= 100
			}
		})
		gsap.to(monsterInfoEl, {
			opacity: 0,
			duration: 1
		})
		// new monster spawn
		reaction.queue.push(
			() => {
				dialog.show(`GO! ${monster.name}!`)
				// Fade-in new monster, init HTML after animation and push new monster at idx 0 of battleMonster Array
				monsterFadeFromBall(monster, trainer, switchBeforeFaint)
			}
		)
		return
	}
	// Monsters in battle dead
	// remove current monster from renderedSprites
	battle.popRenderedSprite()
	// show dialog
	dialog.show(`GO! ${monster.name}!`)
	// Fade-in new monster and init HTML after animation end
	monsterFadeFromBall(monster, trainer, switchBeforeFaint, switchAfterFaint)
}
// Initialize the monster list for choosing player first monster 
export const initFirstMonListHTML = () => {
	monsterListEl.replaceChildren()
	const firstMons = data.getNPCByID({ npcID: 110, mapID: player.currentMapId })?.monsters
	if (!firstMons) {
		console.warn('first monsters of professor not found!')
		return
	}
	for (let i = 0; i < firstMons.length; i++) {
		const monster = firstMons[i]
		// List entry
		const mainWrap = document.createElement('div')
		mainWrap.classList.add('mainWrapMonList')
		// Child 1: Image
		const img = monster.sprite.imageFaceUp
		img.classList.add('monListImg')
		img.removeAttribute('style')
		// Child 2: InfoWrap
		const infoWrap = document.createElement('div')
		infoWrap.classList.add('monListEntryWrap')
		const nameWrap = document.createElement('div')
		nameWrap.classList.add('listNameWrap')
		const nameEl = document.createElement('div')
		nameEl.innerText = monster.name
		const lvlEl = document.createElement('div')
		lvlEl.innerText = `Lv. ${monster.level}`
		nameWrap.appendChild(nameEl)
		nameWrap.appendChild(lvlEl)
		infoWrap.appendChild(nameWrap)
		const hpBarEl = document.createElement('progress')
		hpBarEl.id = `monListProgressBar${i}`
		hpBarEl.classList.add('HPBar')
		if (monster.stats.IS.hp <= monster.maxHP * .5) { hpBarEl.classList.add('HPBarYellow') }
		if (monster.stats.IS.hp <= monster.maxHP * .15) { hpBarEl.classList.replace('HPBarYellow', 'HPBarRed') }
		hpBarEl.max = monster.maxHP
		hpBarEl.value = monster.stats.IS.hp < 0 ? 0 : monster.stats.IS.hp
		infoWrap.appendChild(hpBarEl)
		const statusWrap = document.createElement('div')
		statusWrap.classList.add('listStatusWrap')
		const status = document.createElement('div')
		// TODO show status (poisoned, paralyzed, ...)
		const hps = document.createElement('div')
		hps.id = `monListHP${i}`
		hps.innerText = `HP: ${monster.stats.IS.hp < 0 ? 0 : monster.stats.IS.hp}/${monster.maxHP}`
		statusWrap.appendChild(status)
		statusWrap.appendChild(hps)
		infoWrap.appendChild(statusWrap)
		mainWrap.appendChild(img)
		mainWrap.appendChild(infoWrap)
		// Child 3: Monster List Event Buttons
		const btnsWrap = document.createElement('div')
		btnsWrap.classList.add('monListBtnWrap')
		// choose btn
		const chooseBtn = document.createElement('button')
		chooseBtn.name = 'monBtn1'
		chooseBtn.classList.add('monListBtn')
		chooseBtn.innerHTML = 'CHOOSE'
		// choose monster event
		chooseBtn.onclick = () => {
			// prompt user
			dialog.updateInnerHTML(`Are you sure that you want ${monster.name}?`)
			initQAWrap()
			yesBtn.onclick = () => {
				events.lastFocusEl = null
				flags.setIsAnimating(true)
				// monster chosen
				const prof = data.getNPCByID({ npcID: 110, mapID: player.currentMapId })
				prof?.audio?.look?.stop()
				playAudio('importantItem')
				QAWrap.style.display = 'none'
				// if the game has been saved before player gets his first monster, the local-storage monster conversion
				// makes it to an enemy, so the "isEnemy" and "position" props need to be reset
				if (monster.isEnemy) {
					monster.setIsEnemy(false)
					monster.sprite.position = monster.getMonsterSpawnPos()
				}
				// push monster to player
				player.monsters.push(monster)
				player.monsterDex.addNewEntry(monster)
				player.monsterDex.setIsCatched(monster.idx)
				const rivalMon = firstMons.find(mon => mon.name === getMonsterNameEffectiveType(formatName(monster.name)).toUpperCase())
				if (!rivalMon) { return }
				rivalMon.setIsEnemy(true)
				const rival = data.getNPCByID({ npcID: 106, mapID: player.currentMapId })
				if (!rival) { return }
				rival.monsters?.push(rivalMon)
				rival.rivalFirstMonIdx = rivalMon.idx
				// update rival first mon for all maps
				data.addFirstMonsterForRival(rivalMon.idx)
				dialog.show(`${player.name} received ${monster.name}!`)
				const t = setTimeout(() => {
					flags.setIsAnimating(false)
					clearTimeout(t)
				}, 2500)
			}
			noBtn.onclick = () => {
				// cancel choice
				QAWrap.style.display = 'none'
				dialog.updateInnerHTML('Choose your FIRST MONSTER!')
				monsterListEl.classList.add('display')
				focusFirstChildAfterTimeout(monsterListEl)
			}
			// close monster list
			monsterListEl.classList.remove('display')
		}
		// stats button
		const statusBtn = document.createElement('button')
		statusBtn.name = 'monBtn2'
		statusBtn.classList.add('monListBtn')
		statusBtn.innerHTML = 'STATUS'
		statusBtn.onclick = () => {
			dialog.hide()
			statusBtn.blur()
			// init status page
			initStatusPageHTML(monster)
			monsterStatusEl.style.display = 'flex'
		}
		btnsWrap.appendChild(chooseBtn)
		btnsWrap.appendChild(statusBtn)
		mainWrap.appendChild(btnsWrap)
		// append monster entry to list
		monsterListEl.appendChild(mainWrap)
	}
	monsterListEl.classList.add('display')
	focusFirstChildAfterTimeout(monsterListEl)
}
// Initialize the forget attack UI
export const initAttackForgetUI = (monster: Monster, newAttack: IAttack, evoMon?: Monster, trainer?: NPC) => {
	const UI = getElByQuery('#forgetAttackUI')
	const actualMon = evoMon || monster
	// monster image
	const monImg = getElByQuery<HTMLImageElement>('#attackMonImg')
	monImg.src = actualMon.sprite.imageFaceUp.src
	// monster name
	const name = getElByQuery('#attackMonName')
	name.innerText = actualMon.name
	// monster type
	const type = getElByQuery('#attackMonType')
	const types = getTypesStr({ monster: actualMon })
	type.innerText = types
	// current focused attack power
	const power = getElByQuery('#attackForgetPower')
	power.innerText = `${actualMon.attacks[0].power}`
	// current focused attack accuracy
	const acc = getElByQuery('#attackForgetAcc')
	acc.innerText = `${actualMon.attacks[0].acc * 100}%`
	// curretn focused attack info
	const attInfo = getElByQuery('#attackForgetDes')
	attInfo.innerText = actualMon.attacks[0].info
	// all attacks
	const attWrap = getElByQuery('#forgetAttackBtns')
	attWrap.replaceChildren()
	for (let i = 0; i < actualMon.attacks.length; i++) {
		const att = actualMon.attacks[i]
		const btn = createAttackBtn({ monster: actualMon, att, withEvent: true, trainer })
		attWrap.appendChild(btn)
	}
	const newAttackBtn = createAttackBtn({ monster: actualMon, att: newAttack, withEvent: false, trainer })
	attWrap.appendChild(newAttackBtn)
	// show UI
	UI.style.display = 'grid'
	focusFirstChildAfterTimeout(attWrap)
}
// update the attack info on focus change
export const updateAttackInfo = (newAttFocusEl: HTMLButtonElement | null) => {
	if (!newAttFocusEl) { return }
	const attack = getAttackByName(newAttFocusEl.name as TAttackNames)
	// current focused attack power
	const power = getElByQuery('#attackForgetPower')
	power.innerText = `${attack.power}`
	// current focused attack accuracy
	const acc = getElByQuery('#attackForgetAcc')
	acc.innerText = `${attack.acc * 100}%`
	// curretn focused attack info
	const attInfo = getElByQuery('#attackForgetDes')
	attInfo.innerText = attack.info
}

/********************************************************************************************/

// ball/monster/item/escape button wrap of the battle interface
export const battleMenuEl = getElByQuery('#battleMenu')
// Player Name Element
const battlePlayerNameEl = getElByQuery('#playerName')
// Player Monster count overview
const playerMonCountEl = getElByQuery('#playerMonCount')
// Player Monste Name
const monsterNameEl = getElByQuery('#monsterName')
// Enemy Trainer Name Element
const enemyTrainerNameEl = getElByQuery('#trainerName')
// Enemy Trainer Monster count
const trainerMonCountEl = getElByQuery('#trainerMonCount')

/********************************************************************************************/

// attack forget btn
function createAttackBtn({ monster, att, withEvent, trainer }: { monster: Monster, att: IAttack, withEvent?: boolean, trainer?: NPC }) {
	/* // PREVIEW
	<button class="attackMenuBtn" name="TACKLE">
		<div class="attackTypeAndName">
			<p class="attackInfoType">GRASS</p>
			<span>TACKLE</span>
		</div>
		<span class="forgetAttackExec">40/40</span>
	</button>
	*/
	const btn = document.createElement('button')
	btn.classList.add('attackMenuBtn')
	btn.name = att.name
	if (withEvent) {
		btn.onclick = () => {
			// handle attack replacement
			events.lastFocusEl = null
			const UI = getElByQuery('#forgetAttackUI')
			UI.removeAttribute('style')
			battle.forgetAttack(monster, att, trainer)
			reaction.queue.splice(
				reaction.queue.length - 1,
				0,
				() => dialog.show(`${monster.name} just forgot ${att.name.toUpperCase()}...`),
				() => {
					playAudio('learnMove')
					dialog.show(`and ${monster.name} learned ${monster.lvlAttacks[monster.level].name.toUpperCase()}!`)
				}
			)
		}
	}
	// child
	const typeAndNameEl = document.createElement('div')
	typeAndNameEl.classList.add('attackTypeAndName')
	const type = document.createElement('p')
	type.classList.add('attackInfoType')
	type.innerText = getTypesStr({ attack: att }).toUpperCase()
	const attName = document.createElement('span')
	attName.innerText = att.name
	typeAndNameEl.appendChild(type)
	typeAndNameEl.appendChild(attName)
	btn.appendChild(typeAndNameEl)
	const exec = document.createElement('span')
	exec.classList.add('forgetAttackExec')
	exec.innerText = `${att.remExec}/${att.maxExec}`
	btn.appendChild(exec)
	return btn
}
// status info tabs
function openInfoTab(tabID: string | null, btnID: string | null) {
	if (!tabID) { return }
	const els = document.getElementsByClassName('statusInfoTab') as HTMLCollectionOf<HTMLDivElement>
	for (let i = 0; i < els.length; i++) {
		els[i].style.display = 'none'
	}
	getElByQuery(`#${tabID}`).style.display = 'grid'
	const tab = getElByQuery('#monsterStatus')
	if (tab) {
		if (btnID === 'MonsterInfoTabBtn') {
			tab.style.backgroundColor = '#fff1d8'
		}
		if (btnID === 'MonsterStatsTabBtn') {
			tab.style.backgroundColor = '#d8daff'
		}
		if (btnID === 'MonsterAttacksTabBtn') {
			tab.style.backgroundColor = '#ddffd8'
		}
	}
	showActiveTabBtn(btnID)
}
function showActiveTabBtn(btnID: string | null) {
	if (!btnID) { return }
	const els = document.getElementsByClassName('tabBtn') as HTMLCollectionOf<HTMLButtonElement>
	for (let i = 0; i < els.length; i++) {
		els[i].style.border = 'none'
	}
	getElByQuery(`#${btnID}`).style.border = '2px solid orange'
}
// Initialize the monster status page
const initStatusPageHTML = (monster: Monster) => {
	// monster status tabs
	const infoBtn = getElByQuery('#MonsterInfoTabBtn')
	const statsBtn = getElByQuery('#MonsterStatsTabBtn')
	const attacksBtn = getElByQuery('#MonsterAttacksTabBtn')
	infoBtn.onclick = () => openInfoTab(infoBtn.getAttribute('name'), infoBtn.id)
	statsBtn.onclick = () => openInfoTab(statsBtn.getAttribute('name'), statsBtn.id)
	attacksBtn.onclick = () => openInfoTab(attacksBtn.getAttribute('name'), attacksBtn.id)
	// open first tab
	openInfoTab(infoBtn.getAttribute('name'), infoBtn.id)
	focusFirstChildAfterTimeout(getElByQuery('#statusTabBtnsWrap'))
	// monster status lvl, name, img
	const nameImgWrap = document.createElement('div')
	// nameImgWrap.id = 'nameImgWrapNode'
	nameImgWrap.classList.add('nameImgWrap')
	const statusNameLvlWrap = document.createElement('div')
	statusNameLvlWrap.classList.add('statusNameLvlWrap')
	const statusLvl = document.createElement('span')
	statusLvl.innerText = `Lv. ${monster.level}`
	const statusName = document.createElement('span')
	statusName.innerText = monster.name
	statusNameLvlWrap.appendChild(statusLvl)
	statusNameLvlWrap.appendChild(statusName)
	nameImgWrap.appendChild(statusNameLvlWrap)
	const statusImgWrap = document.createElement('div')
	statusImgWrap.classList.add('statusImgWrap')
	const monsterImg = document.createElement('img')
	monsterImg.classList.add('statusImg')
	monsterImg.setAttribute('src', monster.sprite.imageFaceUp.src)
	if (monster.stats.IS.hp <= 0) {
		monsterImg.style.filter = 'grayscale(100%)'
		monsterImg.style.opacity = '.5'
	} else {
		monsterImg.removeAttribute('style')
	}
	const imgMaxWidth = 135
	const imgMaxHeight = 130
	// console.log('monster.sprite.width: ', monster.sprite.width)
	if (monster.sprite.enemyWidth < imgMaxWidth && monster.sprite.enemyHeight < imgMaxHeight) {
		monsterImg.style.width = `${monster.sprite.enemyWidth}px`
		monsterImg.style.height = `${monster.sprite.enemyHeight}px`
	}
	statusImgWrap.appendChild(monsterImg)
	if (player.monsterDex.isMonsterCatched(monster.idx)) {
		const ballImg = document.createElement('img')
		ballImg.classList.add('statusBall')
		ballImg.setAttribute('src', './imgs/ball/ball.png')
		statusImgWrap.appendChild(ballImg)
	}
	nameImgWrap.appendChild(statusImgWrap)
	const infoParent = getElByQuery('#statusInfoNameImg')
	if (elHasClassName(infoParent.firstElementChild, 'nameImgWrap')) {
		infoParent.replaceChild(nameImgWrap, infoParent.firstElementChild as HTMLElement)
	} else {
		infoParent.insertAdjacentElement('afterbegin', nameImgWrap)
	}
	const statsParent = getElByQuery('#statusStatsNameImg')
	if (elHasClassName(statsParent.firstElementChild, 'nameImgWrap')) {
		statsParent.replaceChild(nameImgWrap.cloneNode(true) as HTMLDivElement, statsParent.firstElementChild as HTMLElement)
	} else {
		statsParent.insertAdjacentElement('afterbegin', nameImgWrap.cloneNode(true) as HTMLDivElement)
	}
	const attacksParent = getElByQuery('#statusAttacksNameImg')
	if (elHasClassName(attacksParent.firstElementChild, 'nameImgWrap')) {
		attacksParent.replaceChild(nameImgWrap.cloneNode(true) as HTMLDivElement, attacksParent.firstElementChild as HTMLElement)
	} else {
		attacksParent.insertAdjacentElement('afterbegin', nameImgWrap.cloneNode(true) as HTMLDivElement)
	}
	// info
	statusIDEl.innerText = formatMonIdx(monster.idx, false)
	statusName2El.innerText = monster.name
	const types = getTypesStr({ monster })
	// for (let i = 0; i < monster.type.length; i++) {
	// 	const type = getMonsterTypeNameById(monster.type[i])
	// 	types += `${type.toUpperCase()}${i < monster.type.length - 1 ? ', ' : ''}`
	// }
	statusTypeEl.innerText = types
	if (player.monsters.length > 0) { statusOTEl.innerText = player.name }
	statusUIDEl.innerText = `${monster.uid}`.substring(0, 8)
	statusItemEl.innerText = '-'
	// memo
	const statusMemo = getElByQuery('#statusMemo')
	if (statusMemo && player.monsters.length > 0 && monster.catchLvl && monster.catchLocation) {
		statusMemo.innerText = `Catched at Lv. ${monster.catchLvl} in ${monster.catchLocation}`
	}
	// stats
	statusHPEl.innerText = `${monster.stats.IS.hp < 0 ? '0' : monster.stats.IS.hp}/${monster.maxHP}`
	// hp progress bar
	statusHPBarEl.setAttribute('min', '0')
	statusHPBarEl.setAttribute('max', `${monster.maxHP}`)
	statusHPBarEl.setAttribute('value', `${monster.stats.IS.hp < 0 ? '0' : monster.stats.IS.hp}`)
	// statusHPBarEl.classList.add('HPBar')
	if (monster.stats.IS.hp <= monster.maxHP * .15) {
		statusHPBarEl.classList.add('HPBarRed')
	}
	else {
		statusHPBarEl.classList.remove('HPBarRed')
	}
	if (monster.stats.IS.hp > monster.maxHP * .15 && monster.stats.IS.hp <= monster.maxHP * .5) {
		statusHPBarEl.classList.add('HPBarYellow')
	}
	else {
		statusHPBarEl.classList.remove('HPBarYellow')
	}
	// base stats
	statusATTEl.innerText = `${monster.stats.IS.att}`
	statusDEFEl.innerText = `${monster.stats.IS.def}`
	statusINIEl.innerText = `${monster.stats.IS.ini}`
	statusSPATTEl.innerText = `${monster.stats.IS.spAtt}`
	statusSPDEFEl.innerText = `${monster.stats.IS.spDef}`
	// xp
	if (monster.level === 100) {
		// statusXPEl.innerText = 'MAX LEVEL!'
		statusXPBar.style.display = 'none'
	}
	else {
		statusXPEl.innerText = `Lv. ${monster.level + 1}: ${monster.xp - xpTable[monster.cat - 1][monster.level - 1]}/${xpTable[monster.cat - 1][monster.level] - xpTable[monster.cat - 1][monster.level - 1]} XP`
		// XP progress bar
		statusXPBar.setAttribute('min', '0')
		statusXPBar.setAttribute('max', `${xpTable[monster.cat - 1][monster.level] - xpTable[monster.cat - 1][monster.level - 1]}`)
		statusXPBar.setAttribute('value', `${monster.xp - xpTable[monster.cat - 1][monster.level - 1]}`)
	}
	statusTotalXP.innerText = monster.xp.toLocaleString()
	statusXPEl.innerText = monster.level === 100 ? 'MAX Lv.' : `${monster.xp - xpTable[monster.cat - 1][monster.level - 1]}/${xpTable[monster.cat - 1][monster.level] - xpTable[monster.cat - 1][monster.level - 1]}`
	// attacks
	const parent = getElByQuery('#statusAttWrap')
	parent?.replaceChildren()
	for (let i = 0; i < 4; i++) {
		const attack = monster.attacks[i]
		const attWrap = document.createElement('div')
		attWrap.className = 'infoFlex attInfo'
		// attack type
		const key = document.createElement('span')
		key.className = 'statusInfoKey'
		key.innerText = !attack ? '--' : getMonsterTypeNameById(attack.type)
		// attack name
		const attName = document.createElement('span')
		attName.innerText = !attack ? '--' : attack.name
		// attack executions
		const attEx = document.createElement('span')
		attEx.className = 'attAP'
		attEx.innerText = !attack ? '--' : `${attack.remExec}/${attack.maxExec}`
		attWrap.appendChild(key)
		attWrap.appendChild(attName)
		attWrap.appendChild(attEx)
		parent?.appendChild(attWrap)
	}
}
// Item Button Event FRONTEND
const itemEvent = (item: Item, enemy?: Monster) => {
	// Push selected item in selectedItem array
	if (player.selectedItems.length > 0) { clearArr(player.selectedItems) }
	player.selectedItems.push(item)
	switch (item.name) {
		case 'POTION':
			dialog.updateInnerHTML('Which MONSTER should get it?')
			displayMonList({ enemy, forItem: true })
			break

		case 'RARE CANDY':
			dialog.updateInnerHTML('Which MONSTER should get it?')
			displayMonList({ enemy, forItem: true })
			break

		case 'BALL':
			if (!enemy?.isWild || !enemy || !battle.isInitiated) {
				dialog.show(`Nice try, ${player.name}!`)
				events.lastFocusEl = null
				if (battle.isInitiated) {
					itemListEl.classList.remove('display')
					return
				}
				if (!battle.isInitiated) {
					reaction.queue.push(() => {
						dialog.hide()
						focusFirstChildAfterTimeout(itemListEl)
					})
				}
				return
			}
			// flag
			flags.setIsAnimating(true)
			// close item list
			itemListEl.classList.remove('display')
			dialog.show(`${player.name} used ${item.name}!`)
			item.use()
			// rerender
			// rerender
			initBattleMenuBtns(enemy)
			animateBallThrow(enemy, item.name)
			break

		default:
			break
	}
}
// Use Potion HTML
const usePotion = (monster: Monster, listIdx: number, enemy?: Monster, trainer?: NPC) => {
	// early return
	if (!player.selectedItems.length) {
		cancelItemUsage()
		return
	}
	// monster already has max HP
	if (monster.stats.IS.hp === monster.maxHP) {
		dialog.updateInnerHTML(`${monster.name} already has MAX. HP!`)
		events.lastFocusEl = null
		reaction.queue.push(() => {
			dialog.updateInnerHTML('Which MONSTER should get it?')
			focusFirstChildAfterTimeout(monsterListEl)
		})
		return
	}
	// monster is fainted
	if (monster.stats.IS.hp <= 0) {
		dialog.show('You can not use this on a defeated MONSTER!')
		if (!battle.isInitiated) {
			itemListEl.classList.add('display')
		}
		cancelItemUsage(false)
		return
	}
	// good case
	if (player.selectedItems.length === 1) {
		const hpBefore = monster.stats.IS.hp
		player.selectedItems[0].use(monster)
		player.selectedItems.pop()
		// Play sound
		playAudio('hpUp')
		// Animate progress bar
		attackSequences?.hpProgress(monster, `#monListProgressBar${listIdx}`)
		const newHPTxt = `HP: ${monster.stats.IS.hp < 0 ? 0 : monster.stats.IS.hp}/${monster.maxHP}`
		getElByQuery(`#monListHP${listIdx}`).innerText = newHPTxt
		// Update info section HTML
		if (monster === battle.monsterParticipated[0]) {
			attackSequences?.hpProgress(monster, `#${playerMonsterHPBar.id}`)
			monHPTxtEl.innerText = newHPTxt
			playerMonsterHPBar.value = monster.stats.IS.hp
		}
		battle.hideUI()
		dialog.show(`${monster.name} regenerated ${monster.stats.IS.hp - hpBefore} HP!`)
		if (battle.isInitiated) {
			reaction.queue.push(() => enemyRandomAttackAfterItemUsage(enemy, trainer))
			return
		}
		reaction.queue.push(() => finishItemUsageOnMonsterList(enemy))
	}
}
// Use Rare Candy HTML
const useRareCandy = (monster: Monster, listIdx: number, enemy?: Monster, trainer?: NPC) => {
	// early return
	if (!player.selectedItems.length) {
		cancelItemUsage()
		return
	}
	// monster already has max Level
	if (monster.level === 100) {
		dialog.updateInnerHTML(`${monster.name} already has reached MAX. LEVEL!`)
		events.lastFocusEl = null
		reaction.queue.push(() => {
			dialog.updateInnerHTML('Which MONSTER should get it?')
			focusFirstChildAfterTimeout(monsterListEl)
		})
		return
	}
	// good case
	if (player.selectedItems.length === 1) {
		player.selectedItems[0].use(monster)
		player.selectedItems.pop()
		// Play sound
		playAudio('lvlup')
		battle.hideUI()
		// update list info
		const newHPTxt = `HP: ${monster.stats.IS.hp < 0 ? 0 : monster.stats.IS.hp}/${monster.maxHP}`
		getElByQuery(`#monListHP${listIdx}`).innerText = newHPTxt
		getElByQuery(`#monListLevel${listIdx}`).innerText = `Lv. ${monster.level}`
		// update info section HTML
		if (monster === battle.monsterParticipated[0]) {
			monHPTxtEl.innerText = newHPTxt
			playerMonsterHPBar.value = monster.stats.IS.hp
			monsterLvlEl.innerText = `Lv. ${monster.level}`
		}
		dialog.show(`${monster.name} has reached LEVEL ${monster.level}!`)
		if (battle.isInitiated) {
			reaction.queue.push(() => enemyRandomAttackAfterItemUsage(enemy, trainer))
			return
		}
		reaction.queue.push(() => finishItemUsageOnMonsterList(enemy))
	}
}
// Cancel Item usage
const cancelItemUsage = (popItem = true) => {
	focusFirstChildAfterTimeout(monsterListEl)
	if (popItem) {
		player.selectedItems.pop()
	}
}
const enemyRandomAttackAfterItemUsage = (enemy?: Monster, trainer?: NPC) => {
	if (!enemy || !trainer) { return }
	monsterListEl.classList.remove('display')
	dialog.hide()
	// Enemy attack
	if (enemy?.isWild) {
		enemy?.randomAttack(battle.monsterParticipated[0], trainer)
		return
	}
	// case trainer monster
	battle.enemyCurrentMon?.randomAttack(battle.monsterParticipated[0], trainer)
}
const finishItemUsageOnMonsterList = (enemy?: Monster) => {
	monsterListEl.classList.remove('display')
	dialog.hide()
	initItemListHTML(enemy)
	itemListEl.classList.add('display')
	focusFirstChildAfterTimeout(itemListEl)
}
// Initialize Enemy HTML Elements
const initWildEnemyHTML = (enemy?: Monster) => {
	if (!enemy) {
		console.warn('enemy not found for HTML initialization!')
		return
	}
	// reset position due to first fade effect
	enemy.sprite.setPos(enemy.getMonsterSpawnPos())
	// enemy name
	const enemyNameEl = getElByQuery('#enemyName')
	enemyNameEl.innerHTML = `<span>${enemy.name}</span>`
	// wild enemy catched
	if (enemy.isWild && player.monsterDex.isMonsterCatched(enemy.idx)) {
		const ballImg = document.createElement('img')
		ballImg.src = './imgs/ball/ball.png'
		ballImg.classList.add('wildCatchedBall')
		enemyNameEl.appendChild(ballImg)
	}
	// health points progress bars
	const enemyHPBar = getElByQuery<HTMLProgressElement>('#enemyHealthBar')
	enemyHPBar.max = enemy.maxHP
	enemyHPBar.value = enemy.stats.IS.hp
	// change hp progress color back to green
	enemyHPBar.className = 'HPBar'
	const enemyLvl = getElByQuery<HTMLDivElement>('#enemyLvl')
	enemyLvl.innerText = `Lv. ${enemy.level}`
}
// Initialize Battle Menu Buttons
const initBattleMenuBtns = (enemy?: Monster, trainer?: NPC) => {
	// replace previous menu btns
	battleMenuEl.replaceChildren()
	// hide menu list
	itemListEl.classList.remove('display')
	/****** THROW BALL ******/
	const ballBtn = document.createElement('button')
	ballBtn.id = 'menu1'
	if (enemy?.isWild) {
		// get best ball from player
		const bestBall = player.getBestBall()
		ballBtn.innerText = bestBall ? bestBall.name : 'BALL'
		ballBtn.onclick = () => {
			if (!bestBall) { return }
			events.lastFocusEl = null
			monsterListEl.classList.remove('display')
			itemListEl.removeAttribute('style')
			flags.setIsAnimating(true)
			// close item list
			itemListEl.classList.remove('display')
			dialog.show(`${player.name} used ${bestBall.name}!`)
			bestBall.use()
			// rerender
			initBattleMenuBtns(enemy)
			animateBallThrow(enemy, bestBall.name)
		}
	}
	/****** MONSTERS MENU ******/
	const monsterBtn = document.createElement('button')
	monsterBtn.id = 'menu3'
	// monsterBtn.classList.add('battleMenuBtn')
	monsterBtn.innerText = 'MONSTER'
	monsterBtn.onclick = () => {
		battle.hideUI()
		displayMonList({ enemy, trainer })
	}
	/****** ITEMS MENU ******/
	const itemBtn = document.createElement('button')
	itemBtn.id = 'menu2'
	itemBtn.innerText = 'ITEM'
	itemBtn.onclick = () => {
		if (dialog.isShowing()) { return }
		battle.hideUI()
		initItemListHTML(enemy)
		if (elHasClassName(monsterListEl, 'display')) {
			monsterListEl.classList.remove('display')
		}
		itemListEl.classList.toggle('display')
		focusFirstChildAfterTimeout(itemListEl)
	}
	/****** ESCAPE BATTLE ******/
	const escapeBtn = document.createElement('button')
	escapeBtn.id = 'menu4'
	escapeBtn.innerText = 'ESCAPE'
	escapeBtn.onclick = () => {
		if (dialog.isShowing()) { return }
		events.lastFocusEl = null
		itemListEl.classList.remove('display')
		monsterListEl.classList.remove('display')
		if (!enemy?.isWild) {
			dialog.show('Escaping is not possible!')
			return
		}
		dialog.show('You escaped...')
		attacksSection.replaceChildren()
		reaction.queue.push(() => {
			battle.escape()
		})
	}
	// Append depending on Player inventory
	if (enemy?.isWild && player.items.findIndex(x => x.name === 'BALL') >= 0) {
		battleMenuEl.appendChild(ballBtn)
	}
	if (player.items.length) {
		battleMenuEl.appendChild(itemBtn)
	}
	if (player.monsters.length) {
		battleMenuEl.appendChild(monsterBtn)
	}
	battleMenuEl.appendChild(escapeBtn)
}