import { player } from '../..'
import { canvas } from '../../classes/Canvas'
import { data } from '../../classes/Data'
import { dialog } from '../../classes/Dialog'
import { events } from '../../classes/Events'
import { flags } from '../../classes/Flags'
import { Img } from '../../classes/Img'
import { base64BattleImgs } from '../../data/base64Imgs'
import { getLocationStr } from '../../data/map/location'
import { toJsonStr } from '../../models/typeGuards'
import { focusFirstChildAfterTimeout, getElByQuery } from '../helper'
import { setLocalStorage } from '../localStorage'
import { displayMonList, initItemListHTML } from './battle'
import { itemListEl, noBtn, pauseOptionsPage, ovDexEntries, pauseMenuEl, playerMoneyEl, playerNameEl, playerOvEl, playerTimeEl, QAWrap, srcCodeBtn, srcCodeBtnWrap, yesBtn, badgesWrapEl } from './elements'

/**
 * This function initializes the HTML of the pause menu
 */
export const initPauseMenu = () => {
	pauseMenuEl.replaceChildren()

	// Monster-Dex
	const monsterDexBtn = document.createElement('button')
	monsterDexBtn.classList.add('pauseBtn')
	monsterDexBtn.id = 'monsterDexBtn'
	monsterDexBtn.innerHTML = 'MONSTER-DEX'
	monsterDexBtn.onclick = () => player.monsterDex.open()

	// Monster List
	const monsterBtn = document.createElement('button')
	monsterBtn.classList.add('pauseBtn')
	monsterBtn.innerHTML = 'MONSTER'
	monsterBtn.onclick = () => {
		pauseMenuEl.removeAttribute('style')
		displayMonList({ inMap: true })
	}

	// Item List
	const itemsBtn = document.createElement('button')
	itemsBtn.classList.add('pauseBtn')
	itemsBtn.innerHTML = 'BAG'
	itemsBtn.onclick = () => {
		initItemListHTML()
		itemListEl.classList.toggle('display')
		focusFirstChildAfterTimeout(itemListEl)
		pauseMenuEl.removeAttribute('style')
	}

	// Player Button
	const playerBtn = document.createElement('button')
	playerBtn.classList.add('pauseBtn')
	playerBtn.innerHTML = player.name
	playerBtn.onclick = () => initPlayerOverviewHTML()

	// Save Button
	const saveBtn = document.createElement('button')
	saveBtn.classList.add('pauseBtn')
	saveBtn.innerHTML = 'SAVE'
	saveBtn.onclick = () => {
		const saveWindowInfo = getElByQuery('#saveWindowId')
		initSaveWindow(saveWindowInfo)
		pauseMenuEl.removeAttribute('style')
		dialog.el.style.display = 'block'
		dialog.el.innerText = 'Do you want to SAVE your progress?'
		QAWrap.style.display = 'grid'
		yesBtn.innerText = 'SAVE'
		noBtn.innerText = 'CANCEL'
		focusFirstChildAfterTimeout(QAWrap)
		yesBtn.onclick = () => {
			events.lastFocusEl = null
			dialog.show(`${player.name} saved the GAME!`)
			saveWindowInfo.removeAttribute('style')
			QAWrap.removeAttribute('style')
			const t = setTimeout(() => {
				dialog.hide()
				flags.setIsPaused(false)
				clearTimeout(t)
			}, 1500)
			setLocalStorage('player', toJsonStr(player) || '')
			setLocalStorage('canvas', toJsonStr(canvas) || '')
			setLocalStorage('mapdata', toJsonStr(data) || '')
		}
		noBtn.onclick = () => {
			saveWindowInfo.removeAttribute('style')
			events.dontSaveGameProgress()
		}
	}
	// Options Button
	const optionBtn = document.createElement('button')
	optionBtn.classList.add('pauseBtn')
	optionBtn.innerHTML = 'OPTIONS'
	optionBtn.onclick = () => initOptionsPageHTML()
	// Back Button
	const backBtn = document.createElement('button')
	backBtn.classList.add('pauseBtn')
	backBtn.innerHTML = 'BACK'
	backBtn.onclick = () => {
		pauseMenuEl.removeAttribute('style')
		const t = setTimeout(() => {
			clearTimeout(t)
			flags.setIsPaused(false)
		}, 200)
		events.lastFocusEl = null
	}
	if (player.monsterDex.available) {
		pauseMenuEl.appendChild(monsterDexBtn)
	}
	if (player.monsters.length) {
		pauseMenuEl.appendChild(monsterBtn)
	}
	if (player.items.length) {
		pauseMenuEl.appendChild(itemsBtn)
	}
	pauseMenuEl.appendChild(playerBtn)
	pauseMenuEl.appendChild(saveBtn)
	pauseMenuEl.appendChild(optionBtn)
	pauseMenuEl.appendChild(backBtn)
}

const initPlayerOverviewHTML = () => {
	const satsImg = getElByQuery<HTMLImageElement>('#satsImg')
	satsImg.src = './imgs/sats.png'
	const playerImg = getElByQuery<HTMLImageElement>('#playerOvImg')
	playerImg.src = base64BattleImgs.playerPause
	playerNameEl.innerText = `NAME: ${player.name}`
	// player sats
	playerMoneyEl.innerText = `MONEY: ${player.satoshis} `
	// player time played
	playerTimeEl.innerText = `TIME: ${player.getTimeStr()}`
	// player dex entries
	ovDexEntries.innerText = `MONSTER: ${player.monsterDex.getCatchedCount()} / SEEN: ${player.monsterDex.getEntriesCount()}`
	// player location
	// ovLocation.innerText = `${getLocationStr(player.currentMapId).toUpperCase()}`
	badgesWrapEl.replaceChildren()
	for (const key in player.badges) {
		if (Object.prototype.hasOwnProperty.call(player.badges, key)) {
			const badge = player.badges[key]
			if (badge.inPossession) {
				const img = new Img(badge.img.src)
				img.classList.add('badgeImg')
				badgesWrapEl.appendChild(img)
			}
		}
	}
	pauseMenuEl.removeAttribute('style')
	playerOvEl.style.display = 'block'
}

const initSaveWindow = (window: HTMLElement) => {
	// header
	const saveHeader = getElByQuery('#saveLocation')
	saveHeader.innerText = getLocationStr(player.currentMapId)
	// name
	const saveName = getElByQuery('#saveName')
	saveName.innerText = player.name
	// money
	const saveMoney = getElByQuery('#saveMoney')
	saveMoney.innerHTML = `${player.satoshis} <img src="./imgs/sats.png" class="savedSatsImg"></img>`
	// badges
	const saveBadges = getElByQuery('#saveMedals')
	saveBadges.innerText = `${player.getBadgesCount()}`
	// dex
	const dexCatched = getElByQuery('#saveWindowDex')
	dexCatched.innerText = `${player.monsterDex.getCatchedCount()}`
	// time
	const saveTime = getElByQuery('#saveTime')
	saveTime.innerText = player.getTimeStr()
	window.style.display = 'block'
}

const initOptionsPageHTML = () => {
	pauseOptionsPage.style.display = 'block'
	focusFirstChildAfterTimeout(srcCodeBtnWrap)
}

// open source code
srcCodeBtn.onclick = () => {
	window.open('https://gitlab.com/agroland/client/-/issues/41', '_blank')
}