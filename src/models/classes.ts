import type { Howl } from 'howler'
import type { Boundary } from '../classes/Boundary'
import type { Inventory } from '../classes/Inventory'
import type { Item } from '../classes/Items/Item'
import type { Monster } from '../classes/Monster'
import type { MonsterDex } from '../classes/MonsterDex'
import type { NPC } from '../classes/NPC'
import type { Sprite } from '../classes/Sprite'
import type { TMonsterName } from '../data/monster/allMonsters'
import type { MonsterType } from '../data/monster/types'
import type { IAttack } from './functions'

export type TReactionQueue = (() => void)[]

export interface IPlayerArgs {
	name: string
	// sprite: Sprite
	monsters: Monster[]
	items: Item[]
	badges: IBadges
	monsterDex: MonsterDex
	playtime?: number
	satoshis?: number
	inventory: Inventory
	moved?: boolean
	location: number
	currentHouseId?: number
	currentMapId?: number
	lastHospitalData?: {
		mapID: number
		offset: IPosition
	}
}
export interface IBadges {
	[key: string]: {
		img: { src: string }
		inPossession: boolean
	}
}
export interface INPCArgs {
	id: number
	name: string
	firstDialog: (npc: NPC) => (() => void)[]
	image: IImageSrc
	boundary: Boundary
	headImgs?: {
		up: IImageSrc
		right: IImageSrc
		down: IImageSrc
		left: IImageSrc
	}
	audio?: {
		look?: IHowlOptions
		battle: IHowlOptions
		victory: IHowlOptions
	}
	animate: boolean
	frames: IFrames
	position: IPosition
	initialStaticPos?: IPosition
	sprites?: ISpriteDirections
	battleSprite?: Sprite
	battleSpriteSrc?: IImageSrc
	isTrainer?: boolean
	isRival?: boolean
	rivalFirstMonIdx?: number
	isBlocker?: boolean
	secondDialog?: (npc: NPC) => (() => void)[]
	winDialog?: string
	looseDialog?: string
	afterBattleDialog?: (npc: NPC) => (() => void)[]
	isGym?: boolean
	monsters?: Monster[]
	items?: Item[]
	itemGift?: Item
	effort?: number
	isBeaten?: boolean
	isMovingAround?: boolean
	maxRange?: number
}
export interface ICollisionsData {
	[key: string]: {
		tilesCount: IPosition
		offset: {
			smartphone: IPosition
			desktop: IPosition
		}
		audio: IHowlOptions
		houses: IHouseData[]
		collisions: Uint8Array
	}
}
export interface IHouseData {
	id: number
	tilesCount: IPosition
	offset: {
		smartphone: IPosition
		desktop: IPosition
	}
	audio: IHowlOptions
	collisions: Uint8Array
}
export interface IHowlOptions {
	src: string
	html5: boolean
	volume: number
	loop: boolean
	preload: boolean
}
export interface IIventoryArgs {
	items: Item[],
	monsters: Monster[]
}
export interface IMovablesArgs {
	mapID: number
	houseID?: number
}
export interface IMonsterDexArgs {
	available: boolean
	isBroken: boolean
	entries: IDexEntry[]
}
export interface IItemArgs {
	name: string
	count: number
	price?: number
}
export interface IMoveParams {
	moving: boolean
	animationID: number
	inHouse: boolean
}
export interface ICollisionArgs {
	rectangle1: Sprite
	rectangle2: Boundary
	withNPC?: boolean
}
export interface ISpriteArgs {
	position: IPosition
	image?: IImageSrc
	imageFaceUp?: IImageSrc
	frames?: IFrames
	sprites?: ISpriteDirections
	animate?: boolean
	rotation?: number
	opacity?: number
	isEnemy?: boolean
	isBattleBg?: boolean
	scaleFactor?: number
}
export interface IMonsterArgs extends ISpriteArgs {
	idx: number
	uid: number
	name: string
	isWild?: boolean
	opacity?: number
	audio: Howl
	attacks?: IAttack[]
	lvlAttacks: ILvlAttacks
	stats: IMonsterStats
	savedStats?: IMonsterStats
	status: IMonsterStatus
	xp?: number
	cat: number
	type?: MonsterType[]
	level: number
	effort: number
	catchRate: number
	description: string
	weight: number
	height: number
	transformLvl?: number
	transTo?: number
	maxHP?: number
	canEscape?: { chance: number }
	catchLvl?: number
	catchLocation?: string
}
export interface ICollectableArgs {
	id: number
	backgroundSrc: IImageSrc
	foregroundSrc: IImageSrc
	position: IPosition
	item: Item
	boundary: Boundary
}
export interface IHighGrassArgs {
	position: IPosition
	image: IImageSrc
	boundary: Boundary
}
export interface IBoundayArgs {
	id: number
	position: IPosition
	width: number
	height: number
}
export interface IMonsterStats {
	base: IStats
	IV: IStats // 0-15
	EV: IStats // max 255 per stat & max 510 in total
	EVY: IStats
	IS: IStats
}
export interface IImageSrc {
	src: string
}
export interface IStats {
	[key: string]: number
}
export interface IPosition {
	x: number
	y: number
}
export interface IMonsterStatus {
	paralyzed: number
	poisoned: number
	confused: number
	sleeping: number
	burning: number
	bleeding: number
	frozen: number
	inLove: number
	hasEgg: number
	XPPower: number
	affection: number
}
export type ISpriteDirections = {
	[key in 'up' | 'right' | 'down' | 'left']: IImageSrc
}
export interface IFrames {
	max: number
	hold: number
	val?: number
	elapsed?: number
}
export interface ICollectableItems {
	[key: string]: {
		map: {
			[key: string]: Item
		}
		house: {
			[key: string]: {
				[key: string]: Item
			}
		}
	}
}
export interface IInteractions {
	[key: string]: {
		map: {
			[key: string]: {
				txt: string
				direction: string[]
			}
		}
		house: {
			[key: string]: {
				[key: string]: {
					txt: string
					direction: string[]
				}
			}
		}
	}
}
export interface IStoreInventory {
	[key: string]: {
		store: {
			[key: string]: Item[]
		}
	}
}
export interface IAllData {
	[key: string]: IMapData
}
export interface IMapData {
	tilesCount: IPosition
	offset: {
		smartphone: IPosition
		desktop: IPosition
	}
	audio: { src: string }
	boundaries: Boundary[]
	npcs: NPC[]
	houses: IHouseMapData[]
}
export interface IHouseMapData {
	id: number
	tilesCount: IPosition
	offset: {
		smartphone: IPosition
		desktop: IPosition
	}
	audio: { src: string }
	boundaries: Boundary[]
	npcs: NPC[]
}
export interface ILvlAttacks {
	[key: number]: IAttack
}
export interface IDexEntry {
	idx: number
	name: TMonsterName
	catched: boolean
}