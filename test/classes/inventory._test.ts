describe('test Inventory class', () => {

	test('defines method getItems()', async () => {
		const x = await import(__dirname + "/../../src/classes/Inventory")
		console.log('x inventory: ', x)
		const inventory = x.Inventory.newInventory({ items: [], monsters: [] })
		expect(typeof inventory.getItems).toBe('function')
	})

	test('defines method getMonsters()', async () => {
		const x = await import(__dirname + "/../../src/classes/Inventory")
		const inventory = x.Inventory.newInventory({ items: [], monsters: [] })
		expect(typeof inventory.getMonsters).toBe('function')
	})

	test('defines method toJSON()', async () => {
		const x = await import(__dirname + "/../../src/classes/Inventory")
		const inventory = x.Inventory.newInventory({ items: [], monsters: [] })
		expect(typeof inventory.toJSON).toBe('function')
	})

	test('should serialize some Inventory object properties for saving progress purposes', async () => {
		const x = await import(__dirname + "/../../src/classes/Inventory")
		const inventory = x.Inventory.newInventory({ items: [], monsters: [] })
		const result = inventory.toJSON()
		expect(result).toStrictEqual({ "items": [], "monsters": [] })
	})

})