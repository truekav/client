import { flags } from "../../src/classes/Flags"

describe('Test Flags class', () => {
	// get and set property isMapSoundOn
	test('defines method getIsMapSoundOn', () => {
		expect(typeof flags.getIsMapSoundOn).toBe("function")
	})
	test('getIsMapSoundOn returns false', () => {
		const result = flags.getIsMapSoundOn()
		expect(result).toBe(false)
	})
	test('defines method setIsMapSoundOn', () => {
		expect(typeof flags.getIsMapSoundOn).toBe("function")
	})
	test('should set the property isMapSoundOn to true', () => {
		flags.setIsMapSoundOn(true)
		const result = flags.getIsMapSoundOn()
		expect(result).toBe(true)
	})
	// get and set property isAnimating
	test('defines method getIsAnimating', () => {
		expect(typeof flags.getIsAnimating).toBe("function")
	})
	test('getIsAnimating returns false', () => {
		const result = flags.getIsAnimating()
		expect(result).toBe(false)
	})
	test('defines method setIsAnimating', () => {
		expect(typeof flags.setIsAnimating).toBe("function")
	})
	test('should set the property isAnimating to true', () => {
		flags.setIsAnimating(true)
		const result = flags.getIsAnimating()
		expect(result).toBe(true)
	})
	// get and set property isPaused
	test('defines method getIsPaused', () => {
		expect(typeof flags.getIsPaused).toBe("function")
	})
	test('getIsPaused returns false', () => {
		const result = flags.getIsPaused()
		expect(result).toBe(false)
	})
	test('defines method setIsPaused', () => {
		expect(typeof flags.setIsPaused).toBe("function")
	})
	test('should set the property isPaused to true', () => {
		flags.setIsPaused(true)
		const result = flags.getIsPaused()
		expect(result).toBe(true)
	})
	// get and set property isInteracting
	test('defines method getIsInteracting', () => {
		expect(typeof flags.getIsInteracting).toBe("function")
	})
	test('getIsInteracting returns false', () => {
		const result = flags.getIsInteracting()
		expect(result).toBe(false)
	})
	test('defines method setIsInteracting', () => {
		expect(typeof flags.setIsInteracting).toBe("function")
	})
	test('should set the property isInteracting to true', () => {
		flags.setIsInteracting(true)
		const result = flags.getIsInteracting()
		expect(result).toBe(true)
	})

})