import { readFileSync } from "fs"

describe('test Viewport class', () => {

	beforeAll(() => {
		// console.log('env', process.env.NODE_ENV)
		document.body.innerHTML = readFileSync(__dirname + "/../../public/index.html").toString()
	})

	test('defines method isSmartphone()', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		expect(typeof x.viewport.isSmartphone).toBe('function')
	})

	test('defines method isTurnedSmartphone()', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		expect(typeof x.viewport.isTurnedSmartphone).toBe('function')
	})

	test('defines method isTablet()', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		expect(typeof x.viewport.isTablet).toBe('function')
	})

	test('defines method isSmallDesktop()', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		expect(typeof x.viewport.isSmallDesktop).toBe('function')
	})

	test('isSmallDesktop() should return false', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		const result = x.viewport.isSmallDesktop()
		expect(result).toBe(false)
	})

	test('isTablet() should return false', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		const result = x.viewport.isTablet()
		expect(result).toBe(false)
	})

	test('isTurnedSmartphone() should return false', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		const result = x.viewport.isTurnedSmartphone()
		expect(result).toBe(false)
	})

	test('isSmartphone() should return false', async () => {
		const x = await import(__dirname + "/../../src/classes/Viewport")
		const result = x.viewport.isSmartphone()
		expect(result).toBe(false)
	})

})