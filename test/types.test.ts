import { getMonsterTypeIdByName, getMonsterTypeMultiplicator, getMonsterTypeNameById, MonsterType } from "../src/data/monster/types"

describe('Test types functionality', () => {

	test('getMonsterTypeNameById() should return the right type name', () => {
		const result = getMonsterTypeNameById(0)
		expect(result).toEqual('normal')
	})
	test('getMonsterTypeIdByName() should return the right type ID', () => {
		const result = getMonsterTypeIdByName('normal')
		expect(result).toEqual(0)
	})
	test('getMonsterTypeIdByName() should return undefined', () => {
		const result = getMonsterTypeNameById(21)
		expect(result).toBe(undefined)
	})
	test('getMonsterTypeIdByName() should return undefined', () => {
		const result = getMonsterTypeIdByName('moon')
		expect(result).toBe(undefined)
	})
	test('getMonsterTypeMultiplicator should return the right type multiplicator object', () => {
		const result = getMonsterTypeMultiplicator(MonsterType.normal, [MonsterType.fire, MonsterType.flying])
		expect(result).toStrictEqual({ type1: 1, type2: 1 })
	})
	test('getMonsterTypeMultiplicator should return the right type multiplicator object', () => {
		const result = getMonsterTypeMultiplicator(MonsterType.fire, [MonsterType.water, MonsterType.flying])
		expect(result).toStrictEqual({ type1: .5, type2: 1 })
	})
	test('getMonsterTypeMultiplicator should return the right type multiplicator object', () => {
		const result = getMonsterTypeMultiplicator(MonsterType.water, [MonsterType.fire, MonsterType.flying])
		expect(result).toStrictEqual({ type1: 2, type2: 1 })
	})
})