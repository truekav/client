import { clearArr, getGrowthStr } from "../src/util/helper"

describe('Helper Test', () => {
	test('getGrowthString should return the right string', () => {
		const result = getGrowthStr(1)
		expect(result).toEqual('Slow')
	})
	test('getGrowthString should return the right string', () => {
		const result = getGrowthStr(4)
		expect(result).toEqual('Fast')
	})
	test('getGrowthString should return undefined', () => {
		const result = getGrowthStr(6)
		expect(result).toEqual(undefined)
	})
	test('clearArray should clear the array :)', () => {
		const arr = [1, 2]
		clearArr(arr)
		expect(arr).toEqual([])
	})
})