rm -rf public/js
mkdir -p public/js
cd wasm
npm i
npm run asbuild:release
cd ..
sed -i 's/return globalThis.WebAssembly.compile(await (await import("node:fs\/promises")).readFile(url));/ /' public/js/release.js
npm run build
