import { build } from 'esbuild'

// npx esbuild src/index.ts --outfile=public/bundle.js --bundle --format=esm --target=esnext --sourcemap=inline --servedir=public
void build({
	entryPoints: ['src/index.ts'],
	bundle: true,
	outfile: 'public/bundle.js',
	format:'esm',
	// sourcemap:'inline',
	target:'esnext',
	platform:'browser',
	splitting:true,
	treeShaking:true,
	// watch:true
})