<div align="center">
  <p>
    <img src="https://gitlab.com/agroland/client/-/raw/main/public/imgs/appLogo.png">
  </p>
  <!-- <h1>Agroland</h1> -->

  <p>Genesis Edition</p>

  [![play the DEMO][gameBadge]][ProjectPage]
  [![version][versionBadge]][VersionPage]
  [![DocsBadge]][DocsPage]



  <div style="display: flex; align-items: center; justify-content: center">

<!--   [![DEV Preview][devPreviewBadge]][DevPage] -->
  [![pipeline status][PipelineStatusBadge]][ProjectUrl]
  [![coverage report][CoverageBadge]][ProjectUrl]
  </div>
</div>

  <hr>

<div align="center">
  <p>
    <img src="https://gitlab.com/agroland/client/-/raw/main/public/imgs/presentation/sizes.png">
  </p>
</div>

<!-- <a class="button" href="lightning:LNURL1DP68GURN8GHJ7AMPD3KX2AR0VEEKZAR0WD5XJTNRDAKJ7TNHV4KXCTTTDEHHWM30D3H82UNVWQHKCMMEV9KXYETYWFHK7MFEXG55P6EZ">
  ⚡ Donate to LNURL
</a>
<a class="button" href="lightning:billigsteruser@getalby.com">
  ⚡ Donate to Lightning Address
</a>
<hr> -->

## Related repositories

[Tileset collection](https://gitlab.com/agroland/tileset)

## Description

The free, open source Pokémon clone for BROWSERS. Contributors are welcome!

This application is written in Vanilla JS with the help of TypeScript. I also use CSS & the HTML Canvas 2D-context API. There is also a little bit of WebAssemblyScript. 

The fundamental logic of this application is based on a [YouTube video](https://www.youtube.com/watch?v=yP5DKzriqXA) created by [Chris](https://chriscourses.com/courses).
Much appreciated his detailed tutorial, it provides some essential knowledge and is explained very well.
You can find his open source version on [GitHub](https://github.com/chriscourses/pokemon-style-game).

## Features

- Location based random wild monsters
- Monster status (burning, frozen, ...)
- (Special)-Attack type effectiveness
- Critical hits & missed hits based on monster and attack stats
- Catch monster
- Monster level-up
- Monster attack learning
- Monster evolution
- Monster-dex (Pokédex)
- Item collection & usage
- Calculations based on the Pokémon 1st-3rd generation:
  - Monster-stats
  - Damage
  - XP-gain (experience)
  - EV-gain (effort-values)
- Pause-menu:
  - swich monster order
  - show monster status
  - use/drop items
  - player overview
  - save progress (currently using the browsers local storage as proof of concept)
- Interaction with other NPCs
- NPCs moving around
- Walk into buildings
- Battle against NPCs
- Battle against rival
- Gym battles
- Monster hospital (Pokécenter)
- Touchscreen support
- Responsive mobile-first design
- ...and much more!

## Maintainability

- Typescript support
- Object-oriented as good as I can
- Currently basic & not-so-meaningful tests with jest library

## Screenshots

<p>
  <img src="https://gitlab.com/agroland/client/-/raw/main/public/imgs/presentation/collection2.png">
</p>
<p>
  <img src="https://gitlab.com/agroland/client/-/raw/main/public/imgs/presentation/attacks.png">
</p>
<p>
  <img src="https://gitlab.com/agroland/client/-/raw/main/public/imgs/presentation/collection1.png">
</p>

## Credits

This application would not exist without the help of the [project members](https://gitlab.com/agroland/client/-/project_members) and the following resources:

#### **How to build a Pokémon styled game in JS**
[https://www.youtube.com/watch?v=yP5DKzriqXA](https://www.youtube.com/watch?v=yP5DKzriqXA)

#### **Tileset**
[https://cypor.itch.io/12x12-rpg-tileset](https://gitlab.com/agroland/client/-/issues)

#### **Battle backgrounds**
[https://www.deviantart.com/snivy101/art/Pokemon-B2W2-Battle-Backgrounds-319706977](https://www.deviantart.com/snivy101/art/Pokemon-B2W2-Battle-Backgrounds-319706977)

#### **Monster Sprites**
[https://www.pokecommunity.com/showthread.php?t=267728](https://www.pokecommunity.com/showthread.php?t=267728)

[https://www.pokecommunity.com/showthread.php?t=314422](https://www.pokecommunity.com/showthread.php?t=314422)

[https://www.pokecommunity.com/showthread.php?t=368703](https://www.pokecommunity.com/showthread.php?t=368703)

#### **Trainer Sprites**
[https://www.deviantart.com/diy-gamer/art/Trainer-Sprites-158995891](https://www.deviantart.com/diy-gamer/art/Trainer-Sprites-158995891)

#### **Gym badges**
[https://www.deviantart.com/megaman-omega/art/Pokemon-Prism-Gym-Badges-653738121](https://www.deviantart.com/megaman-omega/art/Pokemon-Prism-Gym-Badges-653738121)

#### **Audio**
[https://downloads.khinsider.com/game-soundtracks/album/pokemon-diamond-and-pearl-super-music-collection](https://downloads.khinsider.com/game-soundtracks/album/pokemon-diamond-and-pearl-super-music-collection)

## Installation

Clone or fork the project.

```
git clone https://gitlab.com/agroland/client.git

cd client
```

Install packages.

```
npm i
```

Run the build script for the code in webAssemblyScript.

```
scripts/buildCi.sh
```

Start the dev server.

```
npm start
```

That's it! You can now create a new branch and start modifying the files.

Lint

```
npm run l
```

Run tests

```
npm run test
```

## Contributing

Contributors and committers are very welcome!

Have a look at the [contributing page](https://gitlab.com/agroland/client/-/blob/main/CONTRIBUTING.md) for more detailed info.

Open the [TICKETS/ISSUES](https://gitlab.com/agroland/client/-/issues) to get an overview of the current "ROADMAP" and pick up a ticket that you would like to work on or simply create your own...

Your **help** and **input** is much appreciated, not only in therms of code improvements but in all areas:

- README/CONTRIBUTING markdown updates
- Tileset creators (currently: map 12x12 - interiors 16x16)
- Maps and building-interior creation using [TILED](https://www.mapeditor.org/)
- CSS design (mobile-first)
- Sprites creators (NPCs, Attacks, Items...)
- Attack animations
- Canvas animations for Attacks, Monster-status (frozen, burning...)
- Language Translators
- Sound compositions and format improvements (currently MP3)
- Game tester and bug reports
- **E2E tests or more tests in general**
- **Data structure improvements/memory usage reduction**
- **General code improvements**

## Controls

| Keys                 | Action           |
|:-------------------- | ----------------:|
| W                    | Move up          |
| A                    | Move left        |
| S                    | Move down        |
| D                    | Move right       |
| Shift                | Run              |
| P                    | Open menu        |
| Space/Enter          | Interact         |
| Backspace/Escape     | Close menu       |

Touch-controls are available for touch devices

## Todo's 

- [ ] Logic
  - [x] Move player around
  - [x] Collision detection
  - [x] Battlezone detection
  - [x] Battle sequence
  - [x] Monster class (prototype)
  - [x] Attack animations
  - [x] Battle menu
  - [x] Leave wild battle
  - [x] random wild enemies with random level
  - [x] Attack DirectHit/MissHit
  - [x] Monster type vs. Attack type logic
  - [x] Monster level/hp/attack-damage logic
  - [x] Attack max execution
  - [x] Monster attributes logic Attack/Defense/Speed/Special
  - [x] Monster battle-XP logic
  - [x] Dialog with other characters
  - [x] Item gifts from characters
  - [x] Splitted XP if user switches monsters
  - [x] Item List and usage
  - [x] Open Menu on Map
  - [x] Monster-DEX (pokedex)
  - [x] Catch Monsters
  - [x] Touch support
  - [x] Mobile-first design
  - [x] Monster attack learning
  - [x] Time played
  - [x] Monster evolution
  - [x] Save progress
  - [x] location-dependig Random wild monster
  - [x] Battle against other characters logic
  - [x] Walk into buildings
  - [x] Visible/Hidden items
  - [x] Inventory logic
  - [x] Store logic
  - [x] Attack status effect
  - [x] Walking NPCs
  - [x] Rival battle logic
  - [x] Change elements focus with W/A/S/D keys
  - [x] Gym Battles
  - [ ] Opponent trainer item usage
  - [ ] Opponent trainer monster switch before faint
  - [ ] Opponent trainer monster remaining attack execution
  - [ ] Multiple level-ups (f.e: lvl-up from lvl.2 to 4)

---

- [ ] Map menu
  - [x] Item collection
  - [x] Monster collection
  - [x] Monster status overview page
  - [x] Player overview page
  - [x] Item list and usage in map
  - [x] Drop item(s)
  - [ ] Options: dialog speed, language, accent color

---

- [x] Inventory (Player PC)
  - [x] Item inventory
  - [x] Store/Take/Drop items
  - [x] Monster inventory
  - [ ] Store/Take Monsters

---

- [ ] Sprites
  - [x] Create player Sprite for Battle sequence
  - [x] Create player Sprite for player-overview section
  - [x] Create other Charaters Sprites (map and battle sprites)
  - [x] Create Building inside Sprite
  - [x] Create 10 Monster Sprites and associated monster-object
  - [x] Create 20 Monster Sprites and associated monster-object
  - [x] Create 30 Monster Sprites and associated monster-object
  - [x] Create 40 Monster Sprites and associated monster-object
  - [ ] Create 50 Monster Sprites and associated monster-object

---

- [ ] Attacks
  - [ ] Create 10 attacks and the associated animations
  - [ ] Create 20 attacks and the associated animations
  - [ ] Create 30 attacks and the associated animations
  - [ ] Create 40 attacks and the associated animations
  - [ ] Create 50 attacks and the associated animations
<br>

- Get some [Orientation](https://bulbapedia.bulbagarden.net/wiki/Category:Generation_I_moves) with the pokemon 1st generation attacks.
---

- [ ] Items
  - [ ] Create 10 items and corresponding methods
  - [ ] Create 20 items and corresponding methods
  - [ ] Create 30 items and corresponding methods
  - [ ] Create 40 items and corresponding methods
  - [ ] Create 50 items and corresponding methods
<br>

- Get some [Orientation](https://bulbapedia.bulbagarden.net/wiki/List_of_items_by_index_number_(Generation_I)) with the pokemon 1st generation items.


<!-- ## Integrate with your tools

 [Set up project integrations](https://gitlab.com/FirstTerraner/agroland/-/settings/integrations)

## Collaborate with your team

 [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
 [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
 [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
 [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
 [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

 [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

*** -->

<!-- Badges -->
[PipelineStatusBadge]: https://gitlab.com/agroland/client/badges/main/pipeline.svg
[CoverageBadge]: https://gitlab.com/agroland/client/badges/main/coverage.svg
[DocsBadge]: https://readthedocs.org/projects/docs/badge/?version=latest
[versionBadge]: https://img.shields.io/badge/version-0.0.8-blue.svg
[gameBadge]: https://img.shields.io/badge/Play%20the%20DEMO-up-green.svg
[devPreviewBadge]: https://img.shields.io/badge/DEV%20Preview-up-green.svg
<!-- [DiscordBadge]: https://img.shields.io/discord/835310846480482314?label=Discord&logo=discord&logoColor=white
[TwitterBadge]: https://img.shields.io/twitter/follow/VirtualProSpace.svg?style=flatl&label=Follow&logo=twitter&logoColor=white&color=1da1f2 -->

<!-- Redirect URLs -->
[ProjectUrl]: https://gitlab.com/agroland/client/commits/main
[ProjectPage]: https://agroland.pages.dev/
[DevPage]: https://dev.agroland.pages.dev/
[DocsPage]: https://agroland.gitlab.io/client/docs
[VersionPage]: https://gitlab.com/agroland/client/-/blob/main/package.json
<!-- [DiscordUrl]: https://discord.gg/Y94sAMkCra
[TwitterUrl]: https://twitter.com/ -->

<!-- https://agroland.gitlab.io/client -->
